/**
 * @file adm_com_thread.h
 * @author mengwei@abupdate.com
 * @brief 定义线程相关的函数，包括：线程创建、线程属性设置、互斥锁、条件变量等
 * @version 0.1
 * @date 2023-02-25
 * 
 * @copyright Copyright (c) 2023 上海艾拉比智能科技有限公司
 * 
 * @par 修改日志:
 * <table>
 * <tr><th>Date       <th>Version <th>Author  <th>Description
 * <tr><td>2023-02-25 <td>1.0     <td>mengwei    <td>内容
 * </table>
 */
#pragma once

#include "adm_typedefs.h"
#include "adm_memory.h"
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum ADM_COM_THREAD_ERR_ 
{
    ADM_OS_ECODE_FAILED = -1,                  //!< 默认错误
    ADM_OS_ECODE_SUCCESSED,                    //!< 成功
    ADM_OS_ECODE_INVALID_PARAM,                //!< 参数无效
    ADM_OS_ECODE_APPLY_MEMORY_FAILED,          //!< 内存申请失败

    ADM_OS_ECODE_PTHREAD_CREATE_FAILED    =     550,         //!< 线程创建失败   
    ADM_OS_ECODE_PTHREAD_START_FAILED     =     551,         //!< 启动线程失败   
    ADM_OS_ECODE_PTHREAD_ALREADY_RUNNING  =     552,         //!< 线程已经运行 
    ADM_OS_ECODE_PTHREAD_ATTRSET_FALIED   =     553,         //!< 线程属性设置失败 
    ADM_OS_ECODE_SYSCALL_FALIED           =     554,         //!< 系统函数调用失败，请查看errno 
    ADM_OS_ECODE_MSG_WAIT_TIMEOUT         =     555,         //!< 等待消息超时 
    ADM_OS_ECODE_MODULE_UNINIT            =     556,         //!< 模块未初始化 
    ADM_OS_ECODE_PTHREAD_ATTR_INIT_FAILED =     558,         //!< 获取线程属性句柄失败
    ADM_OS_ECODE_PTHREAD_SET_NAME_FAILED  =     559,         //!< 线程名设置失败
    ADM_OS_ECODE_PTHREAD_GET_NAME_FAILED  =     560,         //!< 线程名获取失败
    ADM_OS_ECODE_PTHREAD_COND_INIT_FAILED =     561,         //!< 条件变量初始化失败
    ADM_OS_ECODE_PTHREAD_COND_WAIT_FAILED =     562,         //!< 条件变量等待失败
    ADM_OS_ECODE_PTHREAD_COND_SIGNAL_FAILED =   563,         //!< 信号通知失败
    ADM_OS_ECODE_PTHREAD_COND_ATTR_INIT_FAILED = 564,         //!< 条件变量属性初始化失败
    ADM_OS_ECODE_PTHREAD_COND_ATTR_SET_FAILED  = 565,         //!< 条件变量属性设置失败
    ADM_OS_ECODE_PTHREAD_COND_ATTR_GET_FAILED  = 565,         //!< 条件变量属性设置失败
    ADM_OS_ECODE_PTHREAD_COND_BROAD_CAST_FAILED = 566,         //!< 条件变量信号，广播失败
    ADM_OS_ECODE_SEM_INIT_FAILED               = 570,          //!< 信号量初始化失败
    ADM_OS_ECODE_SEM_WAIT_FAILED                = 571,         //!< 信号量P操作，阻塞等待失败
    ADM_OS_ECODE_SEM_TRY_WAIT_FAILED           = 572,          //!< 信号量P操作，非阻塞等待失败
    ADM_OS_ECODE_SEM_TIMED_WAIT_FAILED         = 573,          //!< 信号量P操作，超时等待失败
    ADM_OS_ECODE_SEM_POST_FAILED               = 574,          //!< 信号量V操作，失败
    ADM_OS_ECODE_SEM_DESTROY_FAILED            = 575,          //!< 信号量销毁失败
} ADM_COM_THREAD_ERR_E;


/**
 * @brief 通用线程创建的接口，默认使用线程分离的属性
 * 
 * @param pthreadHandle     [out]    线程句柄
 * @param start_routine     [in]    线程处理函数
 * @param argv              [in]    线程处理参数
 * @param threadName        [in]    设置线程名 @note  同样的，要求name 的buffer 空间不能超过16个字节，不然会报错 ERANGE。
 * @return D32S             线程创建的执行结果
 * @retval  ADM_OS_ECODE_FAILED @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_PTHREAD_ATTRSET_FALIED    @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_PTHREAD_CREATE_FAILED     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_PTHREAD_SET_NAME_FAILED   @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED                 @see adm_com_errorCode.h
 */
extern D32S adm_com_threadCreate(DHANDLE* pthreadHandle, void* (*start_routine)(void* param), void* argv, const DCHAR* threadName);

/**
 * @brief 根据线程句柄获取线程的名称
 * 
 * @param pthreadHandle     [in]    线程句柄
 * @param threadName        [out]   线程名称
 * @param nameLen           [in]    nameLen指定threadName中可用的字节数。名称指定的缓冲区长度至少为16个字符。在输出缓冲区中返回的线程名将为以空字符结尾。   
 * @return D32S             获取线程名的结果
 * @retval  ADM_OS_ECODE_FAILED    @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_PTHREAD_GET_NAME_FAILED   @see adm_com_errorCode.h
 */
extern D32S adm_com_threadGetName(DHANDLE pthreadHandle, DCHAR* threadName, size_t nameLen);


/**
 * @brief 初始化条件变量
 * @note 使用此接口初始化条件变量，会将时间属性设置为CLOCK_MONOTONIC，可避免因为系统时间（CLOCK_REALTIME）跳变导致的不可控问题
 * 
 * @param pCondHandle       [out]   条件变量句柄
 * @return D32S         条件变量初始化的结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_PTHREAD_COND_ATTR_INIT_FAILED     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_PTHREAD_COND_ATTR_SET_FAILED      @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_PTHREAD_COND_INIT_FAILED          @see adm_com_errorCode.h
 */
extern D32S adm_com_threadCondCreate(DHANDLE* pCondHandle);
/**
 * @brief 销毁条件变量
 * 
 * @param condHandle       [in]    条件变量句柄
 * @return D32S 
 */
extern D32S adm_com_threadCondDestroy(DHANDLE condHandle);
/**
 * @brief 条件变量等待函数，用于线程的异步转同步
 * 
 * @param condHandle       [in]    条件变量的句柄
 * @param mutexHandle      [in]    互斥锁的句柄
 * @param ulAbsTime     [in]    超时等待的时间，单位：ms 注：如果ulAbsTime = 0, 则无限等待
 * @return D32S         执行结果
 * @retval  ADM_OS_ECODE_FAILED    @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_PTHREAD_COND_WAIT_FAILED  @see adm_com_errorCode.h
 */
extern D32S adm_com_threadCondWait(DHANDLE condHandle, DHANDLE mutexHandle, size_t ulAbsTime);
/**
 * @brief 条件改变信号通知函数
 * 
 * @param condHandle   [in]    条件变量句柄
 * @return D32S     信号通知的结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_PTHREAD_COND_SIGNAL_FAILED    @see adm_com_errorCode.h
 */
extern D32S adm_com_threadCondSignal(DHANDLE condHandle);
/**
 * @brief 条件变量改变，信号广播函数
 * 
 * @param condHandle    [in]    条件变量句柄
 * @return D32S     信号广播的结果
 * @retval ADM_OS_ECODE_INVALID_PARAM
 * @retval ADM_OS_ECODE_FAILED
 * @retval ADM_OS_ECODE_PTHREAD_COND_BROAD_CAST_FAILED
 * @retval ADM_OS_ECODE_SUCCESSED
 */
extern D32S adm_com_threadCondBroadcast(DHANDLE condHandle);

/**
 * @brief 创建信号量，并初始化数值
 * 
 * @param semHandle     [out]   信号量变量
 * @param iPshared      [in]    0:用于线程间，非0：用于进程间
 * @param iVal          [in]    信号量的初始值
 * @return D32S         创建信号量的结果
 * @retval  ADM_OS_ECODE_FAILED                @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_APPLY_MEMORY_FAILED   @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SEM_INIT_FAILED       @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED             @see adm_com_errorCode.h
 */
extern D32S adm_com_threadSemCreate(DHANDLE* semHandle, D32S iPshared, D32S iVal);
/**
 * @brief 信号量P操作，信号量阻塞等待接口
 * 
 * @param semHandle     [in]    信号量句柄
 * @return D32S         阻塞等待的结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SEM_WAIT_FAILED   @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 */
extern D32S adm_com_threadSemWait(DHANDLE semHandle);
/**
 * @brief 信号量P操作，信号量非阻塞接口
 * 
 * @param semHandle     [in]    信号量句柄
 * @return D32S         结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SEM_TRY_WAIT_FAILED   @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 */
extern D32S adm_com_threadSemTryWait(DHANDLE semHandle);
/**
 * @brief 信号量P操作，有时长限制的阻塞等待接口
 * 
 * @param semHandle     [in]    信号量句柄
 * @param ulAbsTime     [in]    超时时间，单位：ms
 * @return D32S         阻塞等待的结果
 * @retval  ADM_OS_ECODE_FAILED                    @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM             @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SEM_TIMED_WAIT_FAILED     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SEM_WAIT_FAILED           @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED                 @see adm_com_errorCode.h
 */
extern D32S adm_com_threadSemTimedWait(DHANDLE semHandle, size_t ulAbsTime);
/**
 * @brief 信号量V操作
 * 
 * @param semHandle     [in]    信号量句柄
 * @return D32S         信号量V操作的结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SEM_POST_FAILED   @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 */
extern D32S adm_com_threadSemPost(DHANDLE semHandle);
/**
 * @brief 信号量销毁
 * 
 * @param semHandle     [in]    信号量句柄 
 */
extern void adm_com_threadSemDestroy(DHANDLE semHandle);


/**
 * @brief 初始化互斥锁
 * 
 * @param mutexHandle   [out]       互斥锁句柄
 * @return D32S         初始化互斥锁句柄
 * @retval  ADM_OS_ECODE_FAILED        @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED     @see adm_com_errorCode.h
 */
extern D32S adm_com_threadMutexCreate(DHANDLE* mutexHandle);
/**
 * @brief 加锁操作
 * 
 * @param mutexHandle   [in]    锁的句柄
 * @return D32S         加锁的结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 */
extern D32S adm_com_threadMutexLock(DHANDLE mutexHandle);
/**
 * @brief 尝试加锁的操作
 * 
 * @param mutexHandle   [in]    锁的句柄
 * @return D32S         尝试加锁的结果
 */
extern D32S adm_com_threadMutexTryLock(DHANDLE mutexHandle);
/**
 * @brief 解锁操作
 * 
 * @param mutexHandle   [in]    锁的句柄
 * @return D32S         解锁的结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 */
extern D32S adm_com_threadMutexUnlock(DHANDLE mutexHandle);
/**
 * @brief 释放锁
 * 
 * @param mutexHandle   [in]    锁的句柄
 */
extern void adm_com_threadMutexDestroy(DHANDLE mutexHandle);

/** 互斥锁初始化 */
#define ADM_COM_LOCK_CREATE(handler)  (void)adm_com_threadMutexCreate(handler)
/** 互斥加锁 */
#define ADM_COM_LOCK(handler)       (void)adm_com_threadMutexLock(handler)
#define ADM_COM_TRY_LOCK(handler)   (void)adm_com_threadMutexTryLock(handler)
/** 互斥解锁 */
#define ADM_COM_UNLOCK(handler)         (void)adm_com_threadMutexUnlock(handler)
/** 销毁互斥锁 */
#define ADM_COM_LOCK_DESTROY(handler)   (void)adm_com_threadMutexDestroy(handler)

/**
 * @brief 初始化读写锁
 * 
 * @param pstRwLock   [out]       读写锁句柄
 * @return D32S         初始化读写锁句柄
 * @retval  ADM_OS_ECODE_FAILED        @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED     @see adm_com_errorCode.h
 */
extern D32S adm_com_threadRWLockCreate(DHANDLE* rwLockHandle);
/**
 * @brief 写加锁操作
 * 
 * @param pstRwLock   [in]    读写锁的句柄
 * @return D32S         写加锁的结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 */
extern D32S adm_com_threadRWLock_wrlock(DHANDLE rwLockHandle);
/**
 * @brief 读加锁操作
 * 
 * @param rwLockHandle   [in]    读写锁的句柄
 * @return D32S         读加锁的结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 */
extern D32S adm_com_threadRWLock_rdlock(DHANDLE rwLockHandle);
/**
 * @brief 读写解锁操作
 * 
 * @param rwLockHandle   [in]    读写锁的句柄
 * @return D32S         读写解锁的结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 */
extern D32S adm_com_threadRWUnLock(DHANDLE rwLockHandle);
/**
 * @brief 销毁读写锁
 * 
 * @param rwLockHandle   [in]    读写锁的句柄
 */
void adm_com_threadRWLockDestroy(DHANDLE rwLockHandle);

/** 读写锁初始化 */
#define ADM_COM_RW_LOCK_CREATE(handler)   (void)adm_com_threadRWLockCreate(handler);
/** 写加锁 */
#define ADM_COM_RW_WR_LOCK(handler)     (void)adm_com_threadRWLock_wrlock(handler)
/** 读加锁 */
#define ADM_COM_RW_RD_LOCK(handler)     (void)adm_com_threadRWLock_rdlock(handler)
/** 读写解锁 */
#define ADM_COM_RW_UNLOCK(handler)      (void)adm_com_threadRWUnLock(handler)
/** 销毁读写锁 */
#define ADM_COM_RW_LOCK_DESTROY(handler)    (void)adm_com_threadRWLockDestroy(handler)

#ifdef __cplusplus  // close out "C" linkage in case of c++ compiling
}
#endif
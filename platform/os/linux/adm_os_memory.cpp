
/*[
 *      Name:                   adm_memory.c
 *
 *      Project:                ADM
]*/

/*! \internal
 * \file
 *          Provides a reference implementation of a memory handling API
 *
 * \brief   Memory handling APIs
 */

#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/syscall.h>  /*添加上此头文件*/

#include <sys/time.h>     ///< 系统时间获取
#include <semaphore.h>    ///< #ifdef __linux__   sem

#include <map>

#include "adm_typedefs.h"
#include "adm_memory.h"

#include <unistd.h>
#include "adm_debug.h"

#include "adm_com_thread.h"



#ifdef ADB_DBG_MEM


#define ADM_DBG_MEM_MAXNUM 1024
#define ADM_DBG_ADDR_MASK (0xEEEEEEEE)
#define ADM_DBG_MEM_SINGLE_MAX_SIZE (10 * 1024 * 1024)
#define ADM_DBG_MEM_PEAK_SIZE ( 256*1024*1024)
#define ADM_DBG_MEM_FILENAME_LEN 32
#define ADM_DBG_MEM_COUNT_PRINT 200     //!< 每200次打印列表


typedef struct
{
    DSIZE addr;          //!< 内存申请地址，用于标识
    DSIZE len;
    DCHAR *funcname;
    D32U lineno;
    pid_t threadID;
} ADM_MEMDBG_RECORD;

typedef struct
{
    DBOOL isInited; //! 标识初始化
    DSIZE totalSize;        //!< 内存申请的峰值
    D32U totalCount;        //!< 内存申请次数记录
    DSIZE singleMaxSize;    //!< 单次申请最大内存
} ADM_MEMDBG_REAL_TOTAL;

// typedef struct
// {
//    DSIZE peakSingleSize;
//    D32U peakCount;
//    DSIZE peakTotalSize;
// } ADM_MEMDBG_PEAK_TOTAL;

static ADM_MEMDBG_RECORD adm_memdbgList[ADM_DBG_MEM_MAXNUM] = {0};
static ADM_MEMDBG_LOG_CB adm_memdbgLogFuncCb = NULL;
static ADM_MEMDBG_REAL_TOTAL adm_memdbgRealTotal = {0};
// static ADM_MEMDBG_PEAK_TOTAL adm_memdbgPeak={0};

#endif

using namespace std;

#define VUS_MEM_CHECKFLAG           0x55AAAA55
#define VUS_MEM_HEADERSIZE          sizeof(vus_mem_header_st)


typedef map<void *, vus_pt> mem_t;
typedef struct
{
    DHANDLE memMapLock;
    mem_t admMemMap;
} adm_mem_strl_t;
static adm_mem_strl_t admMemMapCtrl;

/** 新增锁处理 */
typedef struct
{
    pthread_mutex_t mutex;
    pthread_mutexattr_t mutexattr;
    bool is_mutex;
} adm_lock_ctrl_st, *adm_lock_ctrl_pt;

/** 新增条件处理 */
typedef struct
{
    pthread_cond_t cond;
    pthread_condattr_t condattr;
} adm_cond_ctrl_st, *adm_cond_ctrl_pt;


DBOOL memValidCheck(mem_t *pCtrl, void *pData)
{
    DBOOL bRet = DFALSE;
    vus_pt pVus = adm_getheader(pData);
    DASSERT(pVus != DNULL);

    mem_t::iterator iter = admMemMapCtrl.admMemMap.find(pData);

    if (iter != admMemMapCtrl.admMemMap.end())
    {
        /** 地址校验 */
        if (pVus == iter->second)
        {
            bRet = DTRUE;
        }
        else {}
    }
    else {}

    return bRet;
}

/**
 * @function    memMapFree
 * @brief
 * @details    free时,删除 admMemMap 成员
 *
 * @param    [in] 申请时的数据指针
 * @return
 * @calls        :
 * @called By    :
 * @note   :
 */
void memMapFree(void *pData)
{
    vus_pt pVus = adm_getheader(pData);
    DASSERT(pVus != DNULL);

    adm_com_threadMutexLock(admMemMapCtrl.memMapLock);

    mem_t::iterator iter = admMemMapCtrl.admMemMap.find(pData);

    if (iter != admMemMapCtrl.admMemMap.end())
    {
        /** 地址校验 */
        if (pVus == iter->second)
        {
            admMemMapCtrl.admMemMap.erase(iter);
        }
        else {}
    }
    else {}

    adm_com_threadMutexUnlock(admMemMapCtrl.memMapLock);
}


void adm_meminit()
{
    /** admMemMapCtrl.m_memMap管理数据申请的内存
    * 不然锁资源会在此释放
    */
    adm_com_threadMutexCreate(&(admMemMapCtrl.memMapLock));
    log_d("admMemMapCtrl.admMemMap.size() = %d", admMemMapCtrl.admMemMap.size());

#if 0

    //每次 adm_free 时 删除一个内存成员，此处使用迭代器破坏
    if (admMemMapCtrl.admMemMap.size() > 0)
    {
        DASSERT(DFALSE);

        mem_t::iterator iter = admMemMapCtrl.admMemMap.begin();
        mem_t::iterator iterEnd = admMemMapCtrl.admMemMap.end();

        while (iter != iterEnd)
        {
            void *pData = iter->first;
            DASSERT(pData == iter->second->data);
            adm_free(pData);

            iter++;
        }

        admMemMapCtrl.admMemMap.clear();
    }
    else {}

#endif
}

void adm_memuninit()
{
    adm_com_threadMutexLock(admMemMapCtrl.memMapLock);

    log_d("admMemMapCtrl.admMemMap.size() = %d", admMemMapCtrl.admMemMap.size());
#if 0

    //迭代器破坏
    if (admMemMapCtrl.admMemMap.size() > 0)
    {
        DASSERT(DFALSE);

        mem_t::iterator iter = admMemMapCtrl.admMemMap.begin();
        mem_t::iterator iterEnd = admMemMapCtrl.admMemMap.end();

        while (iter != iterEnd)
        {
            void *pData = iter->first;
            DASSERT(pData == iter->second->data);
            adm_free(pData);

            iter++;
        }

        admMemMapCtrl.admMemMap.clear();
    }
    else {}

#endif
    adm_com_threadMutexUnlock(admMemMapCtrl.memMapLock);
    adm_com_threadMutexDestroy(admMemMapCtrl.memMapLock);
}

void *adm_memzero(void *buf, DSIZE size)
{
    if (!buf)
        return NULL;

    return memset(buf, 0x00, size);
}

void *adm_memset(void *dest, D32S val, DSIZE count, DSIZE dstLen)
{
    if (!dest)
        return NULL;

    if (dstLen < count)
        return NULL;

    return memset(dest, val, count);
}

void *adm_memcpy(void *dest, const void *src, DSIZE srcLen, DSIZE dstLen)
{
    if (!dest || !src)
        return NULL;

    if (dstLen < srcLen)
        return NULL;

    return memcpy(dest, src, srcLen);
}

D32S adm_memcmp(const void *inBuf1, const void *inBuf2, D32U count)
{
    if (!inBuf1)
        return -1;

    if (!inBuf2)
        return 1;

    return memcmp(inBuf1, inBuf2, count);
}

void *adm_memmove(void *dest, const void *src, DSIZE srcLen, DSIZE dstLen)
{
    if (!dest || !src || (srcLen == 0) || (dstLen == 0))
        return NULL;

    if (dstLen < srcLen)
        return NULL;

    return memmove(dest, src, srcLen);
}

/*void *adm_malloc(D32U size)
{
    return (malloc(size));
}*/

pid_t gettid()
{
    return syscall(SYS_gettid);
}


void *__adm_malloc(DSIZE size, const DCHAR *funcname, D32U line)
{
    D32U i = 0;
    static DBOOL isPrintTotalList = FALSE;
    void *buf = malloc(size);

    if (NULL == buf)
    {
#ifdef ADM_DBG_MEM
        log_i("malloc len[%ld], funcname[%s], lineno[%u], threadID[%d] memeory apply failed!!!",
              size,
              (DCHAR *)funcname,
              (D32U)line,
              gettid());
#endif
    }
    else
    {
#ifdef ADM_DBG_MEM

        //!< 标识总的计数结构体是否已经初始化
        if (FALSE == adm_memdbgRealTotal.isInited)
        {
            adm_memdbgRealTotal.isInited = TRUE;
            adm_memdbgRealTotal.singleMaxSize = 0;
            adm_memdbgRealTotal.totalCount = 0;
            adm_memdbgRealTotal.totalSize = 0;
        }

        // log_i("malloc len[%ld], addr[0x%x], funcname[%s], lineno[%u], threadID[%d] memeory apply successed!!!",
        //                      size,
        //                      (DSIZE)buf,
        //                      (DCHAR *)funcname,
        //                      (D32U)line,
        //                      gettid());
        for (i = 0; i < ADM_DBG_MEM_MAXNUM; i++)
        {
            if (0 == adm_memdbgList[i].addr)
            {
                adm_memdbgList[i].len = (DSIZE)size;
                adm_memdbgList[i].addr = (DSIZE)buf;
                adm_memdbgList[i].funcname = (DCHAR *)funcname;
                adm_memdbgList[i].lineno = (D32U)line;
                adm_memdbgList[i].threadID = gettid();
                break;
            }
        }

        // if (ADM_DBG_MEM_MAXNUM == i)
        // {
        //  log_i("malloc len[%ld], addr[0x%x], funcname[%s], lineno[%u], threadID[%d] but not record into  adm_memdbgList!!!",
        //                      size,
        //                      (DSIZE)buf,
        //                      (DCHAR *)funcname,
        //                      (D32U)line,
        //                      gettid());
        // }

        adm_memdbgRealTotal.totalCount += 1;
        //!< 每200次内存申请，打印一次列表信息
        // if (0 == adm_memdbgRealTotal.totalCount % ADM_DBG_MEM_COUNT_PRINT)
        // {
        //  for (i = 0;i < ADM_DBG_MEM_MAXNUM; i++)
        //  {
        //      if (0 != adm_memdbgList[i].addr)
        //      {
        //          log_i("malloc len[%ld], addr[0x%x], funcname[%s], lineno[%u], threadID[%d], print record list every %d times!!!",
        //              adm_memdbgList[i].len,
        //              adm_memdbgList[i].addr,
        //              adm_memdbgList[i].funcname,
        //              adm_memdbgList[i].lineno,
        //              adm_memdbgList[i].threadID,
        //              ADM_DBG_MEM_COUNT_PRINT
        //          );
        //      }
        //  }
        // }

        adm_memdbgRealTotal.totalSize += size;

        //!< 超过总的限制内存大小
        if (ADM_DBG_MEM_PEAK_SIZE < adm_memdbgRealTotal.totalSize)
        {
            if (FALSE == isPrintTotalList)
            {
                for (i = 0; i < ADM_DBG_MEM_MAXNUM; i++)
                {
                    if (0 != adm_memdbgList[i].addr)
                    {
                        log_i("malloc len[%ld], addr[0x%x], funcname[%s], lineno[%u], threadID[%d], totolSize[%ld], totalCount[%u], Peak memory size > %ld Bytes!!!",
                              adm_memdbgList[i].len,
                              adm_memdbgList[i].addr,
                              adm_memdbgList[i].funcname,
                              adm_memdbgList[i].lineno,
                              adm_memdbgList[i].threadID,
                              adm_memdbgRealTotal.totalSize,
                              adm_memdbgRealTotal.totalCount,
                              ADM_DBG_MEM_PEAK_SIZE
                             );
                    }
                }

                isPrintTotalList = TRUE;
            }
        }

        if (adm_memdbgRealTotal.singleMaxSize < size)
        {
            adm_memdbgRealTotal.singleMaxSize = size;
            // if (ADM_DBG_MEM_SINGLE_MAX_SIZE < adm_memdbgRealTotal.singleMaxSize)
            // {
            //  log_i("malloc len[%ld], addr[0x%x], funcname[%s], lineno[%u], threadID[%d] more than single limited size[%ld]!!!",
            //                      size,
            //                      (DSIZE)buf,
            //                      (DCHAR *)funcname,
            //                      (D32U)line,
            //                      gettid(),
            //                      ADM_DBG_MEM_SINGLE_MAX_SIZE);
            // }
        }

#endif
    }

    if (NULL != buf)
    {
        adm_memset(buf, 0x00, size, size);
    }

    return buf;
}

void *__adm_mallocstruct(D32U structType, DSIZE size, const DCHAR *funcname, D32U line)
{
    void *pRet = DNULL;
    DSIZE dataSize = VUS_MEM_HEADERSIZE + size;
    vus_pt pMallocData = (vus_pt)malloc(dataSize);

    if (DNULL == pMallocData)
    {
        return DNULL;
    }

    adm_memset(pMallocData, 0x00, dataSize, dataSize);

    pMallocData->header.data_size = size;
    pMallocData->header.fun_name = funcname;
    pMallocData->header.line = line;
    pMallocData->header.type = structType;
    pMallocData->header.check_flag = VUS_MEM_CHECKFLAG;

    adm_com_threadMutexLock(admMemMapCtrl.memMapLock);

    pRet = (void *)(pMallocData->data);
    admMemMapCtrl.admMemMap[pRet] = pMallocData;

    adm_com_threadMutexUnlock(admMemMapCtrl.memMapLock);

    return pMallocData->data;
}

vus_pt adm_getheader(void *pStruct)
{
    vus_pt pMallocData = (vus_pt)((D8U *)pStruct - VUS_MEM_HEADERSIZE);

    if (pMallocData->header.check_flag != VUS_MEM_CHECKFLAG)
    {
        pMallocData = DNULL;
    }
    else
    {
        DASSERT(DFALSE);
    };

    return pMallocData;
}

void *adm_getdata(vus_pt pdata)
{
    vus_pt pMallocData = (vus_pt)((D8U *)pdata - VUS_MEM_HEADERSIZE);

    if (pMallocData->header.check_flag != VUS_MEM_CHECKFLAG)
    {
        pMallocData = DNULL;
    }
    else {};

    return pMallocData;
}


void __adm_free(void *pp, const DCHAR *funcname, D32U line)
{
    D32U i = 0;
    DBOOL isFound = FALSE;
    DSIZE size = 0;

    if ((pp == NULL))
    {
        return;
    }
    else
    {
#ifdef ADM_DBG_MEM

        for (i = 0; i < ADM_DBG_MEM_MAXNUM; i++)
        {
            if (adm_memdbgList[i].addr == (DSIZE)pp)
            {
                size = adm_memdbgList[i].len;
                isFound = TRUE;
                adm_memdbgList[i].len = 0;
                adm_memdbgList[i].addr = 0;
                adm_memdbgList[i].funcname = NULL;
                adm_memdbgList[i].lineno = 0;
                adm_memdbgList[i].threadID = 0;
                break;
            }
        }

        if (TRUE == isFound)
        {
            adm_memdbgRealTotal.totalSize -= size;
            adm_memdbgRealTotal.totalCount -= 1;

            if (0 > adm_memdbgRealTotal.totalSize)
            {
                adm_memdbgRealTotal.totalSize = 0;
            }

            if (0 > adm_memdbgRealTotal.totalCount)
            {
                adm_memdbgRealTotal.totalCount = 0;
            }
        }

#endif
    }

    free(pp);

    return;
}


#if 0
//新接口释放对应__adm_mallocstruct
void __adm_free(void *pp, const DCHAR *funcname, D32U line)
{
    D32U i = 0;
    DBOOL isFound = FALSE;
    DSIZE size = 0;

    if ((DNULL != pp))
    {
        isFound = memValidCheck(&(admMemMapCtrl.admMemMap), pp);

        if (DTRUE == isFound)
        {
            vus_pt pData = adm_getheader(pp);
            memMapFree(pp);
            free(pData);
        }
        else
        {
            DASSERT(DFALSE);
        }
    }
    else
    {
        DASSERT(DFALSE);
    }

    return;
}
#endif


DBOOL adm_memdbg_view(void)
{
    D32U i = 0;

#ifdef ADM_DBG_MEM

    log_i("--------------------memview total infomation begin--------------------");
    log_i("Allocated peakTotalSize:%ld, peakCount:%u, peakSingleSize:%ld",
          adm_memdbgRealTotal.totalSize,
          adm_memdbgRealTotal.totalCount,
          adm_memdbgRealTotal.singleMaxSize);


    for (i = 0; i < ADM_DBG_MEM_MAXNUM; i++)
    {
        if (0 != adm_memdbgList[i].addr)
        {
            log_i("memory view malloc len[%ld], addr[0x%x], funcname[%s], lineno[%u], threadID[%d], view record list!!!",
                  adm_memdbgList[i].len,
                  adm_memdbgList[i].addr,
                  adm_memdbgList[i].funcname,
                  adm_memdbgList[i].lineno,
                  adm_memdbgList[i].threadID
                 );
        }
    }

    log_i("--------------------memview total infomation end--------------------");
#endif
    return TRUE;
}


/**
void *__adm_malloc(DSIZE size, const DCHAR *funcname, D32U line)
{
    void *buf = malloc(size);

   if (buf == NULL)
   {
#if 0//defined(ADM_ENABLE_PAL_TRACE)
      DSIZE left=0;

      left = 0;
      trace("malloc fail!!, len: %d, left: %d", size, left);
#endif
   }

#ifdef ADM_DBG_MEM
   else
   {
      D32U i=0;
      if (size > adm_memdbgPeak.peakSingleSize)
      {
         adm_memdbgPeak.peakSingleSize = size;
      }

      //adm_dbg_mem_index &= 1023;
      for(i=0;i<ADM_DBG_MEM_MAXNUM;i++)
      {
          if(adm_memdbgList[i].addr==0)
          {
              adm_memdbgList[i].len = (D32U)size;
              adm_memdbgList[i].addr = (DSIZE)buf;
              adm_memdbgList[i].funcname = (DCHAR *)funcname;
              adm_memdbgList[i].lineno = (D32U)line;

              adm_memdbgRealTotal.totalSize += (D32U)size;
              adm_memdbgRealTotal.totalCount++;

              if (adm_memdbgRealTotal.totalCount > adm_memdbgPeak.peakCount)
              {
                 adm_memdbgPeak.peakCount = adm_memdbgRealTotal.totalCount;
              }
              if (adm_memdbgRealTotal.totalSize > adm_memdbgPeak.peakTotalSize)
              {
                 adm_memdbgPeak.peakTotalSize = adm_memdbgRealTotal.totalSize;
              }

#if 0

              if(adm_memdbgLogFuncCb)
                  adm_memdbgLogFuncCb("malloc[%d]: addr=0x%x, size=%d, pos:%d, func:%s, lineno:%d, totalsize:%ld, totalcount:%d", \
                      i, adm_memdbgList[i].addr, adm_memdbgList[i].len, i,\
                      adm_memdbgList[i].funcname, adm_memdbgList[i].len, \
                      adm_memdbgRealTotal.totalSize, adm_memdbgRealTotal.totalCount);
#endif
              break;
          }
      }

      if (i == ADM_DBG_MEM_MAXNUM)
      {
        //ASSERT(0);
      }

      if (adm_memdbgPeak.peakSingleSize > ADM_DBG_MAX_LIMITED_SIZE)
      {
         //ASSERT(0);
          if(adm_memdbgLogFuncCb)
              adm_memdbgLogFuncCb("malloc size[%ld] overflow max single liminted size[%ld] !!!", \
              adm_memdbgPeak.peakSingleSize, ADM_DBG_MAX_LIMITED_SIZE);
      }

      if (adm_memdbgRealTotal.totalCount > ADM_DBG_MEM_MAXNUM)
      {
          //ASSERT(0);
           if(adm_memdbgLogFuncCb)
               adm_memdbgLogFuncCb("malloc count[%d] overflow max liminted count[%d] !!!", \
               adm_memdbgPeak.peakSingleSize, ADM_DBG_MEM_MAXNUM);
      }

   }
#endif
    if (NULL != buf) {
        adm_memset(buf, 0x00, size, size);
    }

    return buf;
}

void __adm_free(void *pp, const DCHAR *funcname, D32U line)
{
    if((pp==NULL))
    {
        return;
    }

#ifdef ADM_DBG_MEM
    {
        D32U i = 0;

        for (i = 0; i < ADM_DBG_MEM_MAXNUM; i++)
        {
          if (adm_memdbgList[i].addr == (DSIZE)pp)
          {
              adm_memdbgRealTotal.totalSize -= adm_memdbgList[i].len;
              adm_memdbgRealTotal.totalCount--;

              if(adm_memdbgRealTotal.totalSize <0)
                adm_memdbgRealTotal.totalSize = 0;
              if(adm_memdbgRealTotal.totalCount < 0)
                adm_memdbgRealTotal.totalCount=0;
#if 0
              if(adm_memdbgLogFuncCb)
                  adm_memdbgLogFuncCb("free[%d]: addr=0x%x, size=%d, pos:%d, func:%s, lineno:%d, totalsize:%ld, totalcount:%d", \
                      i, adm_memdbgList[i].addr, adm_memdbgList[i].len, i,\
                      adm_memdbgList[i].funcname, adm_memdbgList[i].len, \
                      adm_memdbgRealTotal.totalSize, adm_memdbgRealTotal.totalCount);
#endif
             adm_memdbgList[i].addr = 0;
             break;
          }
        }
        if (i == ADM_DBG_MEM_MAXNUM)
        {
          //ASSERT(0);
        }
    }
#endif

    free(pp);
}
*/

/*
void adm_memlog_set(ADM_MEMDBG_LOG_CB func)
{
#ifdef ADM_DBG_MEM
    adm_memdbgLogFuncCb = func;
#endif
}*/

/*
DBOOL adm_memdbg_view(void)
{
    D32U i=0, count=0;
#ifdef ADM_DBG_MEM

    if(adm_memdbgLogFuncCb)
    {
        adm_memdbgLogFuncCb("--------------------memview total infomation begin--------------------");
        adm_memdbgLogFuncCb("Allocated peakTotalSize:%d, peakCount:%d, peakSingleSize:%ld.", \
            adm_memdbgPeak.peakTotalSize, adm_memdbgPeak.peakCount, adm_memdbgPeak.peakSingleSize);
        adm_memdbgLogFuncCb("Not Free totalSize:%ld, totalCount:%d. As list:", \
            adm_memdbgRealTotal.totalSize, adm_memdbgRealTotal.totalCount);
        adm_memdbgLogFuncCb("--------------------memview total infomation end----------------------");
    }

    for(i=0;i<ADM_DBG_MEM_MAXNUM;i++)
    {
        if(adm_memdbgList[i].addr != 0)
        {
            count++;
            if(adm_memdbgLogFuncCb)
                adm_memdbgLogFuncCb("memview[%d]: addr=0x%x, size=%d, pos:%d, func:%s, lineno:%d", \
                    count, adm_memdbgList[i].addr, adm_memdbgList[i].len, i,\
                    adm_memdbgList[i].funcname, adm_memdbgList[i].len);
        }
    }
#endif
    if(count>0)
        return TRUE;
    else
        return FALSE;
}
*/
void *adm_memdup(void *buf, D32U len)
{
    void *pnew = NULL;

    if (!buf || len <= 0)
    {
        return NULL;
    }

    pnew = (void *)adm_malloc(len);

    if (pnew == NULL)
        return NULL;

    adm_memcpy(pnew, buf, len, len);

    return pnew;
}

void *adm_memchr(const void *buf, int ch, DSIZE count)
{
    if (!buf || count == 0) return NULL;

    return (void *)memchr(buf, ch, count);
}


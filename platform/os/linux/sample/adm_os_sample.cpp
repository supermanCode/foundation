#include "adm_com_thread.h"
#include <pthread.h>
#include <unistd.h>

#define LOGM(fmt, ...)   printf("[ %s ] %s : %d " fmt "\n", __DATE__, __FILE__,  __LINE__, ##__VA_ARGS__)

static DHANDLE stCond = {0};
static DHANDLE stMutex = {0};
static size_t value = -1;
static DHANDLE stSem = {0};
static DBOOL flag = FALSE;
static DHANDLE semHandle = {0};

static void* __threadCallback_3(void* param)
{
    D32S iCount = 0;

    while(TRUE)
    {
        sleep(1);
        iCount += 1;

        if (iCount >= 4)
        {
            flag = TRUE;
            LOGM("Sem flag is TRUE");
            adm_com_threadSemPost(stSem);
            break;
        }
    }

    pthread_exit(NULL);

    return NULL;
}

static void* __threadCallback_4(void* param)
{
    D32S iRet = -1;
    adm_com_threadSemWait(stSem);
    while(flag == FALSE)
    {
        sleep(1);
        LOGM("Sem flag is FALSE");
        iRet = adm_com_threadSemTimedWait(stSem, 100 * 1000);
        LOGM("adm_com_threadSemTimedWait :%d", iRet);
    }

    return NULL;
}


static void* __threadCallback_1(void* param)
{
    sleep(2);

    
    (void)ADM_COM_LOCK(stMutex);
    value = 10000000000000000;
    (void)adm_com_threadCondSignal(stCond);
    (void)ADM_COM_UNLOCK(stMutex);

    return NULL;
}

static void* __threadCallback_2(void* param)
{
    D32S iRet = ADM_OS_ECODE_FAILED;

    (void)ADM_COM_LOCK(stMutex);
    iRet = adm_com_threadCondWait(stCond, stMutex, (1000 * 10));
    (void)ADM_COM_UNLOCK(stMutex);
    if (ADM_OS_ECODE_SUCCESSED != iRet)
    {
        LOGM("adm_com_threadCondWait failed, [err:%d]", iRet);
    }

    LOGM("value:%ld", value);

    return NULL;
}
// #include <pthread.h>
// #include <time.h>

int main(int argc, char*argv[])
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    DHANDLE threadHandler_1 = {0};
    DHANDLE threadHandler_2 = {0};
    DHANDLE threadHandler_3 = {0};
    DHANDLE threadHandler_4 = {0};
    DCHAR threadName[16] = {0};



    // pthread_cond_t cond;
    // pthread_mutex_t mutex_;

    // pthread_mutex_init(&mutex_, NULL);
    // pthread_cond_init(&cond, NULL);

    // long timeout_ms = 3; // wait time 100ms
    // struct timespec abstime;
    // abstime.tv_sec = time(NULL) + timeout_ms / 1000;
    // abstime.tv_nsec = (timeout_ms % 1000) * 1000000;

    // pthread_mutex_lock(&mutex_);
    // LOGM("xxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
    // pthread_cond_timedwait(&cond, &mutex_, &abstime);
    // LOGM("xxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
    // pthread_mutex_unlock(&mutex_);

    DHANDLE syncMutex = -1;
    DHANDLE syncCond = -1;


    adm_com_threadCondCreate(&syncCond);
    adm_com_threadMutexCreate(&syncMutex);


    adm_com_threadMutexLock(syncMutex);
    LOGM("xxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
    adm_com_threadCondWait(syncCond, syncMutex, 3000);
    LOGM("xxxxxxxxxxxxxxxxxxxxxxxx");
    adm_com_threadMutexUnlock(syncMutex);

    do {
        // adm_debug_logInitWithFileName("./", "sample.log", (5 * 1024 * 1024), 2, ELOG_LVL_INFO, TRUE);

        ADM_COM_LOCK_CREATE(&stMutex);

        iRet = adm_com_threadCondCreate(&stCond);
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            LOGM("pthread_cond_init failed, [err:%d]", iRet);
            break;
        }

        iRet = adm_com_threadCreate(&threadHandler_1, __threadCallback_1, NULL, "THREAD_1");
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            LOGM("adm_com_threadCreate __threadCallback_1 failed, [err:%d]", iRet);
            break;
        }

        iRet = adm_com_threadCreate(&threadHandler_2, __threadCallback_2, NULL, "THREAD_2");
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            LOGM("adm_com_threadCreate __threadCallback_2 failed, [err:%d]", iRet);
            break;
        }
        
        (void)adm_memzero(threadName, sizeof(threadName));
        iRet = adm_com_threadGetName(threadHandler_1, threadName, sizeof(threadName));
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            LOGM("adm_com_threadGetName threadHandler_1 failed, [err:%d]", iRet);
            break;
        }
        LOGM("threadHandler_1 threadName:%s", threadName);

        (void)adm_memzero(threadName, sizeof(threadName));
        iRet = adm_com_threadGetName(threadHandler_2, threadName, sizeof(threadName));
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            LOGM("adm_com_threadGetName threadHandler_2 failed, [err:%d]", iRet);
            break;
        }
        LOGM("threadHandler_1 threadName:%s", threadName);


        iRet = adm_com_threadSemCreate(&stSem, 0, 3);
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            LOGM("adm_com_threadSemCreate failed, [err:%d]", iRet);
            break;
        }

        iRet = adm_com_threadCreate(&threadHandler_3, __threadCallback_3, NULL, "THREAD_3");
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            LOGM("adm_com_threadCreate __threadCallback_3 failed, [err:%d]", iRet);
            break;
        }

        iRet = adm_com_threadCreate(&threadHandler_4, __threadCallback_4, NULL, "THREAD_4");
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            LOGM("adm_com_threadCreate __threadCallback_4 failed, [err:%d]", iRet);
            break;
        }
        
        (void)adm_memzero(threadName, sizeof(threadName));
        iRet = adm_com_threadGetName(threadHandler_3, threadName, sizeof(threadName));
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            LOGM("adm_com_threadGetName threadHandler_3 failed, [err:%d]", iRet);
            break;
        }
        LOGM("threadHandler_3 threadName:%s", threadName);

        (void)adm_memzero(threadName, sizeof(threadName));
        iRet = adm_com_threadGetName(threadHandler_4, threadName, sizeof(threadName));
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            LOGM("adm_com_threadGetName threadHandler_4 failed, [err:%d]", iRet);
            break;
        }
        LOGM("threadHandler_4 threadName:%s", threadName);

        
    } while(0);

    while(1)
    {
        sleep(30);
    }

    (void)adm_com_threadSemDestroy(stSem);
    (void)adm_com_threadCondDestroy(stCond);
    (void)ADM_COM_LOCK_DESTROY(stMutex);
    // adm_debug_logDeinit();
    return iRet;
}

#include "adm_com_thread.h"
#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <semaphore.h>
#include <errno.h>
#include <string>
#include "adm_debug.h"

#ifdef LOG_TAG
#undef LOG_TAG
#endif

#define LOG_TAG     "ADM_OS"

#define ADM_IS_ERR(iRet)        (ADM_OS_ECODE_SUCCESSED != (iRet))
#define ADM_IS_SUCCESS(iRet)    (ADM_OS_ECODE_SUCCESSED == (iRet))
#define ADM_IS_NULL(pstCb)      (NULL == (pstCb))
#define ADM_NOT_NULL(pstCb)     (NULL != (pstCb))

// #include "adm_com_threadLog.h"

/**
 * @brief 通用线程创建的接口，默认使用线程分离的属性
 * 
 * @param pthreadHandle     [out]    线程句柄
 * @param start_routine     [in]    线程处理函数
 * @param argv              [in]    线程处理参数
 * @param threadName        [in]    设置线程名
 * @return D32S             线程创建的执行结果
 * @retval  ADM_OS_ECODE_FAILED @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_PTHREAD_ATTRSET_FALIED    @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_PTHREAD_CREATE_FAILED     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_PTHREAD_SET_NAME_FAILED   @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED                 @see adm_com_errorCode.h
 */
D32S adm_com_threadCreate(DHANDLE* pthreadHandle, void* (*start_routine)(void* param), void* argv, const DCHAR* threadName)
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    pthread_attr_t stThreadAttr = {0};
    pthread_t threadHandler = 0;

    do {
        /** 获取线程属性句柄 */
        iRet = pthread_attr_init(&stThreadAttr);
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            iRet = ADM_OS_ECODE_PTHREAD_ATTR_INIT_FAILED;
            break;
        }

        /** 设置线程分离属性 */
        iRet = pthread_attr_setdetachstate(&stThreadAttr, PTHREAD_CREATE_DETACHED);
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            iRet = ADM_OS_ECODE_PTHREAD_ATTRSET_FALIED;
            break;
        }


        /** 线程启动 */
        iRet = pthread_create(&threadHandler, &stThreadAttr, start_routine, argv);
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            iRet = ADM_OS_ECODE_PTHREAD_CREATE_FAILED;
            break;
        }

        /** 设置线程名 */
        if (NULL != threadName)
        {
            iRet = pthread_setname_np(threadHandler, threadName);
            if (ADM_OS_ECODE_SUCCESSED != iRet)
            {
                iRet = ADM_OS_ECODE_PTHREAD_SET_NAME_FAILED;
                break;
            }
        }
        *pthreadHandle = (DHANDLE)threadHandler;
        iRet = ADM_OS_ECODE_SUCCESSED;
    } while(FALSE);

    (void)pthread_attr_destroy(&stThreadAttr);

    return iRet;
}


/**
 * @brief 根据线程句柄获取线程的名称
 * 
 * @param pthreadHandle     [in]    线程句柄
 * @param threadName        [out]   线程名称
 * @param nameLen           [in]    nameLen指定threadName中可用的字节数。名称指定的缓冲区长度至少为16个字符。在输出缓冲区中返回的线程名将为以空字符结尾。   
 * @return D32S             获取线程名的结果
 * @retval  ADM_OS_ECODE_FAILED    @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_PTHREAD_GET_NAME_FAILED   @see adm_com_errorCode.h
 */
D32S adm_com_threadGetName(DHANDLE pthreadHandle, DCHAR* threadName, size_t nameLen)
{
    D32S iRet = ADM_OS_ECODE_FAILED;

    do {
        iRet = pthread_getname_np((pthread_t)pthreadHandle, threadName, nameLen);
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            iRet = ADM_OS_ECODE_PTHREAD_GET_NAME_FAILED;
            break;
        }
    } while(FALSE);

    return iRet;
}

/**
 * @brief 初始化条件变量
 * @note 使用此接口初始化条件变量，会将时间属性设置为CLOCK_MONOTONIC，可避免因为系统时间（CLOCK_REALTIME）跳变导致的不可控问题
 * 
 * @param pCondHandle       [out]   条件变量句柄
 * @return D32S         条件变量初始化的结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_PTHREAD_COND_ATTR_INIT_FAILED     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_PTHREAD_COND_ATTR_SET_FAILED      @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_PTHREAD_COND_INIT_FAILED          @see adm_com_errorCode.h
 */
D32S adm_com_threadCondCreate(DHANDLE* pCondHandle)
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    pthread_condattr_t stCondAttr = {0};
    pthread_cond_t* pstCond = NULL;

    do {
        *pCondHandle = -1;
        pstCond = (pthread_cond_t*)adm_malloc(sizeof(pthread_cond_t));
        if (NULL == pstCond)
        {
            iRet = ADM_OS_ECODE_APPLY_MEMORY_FAILED;
            break;
        }

        iRet = pthread_condattr_init(&stCondAttr);
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            iRet = ADM_OS_ECODE_PTHREAD_COND_ATTR_INIT_FAILED;
            break;
        }

        iRet = pthread_condattr_setclock(&stCondAttr, CLOCK_MONOTONIC);
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            iRet = ADM_OS_ECODE_PTHREAD_COND_ATTR_SET_FAILED;
            break;
        }

        iRet = pthread_cond_init(pstCond, &stCondAttr);
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            iRet = ADM_OS_ECODE_PTHREAD_COND_INIT_FAILED;
            break;
        }
        (void)pthread_condattr_destroy(&stCondAttr);
        *pCondHandle = (DHANDLE)pstCond;
    } while(FALSE);

    return iRet;
}

/**
 * @brief 销毁条件变量
 * 
 * @param condHandle       [in]    条件变量句柄
 * @return D32S         pthread_cond_destroy 的返回值
 */
D32S adm_com_threadCondDestroy(DHANDLE condHandle)
{
    pthread_cond_t* pstCond = (pthread_cond_t*)condHandle;
    D32S iRet = ADM_OS_ECODE_FAILED;

    if (ADM_IS_NULL(pstCond))
    {
        return ADM_OS_ECODE_INVALID_PARAM;
    }

    iRet = pthread_cond_destroy(pstCond);
    adm_free(pstCond);

    return iRet;
}

/**
 * @brief 条件变量等待函数，用于线程的异步转同步
 * @note 在调用此接口的前后，需要加上对互斥锁的上锁和解锁，并在上锁的时候处理数据
 * 
 * @param condHandle       [in]    条件变量的句柄
 * @param mutexHandle      [in]    互斥锁的句柄
 * @param ulAbsTime     [in]    超时等待的时间，单位：ms
 * @return D32S         执行结果
 * @retval  ADM_OS_ECODE_FAILED    @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_PTHREAD_COND_WAIT_FAILED  @see adm_com_errorCode.h
 */
D32S adm_com_threadCondWait(DHANDLE condHandle, DHANDLE mutexHandle, size_t ulAbsTime)
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    struct timespec stTime;
    pthread_condattr_t stCondAttr = {0};
    clockid_t clockId = CLOCK_REALTIME;
    pthread_cond_t* pstCond = (pthread_cond_t*)condHandle;
    pthread_mutex_t* pstMutexLock = (pthread_mutex_t*)mutexHandle;
    
    if (NULL == pstCond
        || NULL == pstMutexLock
        || 0 > ulAbsTime)
    {
        iRet = ADM_OS_ECODE_INVALID_PARAM;
        return iRet;
    }

    do {

        //!< 获取condarr的clockid有问题，只获取 CLOCK_MONOTONIC 即可
        // iRet = pthread_condattr_getclock(&stCondAttr, &clockId);
        // if (ADM_OS_ECODE_SUCCESSED != iRet)
        // {
        //     iRet = ADM_OS_ECODE_PTHREAD_COND_ATTR_GET_FAILED;
        //     break;
        // }
        if (0 == ulAbsTime)
        {
            iRet = pthread_cond_wait(pstCond, (pthread_mutex_t*)pstMutexLock); 
            if (ADM_OS_ECODE_SUCCESSED != iRet)
            {
                iRet = ADM_OS_ECODE_PTHREAD_COND_WAIT_FAILED;
                break;
            }
        }
        else
        {
            (void)adm_memzero(&stTime, sizeof(stTime));
            /*
            * pthread_cond_timedwait()默认使用的是CLOCK_REALTIME,
            * CLOCK_REALTIME容易受系统影响,比如校时操作
            * 所以条件变量使用的时钟改为CLOCK_MONOTONIC
            * 参考:https://man7.org/linux/man-pages/man3/pthread_cond_timedwait.3p.html
            */
            if ( 0 == clock_gettime(CLOCK_MONOTONIC, &stTime))
            {
                stTime.tv_nsec += ulAbsTime % 1000 * 1000000;
                stTime.tv_sec  += ulAbsTime / 1000;
                if (1000000000 < stTime.tv_nsec)
                {
                    stTime.tv_sec += 1;
                    stTime.tv_nsec -= 1000000000;
                }
            }
            /** 开始等待 */
            iRet = pthread_cond_timedwait(pstCond, (pthread_mutex_t*)pstMutexLock, &stTime);
            if (ADM_OS_ECODE_SUCCESSED != iRet)
            {
                iRet = ADM_OS_ECODE_PTHREAD_COND_WAIT_FAILED;
                break;
            }
        }
    } while(FALSE);

    return iRet;
}

/**
 * @brief 条件改变信号通知函数
 * @note 在调用此接口的前后，需要加上对互斥锁的上锁和解锁，并在上锁的时候处理数据
 * 
 * @param condHandle   [in]    条件变量句柄
 * @return D32S     信号通知的结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_PTHREAD_COND_SIGNAL_FAILED    @see adm_com_errorCode.h
 */
D32S adm_com_threadCondSignal(DHANDLE condHandle)
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    pthread_cond_t* pstCond = (pthread_cond_t*)condHandle;

    if (NULL == pstCond)
    {
        iRet = ADM_OS_ECODE_INVALID_PARAM;
        return iRet;
    }

    do {
        iRet = pthread_cond_signal(pstCond);
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            iRet = ADM_OS_ECODE_PTHREAD_COND_SIGNAL_FAILED;
            break;
        }
    } while(FALSE);

    return iRet;
}

/**
 * @brief 条件变量改变，信号广播函数
 * 
 * @param condHandle    [in]    条件变量句柄
 * @return D32S     信号广播的结果
 * @retval ADM_OS_ECODE_INVALID_PARAM
 * @retval ADM_OS_ECODE_FAILED
 * @retval ADM_OS_ECODE_PTHREAD_COND_BROAD_CAST_FAILED
 * @retval ADM_OS_ECODE_SUCCESSED
 */
D32S adm_com_threadCondBroadcast(DHANDLE condHandle)
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    pthread_cond_t* pstCond = (pthread_cond_t*)condHandle;

    if (NULL == pstCond)
    {
        iRet = ADM_OS_ECODE_INVALID_PARAM;
        return iRet;
    }

    do {
        iRet = pthread_cond_broadcast(pstCond);
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            iRet = ADM_OS_ECODE_PTHREAD_COND_BROAD_CAST_FAILED;
            break;
        }
    } while(FALSE);

    return iRet;
}

/**
 * @brief 创建信号量，并初始化数值
 * 
 * @param semHandle     [out]   信号量变量
 * @param iPshared      [in]    0:用于线程间，非0：用于进程间
 * @param iVal          [in]    信号量的初始值
 * @return D32S         创建信号量的结果
 * @retval  ADM_OS_ECODE_FAILED                @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_APPLY_MEMORY_FAILED   @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SEM_INIT_FAILED       @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED             @see adm_com_errorCode.h
 */
D32S adm_com_threadSemCreate(DHANDLE* semHandle, D32S iPshared, D32S iVal)
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    sem_t* pstSem = NULL;

    do {
        *semHandle = -1;
        pstSem = (sem_t*)adm_malloc(sizeof(sem_t));
        if (ADM_IS_NULL(pstSem))
        {
            iRet = ADM_OS_ECODE_APPLY_MEMORY_FAILED;
            break;
        }

        iRet = sem_init(pstSem, iPshared, iVal);
        if (ADM_IS_ERR(iRet))
        {
            iRet = ADM_OS_ECODE_SEM_INIT_FAILED;
            break;
        }
        *semHandle = (DHANDLE)pstSem;
    } while(FALSE);

    return iRet;
}
/**
 * @brief 信号量P操作，信号量阻塞等待接口
 * 
 * @param semHandle     [in]    信号量句柄
 * @return D32S         阻塞等待的结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SEM_WAIT_FAILED   @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 */
D32S adm_com_threadSemWait(DHANDLE semHandle)
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    sem_t* pstSem = (sem_t*)semHandle;

    do {
        if (ADM_IS_NULL(pstSem))
        {
            iRet = ADM_OS_ECODE_INVALID_PARAM;
            break;
        }

        iRet = sem_wait(pstSem);
        if (ADM_IS_ERR(iRet))
        {
            iRet = ADM_OS_ECODE_SEM_WAIT_FAILED;
            break;
        }
    } while(FALSE);

    return iRet;
}
/**
 * @brief 信号量P操作，信号量非阻塞接口
 * 
 * @param semHandle     [in]    信号量句柄
 * @return D32S         结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SEM_TRY_WAIT_FAILED   @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 */
D32S adm_com_threadSemTryWait(DHANDLE semHandle)
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    sem_t* pstSem = (sem_t*)semHandle;

    do {
        if (ADM_IS_NULL(pstSem))
        {
            iRet = ADM_OS_ECODE_INVALID_PARAM;
            break;
        }

        iRet = sem_trywait(pstSem);
        if (ADM_IS_ERR(iRet))
        {
            iRet = ADM_OS_ECODE_SEM_TRY_WAIT_FAILED;
            break;
        }
    } while(FALSE);

    return iRet;
}

// 获取自系统启动的调单递增的时间
inline uint64_t GetTimeConvSeconds( timespec* curTime, uint32_t factor )
{
    // CLOCK_MONOTONIC：从系统启动这一刻起开始计时,不受系统时间被用户改变的影响
    clock_gettime( CLOCK_MONOTONIC, curTime );
    return static_cast<uint64_t>(curTime->tv_sec) * factor;
}

// 获取自系统启动的调单递增的时间 -- 转换单位为微秒
uint64_t GetMonnotonicTime()
{
    timespec curTime;
    uint64_t result = GetTimeConvSeconds( &curTime, 1000000 );
    result += static_cast<uint32_t>(curTime.tv_nsec) / 1000;
    return result;
}

/**
 * @brief 信号量P操作，有时长限制的阻塞等待接口
 * 
 * @param semHandle     [in]    信号量句柄
 * @param ulAbsTime     [in]    超时时间，单位：ms
 * @return D32S         阻塞等待的结果
 * @retval  ADM_OS_ECODE_FAILED                    @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM             @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SEM_TIMED_WAIT_FAILED     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SEM_WAIT_FAILED           @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED                 @see adm_com_errorCode.h
 */
D32S adm_com_threadSemTimedWait(DHANDLE semHandle, size_t ulAbsTime)
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    sem_t* pstSem = (sem_t*)semHandle;
    struct timespec stTime;
    const size_t timeoutUs = ulAbsTime * 1000; // 延时时间由毫米转换为微秒
    const size_t maxTimeWait = 100000; // 最大的睡眠的时间为10000微秒，也就是100毫秒

    size_t timeWait = 1; // 睡眠时间，默认为1微秒
    size_t delayUs = 0; // 剩余需要延时睡眠时间

    const uint64_t startUs = GetMonnotonicTime(); // 循环前的开始时间，单位微秒
    uint64_t elapsedUs = 0; // 过期时间，单位微秒

    if (ADM_IS_NULL(pstSem) || 0 >= ulAbsTime)
    {
        iRet = ADM_OS_ECODE_INVALID_PARAM;
        return iRet;
    }

    do
    {
        // 如果信号量大于0，则减少信号量并立马返回true
        if(0 == sem_trywait(pstSem))
        {
            iRet = ADM_OS_ECODE_SUCCESSED;
            return iRet;
        }

        // 系统信号则立马返回false
        if( errno != EAGAIN )
        {
            return false;
        }

        // delayUs一定是大于等于0的，因为do-while的条件是elapsedUs <= timeoutUs.
        delayUs = timeoutUs - elapsedUs;

        // 睡眠时间取最小的值
        timeWait = std::min( delayUs, timeWait );

        // 进行睡眠 单位是微秒
        iRet = usleep( timeWait );
        if(ADM_OS_ECODE_SUCCESSED != iRet) 
        {
            return iRet;
        }

        // 睡眠延时时间双倍自增
        timeWait *= 2;

        // 睡眠延时时间不能超过最大值
        timeWait = std::min( timeWait, maxTimeWait );

        // 计算开始时间到现在的运行时间 单位是微秒
        elapsedUs = GetMonnotonicTime() - startUs;
    } while( elapsedUs <= timeoutUs ); // 如果当前循环的时间超过预设延时时间则退出循环

    return iRet;
}
/**
 * @brief 信号量V操作
 * 
 * @param semHandle     [in]    信号量句柄
 * @return D32S         信号量V操作的结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SEM_POST_FAILED   @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 */
D32S adm_com_threadSemPost(DHANDLE semHandle)
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    sem_t* pstSem = (sem_t*)semHandle;

    do {
        if (ADM_IS_NULL(pstSem))
        {
            iRet = ADM_OS_ECODE_INVALID_PARAM;
            break;
        }

        iRet = sem_post(pstSem);
        if (ADM_IS_ERR(iRet))
        {
            iRet = ADM_OS_ECODE_SEM_POST_FAILED;
            break;
        }
    } while(FALSE);

    return iRet;
}
/**
 * @brief 信号量销毁
 * 
 * @param semHandle     [in]    信号量句柄 
 */
void adm_com_threadSemDestroy(DHANDLE semHandle)
{
    sem_t* pstSem = (sem_t*)semHandle;

    if (ADM_IS_NULL(pstSem))
    {
        return;
    }

    (void)sem_destroy(pstSem);
    adm_free(pstSem);

    return;
}


/**
 * @brief 初始化互斥锁
 * 
 * @param mutexHandle   [out]       互斥锁句柄
 * @return D32S         初始化互斥锁句柄
 * @retval  ADM_OS_ECODE_FAILED        @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED     @see adm_com_errorCode.h
 */
D32S adm_com_threadMutexCreate(DHANDLE* mutexHandle)
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    pthread_mutex_t* pstMutexLock = NULL;
    pthread_mutexattr_t mutexattr = {0};

    do {
        pstMutexLock = (pthread_mutex_t*)adm_malloc(sizeof(pthread_mutex_t));
        if (ADM_IS_NULL(pstMutexLock))
        {
            iRet = ADM_OS_ECODE_APPLY_MEMORY_FAILED;
            break;
        }

        //!< 参数PTHREAD_MUTEX_RECURSIVE表示在同一个线程中可以多次获取同一把锁。并且不会死锁
        pthread_mutexattr_init(&mutexattr);
        pthread_mutexattr_settype(&mutexattr, PTHREAD_MUTEX_RECURSIVE);
        iRet = pthread_mutex_init(pstMutexLock, &mutexattr);
        if (ADM_IS_SUCCESS(iRet))
        {
            *mutexHandle = (DHANDLE)pstMutexLock;
        }
        pthread_mutexattr_destroy(&mutexattr);

    } while(FALSE);
    
    return iRet;
}

/**
 * @brief 加锁操作
 * @note 此接口会默认调用互斥锁初始化接口
 * 
 * @param mutexHandle   [in]    锁的句柄
 * @return D32S         加锁的结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 */
D32S adm_com_threadMutexLock(DHANDLE mutexHandle)
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    pthread_mutex_t* pstMutexLock = (pthread_mutex_t*)mutexHandle;

    do {
        if (NULL == pstMutexLock)
        {
            iRet = ADM_OS_ECODE_INVALID_PARAM;
            return iRet;
        }

        iRet = pthread_mutex_lock((pthread_mutex_t*)pstMutexLock);
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            break;
        }

    } while(FALSE);

    return iRet;
}
/**
 * @brief 尝试加锁的操作
 * 
 * @param mutexHandle   [in]    锁的句柄
 * @return D32S         尝试加锁的结果
 */
D32S adm_com_threadMutexTryLock(DHANDLE mutexHandle)
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    pthread_mutex_t* pstMutexLock = (pthread_mutex_t*)mutexHandle;

    do {
        if (NULL == pstMutexLock)
        {
            iRet = ADM_OS_ECODE_INVALID_PARAM;
            return iRet;
        }

        iRet = pthread_mutex_trylock(pstMutexLock);
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            break;
        }
    } while(FALSE);

    return iRet;
}
/**
 * @brief 解锁操作
 * 
 * @param mutexHandle   [in]    锁的句柄
 * @return D32S         解锁的结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 */
D32S adm_com_threadMutexUnlock(DHANDLE mutexHandle)
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    pthread_mutex_t* pstMutexLock = (pthread_mutex_t*)mutexHandle;

    do {
        if (NULL == pstMutexLock)
        {
            iRet = ADM_OS_ECODE_INVALID_PARAM;
            return iRet;
        }

        iRet = pthread_mutex_unlock((pthread_mutex_t*)pstMutexLock);
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            break;
        }

    } while(FALSE);

    return iRet;
}

/**
 * @brief 释放锁
 * 
 * @param mutexHandle   [in]    锁的句柄
 */
void adm_com_threadMutexDestroy(DHANDLE mutexHandle)
{
    pthread_mutex_t* pstMutexLock = (pthread_mutex_t*)mutexHandle;

    do {
        if (NULL == pstMutexLock)
        {
            break;
        }

        (void)pthread_mutex_destroy(pstMutexLock);
        adm_free(pstMutexLock);
    } while(FALSE);

    return;
}


/**
 * @brief 初始化读写锁
 * 
 * @param pstRwLock   [out]       读写锁句柄
 * @return D32S         初始化读写锁句柄
 * @retval  ADM_OS_ECODE_FAILED        @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED     @see adm_com_errorCode.h
 */
D32S adm_com_threadRWLockCreate(DHANDLE* rwLockHandle)
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    pthread_rwlock_t* pstReadWriteLock = NULL;

    do {
        pstReadWriteLock = (pthread_rwlock_t*)adm_malloc(sizeof(pthread_rwlock_t));
        if (NULL == pstReadWriteLock)
        {
            iRet = ADM_OS_ECODE_INVALID_PARAM;
            return iRet;
        }

        iRet = pthread_rwlock_init(pstReadWriteLock, NULL);
        if (ADM_OS_ECODE_SUCCESSED == iRet)
        {
            *rwLockHandle = (DHANDLE)pstReadWriteLock;
        }
    } while(FALSE);
    
    return iRet;
}
/**
 * @brief 写加锁操作
 * @note 此接口会默认调用读写锁初始化接口
 * 
 * @param pstRwLock   [in]    读写锁的句柄
 * @return D32S         写加锁的结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 */
D32S adm_com_threadRWLock_wrlock(DHANDLE rwLockHandle)
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    pthread_rwlock_t* pstReadWriteLock = (pthread_rwlock_t*)rwLockHandle;

    do {
        if (NULL == pstReadWriteLock)
        {
            iRet = ADM_OS_ECODE_INVALID_PARAM;
            return iRet;
        }

        iRet = pthread_rwlock_wrlock((pthread_rwlock_t*)pstReadWriteLock);
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            break;
        }

    } while(FALSE);

    return iRet;
}
/**
 * @brief 读加锁操作
 * @note 此接口会默认调用读写锁初始化接口
 * 
 * @param rwLockHandle   [in]    读写锁的句柄
 * @return D32S         读加锁的结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 */
D32S adm_com_threadRWLock_rdlock(DHANDLE rwLockHandle)
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    pthread_rwlock_t* pstReadWriteLock = (pthread_rwlock_t*)rwLockHandle;

    do {
        if (NULL == pstReadWriteLock)
        {
            iRet = ADM_OS_ECODE_INVALID_PARAM;
            return iRet;
        }

        iRet = pthread_rwlock_rdlock((pthread_rwlock_t*)pstReadWriteLock);
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            break;
        }

    } while(FALSE);

    return iRet;
}
/**
 * @brief 读写解锁操作
 * 
 * @param rwLockHandle   [in]    读写锁的句柄
 * @return D32S         读写解锁的结果
 * @retval  ADM_OS_ECODE_FAILED            @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_INVALID_PARAM     @see adm_com_errorCode.h
 * @retval  ADM_OS_ECODE_SUCCESSED         @see adm_com_errorCode.h
 */
D32S adm_com_threadRWUnLock(DHANDLE rwLockHandle)
{
    D32S iRet = ADM_OS_ECODE_FAILED;
    pthread_rwlock_t* pstReadWriteLock = (pthread_rwlock_t*)rwLockHandle;

    do {
        if (NULL == pstReadWriteLock)
        {
            iRet = ADM_OS_ECODE_INVALID_PARAM;
            return iRet;
        }

        iRet = pthread_rwlock_unlock((pthread_rwlock_t*)pstReadWriteLock);
        if (ADM_OS_ECODE_SUCCESSED != iRet)
        {
            break;
        }

    } while(FALSE);

    return iRet;
}
/**
 * @brief 销毁读写锁
 * 
 * @param rwLockHandle   [in]    读写锁的句柄
 */
void adm_com_threadRWLockDestroy(DHANDLE rwLockHandle)
{
    pthread_rwlock_t* pstReadWriteLock = (pthread_rwlock_t*)rwLockHandle;

    do {
        if (NULL == pstReadWriteLock)
        {
            break;
        }

        (void)pthread_rwlock_destroy((pthread_rwlock_t*)pstReadWriteLock);
        adm_free(pstReadWriteLock);
    } while(FALSE);

    return;
}
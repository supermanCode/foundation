#!/bin/sh
prefix_dir=${1}

cd $(dirname $0) && CFLAGS='-fPIC ' ./configure  --prefix=${prefix_dir}  --static

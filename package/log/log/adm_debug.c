#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include "adm_debug.h"
#include "elog.h"
#include "elog_file.h"


bool adm_debug_isDirExist(const char* pDirPath)
{
    if (NULL == pDirPath) {
        log_e( "Parameter verification failed");
        return false;
    }

    if (access(pDirPath, F_OK) != 0) {
        return false;
    }

    return true;
}

/**
 * @brief 日志文件存储目录初始化
 * 
 * @param logDir        [IN]    const char*     日志文件的存储目录
 * @param max_size      [IN]    const size_t    单个日志文件存储的大小
 * @param max_rotate    [IN]    const int       最多存储的文件个数
 */
void adm_debug_logInit(const char* logDir, const size_t max_size, const int max_rotate)
{
    char* formatDirStr = NULL;
    /* close printf buffer */
    setbuf(stdout, NULL);
    /* initialize EasyLogger */
    elog_init();
    
#ifdef ELOG_FILE_ENABLE
    if (NULL == logDir) {
        formatDirStr = ADM_DEBUG_DEFAULT_DIR;
    } else {
        formatDirStr = (char*)logDir;
    }
    elog_file_init(formatDirStr, ADM_DEBUG_FILE_NAME, max_size, max_rotate);
#endif
    /* set EasyLogger log format */
    elog_set_fmt(ELOG_LVL_ASSERT, ELOG_FMT_ALL & ~ELOG_FMT_P_INFO);
    elog_set_fmt(ELOG_LVL_ERROR, ELOG_FMT_ALL & ~ELOG_FMT_P_INFO);
    elog_set_fmt(ELOG_LVL_WARN, ELOG_FMT_ALL & ~ELOG_FMT_P_INFO);
    elog_set_fmt(ELOG_LVL_INFO, ELOG_FMT_ALL & ~ELOG_FMT_P_INFO);
    elog_set_fmt(ELOG_LVL_DEBUG, ELOG_FMT_ALL & ~ELOG_FMT_P_INFO);
    elog_set_fmt(ELOG_LVL_VERBOSE, ELOG_FMT_ALL & ~ELOG_FMT_P_INFO);
    // elog_set_fmt(ELOG_LVL_ERROR, ELOG_FMT_LVL | ELOG_FMT_TAG | ELOG_FMT_TIME);
    // elog_set_fmt(ELOG_LVL_WARN, ELOG_FMT_LVL | ELOG_FMT_TAG | ELOG_FMT_TIME);
    // elog_set_fmt(ELOG_LVL_INFO, ELOG_FMT_LVL | ELOG_FMT_TAG | ELOG_FMT_TIME);
    // elog_set_fmt(ELOG_LVL_DEBUG, ELOG_FMT_ALL & ~ELOG_FMT_FUNC);
    // elog_set_fmt(ELOG_LVL_VERBOSE, ELOG_FMT_ALL & ~ELOG_FMT_FUNC);

    elog_set_filter_lvl(ELOG_LVL_DEBUG);
#ifdef ELOG_COLOR_ENABLE
    elog_set_text_color_enabled(true);
#endif

    elog_start();
    return;
}

/**
 * @brief 日志文件存储目录和日志标识初始化
 * @note 1. 日志文件的存储目录必须存在
 *       2. 日志文件格式： logFileName_timeStr.log/logFileName_timeStr.log.0/logFileName_timeStr.log.1
 *                        admfotaapp_20220425_141308.log
 * 
 * @param logDir        [IN]    const char*     日志文件的存储目录
 * @param logFileName   [IN]    const char*     日志文件的存储文件名
 * @param max_size      [IN]    const size_t    单个日志文件存储的大小
 * @param max_rotate    [IN]    const int       最多存储的文件个数
 * @param bOutputTerminal    [IN]    bool  串口终端输出Log使能标志
 */
void adm_debug_logInitWithFileName(const char* logDir, const char* logFileName, const size_t max_size, const int max_rotate, const unsigned char loglevel, bool bOutputTerminal)
{
    char* formatNameStr = NULL;
    char* formatDirStr = NULL;

    /* close printf buffer */
    setbuf(stdout, NULL);

	elog_set_terminal_flag(bOutputTerminal);
    /* initialize EasyLogger */
    elog_init();
#ifdef ELOG_FILE_ENABLE
    if (NULL == logDir) {
        formatDirStr = ADM_DEBUG_DEFAULT_DIR;
    } else {
        formatDirStr = (char*)logDir;
    }
    if (NULL == logFileName) {
        formatNameStr  = ADM_DEBUG_FILE_NAME;
    } else {
        formatNameStr = (char*)logFileName;
    }
    elog_file_init(formatDirStr, formatNameStr, max_size, max_rotate);
#endif

    /* set EasyLogger log format */
    elog_set_fmt(ELOG_LVL_ASSERT, ELOG_FMT_ALL & ~ELOG_FMT_P_INFO);
    elog_set_fmt(ELOG_LVL_ERROR, ELOG_FMT_ALL & ~ELOG_FMT_P_INFO);
    elog_set_fmt(ELOG_LVL_WARN, ELOG_FMT_ALL & ~ELOG_FMT_P_INFO);
    elog_set_fmt(ELOG_LVL_INFO, ELOG_FMT_ALL & ~ELOG_FMT_P_INFO);
    elog_set_fmt(ELOG_LVL_DEBUG, ELOG_FMT_ALL & ~ELOG_FMT_P_INFO);
    elog_set_fmt(ELOG_LVL_VERBOSE, ELOG_FMT_ALL & ~ELOG_FMT_P_INFO);
    // elog_set_fmt(ELOG_LVL_ERROR, ELOG_FMT_LVL | ELOG_FMT_TAG | ELOG_FMT_TIME);
    // elog_set_fmt(ELOG_LVL_WARN, ELOG_FMT_LVL | ELOG_FMT_TAG | ELOG_FMT_TIME);
    // elog_set_fmt(ELOG_LVL_INFO, ELOG_FMT_LVL | ELOG_FMT_TAG | ELOG_FMT_TIME);
    // elog_set_fmt(ELOG_LVL_DEBUG, ELOG_FMT_ALL & ~ELOG_FMT_FUNC);
    // elog_set_fmt(ELOG_LVL_VERBOSE, ELOG_FMT_ALL & ~ELOG_FMT_FUNC);
    elog_set_filter_lvl(loglevel);
#ifdef ELOG_COLOR_ENABLE
    elog_set_text_color_enabled(true);
#endif

    elog_start();
    return;
}


void adm_initLog(void)
{
    /* close printf buffer */
    setbuf(stdout, NULL);
    /* initialize EasyLogger */
    elog_init();
    /* set EasyLogger log format */
    elog_set_fmt(ELOG_LVL_ASSERT, ELOG_FMT_ALL);
    elog_set_fmt(ELOG_LVL_ERROR, ELOG_FMT_ALL_NO_DIR);
    elog_set_fmt(ELOG_LVL_WARN, ELOG_FMT_ALL_NO_DIR);
    elog_set_fmt(ELOG_LVL_INFO, ELOG_FMT_ALL_NO_DIR);
    elog_set_fmt(ELOG_LVL_DEBUG, ELOG_FMT_ALL_NO_DIR);
    elog_set_fmt(ELOG_LVL_VERBOSE, ELOG_FMT_ALL_NO_DIR);
    // elog_set_fmt(ELOG_LVL_ERROR, ELOG_FMT_LVL | ELOG_FMT_TAG | ELOG_FMT_TIME);
    // elog_set_fmt(ELOG_LVL_WARN, ELOG_FMT_LVL | ELOG_FMT_TAG | ELOG_FMT_TIME);
    // elog_set_fmt(ELOG_LVL_INFO, ELOG_FMT_LVL | ELOG_FMT_TAG | ELOG_FMT_TIME);
    // elog_set_fmt(ELOG_LVL_DEBUG, ELOG_FMT_ALL & ~ELOG_FMT_FUNC);
    // elog_set_fmt(ELOG_LVL_VERBOSE, ELOG_FMT_ALL & ~ELOG_FMT_FUNC);
#ifdef ELOG_COLOR_ENABLE
    elog_set_text_color_enabled(true);
#endif

    elog_start();
    return;
}

void adm_deinitLog(void)
{
    return;
}

void adm_debug_logDeinit(void)
{
    elog_set_output_enabled(false);
    elog_file_deinit();    
    return;
}

LOG_TRACER_OUTPUT_CALLBACK_F tracer_output = NULL;

void adm_logTracerRegister(LOG_TRACER_OUTPUT_CALLBACK_F logTracerHandler)
{
    tracer_output = logTracerHandler;
}


/*
 * This file is part of the EasyLogger Library.
 *
 * Copyright (c) 2015-2019, Qintl, <qintl_linux@163.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * 'Software'), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Function: Save log to file.
 * Created on: 2019-01-05
 */

 #define LOG_TAG    "elog.file"

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <dirent.h>

#include "elog_file.h"

/* initialize OK flag */
static bool abup_init_ok = false;
static FILE *abup_fp = NULL;
static ElogFileCfg abup_local_cfg;

/**
 * @brief 删除指定目录下最旧的文件
 * @note 按照文件命名规范，找到最小的字符串，然后删除
 * 
 * @param logDir [IN]   const char*     日志存储目录
 * @param logname [IN]  const char*     日志存储的名字
 */
static void elog_file_removeOldFile(const char* logDir, const char* logname);
/**
 * @brief 获取指定目录下的日志文件个数
 * 
 * @param logDir    [IN]    日志文件目录
 * @param logname [IN]  const char*     日志存储的名字
 * 
 * @return int      日志文件个数
 */
static int elog_file_getfilenum(const char* logDir, const char* logname);
/**
 * @brief 格式化文件名
 * @note 1. 日志文件格式： logFileName_timeStr.log/logFileName_timeStr.log.0/logFileName_timeStr.log.1
 *                        admfotaapp_20220425_141308.log
 * 
 * @param logFileName   [IN]    const char*     日志文件的存储文件名
 * 
 */
static char* elog_file_nameFormat(const char* logFilename);
/**
 * @brief 日志文件输出全路径生成函数
 * @note 1. 日志文件格式： logFileName_timeStr.log/logFileName_timeStr.log.0/logFileName_timeStr.log.1
 *                        admfotaapp_20220425_141308.log
 * 
 * @param logDir        [IN]    const char*     日志文件的存储路径
 * @param logFileName   [IN]    const char*     日志文件的存储文件名
 * 
 * @return 返回日志存储的路径
 * 
 */
static char* elog_file_fullpathFormat(const char* logDir, const char* logFilename);

/**
 * @brief 拼接日志文件全路径
 * 
 * @param logDir        [IN]    const char*     日志文件的存储路径
 * @param logFileName   [IN]    const char*     日志文件的存储文件名
 * 
 * @return 返回日志存储的路径
 */
static char* elog_file_fullpathSplicing(const char* logDir, const char* logFilename);


ElogErrCode elog_file_init(char* dir, char* name, size_t max_size, int max_rotate)
{
    ElogErrCode result = ELOG_NO_ERR;
    ElogFileCfg cfg;

    if (abup_init_ok)
        goto __exit;

    elog_file_port_init();
    if (NULL == dir) {
        cfg.dir = ELOG_DIR_NAME;
    } else {
        cfg.dir = dir;
    }

    if (NULL == name) {
        cfg.name = ELOG_FILE_NAME;
    } else {
        cfg.name = name;
    }

    if (max_size <= 0) {
        cfg.max_size = ELOG_FILE_MAX_SIZE;
    } else {
        cfg.max_size = max_size;
    }

    if (max_rotate <= 0) {
        cfg.max_rotate = ELOG_FILE_MAX_ROTATE;
    } else {
        cfg.max_rotate = max_rotate;
    }

    elog_file_config(&cfg);

    abup_init_ok = true;
__exit:
    return result;
}

// ElogErrCode elog_file_init(void)
// {
//     ElogErrCode result = ELOG_NO_ERR;
//     ElogFileCfg cfg;

//     if (abup_init_ok)
//         goto __exit;

//     elog_file_port_init();

//     cfg.name = ELOG_FILE_NAME;
//     cfg.max_size = ELOG_FILE_MAX_SIZE;
//     cfg.max_rotate = ELOG_FILE_MAX_ROTATE;

//     elog_file_config(&cfg);

//     abup_init_ok = true;
// __exit:
//     return result;
// }

/*
 * rotate the log file xxx.log.n-1 => xxx.log.n, and xxx.log => xxx.log.0
 */
static bool elog_file_rotate(void)
{
#if 1
    /* mv abup_local_cfg.dir/abup_local_cfg.name => abup_local_cfg.dir/abup_local_cfg.name_timestring.log */
    int err = 0;
    char oldpath[256] = {0};
    char newpath[256] = {0};
    bool result = true;
    int max_rotate = 0;
    int file_num = 0;
    int i = 0;

    snprintf(oldpath, sizeof(oldpath), "%s", elog_file_fullpathSplicing(abup_local_cfg.dir, abup_local_cfg.name));
    snprintf(newpath, sizeof(newpath), "%s", elog_file_fullpathFormat(abup_local_cfg.dir, abup_local_cfg.name));

    if (NULL != abup_fp) {
        fclose(abup_fp);
    }

    //!< 判断如果当前目录下的日志文件个数，大于或等于用户配置的最大文件个数，需要删除最旧的文件
    max_rotate = abup_local_cfg.max_rotate + 1;
    file_num = elog_file_getfilenum(abup_local_cfg.dir, abup_local_cfg.name);
    if (file_num > max_rotate) {
        for (i = 0; i <= (file_num - max_rotate); i++) {
            elog_file_removeOldFile(abup_local_cfg.dir, abup_local_cfg.name);
        }
    }

    // if ((abup_local_cfg.max_rotate + 1) < elog_file_getfilenum(abup_local_cfg.dir, abup_local_cfg.name)) {
    //     elog_file_removeOldFile(abup_local_cfg.dir, abup_local_cfg.name);
    // }

    err = rename(oldpath, newpath);
    if (err < 0) {
        result = false;
    }

    /* reopen the file */
    abup_fp = fopen(oldpath, "a+");

    return result;
#else
#define SUFFIX_LEN                     10
    /* mv xxx.log.n-1 => xxx.log.n, and xxx.log => xxx.log.0 */
    int n, err = 0;
    char oldpath[256], newpath[256];
    size_t base = strlen(abup_local_cfg.name);
    bool result = true;
    FILE *tmp_fp;

    memcpy(oldpath, abup_local_cfg.name, base);
    memcpy(newpath, abup_local_cfg.name, base);

    if (NULL != fp) {
        fclose(fp);
    }

    for (n = abup_local_cfg.max_rotate - 1; n >= 0; --n) {
        snprintf(oldpath + base, SUFFIX_LEN, n ? ".%d" : "", n - 1);
        snprintf(newpath + base, SUFFIX_LEN, ".%d", n);
        /* remove the old file */
        if ((tmp_fp = fopen(newpath , "r")) != NULL) {
            fclose(tmp_fp);
            remove(newpath);
        }
        /* change the new log file to old file name */
        if ((tmp_fp = fopen(oldpath , "r")) != NULL) {
            fclose(tmp_fp);
            err = rename(oldpath, newpath);
        }

        if (err < 0) {
            result = false;
            goto __exit;
        }
    }

__exit:
    /* reopen the file */
    fp = fopen(elog_file_fullpathSplicing(abup_local_cfg.dir, abup_local_cfg.name), "a+");

    return result;
#endif
}


void elog_file_write(const char *log, size_t size)
{
    size_t file_size = 0;

    ELOG_ASSERT(abup_init_ok);
    ELOG_ASSERT(log);

    elog_file_port_lock();

    if (NULL != abup_fp) {
        fseek(abup_fp, 0L, SEEK_END);
        file_size = ftell(abup_fp);
    }

    if (unlikely(file_size > abup_local_cfg.max_size)) {

        if (abup_local_cfg.max_rotate > 0) {
            if (!elog_file_rotate()) {
                goto __exit;
            }
        } else {
            goto __exit;

        }
// #if ELOG_FILE_MAX_ROTATE > 0
//         if (!elog_file_rotate()) {
//             goto __exit;
//         }
// #else
//         goto __exit;
// #endif
    }

    if (NULL != abup_fp) {
        fwrite(log, size, 1, abup_fp);
    }

#ifdef ELOG_FILE_FLUSH_CAHCE_ENABLE
    if (NULL != abup_fp) {
        fflush(abup_fp);
    }
#endif

#ifdef ELOG_FILE_FSYNC_FLASH_ENABLE
    if (NULL != abup_fp) {
        int fd = fileno(abup_fp);
        fsync(fd);
    }
#endif

__exit:
    elog_file_port_unlock();
}

void elog_file_deinit(void)
{
    ELOG_ASSERT(abup_init_ok);

    elog_file_port_deinit();
    elog_file_port_lock();
    if (NULL != abup_local_cfg.dir)
    {
        free(abup_local_cfg.dir);
    }
    if (NULL != abup_local_cfg.name)
    {
        free(abup_local_cfg.name);
    }
    if (abup_fp) {
        fclose(abup_fp);
    }
    elog_file_port_unlock();
}

void elog_file_config(ElogFileCfg *cfg)
{
    if (abup_fp) {
        fclose(abup_fp);
    }

    elog_file_port_lock();

    if (NULL != abup_local_cfg.dir) {
        free(abup_local_cfg.dir);
    }
    abup_local_cfg.dir = strdup(cfg->dir);

    if (NULL != abup_local_cfg.name) {
        free(abup_local_cfg.name);
    }
    abup_local_cfg.name = strdup(cfg->name);
    abup_local_cfg.max_size = cfg->max_size;
    abup_local_cfg.max_rotate = cfg->max_rotate;

    abup_fp = fopen(elog_file_fullpathSplicing(abup_local_cfg.dir, abup_local_cfg.name), "a+");

    elog_file_port_unlock();
}

static char* elog_file_timeStr()
{
    static char timeStr[16] = {0};
    time_t timep;
    struct tm *p;
    struct timeval tv;

    time(&timep);
    p = localtime(&timep);
    gettimeofday(&tv, NULL);
    if (p == NULL) {
        return "";
    }
    snprintf(timeStr, sizeof(timeStr), "%04d%02d%02d_%02d%02d%02d", 1900 + p->tm_year, p->tm_mon + 1, p->tm_mday,
            p->tm_hour, p->tm_min, p->tm_sec);
            
    return timeStr;
}

/**
 * @brief 格式化文件名
 * @note 1. 日志文件格式： logFileName_timeStr.log/logFileName_timeStr.log.0/logFileName_timeStr.log.1
 *                        admfotaapp_20220425_141308.log
 * 
 * @param logFileName   [IN]    const char*     日志文件的存储文件名
 * 
 * @return 返回日志存储的格式化后的文件名
 */
static char* elog_file_nameFormat(const char* logFilename)
{
    static char formatStr[32] = {0};
    
    snprintf(formatStr, sizeof(formatStr), "%s_%s.log", logFilename, elog_file_timeStr());

    return formatStr;
}


/**
 * @brief 日志文件输出全路径生成函数
 * @note 1. 日志文件格式： logFileName_timeStr.log/logFileName_timeStr.log.0/logFileName_timeStr.log.1
 *                        admfotaapp_20220425_141308.log
 * 
 * @param logDir        [IN]    const char*     日志文件的存储路径
 * @param logFileName   [IN]    const char*     日志文件的存储文件名
 * 
 * @return 返回日志存储的路径
 * 
 */
static char* elog_file_fullpathFormat(const char* logDir, const char* logFilename)
{
    static char formatFullPath[256] = {0};
    size_t len = 0;

    len = strlen(logDir);
    
    if ('/' == logDir[len - 1]) {
        snprintf(formatFullPath, sizeof(formatFullPath), "%s%s", logDir, elog_file_nameFormat(logFilename));
    } else {
        snprintf(formatFullPath, sizeof(formatFullPath), "%s/%s", logDir, elog_file_nameFormat(logFilename));
    }
    
    return formatFullPath;
}

/**
 * @brief 拼接日志文件全路径
 * 
 * @param logDir        [IN]    const char*     日志文件的存储路径
 * @param logFileName   [IN]    const char*     日志文件的存储文件名
 * 
 * @return 返回日志存储的路径
 */
char* elog_file_fullpathSplicing(const char* logDir, const char* logFilename)
{
    static char splicingFullpath[256] = {0};
    size_t len = 0;

    len = strlen(logDir);
    
    if ('/' == logDir[len - 1]) {
        snprintf(splicingFullpath, sizeof(splicingFullpath), "%s%s.log", logDir, logFilename);
    } else {
        snprintf(splicingFullpath, sizeof(splicingFullpath), "%s/%s.log", logDir, logFilename);
    }
    
    return splicingFullpath;
}

/**
 * @brief 删除指定目录下最旧的文件
 * @note 按照文件命名规范，找到最小的字符串，然后删除
 * 
 * @param logDir [IN]   const char*     日志存储目录
 * @param logname [IN]  const char*     日志存储的名字
 */
static void elog_file_removeOldFile(const char* logDir, const char* logname)
{
    DIR* dir = NULL;
    struct dirent* ptr = NULL;
    char basename[32] = {0};
    char oldname[256] = {0};
    char oldpath[256] = {0};
    size_t len = 0;
    size_t oldpathlen = 0;

    do {
        //!< 判空
        if (NULL == logDir || NULL == logname) {
            printf("Input param logDir || logname is nil\n");
            break;
        }
        //!< 打开目录
        dir = opendir(logDir);
        if (NULL == dir) {
            printf("opendir:%s failed\n", logDir);
            break;
        }

        snprintf(basename, sizeof(basename), "%s.log", logname);
        len = strlen(basename);
        while(NULL != (ptr = readdir(dir))) {
            printf("d_name:%s\n", ptr->d_name);
            // 忽略掉.和..
            if (strcmp(ptr->d_name, ".") == 0 || strcmp(ptr->d_name, "..") == 0)
                continue;

            //!< 判断类型如果不是文件，执行下一个文件类型的类型判断
            if (8 != ptr->d_type) {
                printf("ptr->d_type:%d is not file\n", ptr->d_type);
                continue;
            }

            //!< base name 不参与判断
            if (0 == strncmp(ptr->d_name, basename, len)) {
                printf("ptr->d_name:%s is basepath\n", basename);
                continue;
            }

            //!< 只有在日志文件名相同的情况下才执行此操作
            if (0 != strncmp(ptr->d_name, logname, strlen(logname))) {
                printf("ptr->d_name:%s is not sampe with logname:%s\n", ptr->d_name, logname);
                continue;
            }

            //!< 如果olaname为空，默认将第一个文件填充
            if ('\0' == oldname[0]) {
                snprintf(oldname, sizeof(oldname), "%s", ptr->d_name);
            }

            //!< 通过比较字符串，获取最旧的文件
            if (0 > strcmp(ptr->d_name, oldname)) {
                memset(oldname, 0x00, sizeof(oldname));
                snprintf(oldname, sizeof(oldname), "%s", ptr->d_name);
            }
        }

        //!< remove 最旧的文件
        if (strlen(oldname) > 0) {
            oldpathlen = strlen(logDir);
            if ('/' == logDir[oldpathlen - 1]) {
                snprintf(oldpath, sizeof(oldpath), "%s%s", logDir, oldname);
            } else {
                snprintf(oldpath, sizeof(oldpath), "%s/%s", logDir, oldname);
            }
            printf("remove oldpath:%s\n", oldpath);
            remove(oldpath);
        }
        
        closedir(dir);

    } while(0);

    return;
}

/**
 * @brief 获取指定目录下的日志文件个数
 * 
 * @param logDir    [IN]    日志文件目录
 * @param logname [IN]  const char*     日志存储的名字
 * 
 * @return int      日志文件个数
 */
static int elog_file_getfilenum(const char* logDir, const char* logname)
{
    DIR* dir = NULL;
    struct dirent* ptr = NULL;
    int filenum = 0;

    do {
        if (NULL == logDir || NULL == logname) {
            printf("Input param logDir is nil\n");
            break;
        }
        //!< 打开目录
        dir = opendir(logDir);
        if (NULL == dir) {
            printf("opendir:%s failed\n", logDir);
            break;
        }

        //!< 循环读取目录下的文件
        while(NULL != (ptr = readdir(dir))) {
            // 忽略掉.和..
            if (strcmp(ptr->d_name, ".") == 0 || strcmp(ptr->d_name, "..") == 0)
                continue;

            //!< 判断类型如果不是文件，执行下一个文件类型的类型判断
            if (8 != ptr->d_type) {
                printf("ptr->d_type:%d is not file\n", ptr->d_type);
                continue;
            }

            //!< 过滤 abup_local_cfg.name 开头的文件名才计数
            if (0 == strncmp(logname, ptr->d_name, strlen(logname))) {
                filenum++;
            }
        }

        closedir(dir);

    } while(0);

    return filenum;
}
cmake_minimum_required(VERSION 3.5.0)
project(ELOG)

include_directories(demo/os/linux/easylogger/inc/)
include_directories(easylogger/inc/)
include_directories(easylogger/plugins/file/)


set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} \
    -lm   \
    -Wall \
    -g \
    -O0 \
    -Wno-unused-parameter \
    -std=gnu11\
    -fPIC\
    -Wl,-Bsymbolic \
    ")

set(LOG_C_SOURCES
    easylogger/src/elog.c
    easylogger/src/elog_async.c
    easylogger/src/elog_buf.c
    easylogger/src/elog_utils.c
    easylogger/plugins/file/elog_file.c
    demo/os/linux/easylogger/port/elog_file_port.c
    demo/os/linux/easylogger/port/elog_port.c)

if(CUSTOM_CMAKE_SYSTEM_NAME MATCHES "Android")
	add_definitions(-DELOG_SYSV_SEM_FEATURE_DISABLE)
	add_definitions(-DELOG_ASSERT_FEATURE_DISABLE)
endif()

add_library(
    admlog
    STATIC
    ${LOG_C_SOURCES}
)
target_link_libraries(admlog pthread)

install(TARGETS admlog
    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})

#target_link_libraries(admlog pthread)

#set(Easylogger_C_Files 
#    demo/os/linux/main.c)
##    demo/os/linux/easylogger/port/elog_file_port.c
##    demo/os/linux/easylogger/port/elog_port.c)
#
#add_executable(EasyloggerDemo ${Easylogger_C_Files})
#target_link_libraries(EasyloggerDemo admlog)

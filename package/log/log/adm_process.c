 
#include <stdio.h> 
#include <string.h> 
#include <unistd.h>  
#include <sys/types.h>  
#include <sys/stat.h>  
#include <fcntl.h>  
#include <syslog.h>  
#include <dirent.h>  
  
#define PROC_READ_BUF_SIZE 100  
    
static int process_pidIsExist(char* pidName)
{  
    DIR *dir=NULL;  
    struct dirent *next=NULL;  
    //long* pidList=NULL;  
    int i=0;  
  
    FILE *status;  
    char filename[PROC_READ_BUF_SIZE];  
    char buffer[PROC_READ_BUF_SIZE];  
    char name[PROC_READ_BUF_SIZE];  
   
    dir = opendir("/proc");  
    if (!dir)  
    {  
        printf("Cannot open /proc\n");  
        return 0;  
    }  
       
	//scan 
	while ((next = readdir(dir)) != NULL)   
	{  
		/* Must skip ".." since that is outside /proc */  
		if (strcmp(next->d_name, "..") == 0)  
		{  
			continue;  
		}  

		/* If it isn't a number, we don't want it */  
		if (!isdigit(*next->d_name))  
		{  
			continue;  
		}  

		snprintf(filename, PROC_READ_BUF_SIZE, "/proc/%s/status", next->d_name);  
		if (! (status = fopen(filename, "r")) )   
		{  
			continue;  
		}  

		if (fgets(buffer, PROC_READ_BUF_SIZE-1, status) == NULL)   
		{  
			fclose(status);  
			continue;  
		}  
		fclose(status);  

		//get proc id
		/* Buffer should contain a string like "Name:   binary_name" */  
		sscanf(buffer, "%*s %s", name);  
		if (strcmp(name, pidName) == 0)   
		{  
			return 1;  
		}  
	}  

	return 0;  
}  
 
int adm_processStart(char* pidName, char *systemCmd)  
{    
    if(pidName == NULL)
  		return -1;
  	if(systemCmd == NULL)
  		return -2;
  		
    do  
    {          
        //check if program file is running  
        if (process_pidIsExist(pidName))  
        {  
					return 1; //exist   
        }  
        else  
        {  
            //yet not run, so start it  
            system(systemCmd);
        }  
          
        //sleep
        //sleep(1);  
    }  while (0);
   
    return 0;  
}
  

include (../cmake/ExternalProjectHelper)

if(${CMAKE_BUILD_TYPE} STREQUAL "Debug")
	find_external_project_add(NAME json
	    DEPENDS ""
	    CONFIGURE_COMMANDS -DCMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX} -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
	    EXPORT_LIBRARIES ${CMAKE_INSTALL_PREFIX}/lib/libjson-c.a)
else()
	find_external_project_add(NAME gtest
	    DEPENDS ""
	    CONFIGURE_COMMANDS -DCMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX} -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
	    EXPORT_LIBRARIES ${CMAKE_INSTALL_PREFIX}/lib/libjson-c.a)
endif()

file(REMOVE_RECURSE
  "CMakeFiles/leveldb"
  "CMakeFiles/leveldb-complete"
  "package/leveldb/src/leveldb-stamp/leveldb-build"
  "package/leveldb/src/leveldb-stamp/leveldb-configure"
  "package/leveldb/src/leveldb-stamp/leveldb-download"
  "package/leveldb/src/leveldb-stamp/leveldb-install"
  "package/leveldb/src/leveldb-stamp/leveldb-mkdir"
  "package/leveldb/src/leveldb-stamp/leveldb-patch"
  "package/leveldb/src/leveldb-stamp/leveldb-update"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/leveldb.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()

file(REMOVE_RECURSE
  "CMakeFiles/zlib"
  "CMakeFiles/zlib-complete"
  "package/zlib/src/zlib-stamp/zlib-build"
  "package/zlib/src/zlib-stamp/zlib-configure"
  "package/zlib/src/zlib-stamp/zlib-download"
  "package/zlib/src/zlib-stamp/zlib-install"
  "package/zlib/src/zlib-stamp/zlib-mkdir"
  "package/zlib/src/zlib-stamp/zlib-patch"
  "package/zlib/src/zlib-stamp/zlib-update"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/zlib.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()

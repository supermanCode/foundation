file(REMOVE_RECURSE
  "CMakeFiles/log"
  "CMakeFiles/log-complete"
  "package/log/src/log-stamp/log-build"
  "package/log/src/log-stamp/log-configure"
  "package/log/src/log-stamp/log-download"
  "package/log/src/log-stamp/log-install"
  "package/log/src/log-stamp/log-mkdir"
  "package/log/src/log-stamp/log-patch"
  "package/log/src/log-stamp/log-update"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/log.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()

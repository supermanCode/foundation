file(REMOVE_RECURSE
  "CMakeFiles/iniparser"
  "CMakeFiles/iniparser-complete"
  "package/iniparser/src/iniparser-stamp/iniparser-build"
  "package/iniparser/src/iniparser-stamp/iniparser-configure"
  "package/iniparser/src/iniparser-stamp/iniparser-download"
  "package/iniparser/src/iniparser-stamp/iniparser-install"
  "package/iniparser/src/iniparser-stamp/iniparser-mkdir"
  "package/iniparser/src/iniparser-stamp/iniparser-patch"
  "package/iniparser/src/iniparser-stamp/iniparser-update"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/iniparser.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()

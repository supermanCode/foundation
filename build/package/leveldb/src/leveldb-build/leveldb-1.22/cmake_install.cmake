# Install script for directory: /home/wangyang/superman/project/foundation/package/leveldb/leveldb-1.22

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/wangyang/superman/project/foundation/build/deploy")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/wangyang/superman/project/foundation/build/package/leveldb/src/leveldb-build/leveldb-1.22/libleveldb.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/home/wangyang/superman/project/foundation/build/deploy/include/leveldb/c.h;/home/wangyang/superman/project/foundation/build/deploy/include/leveldb/cache.h;/home/wangyang/superman/project/foundation/build/deploy/include/leveldb/comparator.h;/home/wangyang/superman/project/foundation/build/deploy/include/leveldb/db.h;/home/wangyang/superman/project/foundation/build/deploy/include/leveldb/dumpfile.h;/home/wangyang/superman/project/foundation/build/deploy/include/leveldb/env.h;/home/wangyang/superman/project/foundation/build/deploy/include/leveldb/export.h;/home/wangyang/superman/project/foundation/build/deploy/include/leveldb/filter_policy.h;/home/wangyang/superman/project/foundation/build/deploy/include/leveldb/iterator.h;/home/wangyang/superman/project/foundation/build/deploy/include/leveldb/options.h;/home/wangyang/superman/project/foundation/build/deploy/include/leveldb/slice.h;/home/wangyang/superman/project/foundation/build/deploy/include/leveldb/status.h;/home/wangyang/superman/project/foundation/build/deploy/include/leveldb/table_builder.h;/home/wangyang/superman/project/foundation/build/deploy/include/leveldb/table.h;/home/wangyang/superman/project/foundation/build/deploy/include/leveldb/write_batch.h")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/home/wangyang/superman/project/foundation/build/deploy/include/leveldb" TYPE FILE FILES
    "/home/wangyang/superman/project/foundation/package/leveldb/leveldb-1.22/include/leveldb/c.h"
    "/home/wangyang/superman/project/foundation/package/leveldb/leveldb-1.22/include/leveldb/cache.h"
    "/home/wangyang/superman/project/foundation/package/leveldb/leveldb-1.22/include/leveldb/comparator.h"
    "/home/wangyang/superman/project/foundation/package/leveldb/leveldb-1.22/include/leveldb/db.h"
    "/home/wangyang/superman/project/foundation/package/leveldb/leveldb-1.22/include/leveldb/dumpfile.h"
    "/home/wangyang/superman/project/foundation/package/leveldb/leveldb-1.22/include/leveldb/env.h"
    "/home/wangyang/superman/project/foundation/package/leveldb/leveldb-1.22/include/leveldb/export.h"
    "/home/wangyang/superman/project/foundation/package/leveldb/leveldb-1.22/include/leveldb/filter_policy.h"
    "/home/wangyang/superman/project/foundation/package/leveldb/leveldb-1.22/include/leveldb/iterator.h"
    "/home/wangyang/superman/project/foundation/package/leveldb/leveldb-1.22/include/leveldb/options.h"
    "/home/wangyang/superman/project/foundation/package/leveldb/leveldb-1.22/include/leveldb/slice.h"
    "/home/wangyang/superman/project/foundation/package/leveldb/leveldb-1.22/include/leveldb/status.h"
    "/home/wangyang/superman/project/foundation/package/leveldb/leveldb-1.22/include/leveldb/table_builder.h"
    "/home/wangyang/superman/project/foundation/package/leveldb/leveldb-1.22/include/leveldb/table.h"
    "/home/wangyang/superman/project/foundation/package/leveldb/leveldb-1.22/include/leveldb/write_batch.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/leveldb/leveldbTargets.cmake")
    file(DIFFERENT EXPORT_FILE_CHANGED FILES
         "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/leveldb/leveldbTargets.cmake"
         "/home/wangyang/superman/project/foundation/build/package/leveldb/src/leveldb-build/leveldb-1.22/CMakeFiles/Export/lib/cmake/leveldb/leveldbTargets.cmake")
    if(EXPORT_FILE_CHANGED)
      file(GLOB OLD_CONFIG_FILES "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/leveldb/leveldbTargets-*.cmake")
      if(OLD_CONFIG_FILES)
        message(STATUS "Old export file \"$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/leveldb/leveldbTargets.cmake\" will be replaced.  Removing files [${OLD_CONFIG_FILES}].")
        file(REMOVE ${OLD_CONFIG_FILES})
      endif()
    endif()
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/leveldb" TYPE FILE FILES "/home/wangyang/superman/project/foundation/build/package/leveldb/src/leveldb-build/leveldb-1.22/CMakeFiles/Export/lib/cmake/leveldb/leveldbTargets.cmake")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/leveldb" TYPE FILE FILES "/home/wangyang/superman/project/foundation/build/package/leveldb/src/leveldb-build/leveldb-1.22/CMakeFiles/Export/lib/cmake/leveldb/leveldbTargets-debug.cmake")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/leveldb" TYPE FILE FILES
    "/home/wangyang/superman/project/foundation/build/package/leveldb/src/leveldb-build/leveldb-1.22/cmake/leveldbConfig.cmake"
    "/home/wangyang/superman/project/foundation/build/package/leveldb/src/leveldb-build/leveldb-1.22/cmake/leveldbConfigVersion.cmake"
    )
endif()


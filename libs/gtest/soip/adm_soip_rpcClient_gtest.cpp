#include "gtest/gtest.h"
#include "rpc/adm_soip_rpcClient_c.h"

TEST(client, rpc)
{

    struct json_object* responseMessage = NULL;
    struct json_object* requestMessage = NULL;
    requestMessage = json_object_new_object();
    struct json_object* intVal = json_object_new_int(1);
    struct json_object* strVal = json_object_new_string("rpc request");
    json_object_object_add(requestMessage, "ver", intVal);
    json_object_object_add(requestMessage, "name", strVal);

    service_t serviceId = 0x41;
    instance_t instanceId = 0x42;

    ADM_RPC_VSOMEIP_RPC_CLIENT_T* rpcClient = adm_soip_rpcClientNew(serviceId, instanceId, "127.0.0.1", 1883);

    ASSERT_TRUE(rpcClient);

    ASSERT_EQ(adm_soip_rpcCallMethod(rpcClient, 1, requestMessage, &responseMessage, 5), 0);

    ASSERT_TRUE(responseMessage);
    
    json_object_put(requestMessage);
    json_object_put(responseMessage);
    adm_soip_rpcClientDel(rpcClient);
}
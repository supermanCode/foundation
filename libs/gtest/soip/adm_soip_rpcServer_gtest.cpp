#include "gtest/gtest.h"


#include <stdio.h>
#include <unistd.h>
#include "rpc/adm_soip_rpcServer_c.h"

void test_func1(const method_t method_id, struct json_object* requestMessage, struct json_object** respMessage)
{
    printf("method_id = %d\n", method_id);
    if (requestMessage)
        printf("req = %s\n", json_object_to_json_string(requestMessage));

    *respMessage = json_object_new_object();
    struct json_object* intVal = json_object_new_int(700);
    struct json_object* strVal = json_object_new_string("ioeofpp");
    json_object_object_add(*respMessage, "ver", intVal);
    json_object_object_add(*respMessage, "name", strVal);
}

void test_func2(const method_t method_id, struct json_object* requestMessage)
{
    printf("test_func2 method_id = %d\n", method_id);
    if (requestMessage)
        printf("test_func2 req = %s\n", json_object_to_json_string(requestMessage));
}

void test_func3(const method_t method_id, struct json_object* requestMessage, struct json_object** respMessage)
{
    printf("method_id = %d\n", method_id);
    if (requestMessage)
        printf("req = %s\n", json_object_to_json_string(requestMessage));

    *respMessage = json_object_new_object();
    struct json_object* intVal = json_object_new_int(600);
    struct json_object* strVal = json_object_new_string("addfdf");
    json_object_object_add(*respMessage, "ver", intVal);
    json_object_object_add(*respMessage, "name", strVal);
}

TEST(server, rpc)
{
    service_t serviceId = 0x41;
    instance_t instanceId = 0x42;

    ADM_RPC_VSOMEIP_RPC_SERVER_T* rpcSer = adm_soip_rpcServerNew(serviceId, instanceId, 5, "127.0.0.1", 1883);
    ASSERT_TRUE(rpcSer);

    adm_soip_rpcOfferService(rpcSer);

    adm_soip_rpcRegisterMethodHandler(rpcSer, 1, test_func1);
    adm_soip_rpcRegisterMethodHandler(rpcSer, 10, test_func3);
    adm_soip_RegisterReqMethodHandler(rpcSer, 102, test_func2);

    while (1) {

        sleep(10);
        adm_soip_rpcStopService(rpcSer);
        adm_soip_rpcServerDel(rpcSer);
        break;
    }


}
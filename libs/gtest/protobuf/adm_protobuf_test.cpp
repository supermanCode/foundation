#include <string.h>
#include <iostream>
#include "readDiagnosticInfo.pb.h"

typedef struct
{
    unsigned short dataType; /**!< data type */
    unsigned short dataLength; /**!< data length */
    union
    { /**!< Union of DID corresponding values */
        unsigned char value[1];
        unsigned int valueU32;
        int valueS32;
        unsigned long long int valueU64;
        float valueF;
        unsigned char* valueS;
        // char valueS[1];
    } v;
} ADM_DIM_SOA_VDI_DATA_T;

/**
 * @brief Structure definition used to uniquely identify an ECU.
 *
 */
typedef struct
{
    int requestId; /**!< Request ID */
    int responseId; /**!< Response ID */
    int swDid; /**!< The did of the software version, used to distinguish different upgrade parts of the same requestId
                  ECU */
} ADM_DIM_SOA_VDI_ECU_IDEN_INFO_T;

/***************************************************************************Read diagnostic
 * information**************************************************************************/

/**
 * @brief ECU directly connected bus type
 */
typedef enum
{
    ADM_DIM_SOA_VDI_BUS_TYPE_CAN_C = 1, /**!< CAN */
    ADM_DIM_SOA_VDI_BUS_TYPE_CANFD_C, /**!< CANFD */
    ADM_DIM_SOA_VDI_BUS_TYPE_ETH_C, /**!< ETH  */
    ADM_DIM_SOA_VDI_BUS_TYPE_LIN_C, /**!< LIN */
    ADM_DIM_SOA_VDI_BUS_TYPE_INVALID_C /**!< INVALID */
} ADM_DIM_SOA_VDI_BUS_TYPE_E;

/**
 * @brief The obtained information structure corresponding to DID and DID.
 */
typedef struct
{
    int did; /**!< Data ID */
    ADM_DIM_SOA_VDI_DATA_T*
        didData; /**!< Information corresponding to the DID obtained through the diagnostic command*/
} ADM_DIM_SOA_VDI_DID_DATA_T;

/**
 * @brief DID Array structure.
 */
typedef struct
{
    ADM_DIM_SOA_VDI_DID_DATA_T** didArray; /**!< Array of DID and DID values */
    unsigned short didArrayLength; /**!< Array length of DID and DID values */
} ADM_DIM_SOA_VDI_DID_DATA_ARRAY_T;

/**
 * @brief   Read diagnostic information
 *
 * @param pstEcuIdenInfo        [IN]    const ADM_DIM_SOA_VDI_ECU_IDEN_INFO_T*  Identification information of ECU
 * @param diagScriptPath        [IN]    const char*    Absolute path of script used to obtain diagnostic information
 * @param eBusType              [IN]    const ADM_DIM_SOA_VDI_BUS_TYPE_E    ECU directly connected bus type
 * @param isCGWRouted           [IN]    const DBOOL     True:CGW routed; False:Not CGW routed
 * @param didListRequest        [IN]    const ADM_DIM_SOA_VDI_DID_DATA_ARRAY_T*     Request structure for reading
 * diagnostic information.
 * @param didListResponse       [OUT]   ADM_DIM_SOA_VDI_DID_DATA_ARRAY_T**          Response structure for reading
 * diagnostic information.
 * @param errorCode             [OUT]   int *   The execution result of this request
 * @return int                  0:successed, Non-0:failed
 */
int adm_dim_soa_vdi_readDiagnosticInfo(const ADM_DIM_SOA_VDI_ECU_IDEN_INFO_T* pstEcuIdenInfo,
    const char* diagScriptPath,
    const ADM_DIM_SOA_VDI_BUS_TYPE_E eBusType,
    const bool isCGWRouted,
    const ADM_DIM_SOA_VDI_DID_DATA_ARRAY_T didListRequest,
    ADM_DIM_SOA_VDI_DID_DATA_ARRAY_T** didListResponse,
    int* errorCode)
{

    std::cout << "<===================client code=====================>" << std::endl;
    DiagnosticInfoRequest diaInfoReq;
    soaVdiEcuIdenInfo* embeddedEcuIdenInfo = new soaVdiEcuIdenInfo();

    embeddedEcuIdenInfo->set_requestid(pstEcuIdenInfo->requestId);
    embeddedEcuIdenInfo->set_responseid(pstEcuIdenInfo->responseId);
    embeddedEcuIdenInfo->set_swdid(pstEcuIdenInfo->swDid);

    diaInfoReq.set_allocated_ecuideninfo(embeddedEcuIdenInfo);
    diaInfoReq.set_diagscriptpath(diagScriptPath);
    diaInfoReq.set_ebustype(soaVdiBusType::ADM_DIM_SOA_VDI_BUS_TYPE_LIN);
    diaInfoReq.set_iscgwrouted(isCGWRouted);

    for (int index = 0; index < didListRequest.didArrayLength; ++index) {

        auto didarray = diaInfoReq.add_didarray();
        soaVdiData* embeddedDiddata = new soaVdiData();
        embeddedDiddata->set_datalength(didListRequest.didArray[index]->didData->dataLength);
        embeddedDiddata->set_datatype(didListRequest.didArray[index]->didData->dataType);
        embeddedDiddata->set_values32(didListRequest.didArray[index]->didData->v.valueS32);
        didarray->set_did(didListRequest.didArray[index]->did);
        didarray->set_allocated_diddata(embeddedDiddata);
    }

    std::string outputString;
    diaInfoReq.SerializeToString(&outputString);

    std::cout << "client request <===================call someip=====================>" << std::endl;

    std::cout << "<===================server code=====================>" << std::endl;

    DiagnosticInfoRequest diaInfoResp;
    diaInfoResp.ParseFromString(outputString);
    std::cout << "response requestid <========================================> "
              << diaInfoResp.ecuideninfo().requestid() << std::endl;
    std::cout << "response responseid <========================================> "
              << diaInfoResp.ecuideninfo().responseid() << std::endl;
    std::cout << "response swdid <========================================> " << diaInfoResp.ecuideninfo().swdid()
              << std::endl;
    std::cout << "response diagscriptpath <========================================> " << diaInfoResp.diagscriptpath()
              << std::endl;
    std::cout << "response ebustype <========================================> " << diaInfoResp.ebustype() << std::endl;
    std::cout << "response iscgwrouted <========================================> " << diaInfoResp.iscgwrouted()
              << std::endl;

    std::cout << "did data array length = " << diaInfoResp.didarray_size() << std::endl;

    auto didarrayInfo = diaInfoResp.mutable_didarray();
    auto iter = didarrayInfo->begin();
    for (; iter != didarrayInfo->end(); ++iter) {

        std::cout << "did data datalength = " << iter->diddata().datalength() << std::endl;
        std::cout << "did data datatype = " << iter->diddata().datatype() << std::endl;
        std::cout << "did data value = " << iter->diddata().values32() << std::endl;
    }

    std::cout << "server response <===================call someip=====================>" << std::endl;
    /*response APP*/
    // *didListResponse = (ADM_DIM_SOA_VDI_DID_DATA_ARRAY_T*)malloc(sizeof(ADM_DIM_SOA_VDI_DID_DATA_ARRAY_T));
    // (*didListResponse)->didArray[0] = (ADM_DIM_SOA_VDI_DID_DATA_T*)malloc(sizeof(ADM_DIM_SOA_VDI_DID_DATA_T));
    // (*didListResponse)->didArray[0]->did = 3;
    // (*didListResponse)->didArray[0]->didData = (ADM_DIM_SOA_VDI_DATA_T*)malloc(sizeof(ADM_DIM_SOA_VDI_DATA_T));
    // (*didListResponse)->didArray[0]->didData->dataLength = 500;
    // (*didListResponse)->didArray[0]->didData->dataType = 600;
    // (*didListResponse)->didArray[0]->didData->v.valueS32 = 700;

    // (*didListResponse)->didArray[1] = (ADM_DIM_SOA_VDI_DID_DATA_T*)malloc(sizeof(ADM_DIM_SOA_VDI_DID_DATA_T));
    // (*didListResponse)->didArray[1]->did = 4;
    // (*didListResponse)->didArray[1]->didData = (ADM_DIM_SOA_VDI_DATA_T*)malloc(sizeof(ADM_DIM_SOA_VDI_DATA_T));
    // (*didListResponse)->didArray[1]->didData->dataLength = 800;
    // (*didListResponse)->didArray[1]->didData->dataType = 900;
    // (*didListResponse)->didArray[1]->didData->v.valueS32 = 1000;

    *errorCode = 0;
    return 0;
}

int main()
{
    ADM_DIM_SOA_VDI_ECU_IDEN_INFO_T* pstEcuIdenInfo = nullptr;
    char* diagScriptPath = nullptr;
    ADM_DIM_SOA_VDI_BUS_TYPE_E eBusType;
    bool isCGWRouted;
    ADM_DIM_SOA_VDI_DID_DATA_ARRAY_T didListRequest;
    ADM_DIM_SOA_VDI_DID_DATA_ARRAY_T* didListResponse = NULL;
    int errorCode;

    pstEcuIdenInfo = (ADM_DIM_SOA_VDI_ECU_IDEN_INFO_T*)malloc(sizeof(ADM_DIM_SOA_VDI_ECU_IDEN_INFO_T));
    pstEcuIdenInfo->requestId = 10;
    pstEcuIdenInfo->responseId = 20;
    pstEcuIdenInfo->swDid = 30;

    diagScriptPath = strdup("/tmp/xxx");
    eBusType = ADM_DIM_SOA_VDI_BUS_TYPE_CAN_C;
    isCGWRouted = true;

    didListRequest.didArrayLength = 2;

    didListRequest.didArray = (ADM_DIM_SOA_VDI_DID_DATA_T**)malloc(sizeof(ADM_DIM_SOA_VDI_DID_DATA_T*) * 2);
    std::cout << "sizeof(ADM_DIM_SOA_VDI_DID_DATA_T)*2 = " << sizeof(ADM_DIM_SOA_VDI_DID_DATA_T*) * 2;

    didListRequest.didArray[0] = (ADM_DIM_SOA_VDI_DID_DATA_T*)malloc(sizeof(ADM_DIM_SOA_VDI_DID_DATA_T));
    didListRequest.didArray[0]->did = 0;
    didListRequest.didArray[0]->didData = (ADM_DIM_SOA_VDI_DATA_T*)malloc(sizeof(ADM_DIM_SOA_VDI_DATA_T));
    didListRequest.didArray[0]->didData->dataLength = 50;
    didListRequest.didArray[0]->didData->dataType = 60;
    didListRequest.didArray[0]->didData->v.valueS32 = 70;

    didListRequest.didArray[1] = (ADM_DIM_SOA_VDI_DID_DATA_T*)malloc(sizeof(ADM_DIM_SOA_VDI_DID_DATA_T));
    didListRequest.didArray[1]->did = 1;
    didListRequest.didArray[1]->didData = (ADM_DIM_SOA_VDI_DATA_T*)malloc(sizeof(ADM_DIM_SOA_VDI_DATA_T));
    didListRequest.didArray[1]->didData->dataLength = 80;
    didListRequest.didArray[1]->didData->dataType = 90;
    didListRequest.didArray[1]->didData->v.valueS32 = 100;

    adm_dim_soa_vdi_readDiagnosticInfo(
        pstEcuIdenInfo, diagScriptPath, eBusType, isCGWRouted, didListRequest, &didListResponse, &errorCode);

    free(didListRequest.didArray[0]->didData);
    free(didListRequest.didArray[0]);
    free(didListRequest.didArray[1]->didData);
    free(didListRequest.didArray[1]);
    free(didListRequest.didArray);
    free(pstEcuIdenInfo);
    free(diagScriptPath);

    google::protobuf::ShutdownProtobufLibrary();
}
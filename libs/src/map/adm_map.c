#include "adm_rbtree.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "adm_map.h"

typedef struct rb_node rb_node_t;

struct map
{
    struct rb_node node;
    char* key;
    void* val;
    unsigned int   valSize;
};

root_t* map_new()
{
    root_t* root = (root_t*)malloc(sizeof(root_t));
    if (!root) {
        return NULL;
    }
    root->rb_node = NULL;
    return root;
}


void* map_get_val(const root_t* root, const char* key, unsigned int* valSize)
{
    if (!root || !root->rb_node || !key)
        return NULL;
    int cmp;

    rb_node_t* node = root->rb_node;
    while (node) {
        map_t* data = container_of(node, map_t, node);

        // compare between the key with the keys in map
        cmp = strcmp(key, data->key);
        if (cmp < 0) {
            /*遍历左树*/
            node = node->rb_left;
        } else if (cmp > 0) {
            /*遍历右树*/
            node = node->rb_right;
        } else {
        	*valSize = data->valSize;
            return data->val;
        }
    }
    return NULL;
}

bool map_put(root_t* root, const char* key, void* val, unsigned int valSize)
{
    if (!root || !key || !val)
        return false;

    map_t* data = (map_t*)malloc(sizeof(map_t));
    
    data->key = strdup(key);
    //data->val = strdup(val);
    data->val = malloc(valSize);
    memset(data->val, 0x0, valSize);
    memcpy(data->val, val, valSize);
    data->valSize = valSize;
    
    int result;

    rb_node_t** new_node = &(root->rb_node);
    rb_node_t* parent = NULL;
    while (*new_node) {
        map_t* this_node = container_of(*new_node, map_t, node);
        result = strcmp(key, this_node->key);
        parent = *new_node;

        if (result < 0) {
            /*遍历左树*/
            new_node = &((*new_node)->rb_left);
        } else if (result > 0) {
            /*遍历右树*/
            new_node = &((*new_node)->rb_right);
        } else {
            /*与老元素相同，覆盖老元素*/
            free(this_node->val);
            
            this_node->val = malloc(valSize);
    		memset(this_node->val, 0x0, valSize);
    		memcpy(this_node->val, val, valSize);
    		this_node->valSize = valSize;
    		
            //strcpy(this_node->val, val);
            free(data->key);
            free(data->val);
            free(data);
            return true;
        }
    }

    /*新增节点*/
    rb_link_node(&data->node, parent, new_node);
    rb_insert_color(&data->node, root);
    return true;
}

map_t* map_first(const root_t* tree)
{
    if (!tree)
        return NULL;

    rb_node_t* node = rb_first(tree);
    return (rb_entry(node, map_t, node));
}

map_t* map_next(map_t* map_node)
{
    if (!map_node)
        return NULL;

    rb_node_t* node = &(map_node->node);
    rb_node_t* next = rb_next(node);
    return rb_entry(next, map_t, node);
}

static void map_free_node(map_t* node)
{
    if (node != NULL) {
        if (node->key != NULL) {
            free(node->key);
            node->key = NULL;
            free(node->val);
            node->val = NULL;
        }
        free(node);
        node = NULL;
    }
}

void map_erase(root_t* root, const char* key)
{
	if (!root || !key)
		return ;
	
	map_t* node = NULL;
    for (node = map_first(root); node; node = map_next(node)) 
    {
        if (0 == strcmp(key, node->key)) 
        {
            rb_erase(&node->node, root);
            map_free_node(node);
            return ;
        }
    }
}
void map_free(root_t* root)
{
    if (!root)
        return;
    map_t* nodeFree = NULL;
    map_t* tmpNodeFree = NULL;
    for (nodeFree = map_first(root); nodeFree; ) {
    	tmpNodeFree = map_next(nodeFree);
        rb_erase(&nodeFree->node, root);
        map_free_node(nodeFree);
        nodeFree = tmpNodeFree;
    }
    free(root);
    root = NULL;
}

char* map_get_key(const map_t* node)
{
    if (!node)
        return NULL;
    return node->key;
}

void* map_get_value(const map_t* node)
{
    if (!node)
        return NULL;
    return node->val;
}

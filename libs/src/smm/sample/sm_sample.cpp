#include "adm_sm.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <ctype.h>



#include "adm_debug.h"

#define LOGM(fmt, ...)   printf("[ %s ] %s %s : %d " fmt "\n", __DATE__, __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__)


#define ADM_LOG_REPORT_DEFAULT_SUB_STATE_ID     0XFF            //!< 默认的sub state id



static void wait_sleep(int secs)
{
    //sleep(secs);
    struct timeval tv;
    tv.tv_sec  = secs;
    tv.tv_usec = 0;

    (void)select(0, NULL, NULL, NULL, &tv);
}

/**
 * @brief 日志上报过程的状态值
 * 
 */
typedef enum ADM_LOG_REPORT_STATE_ 
{
    ADM_LOG_REPORT_STATE_COLLECT = 0x00,        //!< 日志收集
    ADM_LOG_REPORT_STATE_PACKAGE,               //!< 日志打包
    ADM_LOG_REPORT_STATE_ENCRYPT,               //!< 日志加密
    ADM_LOG_REPORT_STATE_EXECUTE,               //!< 日志上报执行
    ADM_LOG_REPORT_STATE_DEINIT,                //!< 日志反初始化
} ADM_LOG_REPORT_STATE_E;


/**
 * @brief 各handle执行的结果
 * 
 */
typedef enum ADM_LOG_REPORT_RESULT_
{
    ADM_LOG_REPORT_RESULT_SUCC = 0x00,          //!< handle 执行成功
    ADM_LOG_REPORT_RESULT_FAILED,               //!< handle 执行失败
} ADM_LOG_REPORT_REPORT_RESULT_E;



static D32S __logReportStateCollect(void* pParam1, void* pParam2, void* pParam3)
{
    log_d("Entry............................");
    return ADM_LOG_REPORT_RESULT_SUCC;
}


static D32S __logReportStatePackage(void* pParam1, void* pParam2, void* pParam3)
{
    log_d("Entry............................");
    return ADM_LOG_REPORT_RESULT_SUCC;
}


static D32S __logReportStateEncrypt(void* pParam1, void* pParam2, void* pParam3)
{
    log_d("Entry............................");
    return ADM_LOG_REPORT_RESULT_SUCC;
}

static D32S __logReportStateExec(void* pParam1, void* pParam2, void* pParam3)
{
    log_d("Entry............................");
    return ADM_LOG_REPORT_RESULT_SUCC;
}
/**
 * @brief 状态流转的结构体数组，后续添加流程，直接在此加入即可
 * 
 */
static adm_sm_step_t g_stTaskList[] = 
{
    /** 日志收集 */
    {
        ADM_LOG_REPORT_STATE_COLLECT,
        ADM_LOG_REPORT_DEFAULT_SUB_STATE_ID,
        __logReportStateCollect,
        {
            {ADM_LOG_REPORT_RESULT_SUCC,    ADM_LOG_REPORT_STATE_PACKAGE,   ADM_LOG_REPORT_DEFAULT_SUB_STATE_ID},
            {ADM_LOG_REPORT_RESULT_FAILED,  ADM_LOG_REPORT_STATE_DEINIT,    ADM_LOG_REPORT_DEFAULT_SUB_STATE_ID},
        },
    },
    /** 日志打包 */
    {
        ADM_LOG_REPORT_STATE_PACKAGE,
        ADM_LOG_REPORT_DEFAULT_SUB_STATE_ID,
        __logReportStatePackage,
        {
            {ADM_LOG_REPORT_RESULT_SUCC,    ADM_LOG_REPORT_STATE_ENCRYPT,   ADM_LOG_REPORT_DEFAULT_SUB_STATE_ID},
            {ADM_LOG_REPORT_RESULT_FAILED,  ADM_LOG_REPORT_STATE_DEINIT,    ADM_LOG_REPORT_DEFAULT_SUB_STATE_ID},
        },
    },
    /** 日志加密 */
    {
        ADM_LOG_REPORT_STATE_ENCRYPT,
        ADM_LOG_REPORT_DEFAULT_SUB_STATE_ID,
        __logReportStateEncrypt,
        {
            {ADM_LOG_REPORT_RESULT_SUCC,    ADM_LOG_REPORT_STATE_EXECUTE,   ADM_LOG_REPORT_DEFAULT_SUB_STATE_ID},
            {ADM_LOG_REPORT_RESULT_FAILED,  ADM_LOG_REPORT_STATE_DEINIT,    ADM_LOG_REPORT_DEFAULT_SUB_STATE_ID},
        },
    },
    /** 日志上报执行 */
    {
        ADM_LOG_REPORT_STATE_EXECUTE,
        ADM_LOG_REPORT_DEFAULT_SUB_STATE_ID,
        __logReportStateExec,
        {
            {ADM_LOG_REPORT_RESULT_SUCC,    ADM_LOG_REPORT_STATE_DEINIT,   ADM_LOG_REPORT_DEFAULT_SUB_STATE_ID},
            {ADM_LOG_REPORT_RESULT_FAILED,  ADM_LOG_REPORT_STATE_DEINIT,    ADM_LOG_REPORT_DEFAULT_SUB_STATE_ID},
        },
    }
};


int main()
{
    adm_debug_logInitWithFileName("/renzhendi/log/", "sample.log", (5 * 1024 * 1024), 2, ELOG_LVL_VERBOSE, TRUE);
    
    DHANDLE smHandle = 0;
    const DCHAR* pSmName = "LOG_REPORT";

    adm_sm_init();

    smHandle = adm_sm_createHandle(pSmName, g_stTaskList, sizeof(g_stTaskList) / sizeof(g_stTaskList[0]));

    log_i("adm_sm_start begin");
    //adm_sm_start(smHandle, ADM_LOG_REPORT_STATE_COLLECT, NULL, NULL, NULL);

    DHANDLE hThread = 0xFFFFFFFFFFFFFFFF;
    adm_sm_startAsync(smHandle, &hThread, ADM_LOG_REPORT_STATE_COLLECT, NULL, NULL, NULL);
    log_i("hThread=%llu", hThread);

    //异步时要等待进入另一个线程处理
    wait_sleep(2);
    log_i("adm_sm_stop begin");
    adm_sm_stop(smHandle);

    log_i("adm_sm_destoryHandle begin");
    (void)adm_sm_destoryHandle(smHandle);

    adm_sm_uninit();
}

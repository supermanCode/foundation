
#include "adm_sm.h"
#include "adm_memory.h"
#include <list>
#include <string.h>
#include "adm_debug.h"

#include "adm_com_thread.h"


#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG          "SMM"   //!< 状态机制的日志TAG

/** 状态机异步线程名 */
#define SM_ASYNC_CTRL    "smAsyncCtrl"

using namespace std;

#define ADM_SM_STEPINVALID          ((D32U)(-1))        /* 无效跳转步骤 */
#define ADM_SM_STEPUNDEFINE         ((D32U)(-2))        /* 未找到跳转步骤 */

/**
 * @struct    adm_sm_ctrl_struct
 * @brief     状态机
 * @details   detail description
 */
typedef struct adm_sm_ctrl_struct
{
    /* control param */
    DCHAR smName[ADM_SM_NAMELENGTH];
    D32U stepNumber;                   ///< 状态机流转总数
    const adm_sm_step_t *pSMItems;
    DHANDLE hSMLock;                   ///< 状态机同步锁

    /* run param */
    ADM_SM_STATE_EN runState;        ///< 状态机当前状态
    D32S    errCode;
    D32U    stepIndex;               ///< 状态机当前位置
    void *pParam1;
    void *pParam2;
    void *pParam3;
} adm_sm_ctrl_t, *adm_sm_ctrl_pt;


#define ADM_SMM_LOCKNAME            "ADM_SMM_LOCKNAME"

/* 状态机存储和管理结构 */
typedef struct adm_sm_manager_struct
{
    DHANDLE hCtrlLock;
    list<adm_sm_ctrl_pt> smCtrls;
} adm_sm_manager_t;

/* 存储和管理当前申请的状态机 */
static adm_sm_manager_t m_smManager;


static void smLock(DHANDLE hLock)
{
    adm_com_threadMutexLock(hLock);
}

static void smUnlock(DHANDLE hLock)
{
    adm_com_threadMutexUnlock(hLock);
}


static D32U smUpdateStep(adm_sm_ctrl_pt pCtrl, const adm_sm_result_t *pResult)
{
    D32U retStep = ADM_SM_STEPINVALID;

    for (D32U i = 0; i < pCtrl->stepNumber; i++)
    {
        const adm_sm_step_t *pItem = pCtrl->pSMItems + i;

        if ((pItem->stateStepID == pResult->stateID) &&
                (pItem->stateSubStepID == pResult->stateSubID))
        {
            retStep = i;
            break;
        }
        else {}
    }

    return retStep;
}

static D32S smStart(adm_sm_ctrl_pt pSM)
{
    DASSERT((DNULL != pSM) && (isValidCtrl(pSM) == DTRUE));

    do
    {
        pSM->errCode = ADM_SM_OK;

        while ((pSM->stepIndex) < (pSM->stepNumber))
        {
            D32S retCode = 0;
            const adm_sm_step_t *pStep = &(pSM->pSMItems[pSM->stepIndex]);

            smLock(pSM->hSMLock);
            retCode = pStep->fnHandle(pSM->pParam1, pSM->pParam2, pSM->pParam3);

            if (pSM->runState == ADM_SM_STATE_WAITSTOP)
            {
                pSM->runState = ADM_SM_STATE_STOP;
                smUnlock(pSM->hSMLock);
                break;
            }
            else
            {
                /* 无匹配步骤，则默认跳转到下一步 */
                D32U stepIndex = ADM_SM_STEPUNDEFINE;

                for (D32U i = 0; i < ADM_SM_MAXRESULTJUMP; i++)
                {
                    const adm_sm_result_t *pResult = &(pStep->resultJump[i]);

                    if (ADM_SM_INVALIDCODE == pResult->resultCode)
                    {
                        /* 后续为无效跳转，跳出检索 */
                        stepIndex = ADM_SM_STEPUNDEFINE;
                        break;
                    }
                    else if (pResult->resultCode == retCode)
                    {
                        stepIndex = smUpdateStep(pSM, pResult);

                        if (stepIndex != ADM_SM_STEPINVALID)
                        {
                            pSM->stepIndex = stepIndex;
                        }
                        else {}

                        break;
                    }
                    else {}
                }

                if (ADM_SM_STEPUNDEFINE == stepIndex)
                {
                    pSM->stepIndex++;   // default jump to next index
                }
                else if (ADM_SM_STEPINVALID == stepIndex)
                {
                    pSM->runState = ADM_SM_STATE_STOP;
                    pSM->errCode = ADM_SM_ERR_INVALIDSTEP;
                    smUnlock(pSM->hSMLock);
                    break;
                }
                else {}

                smUnlock(pSM->hSMLock);
            }
        }
    }
    while (DFALSE);

    return pSM->errCode;
}

static void *smAsyncHander(void *argv)
{
    adm_sm_ctrl_pt pSM = (adm_sm_ctrl_pt)argv;
    smStart(pSM);

    return DNULL;
}

static DBOOL isValidCtrl(adm_sm_ctrl_pt pCtrl)
{
    DBOOL bRet = DFALSE;
    DASSERT(pCtrl != DNULL);
    //smLock(pCtrl->hSMLock);
    smLock(m_smManager.hCtrlLock);

    list<adm_sm_ctrl_pt>::iterator iter = m_smManager.smCtrls.begin();
    list<adm_sm_ctrl_pt>::iterator iterEnd = m_smManager.smCtrls.end();

    while (iter != iterEnd)
    {
        if (*iter == pCtrl)
        {
            bRet = DTRUE;
            break;
        }
        else {}

        iter++;
    }

    //smUnlock(pCtrl->hSMLock);
    smUnlock(m_smManager.hCtrlLock);

    return bRet;
}

/* 释放所有创建的状态机 */
static void freeSMCtrl()
{
    smLock(m_smManager.hCtrlLock);
    list<adm_sm_ctrl_pt>::iterator iter = m_smManager.smCtrls.begin();
    list<adm_sm_ctrl_pt>::iterator iterEnd = m_smManager.smCtrls.end();

    while (iter != iterEnd)
    {
        adm_sm_ctrl_pt pSMCtrl = *iter;
        adm_com_threadMutexDestroy(pSMCtrl->hSMLock);
        adm_free(pSMCtrl);
        iter++;
    }

    //
    m_smManager.smCtrls.clear();

    smUnlock(m_smManager.hCtrlLock);
}

static void initSMCtrl()
{
    adm_com_threadMutexCreate(&(m_smManager.hCtrlLock));
    DASSERT(m_smManager.smCtrls.size() == 0);
    freeSMCtrl();
}

static void stopSMCtrl(adm_sm_ctrl_pt pCtrl)
{

}

static DBOOL isSMBusy(adm_sm_ctrl_pt pCtrl)
{
    DBOOL retValue = DFALSE;

    if ((ADM_SM_STATE_RUN == pCtrl->runState) ||
            (ADM_SM_STATE_WAITSTOP == pCtrl->runState))
    {
        retValue = DTRUE;
    }
    else {}

    return retValue;
}

void adm_sm_init()
{
    initSMCtrl();
}

void adm_sm_uninit()
{
    freeSMCtrl();
}

ADM_SM_STATE_EN adm_sm_getState(DHANDLE smHandle)
{
    ADM_SM_STATE_EN retCode = ADM_SM_STATE_IDLE;
    adm_sm_ctrl_pt pSM = (adm_sm_ctrl_pt)smHandle;

    if ((DNULL != pSM) && (isValidCtrl(pSM) == DTRUE))
    {
        retCode = pSM->runState;
    }
    else
    {
        retCode = ADM_SM_STATE_INVALID;
    }

    return retCode;
}

DHANDLE adm_sm_createHandle(const char *smName, const adm_sm_step_t *pSMItems, D32U itemNumber)
{
    D32U smCtrlSize = sizeof(adm_sm_ctrl_t);
    adm_sm_ctrl_pt pSM = (adm_sm_ctrl_pt)adm_malloc(smCtrlSize);
    adm_memzero(pSM, smCtrlSize);

    /* 复制名称*/
    //strncpy_s(pSM->smName, smName, ADM_SM_NAMELENGTH);
    strncpy(pSM->smName, smName, ADM_SM_NAMELENGTH);
    pSM->smName[ADM_SM_NAMELENGTH - 1] = 0;

    pSM->stepNumber = itemNumber;
    pSM->pSMItems = pSMItems;
    pSM->stepIndex = 0;
    pSM->runState = ADM_SM_STATE_IDLE;
    adm_com_threadMutexCreate(&(pSM->hSMLock));

    smLock(m_smManager.hCtrlLock);
    m_smManager.smCtrls.push_back(pSM);
    smUnlock(m_smManager.hCtrlLock);

    return (DHANDLE)pSM;
}

D32S adm_sm_destoryHandle(DHANDLE smHandle)
{
    D32S retCode = ADM_SM_OK;
    adm_sm_ctrl_pt pSM = (adm_sm_ctrl_pt)smHandle;

    if ((DNULL != pSM) && (isValidCtrl(pSM) == DTRUE))
    {
        /* 释放前先停止当前状态机 */
        adm_sm_stop(smHandle);

        while (isSMBusy(pSM) == DTRUE)
        {
            /* 等待状态机处理完成 */
            /* sleep */
        }

        smLock(pSM->hSMLock);
        m_smManager.smCtrls.remove(pSM);
        smUnlock(pSM->hSMLock);
        adm_com_threadMutexDestroy(pSM->hSMLock);
        adm_free(pSM);
    }
    else
    {
        retCode = ADM_SM_ERR_INVALIDHANDLE;
    }

    return retCode;
}

D32S adm_sm_reset(DHANDLE smHandle)
{
    D32S retCode = ADM_SM_OK;
    adm_sm_ctrl_pt pSM = (adm_sm_ctrl_pt)smHandle;
    smLock(pSM->hSMLock);

    if ((DNULL != pSM) && (isValidCtrl(pSM) == DTRUE))
    {
        if (isSMBusy(pSM) == DFALSE)
        {
            pSM->runState = ADM_SM_STATE_IDLE;
            pSM->pParam1 = DNULL;
            pSM->pParam2 = DNULL;
            pSM->pParam3 = DNULL;
        }
        else
        {
            /* 先Stop */
            retCode = ADM_SM_ERR_BUSY;
        }
    }
    else
    {
        retCode = ADM_SM_ERR_INVALIDHANDLE;
    }

    smUnlock(pSM->hSMLock);
    return retCode;
}

D32S adm_sm_startAsync(DHANDLE smHandle, DHANDLE *hThread, D32U startIndex, void *pParam1,
                       void *pParam2, void *pParam3)
{
    D32S retCode = ADM_SM_OK;
    adm_sm_ctrl_pt pSM = (adm_sm_ctrl_pt)smHandle;

    do
    {
        if ((DNULL == pSM) || (isValidCtrl(pSM) == DFALSE))
        {
            retCode = ADM_SM_ERR_INVALIDHANDLE;
            break;
        }

        if (isSMBusy(pSM) == DTRUE)
        {
            /* 当前状态机已经在工作 */
            retCode = ADM_SM_ERR_BUSY;
            break;
        }

        if (startIndex >= pSM->stepNumber)
        {
            retCode = ADM_SM_ERR_INVALIDSTEP;
            break;
        }

        /* 当前状态机上锁 */
        smLock(pSM->hSMLock);
        pSM->stepIndex = startIndex;
        pSM->runState = ADM_SM_STATE_RUN;
        pSM->pParam1 = pParam1;
        pSM->pParam2 = pParam2;
        pSM->pParam3 = pParam3;
        smUnlock(pSM->hSMLock);

        if (ADM_OS_ECODE_SUCCESSED !=
                adm_com_threadCreate(hThread, smAsyncHander, pSM, SM_ASYNC_CTRL))
        {
            retCode = ADM_SM_THREADCREATEERROR;
        }
    }
    while (DFALSE);

    return retCode;

}

D32S adm_sm_start(DHANDLE smHandle, D32U startIndex, void *pParam1, void *pParam2, void *pParam3)
{
    D32S retCode = ADM_SM_OK;
    adm_sm_ctrl_pt pSM = (adm_sm_ctrl_pt)smHandle;

    do
    {
        if ((DNULL == pSM) || (isValidCtrl(pSM) == DFALSE))
        {
            retCode = ADM_SM_ERR_INVALIDHANDLE;
            break;
        }

        if (isSMBusy(pSM) == DTRUE)
        {
            /* 当前状态机已经在工作 */
            retCode = ADM_SM_ERR_BUSY;
            break;
        }

        if (startIndex >= pSM->stepNumber)
        {
            retCode = ADM_SM_ERR_INVALIDSTEP;
            break;
        }

        /* 当前状态机上锁 */
        smLock(pSM->hSMLock);
        pSM->stepIndex = startIndex;
        pSM->runState = ADM_SM_STATE_RUN;
        pSM->pParam1 = pParam1;
        pSM->pParam2 = pParam2;
        pSM->pParam3 = pParam3;
        smUnlock(pSM->hSMLock);

        retCode = smStart(pSM);
    }
    while (DFALSE);

    return retCode;
}

D32S adm_sm_stop(DHANDLE smHandle)
{
    D32S retCode = ADM_SM_OK;
    adm_sm_ctrl_pt pSM = (adm_sm_ctrl_pt)smHandle;
    smLock(pSM->hSMLock);

    if (pSM != DNULL && isValidCtrl(pSM) == DTRUE)
    {
        pSM->stepIndex = 0;
        pSM->runState = ADM_SM_STATE_STOP;
    }
    else
    {
        retCode = ADM_SM_ERR_INVALIDHANDLE;
    }

    smUnlock(pSM->hSMLock);
    return retCode;
}


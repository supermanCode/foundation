#include "adm_com_kvstorage.h"

#include "adm_string.h"
#include "adm_memory.h"
#include "adm_debug.h"

static ADM_COM_KVSTORAGE_ENCRYPT_CALLBACK_F g_encCb = NULL;     //!< 加密接口
static ADM_COM_KVSTORAGE_DECRYPT_CALLBACK_F g_decCb = NULL;     //!< 解密接口
static DBOOL isKvEncDecOk = FALSE;                              //!< 加解密接口初始化标识

/**
 * @brief 用于打开kvstorage数据库
 * 
 * @param databaseName  [IN]    kvstorage数据库存储目录，绝对路径
 * @return KvStorage_t*     NULL:Failed, None-NULL: kvstorage句柄
 */
KvStorage_t* adm_com_kvstorage_open(const char* databaseName)
{
    KvStorage_t* kvs = NULL;
    DCHAR* errorStr = NULL;

    do {
        if (NULL == databaseName) {
            log_e("Input param databasename is nil");
            return NULL;
        }

        kvs = adm_kvStorage_open(databaseName, &errorStr);
        if (NULL == kvs) {
            log_e("adm_kvStorage_open %s failed, errorStr:%s", databaseName, errorStr);
            adm_free(errorStr);
            errorStr = NULL;
        }
    } while(FALSE);

    return kvs;
}

DBOOL adm_com_kvstorage_repare(const char* pDbName)
{
    DCHAR* errorStr = NULL;
    DBOOL isRepareOk = FALSE;
    if (NULL == pDbName) 
    {
        log_e("Input param pDbName is nil");
        return FALSE;
    }

    isRepareOk = adm_kvstorage_repare(pDbName,&errorStr);
    if(isRepareOk == FALSE)
    {
        log_e("adm_kvstorage_repare failed, errorStr:%s", errorStr);
        adm_free(errorStr);
        errorStr = NULL;
    }

    return isRepareOk;
}

/**
 * @brief 用于摧毁kvstorage数据库，即删除数据库
 * 
 * @param kvs   [IN]    kvStorage 数据库句柄
 * @return DBOOL 
 */
DBOOL adm_com_kvstorage_destroy(KvStorage_t* kvs)
{
    DBOOL isDestoryOk = FALSE;
    DCHAR* errorStr = NULL;

    do {
        if (NULL == kvs) {
            log_e("Input param kvs is nil");
            break;
        }

        isDestoryOk = adm_kvStorage_destroy(kvs, &errorStr);
        if (TRUE == isDestoryOk) {
            log_i("adm_kvStorage_destroy successed");
        } else {
            log_e("adm_kvStorage_destroy failed, errorStr:%s", errorStr);
            adm_free(errorStr);
            errorStr = NULL;
        }

    } while(FALSE);


    return isDestoryOk;
}

/**
 * @brief 用于关闭打开的kvstorage数据库句柄
 * 
 * @param kvs   [IN]    kvstorage数据库句柄
 */
void adm_com_kvstorage_close(KvStorage_t* kvs)
{
    if (NULL == kvs) {
        log_e("Input param kvs is nil");
        return;
    }

    adm_kvStorage_close(kvs);

    return;
}

/**
 * @brief 用于判断此kvstorage数据库中是否有此key
 * 
 * @param kvs [IN]  kvstorage数据库句柄
 * @param key [IN]  key字段
 * @return DBOOL    TRUE: 存在，FALSE: 不存在
 */
DBOOL adm_com_kvstorage_has_key(KvStorage_t* kvs, const char* key)
{
    DBOOL isHasKey = FALSE;
    DCHAR* errorStr = NULL;

    do {
        if (NULL == kvs) {
            log_e("Input param kvs is nil");
            break;
        }

        if (NULL == key) {
            log_e("Input param key is nil");
            break;
        }

        isHasKey = adm_kvStorage_has_key(kvs, key, &errorStr);
        if (TRUE == isHasKey) {
            log_i("adm_kvStorage_has_key haskey:%s", key);
        } else {
            log_e("adm_kvStorage_has_key is not has key:%s, errorStr:%s", key, errorStr);
            adm_free(errorStr);
            errorStr = NULL;
        }
    } while(FALSE);

    return isHasKey;
 }

/**
 * @brief 用于删除kvstorage数据库中key对应的信息
 * 
 * @param kvs [IN]  kvstorage数据库句柄
 * @param key [IN]  key字段
 * @return DBOOL    TRUE: 成功，FALSE: 失败
 */
DBOOL adm_com_kvstorage_remove_key(KvStorage_t* kvs, const char* key)
{
    DBOOL isRemoveOk = FALSE;
    DCHAR* errorStr = NULL;

    do {
        if (NULL == kvs) {
            log_e("Input param kvs is nil");
            break;
        }

        if (NULL == key) {
            log_e("Input param key is nil");
            break;
        }

        isRemoveOk = adm_kvStorage_remove_key(kvs, key, &errorStr);
        if (TRUE == isRemoveOk) {
            log_i("adm_kvStorage_remove_key %s success", key);
        } else {
            log_e("adm_kvStorage_remove_key %s failed, errorStr:%s", key, errorStr);
            adm_free(errorStr);
            errorStr = NULL;
        }
    } while(FALSE);


    return isRemoveOk;
}

/**
 * @brief 用于存储key对应的value信息
 * 
 * @param kvs [IN]  kvstorage数据库句柄
 * @param key [IN]  key字段
 * @param val [IN]  key字段对应的value
 * @param eValType [IN] key字段的存储的value数据类型
 * @return DBOOL    TRUE: 成功，FALSE: 失败
 */
DBOOL adm_com_kvstorage_set_string(KvStorage_t* kvs, const char* key, const char* val, const ADM_KV_STORAGE_VALUE_TYPE_E eValType)
{
    D32S iRet = ADM_COM_KVSTORAGE_ERR_DEFAULT;
    DCHAR* encVal = NULL;
    D32U encValLen = 0;
    DBOOL bRet = FALSE;
    DCHAR* errorStr = NULL;

    //!< 参数校验
    if (NULL == kvs) {
        log_e("Input param kvs is nil");
        return FALSE;
    }

    if (NULL == key) {
        log_e("Input param key is nil");
        return FALSE;
    }

    if (NULL == val) {
        log_e("Input param val is nil");
        return FALSE;
    }

    //!< 参数value type校验
    if (ADM_KV_STORAGE_VALUE_TYPE_UINT8 > eValType || ADM_KV_STORAGE_VALUE_TYPE_INVALID < eValType) {
        log_e("Input param evalType:%d is nil", eValType);
        return FALSE;
    }

    //!< 对输入的value信息进行加密处理
    do {
        //!< 只有在加解密的回调初始化成功后，才默认使用加密函数
        if (TRUE == isKvEncDecOk && NULL != g_encCb) {
            iRet = g_encCb(val, adm_strlen(val), &encVal, &encValLen);
            if (ADM_COM_KVSTORAGE_ERR_SUCCESSED != iRet) {
                log_e("Value:%s  encrypt failed, err:%d", iRet);
                bRet = FALSE;
                break;
            }

            if (NULL == encVal) {
                log_e("Value:%s encrypt failed", val);
                bRet = FALSE;
                break;
            }
        } else {
            // log_i("isKvEncDecOk is FALSE, do not encrypt value");
            encVal = adm_strdup(val);
        }

        bRet = adm_kvStorage_set_string(kvs, key, encVal, eValType, &errorStr);
        if (TRUE == bRet) {
            log_i("adm_kvStorage_set_string key:%s:%s successed", key, encVal);
        } else {
            log_i("adm_kvStorage_set_string key:%s:%s failed, errorStr:%s", key, encVal, errorStr);
            adm_free(errorStr);
            errorStr = NULL;
        }
    } while(FALSE);
    
    if (NULL != encVal) {
        adm_free(encVal);
    }

    return bRet;
}

/**
 * @brief 用于获取key对应的value信息
 * 
 * @param kvs [IN]  kvstorage数据库句柄
 * @param key [IN]  key字段
 * @param val [OUT] key字段对应的value值
 * @param length [OUT]  key字段对应的value值的长度
 * @param eValType [OUT]  key字段对应的value值的类型
 * @return DBOOL    TRUE: 成功，FALSE: 失败
 */
DBOOL adm_com_kvstorage_get_string(KvStorage_t* kvs, const char* key, char** val, size_t* length, ADM_KV_STORAGE_VALUE_TYPE_E* eValType)
{
    D32S iRet = ADM_COM_KVSTORAGE_ERR_DEFAULT;
    DBOOL bRet = FALSE;
    DCHAR* encVal = NULL;
    size_t encValLen = 0;
    DCHAR* errorStr = NULL;

    //!< 参数校验
    if (NULL == kvs) {
        log_e("Input param kvs is nil");
        return FALSE;
    }

    if (NULL == key) {
        log_e("Input param key is nil");
        return FALSE;
    }

    do {
        //!< 从kvstorage中获取存储的数据
        bRet = adm_kvStorage_get_string(kvs, key, &encVal, eValType, &errorStr);
        if (FALSE == bRet) {
            log_e("kvstorage_get_string key:%s failed, errorStr:%s", key, errorStr);
            adm_free(errorStr);
            errorStr = NULL;
            break;
        }

        //!< 校验获取到的参数
        if (NULL == encVal) {
            log_e("kvstorage_get_string encVal is nil");
            bRet = FALSE;
            break;
        }

        encValLen = adm_strlen(encVal);
        //!< 只有在加解密的回调初始化成功后，才默认使用解密函数
        if (TRUE == isKvEncDecOk && NULL != g_decCb) {
            iRet = g_decCb(encVal, encValLen, val, length);
            if (ADM_COM_KVSTORAGE_ERR_SUCCESSED != iRet) {
                log_e("g_decCb decrypt encVal:%s is failed, err:%d", iRet);
                bRet = FALSE;
                break;
            }

            if (NULL == val) {
                log_e("g_decCb decrypt encVal:%s failed", encVal);
                bRet = FALSE;
                break;
            }
        } else {
            //!< 数据不需要解密，只需要返回即可
            *val = adm_strdup(encVal);
            *length = encValLen;
        }

    } while(FALSE);

    log_i("kvstorage_get_string get key:%s, %d", key, bRet);

    if (NULL != encVal) {
        adm_free(encVal);
    }
    return bRet;
}

/**
 * @brief 获取所有的key列表
 * @note 
 *  1. 返回值为true时，key_list 使用结束后需要释放内存空间
 * 
 * @param kvs       [IN]    kv句柄
 * @param keyList  [OUT]   key列表地址
 * @return true     获取成功
 * @return false    获取失败
 */
DBOOL adm_com_kvstorage_list_key(KvStorage_t* kvs, adm_kvStorage_all_key_t** keyList)
{
    DCHAR* errorStr = NULL;
    DBOOL isListKeyOk = FALSE;

    if (adm_kvStorage_get_allKeys(kvs, keyList, &errorStr)) {
        log_i("adm_kvStorage_get_allKeys successed");
        isListKeyOk = TRUE;
    } else {
        log_e("adm_kvStorage_get_allKeys failed, errorStr = %s\n", errorStr);
        adm_free(errorStr);
        errorStr = NULL;
    }

    return isListKeyOk;
}


/**
 * @brief 通用的kv存储初始化接口
 * @note 
 *   1. 设置kvstorage使用的加解密函数;
 *   2. 加解密函数可以为空，但必须同时为空;
 *   3. 加解密函数为空时，默认不加密;
 *   4. 加解密函数不为空时，默认需要加密
 * @param encCb [IN]    加密的回调接口
 * @param decCb [IN]    解密的回调接口
 */
void adm_com_kvstorageInit(const ADM_COM_KVSTORAGE_ENCRYPT_CALLBACK_F encCb, const ADM_COM_KVSTORAGE_DECRYPT_CALLBACK_F decCb)
{
    //!< 直接赋值，可以为空，但必须同时为空 或者 同时非空
    g_encCb = encCb;
    g_decCb = decCb;

    if (NULL != g_encCb && NULL != g_decCb) {
        isKvEncDecOk = TRUE;
    } else {
        isKvEncDecOk = FALSE;
    }

    return;
}

/**
 * @brief 通用的kvstorage反初始化接口
 * @note 
 *   1. 主要将kvstorage需要的加解密函数置空
 * 
 */
void adm_com_kvstorageDeinit(void)
{
    isKvEncDecOk = FALSE;
    g_encCb = NULL;
    g_decCb = NULL;

    return;
}

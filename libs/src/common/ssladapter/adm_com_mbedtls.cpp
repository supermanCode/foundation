#include "adm_com_sslAdapter.h"

#ifdef USE_MBEDTLS
#include "adm_com_ssllog.h"
#include "adm_memory.h"
#include "adm_string.h"
#include <errno.h>
#include <stdlib.h>

#include <mbedtls/ssl.h>
#if MBEDTLS_VERSION_NUMBER < 0x03000000L
#include <mbedtls/certs.h>
#endif
#include <mbedtls/ctr_drbg.h>
#include <mbedtls/x509.h>
#include <mbedtls/rsa.h>
#include <mbedtls/error.h>
#include <mbedtls/version.h>
#include <mbedtls/entropy.h>

#if MBEDTLS_VERSION_NUMBER < 0x02040000L
#include <mbedtls/net.h>
#else
#include <mbedtls/net_sockets.h>
#endif

#if defined(MBEDTLS_SSL_CACHE_C)
#include <mbedtls/ssl_cache.h>
#endif

#define ADM_COM_SSL_TLS1v3 "TLSv1.3"
#define ADM_COM_SSL_TLS1v2 "TLSv1.2"
#define ADM_COM_SSL_MAX_CONF_ERRSTR 512

typedef struct{
    mbedtls_ssl_config          conf;
    mbedtls_pk_context          pkey;
    mbedtls_x509_crt            ca_cert;
    mbedtls_x509_crt            cert;
    mbedtls_entropy_context     entropy;
    mbedtls_ctr_drbg_context    ctr_drbg;
#if defined(MBEDTLS_SSL_CACHE_C)
    mbedtls_ssl_cache_context   cache;
#endif
}ADM_MBEDTLS_CTX_T;

typedef struct{
    mbedtls_ssl_context ssl;
    mbedtls_net_context net;
}ADM_MBEDTLS_SSL_T;


static void __adm_com_sslStrError(const D32S ret, const DCHAR *fmt)
{
    D8U errstr[ADM_COM_SSL_MAX_CONF_ERRSTR];
    mbedtls_strerror( ret, errstr, sizeof(errstr));
    log_e("%s %s",fmt, errstr);
}

static D32S __adm_com_setCiphers(ADM_MBEDTLS_CTX_T * ctx, const DCHAR *ciphers)
{

    D32S *ciphersuites = NULL, *tmp = NULL, id = 0;
    DCHAR *cipherstr = NULL, *p = NULL, *last = NULL, c = 0;
    size_t len = 0;

    if (ciphers == NULL)
    {
        return -1;
    }

    cipherstr = adm_strdup(ciphers);

    if (cipherstr == NULL)
    {
        return -1;
    }

    for (p = cipherstr, last = p;; p++) {
        if (*p == ':' || *p == 0) {
            c = *p;
            *p = 0;

            id = mbedtls_ssl_get_ciphersuite_id(last);

            if (id != 0) {
                tmp = (D32S *)realloc(ciphersuites, (len + 2) * sizeof(D32S));

                if (tmp == NULL) {
                    adm_free(ciphersuites);
                    adm_free(cipherstr);

                    return -1;
                }

                ciphersuites = tmp;
                ciphersuites[len++] = id;
                ciphersuites[len] = 0;
            }

            if (c == 0)
            {
                break;
            }
            last = p + 1;
        }

        /*
         * mbedTLS expects cipher names with dashes while many sources elsewhere
         * like the Firefox wiki or Wireshark specify ciphers with underscores,
         * so simply convert all underscores to dashes to accept both notations.
         */
        else if (*p == '_') {
            *p = '-';
        }
    }

    adm_free(cipherstr);

    if (len == 0)
        return -1;

    (void)mbedtls_ssl_conf_ciphersuites(&ctx->conf, ciphersuites);
    adm_free(ciphersuites);
    return 0;
}

ADM_COM_SSL_CTX_T *adm_com_sslCtxNew(const ADM_COM_SSL_CTX_OPT_T *param)
{
    ADM_MBEDTLS_CTX_T* ctx = (ADM_MBEDTLS_CTX_T*)adm_malloc(sizeof(ADM_MBEDTLS_CTX_T));
    if (ctx == NULL)
    {
        return NULL;
    }
    mbedtls_ssl_config_init(&ctx->conf);
#if defined(MBEDTLS_SSL_CACHE_C)
    mbedtls_ssl_cache_init(&ctx->cache);
#endif
    mbedtls_x509_crt_init(&ctx->cert);
    mbedtls_x509_crt_init(&ctx->ca_cert);
    mbedtls_pk_init(&ctx->pkey);
    mbedtls_entropy_init(&ctx->entropy);
    mbedtls_ctr_drbg_init(&ctx->ctr_drbg);

    D32S mode = MBEDTLS_SSL_VERIFY_NONE;
    if (NULL != param) {
        if ((NULL != param->version) && (*param->version))
        {
            if (adm_strcmp(param->version, ADM_COM_SSL_TLS1v2) == 0)
            {
                mbedtls_ssl_conf_min_version(&ctx->conf, MBEDTLS_SSL_MAJOR_VERSION_3,MBEDTLS_SSL_MINOR_VERSION_3);
                mbedtls_ssl_conf_max_version(&ctx->conf, MBEDTLS_SSL_MAJOR_VERSION_3,MBEDTLS_SSL_MINOR_VERSION_3);
            }
            else if (adm_strcmp(param->version, ADM_COM_SSL_TLS1v3) == 0)
            {
                mbedtls_ssl_conf_min_version(&ctx->conf, MBEDTLS_SSL_MAJOR_VERSION_3,MBEDTLS_SSL_MINOR_VERSION_4);
                mbedtls_ssl_conf_max_version(&ctx->conf, MBEDTLS_SSL_MAJOR_VERSION_3,MBEDTLS_SSL_MINOR_VERSION_4);
            }
            else
            {
                mbedtls_ssl_conf_min_version(&ctx->conf, MBEDTLS_SSL_MAJOR_VERSION_3,MBEDTLS_SSL_MINOR_VERSION_3);
                mbedtls_ssl_conf_max_version(&ctx->conf, MBEDTLS_SSL_MAJOR_VERSION_3,MBEDTLS_SSL_MINOR_VERSION_4);
            }
        }

        if ((NULL != param->ciphers) && (*param->ciphers))
        {
            if (0 != __adm_com_setCiphers(ctx, param->ciphers))
            {
                log_w("ssl set ciphers error!use default ciphers");                
            }
        }

        if ((NULL != param->ca_file) && (*param->ca_file)) {
            if (mbedtls_x509_crt_parse_file(&ctx->ca_cert, param->ca_file) != 0) {
                log_e("ssl crt_file error!");
                adm_free(ctx);
                return NULL;
            }
            (void)mbedtls_ssl_conf_ca_chain(&ctx->conf, &ctx->ca_cert, NULL);
            if (mbedtls_ssl_conf_own_cert(&ctx->conf, &ctx->cert, &ctx->pkey) != 0) {
                log_e("ssl key_file check failed!");
                adm_free(ctx);
                return NULL;
            }
        }

        if ((NULL != param->crt_file) && (*param->crt_file)) {
            if (mbedtls_x509_crt_parse_file(&ctx->cert, param->crt_file) != 0) {
                log_e("ssl crt_file error!\n");
                adm_free(ctx);
                return NULL;
            }
        }
        if ((NULL!=param->key_file) && (*param->key_file)) {
#if MBEDTLS_VERSION_NUMBER > 0x03000000L
            if (mbedtls_pk_parse_keyfile(&ctx->pkey, param->key_file, NULL, NULL, NULL) != 0) {
                log_e("ssl key_file error!\n");
                adm_free(ctx);
                return NULL;
            }
#else
            if (mbedtls_pk_parse_keyfile(&ctx->pkey, param->key_file, NULL) != 0) {
                 log_e("ssl key_file error!\n");
                adm_free(ctx);
                return NULL;
            }
#endif
        }
        if (param->verify_peer == TRUE)
        {
            if ((NULL == param->key_file) || (NULL == param->crt_file))
            {
                log_e("ssl SSL_VERIFY_PEER mode need client's cert and key!\n");
                adm_free(ctx);
                return NULL;
            }
            else
            {
                mode = MBEDTLS_SSL_VERIFY_REQUIRED;
            }
        }

    }
    (void)mbedtls_ctr_drbg_seed(&ctx->ctr_drbg, mbedtls_entropy_func, &ctx->entropy, NULL, 0);

    if (mbedtls_ssl_config_defaults(&ctx->conf, param->endpoint,
        MBEDTLS_SSL_TRANSPORT_STREAM, MBEDTLS_SSL_PRESET_DEFAULT) != 0) {
        log_e("ssl config error!");
        adm_free(ctx);
        return NULL;
    }
    (void)mbedtls_ssl_conf_authmode(&ctx->conf, mode);
    (void)mbedtls_ssl_conf_rng(&ctx->conf, mbedtls_ctr_drbg_random, &ctx->ctr_drbg);

#if defined(MBEDTLS_SSL_CACHE_C)
    mbedtls_ssl_conf_session_cache(&ctx->conf, &ctx->cache, mbedtls_ssl_cache_get, mbedtls_ssl_cache_set);
#endif
    return ctx;
}

void adm_com_sslCtxFree(ADM_COM_SSL_CTX_T *cssl_ctx)
{
    if (NULL != cssl_ctx)
    {
        ADM_MBEDTLS_CTX_T *mctx = (ADM_MBEDTLS_CTX_T *)cssl_ctx;
        (void)mbedtls_x509_crt_free(&mctx->cert);
        (void)mbedtls_x509_crt_free(&mctx->ca_cert);
        (void)mbedtls_pk_free(&mctx->pkey);
        (void)mbedtls_ssl_config_free(&mctx->conf);

    #if defined(MBEDTLS_SSL_CACHE_C)
        mbedtls_ssl_cache_free(&mctx->cache);
    #endif
        (void)mbedtls_ctr_drbg_free(&mctx->ctr_drbg);
        (void)mbedtls_entropy_free(&mctx->entropy);
        adm_free(mctx);
    }    
}

ADM_COM_SSL_T *adm_com_sslNew(ADM_COM_SSL_CTX_T *cssl_ctx, D32S fd)
{
    D32S ret = 0;
    ADM_MBEDTLS_CTX_T* mctx = (ADM_MBEDTLS_CTX_T*)cssl_ctx;
    //此处申请mbedtls_ssl_context + mbedtls_net_context的内存大小
    //但后续使用ADM_COM_SSL_T时，除free外都当作mbedtls_ssl_context使用
    ADM_MBEDTLS_SSL_T* mssl = (ADM_MBEDTLS_SSL_T*)adm_malloc(sizeof(ADM_MBEDTLS_SSL_T));
    if (mssl != NULL)
    {
        (void)mbedtls_ssl_init(&mssl->ssl);
        ret = mbedtls_ssl_setup(&mssl->ssl, &mctx->conf);
        if (0 == ret)
        {
            mssl->net.fd = fd;
            //mbedtls_ssl_set_bio(cssl, (void*)(intptr_t)fd, __mbedtls_net_send, __mbedtls_net_recv, NULL);
            (void)mbedtls_ssl_set_bio(&mssl->ssl, &mssl->net, mbedtls_net_send, mbedtls_net_recv, NULL);
        }
        else
        {
            adm_free(mssl);
            mssl = NULL;
            log_e("mbedtls_ssl_setup error(%d)", ret);
        }

    }
    return mssl;
}

void adm_com_sslFree(ADM_COM_SSL_T *cssl)
{
    ADM_MBEDTLS_SSL_T* mssl = (ADM_MBEDTLS_SSL_T*)cssl;
    if (NULL != mssl)
    {
        (void)mbedtls_ssl_free(&mssl->ssl);
        (void)mbedtls_net_free(&mssl->net);
        adm_free(mssl);
    }
}

static void adm_com_ssl_verify(ADM_COM_SSL_T *ssl)
{
    D8U errstr[ADM_COM_SSL_MAX_CONF_ERRSTR];
    D32S r = 0;

    r = mbedtls_ssl_get_verify_result((mbedtls_ssl_context *)ssl);
    if (r != 0)
    {
        (void)mbedtls_x509_crt_verify_info( errstr, sizeof( errstr ), "  ! ", r );
        log_i("SSL certificate error(-0x%X): %s\n", (D32U)r, errstr);
    }
}

static D32S adm_com_ssl_handshale(ADM_COM_SSL_T *cssl) {
    D32S ret = mbedtls_ssl_handshake((mbedtls_ssl_context *)cssl);
    if (ret == MBEDTLS_ERR_SSL_WANT_READ) {
        return ADM_SSL_WANT_READ;
    }
    else if (ret == MBEDTLS_ERR_SSL_WANT_WRITE) {
        return ADM_SSL_WANT_WRITE;
    }
    else if (ret == 0)
    {
        adm_com_ssl_verify(cssl);
        return ADM_SSL_OK;
    }
    else
    {
        __adm_com_sslStrError(ret, "adm_com_ssl_handshake() failed");
    }
    return ADM_SSL_ERROR;
}

D32S adm_com_sslAccept(ADM_COM_SSL_T *cssl)
{
    return adm_com_ssl_handshale(cssl);
}

D32S adm_com_sslConnect(ADM_COM_SSL_T *cssl)
{
    return adm_com_ssl_handshale(cssl);
}

D32S adm_com_sslRead(ADM_COM_SSL_T *cssl, DCHAR *buf, D32S len)
{
    D32S ret = mbedtls_ssl_read((mbedtls_ssl_context *)cssl, buf, len);
    if (ret <= 0)
    {
        if (ret == MBEDTLS_ERR_SSL_WANT_READ)
        {
            return ADM_SSL_WANT_READ;
        }
        else if (ret == MBEDTLS_ERR_SSL_WANT_WRITE)
        {
            return ADM_SSL_WANT_WRITE;
        }
        else
        {
            __adm_com_sslStrError(ret, "adm_com_sslRead() failed");
        }
        return ADM_SSL_ERROR;
    }
    return ret;
}

D32S adm_com_sslWrite(ADM_COM_SSL_T *cssl, const DCHAR *buf, D32S len)
{
    D32S ret = mbedtls_ssl_write((mbedtls_ssl_context *)cssl, buf, len);
    if (ret <= 0)
    {
        if (ret == MBEDTLS_ERR_SSL_WANT_READ)
        {
            return ADM_SSL_WANT_READ;
        }
        else if (ret == MBEDTLS_ERR_SSL_WANT_WRITE)
        {
            return ADM_SSL_WANT_WRITE;
        }
        else
        {
            __adm_com_sslStrError(ret, "adm_com_sslWrite() failed");
        }
        return ADM_SSL_ERROR;
    }
    return ret;
}

D32S adm_com_sslClose(ADM_COM_SSL_T *cssl)
{
    (void)cssl;
    return 0;
}
#endif

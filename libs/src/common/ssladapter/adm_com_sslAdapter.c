#include <stddef.h>
#include "adm_com_sslAdapter.h"
#ifdef USE_OPENSSL
#include "adm_string.h"
#include <errno.h>
#include "openssl/ssl.h"
#include "openssl/err.h"
#include "adm_debug.h"

#define ADM_COM_SSL_TLS1v3 "TLSv1.3"
#define ADM_COM_SSL_TLS1v2 "TLSv1.2"
#define ADM_COM_SSL_MAX_CONF_ERRSTR 512

static void __adm_com_sslStrError(const DCHAR *fmt)
{
    D32S flags = 0;
    const DCHAR *data = NULL;
    DCHAR *p = NULL;
    const DCHAR *first = NULL, *last = NULL;
    D8U errstr[ADM_COM_SSL_MAX_CONF_ERRSTR] = {0};
    DSIZE n = 0;
    ptrdiff_t diff = 0;

    p = errstr;
    first = errstr;
    last = errstr + ADM_COM_SSL_MAX_CONF_ERRSTR;

    errstr[ADM_COM_SSL_MAX_CONF_ERRSTR - 1] = 0;

    if ((unsigned long)0 != ERR_peek_error())
    {
        for (;;)
        {
            n = ERR_peek_error_line_data(NULL, NULL, &data, &flags);
            if (n == (DSIZE)0)
            {
                break;
            }

            if ((p != first) && (p < last))
            {
                *p++ = ' ';
            }

            /* ERR_error_string_n() requires at least one byte */
            if (p >= (last - 1))
            {
                (void)ERR_get_error();
                continue;
            }
            diff = last - p;
            ERR_error_string_n(n, p, (DSIZE)diff);
            while ((p < last) && *p)
            {
                p++;
            }

            if ((p < last) && *data && (flags & ERR_TXT_STRING))
            {
                *p++ = ':';

                while ((p < last) && *data)
                {
                    *p = *data;
                    p++;
                    data++;
                }
            }
            (void)ERR_get_error();
        }
    }
    log_e("%s %s",fmt, errstr);
}

ADM_COM_SSL_CTX_T *adm_com_sslCtxNew(const ADM_COM_SSL_CTX_OPT_T *param)
{
    static D32S s_initialized = 0;
    if (s_initialized == 0)
    {
#if OPENSSL_VERSION_NUMBER < 0x10100000L
        (void)SSL_library_init();
        (void)SSL_load_error_strings();
#else
        (void)OPENSSL_init_ssl(OPENSSL_INIT_SSL_DEFAULT, NULL);
#endif
        s_initialized = 1;
    }

#if OPENSSL_VERSION_NUMBER < 0x10100000L
    SSL_CTX *ctx = SSL_CTX_new(SSLv23_method());
#else
    SSL_CTX *ctx = SSL_CTX_new(TLS_method());
#endif
    if (ctx == NULL)
    {
        return NULL;
    }
    D32S mode = SSL_VERIFY_NONE;
    const DCHAR *ca_file = NULL;
    const DCHAR *ca_path = NULL;
    if (NULL != param)
    {
        if ((NULL != param->ciphers) && *param->ciphers)
        {
            if (!SSL_CTX_set_cipher_list(ctx, param->ciphers))
            {
                log_e("ssl ciphers failed!\n");
                SSL_CTX_free(ctx);
                return NULL;
            }
        }
        if ((NULL != param->version) && *param->version)
        {
            if (adm_strcmp(param->version, ADM_COM_SSL_TLS1v2) == 0)
            {
                (void)SSL_CTX_set_min_proto_version(ctx, TLS1_2_VERSION);
                (void)SSL_CTX_set_max_proto_version(ctx, TLS1_2_VERSION);
            }
            else if (adm_strcmp(param->version, ADM_COM_SSL_TLS1v3) == 0)
            {
                (void)SSL_CTX_set_min_proto_version(ctx, TLS1_3_VERSION);
                (void)SSL_CTX_set_max_proto_version(ctx, TLS1_3_VERSION);
            }
            else
            {
                (void)SSL_CTX_set_min_proto_version(ctx, TLS1_2_VERSION);
                (void)SSL_CTX_set_max_proto_version(ctx, TLS1_3_VERSION);
            }
        }

        if ((NULL != param->ca_file) && *param->ca_file)
        {
            ca_file = param->ca_file;
        }
        if ((NULL != param->ca_path) && *param->ca_path)
        {
            ca_path = param->ca_path;
        }
        if ((NULL != ca_file) || (NULL != ca_path))
        {
            if (!SSL_CTX_load_verify_locations(ctx, ca_file, ca_path))
            {
                log_e("ssl ca_file/ca_path failed!\n");
                SSL_CTX_free(ctx);
                return NULL;
            }
        }

        if ((NULL != param->crt_file) && *param->crt_file)
        {
            if (!SSL_CTX_use_certificate_file(ctx, param->crt_file, SSL_FILETYPE_PEM))
            {
                log_e("ssl crt_file error!\n");
                SSL_CTX_free(ctx);
                return NULL;
            }
        }

        if ((NULL != param->password) && *param->password)
        {
            SSL_CTX_set_default_passwd_cb_userdata(ctx, (void *)param->password);
        }

        if ((NULL != param->key_file) && *param->key_file)
        {
            if (!SSL_CTX_use_PrivateKey_file(ctx, param->key_file, SSL_FILETYPE_PEM))
            {
                log_e("ssl key_file error!\n");
                SSL_CTX_free(ctx);
                return NULL;
            }
            if (!SSL_CTX_check_private_key(ctx))
            {
                log_e("ssl key_file check failed!\n");
                SSL_CTX_free(ctx);
                return NULL;
            }
        }

        if (param->verify_peer == TRUE)
        {
            if ((NULL == param->key_file) || (NULL == param->crt_file))
            {
                log_e("ssl SSL_VERIFY_PEER mode need client's cert and key!\n");
                SSL_CTX_free(ctx);
                return NULL;
            }
            else
            {
                mode = SSL_VERIFY_PEER|SSL_VERIFY_FAIL_IF_NO_PEER_CERT;
            }
        }
    }///< end of if (NULL != param)

    /** verifying the peer without any CA certificates won't
       work so use openssl's built-in default as fallback */
    if ((SSL_VERIFY_NONE != mode) && (NULL== ca_file) && (NULL == ca_path))
    {
        (void)SSL_CTX_set_default_verify_paths(ctx);
    }

    SSL_CTX_set_verify(ctx, mode, NULL);
    return ctx;
}

void adm_com_sslCtxFree(ADM_COM_SSL_CTX_T *cssl_ctx)
{
    if (NULL != cssl_ctx)
    {
        SSL_CTX_free((SSL_CTX *)cssl_ctx);
    }    
}

ADM_COM_SSL_T *adm_com_sslNew(ADM_COM_SSL_CTX_T *cssl_ctx, D32S fd)
{
    SSL *cssl = SSL_new((SSL_CTX *)cssl_ctx);
    if (cssl != NULL)
    {
        (void)SSL_set_fd(cssl, fd);
    }    
    return cssl;
}

void adm_com_sslFree(ADM_COM_SSL_T *cssl)
{
    if (NULL != cssl)
    {
        SSL_free((SSL *)cssl);
        cssl = NULL;
    }
}

D32S adm_com_sslAccept(ADM_COM_SSL_T *cssl)
{
    ERR_clear_error();
    D32S ret = SSL_accept((SSL *)cssl);
    if (ret == 1)
    {
        ret = (D32S)SSL_get_verify_result(cssl);
        if (ret != X509_V_OK)
        {
            log_e("WARNING: SSL certificate error(%d): %s\n", ret, X509_verify_cert_error_string(ret));
        }
        return ADM_SSL_OK;
    }
    ret = SSL_get_error((SSL *)cssl, ret);
    D32S err = (ret == SSL_ERROR_SYSCALL) ? errno : 0;
    if (ret == SSL_ERROR_WANT_READ)
    {
        return ADM_SSL_WANT_READ;
    }
    else if (ret == SSL_ERROR_WANT_WRITE)
    {
        return ADM_SSL_WANT_WRITE;
    }
    else if (ret == SSL_ERROR_ZERO_RETURN || (0 == (D32S)ERR_peek_error()))
    {
        log_e("peer closed connection in SSL handshake, errno %d", err);
    }
    else
    {
        __adm_com_sslStrError("adm_com_sslAccept() failed");
    }
    return ADM_SSL_ERROR;
}

D32S adm_com_sslConnect(ADM_COM_SSL_T *cssl)
{
    ERR_clear_error();
    D32S ret = SSL_connect((SSL *)cssl);
    if (ret == 1)
    {
        ret = (D32S)SSL_get_verify_result(cssl);
        if (ret != X509_V_OK)
        {
            log_e("WARNING: SSL certificate error(%d): %s\n", ret, X509_verify_cert_error_string(ret));
        }
        return ADM_SSL_OK;
    }
    ret = SSL_get_error((SSL *)cssl, ret);
    D32S err = (ret == SSL_ERROR_SYSCALL) ? errno : 0;
    if (ret == SSL_ERROR_WANT_READ)
    {
        return ADM_SSL_WANT_READ;
    }
    else if (ret == SSL_ERROR_WANT_WRITE)
    {
        return ADM_SSL_WANT_WRITE;
    }
    else if (ret == SSL_ERROR_ZERO_RETURN || (0 == (D32S)ERR_peek_error()))
    {
        log_e("peer closed connection in SSL handshake, errno %d", err);
    }
    else
    {
        __adm_com_sslStrError("adm_com_sslConnect() failed");
    }
    return ADM_SSL_ERROR;
}

D32S adm_com_sslRead(ADM_COM_SSL_T *cssl, DCHAR *buf, D32S len)
{
    ERR_clear_error();
    D32S ret = SSL_read((SSL *)cssl, buf, len);
    if (ret <= 0)
    {
        ret = SSL_get_error((SSL *)cssl, ret);
        D32S err = (ret == SSL_ERROR_SYSCALL) ? errno : 0;
        if (ret == SSL_ERROR_WANT_READ)
        {
            return ADM_SSL_WANT_READ;
        }
        else if (ret == SSL_ERROR_WANT_WRITE)
        {
            return ADM_SSL_WANT_WRITE;
        }
        else if (ret == SSL_ERROR_ZERO_RETURN || (0 == (D32S)ERR_peek_error()))
        {
            log_e("peer closed connection in SSL read, errno %d", err);
        }
        else
        {
            __adm_com_sslStrError("adm_com_sslRead() failed");
        }
        return ADM_SSL_ERROR;
    }
    return ret;
}

D32S adm_com_sslWrite(ADM_COM_SSL_T *cssl, const DCHAR *buf, D32S len)
{
    ERR_clear_error();
    D32S ret = SSL_write((SSL *)cssl, buf, len);
    if (ret <= 0)
    {
        ret = SSL_get_error((SSL *)cssl, ret);
        D32S err = (ret == SSL_ERROR_SYSCALL) ? errno : 0;
        if (ret == SSL_ERROR_WANT_READ)
        {
            return ADM_SSL_WANT_READ;
        }
        else if (ret == SSL_ERROR_WANT_WRITE)
        {
            return ADM_SSL_WANT_WRITE;
        }
        else if (ret == SSL_ERROR_ZERO_RETURN || (0 == (D32S)ERR_peek_error()))
        {
            log_e("peer closed connection in SSL write, errno %d", err);
        }
        else
        {
            __adm_com_sslStrError("adm_com_sslWrite() failed");
        }
        return ADM_SSL_ERROR;
    }
    return ret;
}

D32S adm_com_sslClose(ADM_COM_SSL_T *cssl)
{
    (void)SSL_shutdown((SSL *)cssl);
    return 0;
}
#endif ///< WITH_OPENSSL

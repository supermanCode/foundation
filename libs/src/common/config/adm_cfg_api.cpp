#include "adm_cfg_api.h"
#include "adm_string.h"
#include "adm_memory.h"
#include "adm_iniparser.h"
#include "adm_debug.h"

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "CFG"       //!< 重定义LOG TAG 为CFG

static Dictionary_t* g_dict = NULL;

D32S adm_cfg_iniParserInit(const DCHAR* filename)
{
    // DBOOL isFilenameOK = FALSE;
    // const DCHAR* iniPath = NULL;
    D32S iRet = ADM_COM_CFG_API_ERR_DEFAULT;

    do {
        if (NULL == filename) {
            iRet = ADM_COM_CFG_API_ERR_FILE_NOT_EXIST;
            log_e("Input parameter [filename] is nil, iRet:%d", iRet);
            break;
        }

        /** 加载 ini 配置文件 */
        g_dict = adm_iniparser_load(filename);
        if (NULL == g_dict) {
            iRet = ADM_COM_CFG_API_INI_PARSER_FAILED;
            log_e("adm_iniparser_load failed, ini path:[%s], iRet:%d", filename, iRet);
            break;
        } else {
            iRet = ADM_COM_CFG_API_ERR_SUCCESS;
            log_i("adm_iniparser_load successed");
            break;
        }
    } while(FALSE);

    return iRet;
}

void adm_cfg_iniParserDeinit(void)
{
    if (NULL != g_dict) {
        adm_iniparser_freedict(g_dict);
    }
}


const DCHAR* adm_cfg_iniParserGetString(const DCHAR* key, const DCHAR* notfound)
{
    const DCHAR* str = NULL;

    do {
        str = notfound;
        if (NULL == g_dict) {
            log_e("g_dict is nil, please call adm_cfg_iniParserInit to init ini config");
            break;
        }

        str = adm_iniparser_getstring(g_dict, key, notfound);
        log_i("[%s = %s]", key, str);
    } while(FALSE);

    return str;
}

D32S adm_cfg_iniParserGetInt(const DCHAR* key, D32S notfound)
{
    D32S intVal = 0;

    do {
        intVal = notfound;
        if (NULL == g_dict) {
           log_e("g_dict is nil, please call adm_cfg_iniParserInit to init ini config");
            break; 
        }

        intVal = adm_iniparser_getint(g_dict, key, notfound);
        log_i("[%s = %d]", key, intVal);
    } while(FALSE);

    return intVal;
}

long int adm_cfg_iniParserGetLongInt(const DCHAR* key, long int notfound)
{
    long int val = 0;

    do {
        val = notfound;
        if (NULL == g_dict) {
           log_e("g_dict is nil, please call adm_cfg_iniParserInit to init ini config");
            break; 
        }

        val = adm_iniparser_getlongint(g_dict, key, notfound);
        log_i("[%s = %ld]", key, val);

    } while(FALSE);

    return val;
}

double adm_cfg_iniParserGetDouble(const DCHAR* key, double notfound)
{
    double val = 0.0;

    do {
        val = notfound;
        if (NULL == g_dict) {
           log_e("g_dict is nil, please call adm_cfg_iniParserInit to init ini config");
            break; 
        }

        val = adm_iniparser_getdouble(g_dict, key, notfound);
        log_i("[%s = %lf]", key, val);
    } while(FALSE);

    return val;
}

DBOOL adm_cfg_iniParserGetBoolean(const DCHAR* key, DBOOL notfound)
{
    DBOOL val = FALSE;

    do {
        val = notfound;
        if (NULL == g_dict) {
           log_e("g_dict is nil, please call adm_cfg_iniParserInit to init ini config");
            break; 
        }

        val = (DBOOL)adm_iniparser_getboolean(g_dict, key, (int)notfound);
        log_i("[%s = %d]", key, val);
    } while(FALSE);

    return val;
}

#include "async/adm_asyncCall.h"

#include <mutex>
#include <map>
#include <iostream>
#include "threadPool.h"

namespace abup
{
namespace ota
{
namespace async
{

class AsyncCallImpl
{
public:
    AsyncCallImpl(const size_t threads)
        : threadPool_(threads)
        , threads_(threads)
    {}
    ~AsyncCallImpl()
    {}
    void Attach(const service_t serviceId, const instance_t instanceId, const event_t eventId, eventMessageCB cb);
    void Detach(const service_t serviceId, const instance_t instanceId, const event_t eventId);
    void Detach(const service_t serviceId, const instance_t instanceId);
    void Detach(const service_t serviceId);
    void AddEventMessageToCb(const service_t serviceId,
        const instance_t instanceId,
        const event_t eventId,
        const std::string& eventMessage);

private:
    mutable std::mutex eventCbmembersMutex_;
    std::map<service_t, std::map<instance_t, std::map<event_t, eventMessageCB>>> eventCbmembers_;
    ThreadPool threadPool_;
    size_t threads_;
};

void AsyncCallImpl::Attach(const service_t serviceId,
    const instance_t instanceId,
    const event_t eventId,
    eventMessageCB cb)
{
    std::lock_guard<std::mutex> lock(eventCbmembersMutex_);
    eventCbmembers_[serviceId][instanceId][eventId] = cb;
}

void AsyncCallImpl::Detach(const service_t serviceId, const instance_t instanceId, const event_t eventId)
{
    std::lock_guard<std::mutex> lock(eventCbmembersMutex_);
    auto found_service = eventCbmembers_.find(serviceId);
    if (found_service != eventCbmembers_.end()) {
        auto found_instance = found_service->second.find(instanceId);
        if (found_instance != found_service->second.end()) {
            auto found_event = found_instance->second.find(eventId);
            if (found_event != found_instance->second.end()) {
                found_instance->second.erase(eventId);
            }
        }
    }
}

void AsyncCallImpl::Detach(const service_t serviceId, const instance_t instanceId)
{
    std::lock_guard<std::mutex> lock(eventCbmembersMutex_);
    auto found_service = eventCbmembers_.find(serviceId);
    if (found_service != eventCbmembers_.end()) {
        auto found_instance = found_service->second.find(instanceId);
        if (found_instance != found_service->second.end()) {
            found_service->second.erase(instanceId);
        }
    }
}

void AsyncCallImpl::Detach(const service_t serviceId)
{
    std::lock_guard<std::mutex> lock(eventCbmembersMutex_);
    auto found_service = eventCbmembers_.find(serviceId);
    if (found_service != eventCbmembers_.end()) {
        eventCbmembers_.erase(serviceId);
    }
}

void AsyncCallImpl::AddEventMessageToCb(const service_t serviceId,
    const instance_t instanceId,
    const event_t eventId,
    const std::string& eventMessage)
{
    std::lock_guard<std::mutex> lock(eventCbmembersMutex_);

    threadPool_.enqueue([this, serviceId, instanceId, eventId, eventMessage]() -> void {
        auto found_service = this->eventCbmembers_.find(serviceId);
        if (found_service != this->eventCbmembers_.end()) {
            auto found_instance = found_service->second.find(instanceId);
            if (found_instance != found_service->second.end()) {
                auto found_event = found_instance->second.find(eventId);
                if (found_event != found_instance->second.end()) {
                    if (found_event->second) {
                        found_event->second(eventMessage);
                    } else {
                        std::cout << "serviceID = " << serviceId << ", ";
                        std::cout << "instanceID = " << instanceId << ", ";
                        std::cout << "eventID = " << eventId << ", ";
                        std::cout << "cb is nullptr.Error" << std::endl;
                    }
                } else {
                    std::cout << "Error.not found cb eventId = " << eventId << std::endl;
                }
            } else {
                std::cout << "Error.not found cb instanceId = " << instanceId << std::endl;
            }
        } else {
            std::cout << "Error.not found cb serviceId = " << serviceId << std::endl;
        }
        return;
    });
}

AsyncCall::AsyncCall(const size_t threads)
{
    impl_ = std::make_shared<AsyncCallImpl>(threads);
}

AsyncCall::~AsyncCall()
{}

void AsyncCall::Attach(const service_t serviceId, const instance_t instanceId, const event_t eventId, eventMessageCB cb)
{
    impl_->Attach(serviceId, instanceId, eventId, cb);
}

void AsyncCall::Detach(const service_t serviceId, const instance_t instanceId, const event_t eventId)
{
    impl_->Detach(serviceId, instanceId, eventId);
}

void AsyncCall::Detach(const service_t serviceId, const instance_t instanceId)
{
    impl_->Detach(serviceId, instanceId);
}

void AsyncCall::Detach(const service_t serviceId)
{
    impl_->Detach(serviceId);
}

void AsyncCall::AddEventMessageToCb(const service_t serviceId,
    const instance_t instanceId,
    const event_t eventId,
    const std::string& eventMessage)
{
    impl_->AddEventMessageToCb(serviceId, instanceId, eventId, eventMessage);
}

}  // namespace async
}  // namespace ota
}  // namespace abup
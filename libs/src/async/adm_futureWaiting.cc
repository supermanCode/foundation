#include "async/adm_futureWaiting.h"

namespace abup
{
namespace ota
{
namespace async
{

FutureWaitingCenter::FutureWaitingCenter()
{}

FutureWaitingCenter::~FutureWaitingCenter()
{}

std::future<std::string> FutureWaitingCenter::makeFuture(const service_t serviceId,
    const instance_t instanceId,
    const event_t eventId,
    const reqId_t reqId)
{
    std::lock_guard<std::mutex> lock(mMutex);

    std::unique_ptr<std::promise<std::string>> promise;
    promise.reset(new std::promise<std::string>());
    auto future = promise->get_future();

    auto found_service = mPromiseLists.find(serviceId);
    if (found_service != mPromiseLists.end()) {
        auto found_instance = found_service->second.find(instanceId);
        if (found_instance != found_service->second.end()) {
            auto found_event = found_instance->second.find(eventId);
            if (found_event != found_instance->second.end()) {
                /*the request has been in process and will be replaced*/
                //found_event->second->set_value("");
                //found_instance->second.erase(eventId);

				auto found_reqId = found_event->second.find(reqId);
				if (found_reqId != found_event->second.end()) {
                /*the request has been in process and will be replaced*/
                found_reqId->second->set_value("");
                found_event->second.erase(reqId);
            	}
            }
        }
    }

    mPromiseLists[serviceId][instanceId][eventId][reqId] = std::move(promise);
    return std::move(future);
}

bool FutureWaitingCenter::setValue(const service_t serviceId,
    const instance_t instanceId,
    const event_t eventId,
    const reqId_t reqId,
    const std::string& data,
    std::string* errorStr)
{
    std::lock_guard<std::mutex> lock(mMutex);

    auto found_service = mPromiseLists.find(serviceId);
    if (found_service != mPromiseLists.end()) {
        auto found_instance = found_service->second.find(instanceId);
        if (found_instance != found_service->second.end()) {
            auto found_event = found_instance->second.find(eventId);
            if (found_event != found_instance->second.end()) {
                //found_event->second->set_value(data);
                //found_instance->second.erase(eventId);

				auto found_reqId = found_event->second.find(reqId);
				if (found_reqId != found_event->second.end()) {
                	found_reqId->second->set_value(data);
					found_event->second.erase(reqId);
				}
                return true;
            }
        }
    }
    /*Could not find the eventId*/
    *errorStr = "serviceId = " + std::to_string(serviceId) + "  instanceId = " + std::to_string(instanceId)
        + "  eventId = " + std::to_string(eventId) + " ; Could not find the Id";
    return false;
}

bool FutureWaitingCenter::setEmptyValue(const service_t serviceId,
    const instance_t instanceId,
    const event_t eventId,
    const reqId_t reqId,
    std::string* errorStr)
{
    std::lock_guard<std::mutex> lock(mMutex);

    auto found_service = mPromiseLists.find(serviceId);
    if (found_service != mPromiseLists.end()) {
        auto found_instance = found_service->second.find(instanceId);
        if (found_instance != found_service->second.end()) {
            auto found_event = found_instance->second.find(eventId);
            if (found_event != found_instance->second.end()) {
                //found_event->second->set_value("");
                //found_instance->second.erase(eventId);
                
				auto found_reqId = found_event->second.find(reqId);
	            if (found_reqId != found_event->second.end()) {
	                found_reqId->second->set_value("");
	                found_event->second.erase(reqId);
	                return true;
	            }
            }
        }
    }
    *errorStr = "serviceId = " + std::to_string(serviceId) + "  instanceId = " + std::to_string(instanceId)
        + "  eventId = " + std::to_string(eventId) +  "  reqId = " + std::to_string(reqId) + " ; Could not find the Id";
    return false;
}

}  // namespace async
}  // namespace ota
}  // namespace abup
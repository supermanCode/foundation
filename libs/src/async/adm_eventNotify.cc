#include "async/adm_eventNotify.h"
#include <iostream>

namespace abup
{
namespace ota
{
namespace async
{

EventNotify::EventNotify()
    : waitFlag_(false)
{}

EventNotify::~EventNotify()
{}

void EventNotify::notify()
{
    std::unique_lock<std::mutex> lock(mutex_);
    condition_.notify_one();
    waitFlag_ = true;
}

void EventNotify::NotifyReset()
{
    std::unique_lock<std::mutex> lock(mutex_);
    waitFlag_ = false;
}

bool EventNotify::waitFor(const uint16_t timeout)
{
    std::unique_lock<std::mutex> lock(mutex_);
    if (!waitFlag_) {
        if (std::cv_status::timeout == condition_.wait_for(lock, std::chrono::seconds(timeout))) {
            std::cout << "--- wait timeout" << std::endl;
            waitFlag_ = false;
            return false;
        }
    }
    waitFlag_ = false;
    return true;
}

}  // namespace async
}  // namespace ota
}  // namespace abup
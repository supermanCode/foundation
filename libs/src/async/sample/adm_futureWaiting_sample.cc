#include "async/adm_futureWaiting.h"
#include <thread>
#include <iostream>

int main()
{
    abup::ota::async::FutureWaitingCenter futureWaite;

    auto future1 = futureWaite.makeFuture(1, 2, 3, 1);
    auto future2 = futureWaite.makeFuture(1, 2, 3, 2);
    auto future3 = futureWaite.makeFuture(1, 2, 3, 3);
    //auto future4 = futureWaite.makeFuture(7, 8, 9);
    //auto future5 = futureWaite.makeFuture(7, 5, 9);

    //auto future8 = futureWaite.makeFuture(7, 8, 9);

    std::thread t([&futureWaite]() {
        std::string errorStr;

        if (!futureWaite.setValue(1, 2, 3, 1, "klj1", &errorStr)) {
            std::cout << "errorStr = " << errorStr << std::endl;
        }
        std::this_thread::sleep_for(std::chrono::seconds(1));

        if (!futureWaite.setValue(1, 2, 3, 1, "abc1", &errorStr)) {
            std::cout << "errorStr = " << errorStr << std::endl;
        }
        std::this_thread::sleep_for(std::chrono::seconds(1));

        if (!futureWaite.setValue(1, 2, 3, 2, "asd2", &errorStr)) {
            std::cout << "errorStr = " << errorStr << std::endl;
        }
        std::this_thread::sleep_for(std::chrono::seconds(1));

        if (!futureWaite.setValue(1, 2, 3, 3, "sdf3", &errorStr)) {
            std::cout << "errorStr = " << errorStr << std::endl;
        }
        std::this_thread::sleep_for(std::chrono::seconds(1));

/*
        if (!futureWaite.setEmptyValue(7, 5, 9, &errorStr)) {
            std::cout << "errorStr = " << errorStr << std::endl;
        }
        std::this_thread::sleep_for(std::chrono::seconds(5));

        if (!futureWaite.setValue(7, 8, 9, "sdffg", &errorStr)) {
            std::cout << "errorStr = " << errorStr << std::endl;
        }
        std::this_thread::sleep_for(std::chrono::seconds(1));
*/
    });
    t.detach();

    if (future1.wait_for(std::chrono::seconds(3)) == std::future_status::ready) {
        std::cout << "future1  = " << future1.get() << std::endl;
    } else {
        std::cout << "wait error future1" << std::endl;
    }

    if (future2.wait_for(std::chrono::seconds(3)) == std::future_status::ready) {
        std::cout << "future2  = " << future2.get() << std::endl;
    } else {
        std::cout << "wait error future2" << std::endl;
    }

    if (future3.wait_for(std::chrono::seconds(3)) == std::future_status::ready) {
        std::cout << "future3  = " << future3.get() << std::endl;
    } else {
        std::cout << "wait error future3" << std::endl;
    }
/*
    if (future4.wait_for(std::chrono::seconds(3)) == std::future_status::ready) {
        std::cout << "future4  = " << future4.get() << std::endl;
    } else {
        std::cout << "wait error future4" << std::endl;
    }

    if (future5.wait_for(std::chrono::seconds(3)) == std::future_status::ready) {
        std::cout << "future5  = " << future5.get() << std::endl;
    } else {
        std::cout << "wait error future5" << std::endl;
    }

    if (future8.wait_for(std::chrono::seconds(30)) == std::future_status::ready) {
        std::cout << "future8  = " << future8.get() << std::endl;
    } else {
        std::cout << "wait error future8" << std::endl;
    }
*/
    while (1) {
        std::this_thread::sleep_for(std::chrono::seconds(3));
        break;
    }
    return 0;
}
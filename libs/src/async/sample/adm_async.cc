#include "async/adm_asyncCall.h"
#include "async/adm_eventNotify.h"
#include <thread>
#include <iostream>

void AsyncCalltest1(const std::string resp)
{
    std::cout << "test1 resp = " << resp << std::endl;
}

void AsyncCalltest2(const std::string resp)
{
    std::cout << "test2 resp = " << resp << std::endl;
}

int main()
{
    /*asyncCall*/
    abup::ota::async::AsyncCall call(1);

    call.Attach(1, 2, 3, AsyncCalltest1);
    call.Attach(4, 5, 6, AsyncCalltest2);
    call.Attach(3, 5, 7, nullptr);
    std::thread t([&call]() {
        call.AddEventMessageToCb(1, 2, 3, "adv");
        std::this_thread::sleep_for(std::chrono::seconds(1));
        call.AddEventMessageToCb(4, 5, 6, "asd");
        std::this_thread::sleep_for(std::chrono::seconds(1));
        call.AddEventMessageToCb(3, 5, 7, "sdf");
        std::this_thread::sleep_for(std::chrono::seconds(1));
    });
    t.detach();
    std::this_thread::sleep_for(std::chrono::seconds(5));
    call.Detach(4, 5, 6);
    call.AddEventMessageToCb(4, 5, 6, "asd");

    /*EventNotify*/
    std::shared_ptr<abup::ota::async::EventNotify> event = std::make_shared<abup::ota::async::EventNotify>();
    std::thread t1([event]() {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        event->notify();
        std::this_thread::sleep_for(std::chrono::seconds(1));
        event->notify();
    });
    t1.detach();
    if (!event->waitFor(3)) {
        std::cout << "timeout" << std::endl;
    } else {
        std::cout << "wait success" << std::endl;
    }
    std::this_thread::sleep_for(std::chrono::seconds(3));
    if (!event->waitFor(3)) {
        std::cout << "timeout" << std::endl;
    } else {
        std::cout << "wait success" << std::endl;
    }

    while (1) {
        std::this_thread::sleep_for(std::chrono::seconds(3));
        break;
    }
    return 0;
}
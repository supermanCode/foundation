#include "adm_nm.h"
#include "adm_string.h"
#include <string.h>
#include "adm_debug.h"

#include "DataStruct.h"

#include <map>
#include <list>
#include <string>
#include <deque>

#include <unistd.h>
#include "DataTypeDescription.h"

#include "adm_com_thread.h"



#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG     "NM"    //!< Notify Manager 日志TAG


using namespace std;

#define ADM_NM_NOTIFYIDLEN          32
#define ADM_NM_MAINLOCK             "ADM_NM_MAINLOCK"
#define ADM_NM_ASYNCLOCK            "ADM_NM_ASYNCLOCK"
#define ADM_NM_ASYNCSEM             "ADM_NM_ASYNCSEM"

/** NM异步线程名 */
#define NM_ASYNC_NOTIFY             "nmAsyncNtf"

/**
 * @struct    adm_nm_item_struct
 * @brief     单个通知结构
 * @details   detail description
 */
typedef struct adm_nm_item_struct
{
    const char *nmName;                  ///< 事件名
    ADM_NOTIFY_CB fncb;                  ///< 通知回调
    DBOOL isSync;                        ///< 是否同步, DTRUE为同步, DFALSE为异步
    DBOOL needKV;                        ///< 是否存KV
    const vus::DataTypeDef *pObjDef;     ///< 结构体描述对象
    void *pParam;                        ///< 回调参数
    void *pItemNMData;                   ///< 通知数据
    D32U  dataSize;                      ///< 异步队列时记录通知数据大小
    D32U  sessionID;                     ///< 会话id,request和session使用，普通notify不使用
} adm_nm_item_t, *adm_nm_item_pt;

typedef list<adm_nm_item_pt> item_list;

/** 会话id  , session或request请求时使用 */
static D32U gSessionId = 0;

/** 会话id  , session时响应给上层,用作释放深拷贝 */
typedef struct
{
    D32U sessionID;
    //void* pContext;
} adm_nm_Context_t;

typedef struct
{
    char    nmName[ADM_NM_NOTIFYIDLEN];           ///<事件ID
    D32U sessionId;                               ///<会话ID，自动生成，每次请求累加
    ADM_NOTIFYRESP_CB fnResp;                     ///< request 的响应回调
    void *pContext;                               ///< request 上下文
    const vus::DataTypeDef *pRespDef;             ///< 异步转同步时响应数据结构描述
    void *pRespData;                              ///< 异步转同步时响应数据
    D32U  respDataSize;                           ///< 异步转同步时响应数据大小
} adm_nm_respItem_t, *adm_nm_respItem_pt;
typedef map<D32U, adm_nm_respItem_pt> nm_respItem_map;
typedef struct
{
    DHANDLE respItemLock;
    DHANDLE respItemCond;
    nm_respItem_map m_nmResp_map;
} adm_nm_resp_ctrl_t;

static adm_nm_resp_ctrl_t m_nmResp_ctrl;


/**
 * @struct    adm_nm_list_struct
 * @brief     通知的链表结构
 * @details   事件对应多个订阅者
 */
typedef struct adm_nm_list_struct
{
    char    nmName[ADM_NM_NOTIFYIDLEN];   ///<事件ID
    D32S    kvNumber;                     ///< KV订阅者
    const vus::DataTypeDef *pObjDef;    ///< notify出去的描述对象
    DHANDLE itemLock;                     //事件锁
    D32S    curKvCount;                   ///< KV订阅者计数
    const vus::DataTypeDef *pRespDef; ///< 异步转同步时响应数据结构描述,注册时绑定
    item_list nmItems;                    ///< 订阅者链表
} adm_nm_list_t, *adm_nm_list_pt;

typedef map<string, adm_nm_list_pt> adm_nm_map;

/**
 * @struct    adm_nm_ctrl_struct
 * @brief     通知管理, 增加线程锁变量，用于上锁和解锁动作
 * @details   detail description
 */
typedef struct adm_nm_ctrl_struct
{
    DHANDLE hMainLock;
    DHANDLE hAsyncLock;               /* 异步通知锁 */
    DHANDLE hAsyncSem;                /* 异步通知信号 */
    DHANDLE hAsyncThread;             /* 异步线程句柄 */
    DBOOL   IsAsyncThreadExit;
    deque<adm_nm_item_pt> asyncItems;  /* 异步通知队列 */
    adm_nm_map nmMap;                  /* 通知map管理 */
} adm_nm_ctrl_t, *adm_nm_ctrl_pt;

static adm_nm_ctrl_t m_nmCtrl;

static adm_nm_list_pt nmInitItems(const DCHAR *notifyID)
{
    adm_nm_list_pt pNMItems = new adm_nm_list_t;

    strncpy(pNMItems->nmName, notifyID, ADM_NM_NOTIFYIDLEN);

    pNMItems->nmName[ADM_NM_NOTIFYIDLEN - 1] = 0;

    pNMItems->kvNumber = 0;
    pNMItems->curKvCount = 0;
    pNMItems->pObjDef = NULL;

    pNMItems->pRespDef = DNULL;
    adm_com_threadMutexCreate(&(pNMItems->itemLock));

    return pNMItems;
}

static D32S nmUpdateNotifyDef(adm_nm_list_pt pNMItems, const vus::DataTypeDef *pNotifyObjDef)
{
    pNMItems->pObjDef = pNotifyObjDef;

    /** 更新已注册通知对应的描述 */
    item_list *pNotifyItems = &(pNMItems->nmItems);
    item_list::iterator iterItem = pNotifyItems->begin();
    item_list::iterator iterEnd = pNotifyItems->end();

    while (iterItem != iterEnd)
    {
        (*iterItem)->pObjDef = pNotifyObjDef;
        iterItem++;
    }

    return ADM_NM_OK;
}
static D32S nmUpdateRespDef(adm_nm_list_pt pNMItems, const vus::DataTypeDef *pRespObjDef)
{
    /** 更新响应数据的结构体描述 */
    pNMItems->pRespDef = pRespObjDef;

    return ADM_NM_OK;
}
static D32S nm_resp_init()
{
    adm_com_threadMutexCreate(&(m_nmResp_ctrl.respItemLock));
    adm_com_threadCondCreate(&(m_nmResp_ctrl.respItemCond));

    return ADM_NM_OK;
}
static void nm_resp_uninit()
{
    adm_com_threadMutexDestroy(m_nmResp_ctrl.respItemLock);
    adm_com_threadCondDestroy(m_nmResp_ctrl.respItemCond);
}

static void *nmAsyncNMHander(void *argv)
{
    adm_nm_ctrl_t *pNMCtrl = (adm_nm_ctrl_t *)argv;

    adm_nm_item_pt pItem = DNULL;

    do
    {
        if (DNULL == pNMCtrl)
        {
            break;
        }

        adm_com_threadSemWait(pNMCtrl->hAsyncSem);

        /**/
        if (pNMCtrl->IsAsyncThreadExit == DTRUE)
        {
            break;
        }

        while (DTRUE)
        {
            if (pNMCtrl->asyncItems.size() > 0)
            {
                adm_com_threadMutexLock(pNMCtrl->hAsyncLock);
                pItem = pNMCtrl->asyncItems.front();
                pNMCtrl->asyncItems.pop_front();
                adm_com_threadMutexUnlock(pNMCtrl->hAsyncLock);
            }
            else
            {
                break;
            }

            /** 注册端回调,抛出通知 */
            pItem->fncb(pItem->nmName, pItem->sessionID, pItem->pItemNMData, pItem->pParam);

            //释放存在描述的深拷贝
            if (DNULL != pItem->pObjDef)
            {
                vus::DataStructDestructor destructor(pItem->pObjDef);
                destructor.destroy(pItem->pItemNMData, pItem->dataSize);
            }

            adm_free(pItem->pItemNMData);
            pItem->pItemNMData = DNULL;
            //pItem->dataSize  = 0;

            delete pItem;
        }

    }
    while (DTRUE);

    return DNULL;
}

static item_list::iterator nmFindItem(item_list *pNotifyItems, ADM_NOTIFY_CB fncb)
{
    item_list::iterator iterItem = pNotifyItems->begin();
    item_list::iterator iterEnd = pNotifyItems->end();

    while (iterItem != iterEnd)
    {
        if ((*iterItem)->fncb == fncb)
        {
            //iterItem;
            break;
        }

        iterItem++;
    }

    return iterItem;
}

/**
 * @function   nmDMInited
 * @brief
 * @details    DM初始化完成,通知NM, NM加载KV中存的未ACK通知，重新通知支持KV的通知项
 *
 * @param    [in]
 * @param    [in]
 * @param    [in]
 * @return
 * @calls        :
 * @called By    :
 * @note   :
 */
static D32S nmDMInited(const char *notifyID, D32U sessionID, const void *pData, void *pParam)
{

    return ADM_NM_OK;
}

static D32S nm_setdef(const char *notifyID, const vus::DataTypeDef *pObjDef,
                      const vus::DataTypeDef *pObjDefResp)
{
    D32S retCode = ADM_NM_OK;

    do
    {
        adm_nm_list_pt pNMItems = DNULL;

        if (DNULL == notifyID)
        {
            retCode = ADM_NM_INVALIDNOTIFYID;
            break;
        }

        string strNotifyID = notifyID;

        /** 操作注册队列时，锁定 */
        adm_com_threadMutexLock(m_nmCtrl.hMainLock);

        adm_nm_map::iterator iter = m_nmCtrl.nmMap.find(strNotifyID);

        if (iter == m_nmCtrl.nmMap.end())
        {
            /** 当前ID，未注册过 */
            pNMItems = nmInitItems(notifyID);

            nmUpdateNotifyDef(pNMItems, pObjDef);
            nmUpdateRespDef(pNMItems, pObjDefResp);

            m_nmCtrl.nmMap[strNotifyID] = pNMItems;
        }
        else
        {
            pNMItems = iter->second;

            nmUpdateNotifyDef(pNMItems, pObjDef);
            nmUpdateRespDef(pNMItems, pObjDefResp);
        }

        adm_com_threadMutexUnlock(m_nmCtrl.hMainLock);

    }
    while (DFALSE);

    return retCode;
}

static D32S nm_creatRespCtrl(adm_nm_list_pt pNMItems, ADM_NOTIFYRESP_CB fnResp, D32U *sessionId,
                             void *pContext)
{
    adm_com_threadMutexLock(m_nmResp_ctrl.respItemLock);

    gSessionId++;
    adm_nm_respItem_pt pRespItem = DNULL;

    nm_respItem_map::iterator iter = m_nmResp_ctrl.m_nmResp_map.find(gSessionId);

    if (iter == m_nmResp_ctrl.m_nmResp_map.end())
    {
        pRespItem = new adm_nm_respItem_t;
        pRespItem->fnResp = fnResp;
        pRespItem->pContext = pContext;
        pRespItem->pRespDef = pNMItems->pRespDef;    //初始化绑定的响应描述
        pRespItem->pRespData = DNULL;
        pRespItem->respDataSize = 0;
        pRespItem->sessionId = gSessionId;
        strncpy(pRespItem->nmName, pNMItems->nmName, ADM_NM_NOTIFYIDLEN);

        m_nmResp_ctrl.m_nmResp_map[gSessionId] = pRespItem;
    }
    else
    {
        pRespItem = iter->second;

        pRespItem->fnResp = fnResp;
        pRespItem->pContext = pContext;
        pRespItem->pRespDef = pNMItems->pRespDef;
        pRespItem->sessionId = gSessionId;
    }

    adm_com_threadMutexUnlock(m_nmResp_ctrl.respItemLock);

    if (DNULL != sessionId)
    {
        *sessionId = gSessionId;
    }

    return ADM_NM_OK;
}

//创建深拷贝使用的描述
static vus::DataTypeDef *nm_creat_structDescrip(const char *pStructDescrip)
{
    vus::DataTypeDef *pType = DNULL;
    D32S retCode = ADM_NM_OK;

    do
    {
        if (DNULL == pStructDescrip)
        {
            break;
        }

        vus::DataTypeDescriptionParser parser(pStructDescrip);
        parser.init();
        pType = new vus::DataTypeDef;
        retCode = parser.parse(pType);

        if (ADM_NM_OK != retCode)
        {
            vus::DataTypeDescriptionParser::destroyType(pType);
            delete pType;
            pType = DNULL;
        }

    }
    while (DFALSE);

    return pType;
}


static D32S nmNotify(adm_nm_list_pt pNMItems, void *pNotifyData, D32U notifyDataSize,
                     D32U sessionId)
{
    D32S retCode = ADM_NM_OK;
    void *pAsyncNtf = DNULL;

    do
    {
        DASSERT(pNMItems != DNULL);

        item_list *pNotifyItems = &(pNMItems->nmItems);
        D32U asyncCount = 0;    ///< 异步通知计数

        /** 锁定当前通知 */
        adm_com_threadMutexLock(pNMItems->itemLock);

        pNMItems->curKvCount = pNMItems->kvNumber;
        item_list::iterator iterItem = pNotifyItems->begin();
        item_list::iterator iterEnd = pNotifyItems->end();

        while (iterItem != iterEnd)
        {
            adm_nm_item_pt pItem = (*iterItem);

            if (pItem->isSync == DFALSE)
            {
                adm_com_threadMutexLock(m_nmCtrl.hAsyncLock);
                /** 推送到异步线程队列中，同时复制数据 */
                adm_nm_item_pt pAsyncItem = new adm_nm_item_t;
                *pAsyncItem = *pItem;
                pAsyncItem->sessionID = sessionId;

                /** 深拷贝方式 */
                if ((DNULL != pNotifyData) && (0 < notifyDataSize))
                {
                    pAsyncNtf = adm_malloc(notifyDataSize);

                    if (DNULL != pNMItems->pObjDef)
                    {
                        // 存在描述的深拷贝
                        vus::DataStructCloner cloner(pNMItems->pObjDef);
                        cloner.clone(pNotifyData, notifyDataSize, pAsyncNtf, notifyDataSize);
                    }
                    else
                    {
                        //不存在描述 浅拷贝
                        adm_memcpy(pAsyncNtf, pNotifyData, notifyDataSize, notifyDataSize);
                    }
                }

                pAsyncItem->pItemNMData = pAsyncNtf;
                pAsyncItem->dataSize = notifyDataSize;

                m_nmCtrl.asyncItems.push_back(pAsyncItem);
                adm_com_threadMutexUnlock(m_nmCtrl.hAsyncLock);
                /* ASYNC Thread*/
                asyncCount++;
            }
            else
            {
                /* callback */
                retCode = pItem->fncb(pNMItems->nmName, sessionId, pNotifyData, pItem->pParam);
            }

            iterItem++;
        }

        if (pNMItems->kvNumber > 0)
        {
            /** KV 存储 */
            // notifyID
            // void* pNotifyParam
        }
        else {}

        adm_com_threadMutexUnlock(pNMItems->itemLock);

        if (asyncCount > 0)
        {
            /** 触发信号量 */
            adm_com_threadSemPost(m_nmCtrl.hAsyncSem);
        }
        else {}
    }
    while (DFALSE);

    return retCode;
}


D32S adm_nm_init()
{
    /** 内存申请初始化        */
    //adm_meminit();

    D32S retCode = ADM_NM_OK;
    /* 初始化线程锁*/
    adm_com_threadMutexCreate(&(m_nmCtrl.hMainLock));
    adm_com_threadMutexCreate(&(m_nmCtrl.hAsyncLock));
    adm_com_threadSemCreate(&(m_nmCtrl.hAsyncSem), 0, 0);
    m_nmCtrl.IsAsyncThreadExit = DFALSE;

    DASSERT(m_nmCtrl.nmMap.size() == 0);

    /* 注册DM初始化启动完成通知，可以恢复KV注册内容 */
    retCode = adm_nm_register(ADM_NID_DMINITED, nmDMInited, (void *)DNULL, ADM_NM_FLAG_SYNC);
    DASSERT(retCode == ADM_NM_OK);

    retCode = nm_resp_init();

    /** 创建线程处理异步队列 */
    if (ADM_OS_ECODE_SUCCESSED != adm_com_threadCreate(&(m_nmCtrl.hAsyncThread), nmAsyncNMHander,
            &m_nmCtrl, NM_ASYNC_NOTIFY))
    {
        retCode = ADM_NM_THREADCREATEERROR;
    }

    return retCode;
}

D32S adm_nm_uninit()
{
    D32S retCode = adm_nm_unregister(ADM_NID_DMINITED, nmDMInited);

    if (ADM_NM_OK != retCode)
    {
        //log_e("adm_nm_unregister ADM_NID_DMINITED failed");
    }

    m_nmCtrl.IsAsyncThreadExit = DTRUE;
    adm_com_threadSemPost(m_nmCtrl.hAsyncSem);

    /** 反初始化时,避免设置等待信号量过程中,信号量被释放
    * 后续使用公共函数
    */
    struct timeval tv;
    tv.tv_sec  = 1;
    tv.tv_usec = 0;
    (void)select(0, NULL, NULL, NULL, &tv);


    /*清除Notify Manager 中未注销的通知，正常应该为空*/
    adm_com_threadMutexLock(m_nmCtrl.hMainLock);
    adm_nm_map::iterator iter = m_nmCtrl.nmMap.begin();
    adm_nm_map::iterator iterEnd = m_nmCtrl.nmMap.end();

    while (iter != iterEnd)
    {
        adm_nm_list_pt pNMItems = iter->second;
        item_list *pNotifyItems = &(pNMItems->nmItems);
        /* 清除注册的通知 */
        item_list::iterator iterItem = pNotifyItems->begin();
        item_list::iterator iterItemEnd = pNotifyItems->end();

        while (iterItem != iterItemEnd)
        {
            delete *iterItem;
            iterItem++;
        }

        adm_com_threadMutexDestroy(pNMItems->itemLock);

        pNotifyItems->clear();

        delete pNMItems;
        iter++;
    }

    m_nmCtrl.nmMap.clear();
    adm_com_threadMutexUnlock(m_nmCtrl.hMainLock);

    adm_com_threadMutexDestroy(m_nmCtrl.hMainLock);
    adm_com_threadMutexDestroy(m_nmCtrl.hAsyncLock);
    adm_com_threadSemDestroy(m_nmCtrl.hAsyncSem);

    nm_resp_uninit();

    /** 内存申请反初始化        */
    //adm_memuninit();

    return ADM_NM_OK;
}

D32S adm_nm_register(const char *notifyID, ADM_NOTIFY_CB fncb, void *pParam, D32U flag)
{
    D32S retCode = ADM_NM_OK;

    do
    {
        adm_nm_list_pt pNMItems = DNULL;

        if (DNULL == notifyID)
        {
            retCode = ADM_NM_INVALIDNOTIFYID;
            break;
        }

        if (DNULL == fncb)
        {
            retCode = ADM_NM_INVALIDCBFN;
            break;
        }

        string strNotifyID = notifyID;

        /* 操作注册队列时，锁定*/
        adm_com_threadMutexLock(m_nmCtrl.hMainLock);
        item_list *pNewItems = DNULL;

        adm_nm_map::iterator iter = m_nmCtrl.nmMap.find(strNotifyID);

        if (iter == m_nmCtrl.nmMap.end())
        {
            /* 当前ID，未注册过 */
            pNMItems = nmInitItems(notifyID);

            m_nmCtrl.nmMap[strNotifyID] = pNMItems;
        }
        else
        {
            pNMItems = iter->second;
            pNewItems = &(pNMItems->nmItems);

            /** 订阅者链表中find订阅者 */
            item_list::iterator findItem = nmFindItem(pNewItems, fncb);

            if (findItem != pNewItems->end())
            {
                retCode = ADM_NM_HASREGISTED;
            }
            else {}
        }

        if (retCode == ADM_NM_OK)
        {
            /** 新增订阅者 */
            adm_nm_item_pt pNewItem = DNULL;

            pNewItems = &(pNMItems->nmItems);
            pNewItem = new adm_nm_item_t;
            pNewItem->nmName = pNMItems->nmName;
            pNewItem->fncb = fncb;
            pNewItem->pParam = pParam;
            pNewItem->pObjDef = pNMItems->pObjDef;
            pNewItem->pItemNMData = DNULL;
            pNewItem->dataSize = 0;
            pNewItem->sessionID = 0;

            if (DCHECKFLAG(flag, ADM_NM_FLAG_ASYNC))
            {
                pNewItem->isSync = DFALSE;
            }
            else
            {
                pNewItem->isSync = DTRUE;
            }

            if (DCHECKFLAG(flag, ADM_NM_FLAG_KV))
            {
                pNewItem->needKV = DTRUE;
                pNMItems->kvNumber++;
            }
            else
            {
                pNewItem->needKV = DFALSE;
            }

            pNewItems->push_back(pNewItem);
        }

        /* 是否支持KV保存 */
        if (DCHECKFLAG(flag, ADM_NM_FLAG_KV))
        {
            DASSERT(pNMItems != DNULL);
            pNMItems->kvNumber++;
        }
        else {}

        adm_com_threadMutexUnlock(m_nmCtrl.hMainLock);

    }
    while (DFALSE);

    return retCode;
}

D32S adm_nm_unregister(const char *notifyID, ADM_NOTIFY_CB fncb)
{
    D32S retCode = ADM_NM_OK;

    do
    {
        if (notifyID == DNULL)
        {
            retCode = ADM_NM_INVALIDNOTIFYID;
            break;
        }

        if (fncb == DNULL)
        {
            retCode = ADM_NM_INVALIDCBFN;
            break;
        }

        string strNotifyID = notifyID;
        adm_com_threadMutexLock(m_nmCtrl.hMainLock);
        adm_nm_map::iterator iter = m_nmCtrl.nmMap.find(strNotifyID);

        if (iter != m_nmCtrl.nmMap.end())
        {
            adm_nm_list_pt pNMItems = iter->second;
            item_list *pNotifyItems = &(pNMItems->nmItems);

            adm_com_threadMutexLock(pNMItems->itemLock);
            item_list::iterator iterItem = nmFindItem(pNotifyItems, fncb);

            //注销单个订阅者
            if (iterItem != pNotifyItems->end())
            {
                adm_nm_item_pt pItem = *iterItem;
                pNotifyItems->erase(iterItem);

                if (pItem->needKV == DTRUE)
                {
                    DASSERT(pNMItems->kvNumber >= 0);
                    pNMItems->kvNumber--;
                }

                DASSERT(pItem->pItemNMData == DNULL);

                if (DNULL != pItem->pObjDef)
                {
                    vus::DataTypeDescriptionParser::destroyType(pItem->pObjDef);
                    delete pItem->pObjDef;  ///< 通知描述释放
                }

                delete pItem;
            }

            //遗留问题 整个订阅list都注销了
            //目前只支持1对1
            if (DNULL != pNMItems->pRespDef)
            {
                vus::DataTypeDescriptionParser::destroyType(pNMItems->pRespDef);
                delete pNMItems->pRespDef;  ///< 响应描述释放
            }

            adm_com_threadMutexUnlock(pNMItems->itemLock);

            /** 通知list为空 */
            if (pNMItems->nmItems.empty())
            {
                adm_com_threadMutexDestroy(pNMItems->itemLock);
                delete pNMItems;
                m_nmCtrl.nmMap.erase(iter);
            }
        }
        else
        {
            retCode = ADM_NM_NOTREGISTED;
        }

        adm_com_threadMutexUnlock(m_nmCtrl.hMainLock);
    }
    while (DFALSE);

    return retCode;
}

D32S adm_nm_notify(const char *notifyID, void *pData, D32U dataSize)
{
    D32S retCode = ADM_NM_OK;

    do
    {
        if (DNULL == notifyID)
        {
            retCode = ADM_NM_INVALIDNOTIFYID;
            break;
        }

        string strNotifyID = notifyID;

        adm_nm_map::iterator iter = m_nmCtrl.nmMap.find(strNotifyID);

        if (iter != m_nmCtrl.nmMap.end())
        {
            adm_nm_list_pt pNMItems = iter->second;
            retCode = nmNotify(pNMItems, pData, dataSize, 0);
        }
        else
        {
            retCode = ADM_NM_NOTREGISTED;
        }
    }
    while (DFALSE);

    return retCode;
}

D32S adm_nm_request(const char *notifyID, void *pData, D32U dataSize,
                    ADM_NOTIFYRESP_CB fnResp, void *pContext)
{
    D32S retCode = ADM_NM_OK;
    D32U localSessionid = 0;

    do
    {
        if (DNULL == notifyID)
        {
            retCode = ADM_NM_INVALIDNOTIFYID;
            break;
        }

        string strNotifyID = notifyID;

        adm_nm_map::iterator iter = m_nmCtrl.nmMap.find(strNotifyID);

        if (iter != m_nmCtrl.nmMap.end())
        {
            adm_nm_list_pt pNMItems = iter->second;

            nm_creatRespCtrl(pNMItems, fnResp, &localSessionid, pContext);
            retCode = nmNotify(pNMItems, pData, dataSize, localSessionid);
        }
        else
        {
            retCode = ADM_NM_NOTREGISTED;
        }
    }
    while (DFALSE);

    return retCode;
}

static D32S nmRespHandler(const char *notifyID, const void *pData, D32U respSize, void *pContext)
{
    D32S retCode = ADM_NM_OK;

    void *pRespData = DNULL;

    do
    {
        if (DNULL == notifyID)
        {
            retCode = ADM_NM_INVALIDNOTIFYID;
            break;
        }

        adm_nm_Context_t *pSessionId = (adm_nm_Context_t *)pContext;

        if (DNULL == pSessionId)
        {
            retCode = ADM_NM_NOTREGISTED;
            break;
        }

        nm_respItem_map::iterator iter = m_nmResp_ctrl.m_nmResp_map.find(pSessionId->sessionID);

        if (iter != m_nmResp_ctrl.m_nmResp_map.end())
        {
            adm_nm_respItem_pt pRespItems = iter->second;

            adm_com_threadMutexLock(m_nmResp_ctrl.respItemLock);

            if ((DNULL != pData) && (respSize > 0))
            {
                //响应数据深拷贝
                pRespData = adm_malloc(respSize);

                if (DNULL != pRespItems->pRespDef)
                {
                    vus::DataStructCloner cloner(pRespItems->pRespDef);
                    cloner.clone(pData, respSize, pRespData, respSize);
                }
                else
                {
                    adm_memcpy(pRespData, pData, respSize, respSize);
                }
            }

            pRespItems->pRespData = pRespData;
            pRespItems->respDataSize = respSize;
            pRespItems->pContext = pContext;

            adm_com_threadCondSignal(m_nmResp_ctrl.respItemCond);

            adm_com_threadMutexUnlock(m_nmResp_ctrl.respItemLock);
        }
    }
    while (DFALSE);

    return retCode;
}

//删除参数 const vus::DataTypeDef *pRespDef
D32S adm_nm_session(const char *notifyID, void *pData, D32U dataSize,
                    D32U msTime, void *pRespData, D32U *sessionId)
{
    D32S retCode = ADM_NM_OK;
    D32U localSessionid = 0;

    do
    {
        if (notifyID == DNULL)
        {
            retCode = ADM_NM_INVALIDNOTIFYID;
            break;
        }

        string strNotifyID = notifyID;
        adm_nm_map::iterator iter = m_nmCtrl.nmMap.find(strNotifyID);

        if (iter != m_nmCtrl.nmMap.end())
        {
            adm_nm_list_pt pNMItems = iter->second;

            nm_creatRespCtrl(pNMItems, nmRespHandler, &localSessionid, DNULL);

            retCode = nmNotify(pNMItems, pData, dataSize, localSessionid);

            //session注册为同步时暂不支持, 超时
            adm_com_threadMutexLock(m_nmResp_ctrl.respItemLock);

            if (ADM_OS_ECODE_SUCCESSED != adm_com_threadCondWait(m_nmResp_ctrl.respItemCond,
                    m_nmResp_ctrl.respItemLock, msTime))
            {
                retCode = ADM_NM_REQUEST_TIMEOUT;
            }

            /** 深拷贝的响应数据需外部释放
            * 修改为一级指针, 外面已知结构体,申请最外层内存
            */
            //*pRespData = pNMItems->pRespData;
            auto iter = m_nmResp_ctrl.m_nmResp_map.find(localSessionid);

            if (iter != m_nmResp_ctrl.m_nmResp_map.end())
            {
                adm_nm_respItem_pt pRespItems = iter->second;

                if ((DNULL != pRespData) && (DNULL != pRespItems->pRespData))
                {
                    adm_memcpy(pRespData, pRespItems->pRespData, pRespItems->respDataSize, pRespItems->respDataSize);
                }

                if (DNULL != sessionId)
                {
                    *sessionId = localSessionid;
                }
            }

            adm_com_threadMutexUnlock(m_nmResp_ctrl.respItemLock);

        }
        else
        {
            retCode = ADM_NM_NOTREGISTED;
        }
    }
    while (DFALSE);

    return retCode;
}

/** 释放响应时深拷贝数据 */
D32S adm_nm_respData_Free(D32U sessionId)
{
    D32S retCode = ADM_NM_OK;

    do
    {
        adm_com_threadMutexLock(m_nmResp_ctrl.respItemLock);

        auto iter = m_nmResp_ctrl.m_nmResp_map.find(sessionId);

        if (iter != m_nmResp_ctrl.m_nmResp_map.end())
        {
            adm_nm_respItem_pt pRespItem = iter->second;

            if (DNULL != pRespItem->pRespData)
            {
                //说明是session接口，返回时有深拷贝
                if (DNULL != pRespItem->pRespDef)
                {
                    vus::DataStructDestructor destructor(pRespItem->pRespDef);
                    destructor.destroy(pRespItem->pRespData, pRespItem->respDataSize);
                }

                adm_free(pRespItem->pRespData);

                pRespItem->pRespData = DNULL;
                pRespItem->respDataSize = 0;
            }

            //request无深拷贝

            //释放
            delete pRespItem;
            m_nmResp_ctrl.m_nmResp_map.erase(iter);

            //描述对象在注销时释放

        }

        adm_com_threadMutexUnlock(m_nmResp_ctrl.respItemLock);
    }
    while (DFALSE);

    return retCode;
}

D32S adm_nm_response(const char *notifyID, D32U sessionID, const void *pData, D32U respSize)
{
    D32S retCode = ADM_NM_OK;

    do
    {
        if (notifyID == DNULL)
        {
            retCode = ADM_NM_INVALIDNOTIFYID;
            break;
        }

        nm_respItem_map::iterator iter = m_nmResp_ctrl.m_nmResp_map.find(sessionID);

        if (iter != m_nmResp_ctrl.m_nmResp_map.end())
        {
            adm_nm_respItem_pt pRespItem = iter->second;

            if ((0 == strncmp(pRespItem->nmName, notifyID, ADM_NM_NOTIFYIDLEN)) &&
                    (DNULL != pRespItem->fnResp))
            {
                if (nmRespHandler == pRespItem->fnResp)
                {
                    /** session */
                    adm_nm_Context_t stSessionId;
                    stSessionId.sessionID = sessionID;
                    retCode = pRespItem->fnResp(notifyID, pData, respSize, (void *)&stSessionId);
                    //外部调用  adm_nm_respData_Free
                }
                else
                {
                    /** request */
                    retCode = pRespItem->fnResp(notifyID, pData, respSize, pRespItem->pContext);
                    adm_nm_respData_Free(sessionID);
                }
            }
            else
            {
                retCode = ADM_NM_EXPIRED;
            }
        }
        else
        {
            retCode = ADM_NM_NOTREGISTED;
        }
    }
    while (DFALSE);

    return retCode;
}

D32S adm_nm_notifyack(const char *notifyID, ADM_NOTIFY_CB fnCB)
{
    D32S retCode = ADM_NM_OK;

    do
    {
        if (notifyID == DNULL)
        {
            retCode = ADM_NM_INVALIDNOTIFYID;
            break;
        }

        string strNotifyID = notifyID;

        adm_nm_map::iterator iter = m_nmCtrl.nmMap.find(strNotifyID);

        if (iter != m_nmCtrl.nmMap.end())
        {
            adm_nm_list_pt pNMItems = iter->second;
            item_list *pNotifyItems = &(pNMItems->nmItems);

            /* 锁定当前通知 */
            adm_com_threadMutexLock(pNMItems->itemLock);
            item_list::iterator iterItem = pNotifyItems->begin();
            item_list::iterator iterEnd = pNotifyItems->end();

            while (iterItem != iterEnd)
            {
                adm_nm_item_pt pItem = (*iterItem);

                if (pItem->fncb == fnCB)
                {
                    DASSERT(pItem->needKV == DTRUE);
                    pNMItems->curKvCount--;

                    if (pNMItems->curKvCount == 0)
                    {
                        /* 清除KV */
                    }

                    break;
                }
                else {}

                iterItem++;
            }

            adm_com_threadMutexUnlock(pNMItems->itemLock);
        }
        else
        {
            retCode = ADM_NM_NOTREGISTED;
        }
    }
    while (DFALSE);

    return retCode;
}

D32S adm_nm_structDescrip(const char *notifyID, const char *pStructDescrip,
                          const char *pRespDescrip)
{
    D32S retCode = ADM_NM_OK;

    vus::DataTypeDef *pTypeNotify = DNULL;
    vus::DataTypeDef *pTypeResp = DNULL;

    do
    {
        if (DNULL == notifyID)
        {
            retCode = ADM_NM_INVALIDPARAM;
            break;
        }

        if (DNULL != pStructDescrip)
        {
            pTypeNotify = nm_creat_structDescrip(pStructDescrip);
        }

        if (DNULL != pRespDescrip)
        {
            pTypeResp = nm_creat_structDescrip(pRespDescrip);
        }

        retCode = nm_setdef(notifyID, pTypeNotify, pTypeResp);
    }
    while (DFALSE);

    return retCode;
}




#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <ctype.h>


#include "adm_string.h"
#include "adm_memory.h"
#include "adm_debug.h"

#include "adm_nm.h"
#include "vus_nm_structdescrip.h"


static int s_break = 0;

static void wait_sleep(int secs)
{
    //sleep(secs);
    struct timeval tv;
    tv.tv_sec  = secs;
    tv.tv_usec = 0;

    (void)select(0, NULL, NULL, NULL, &tv);
}


#define ADM_NM_DIM_NOTIFY_1 "dim_norify_1"
#define ADM_NM_DIM_REQUSET_2 "dim_request_2"
#define ADM_NM_DIM_SESSION_3 "dim_session_2"



//Recv端收到 adm_nm_notify
static D32S dim_req_1_cb(const char *notifyID, D32U sessionID, const void *pData, void *pParam)
{
    log_i("dim_req_1_cb");
    log_i("sessionID=%u", sessionID);

    ADM_VDI_READDIAG_T *pReadDiag = (ADM_VDI_READDIAG_T *)pData;
    log_i("pReadDiag=%p", pReadDiag);

    log_i("pstEcuIdenInfo.requestId = %#x", pReadDiag->pstEcuIdenInfo.requestId);
    log_i("pstEcuIdenInfo.responseId = %#x", pReadDiag->pstEcuIdenInfo.responseId);
    log_i("pstEcuIdenInfo.functionId = %#x", pReadDiag->pstEcuIdenInfo.functionId);
    log_i("pstEcuIdenInfo.swDid = %#x", pReadDiag->pstEcuIdenInfo.swDid);
    log_i("diagScriptPath = %s", pReadDiag->diagScriptPath);
    log_i("eBusType = %d", pReadDiag->eBusType);
    log_i("eChannelType = %d", pReadDiag->eChannelType);

    log_i("didArray=%p", pReadDiag->didListRequest.didArray);

    log_i("didArray[0].did = %d", pReadDiag->didListRequest.didArray[0].did);
    log_i("didArray[0].startByte = %d", pReadDiag->didListRequest.didArray[0].startByte);
    log_i("didArray[0].endByte = %d", pReadDiag->didListRequest.didArray[0].endByte);
    log_i("didArray[0].errorCode = %d", pReadDiag->didListRequest.didArray[0].errorCode);
    log_i("didArray[1].did = %d", pReadDiag->didListRequest.didArray[1].did);
    log_i("didArray[1].startByte = %d", pReadDiag->didListRequest.didArray[1].startByte);
    log_i("didArray[1].endByte = %d", pReadDiag->didListRequest.didArray[1].endByte);
    log_i("didArray[1].errorCode = %d", pReadDiag->didListRequest.didArray[1].errorCode);


    return 0;
}

//adm_nm_request---Recv端 收到----adm_nm_response响应----走adm_nm_request带的回调
static D32S dim_req_2_cb(const char *notifyID, D32U sessionID, const void *pData, void *pParam)
{
    log_i("dim_req_2_cb");
    log_i("sessionID=%u", sessionID);

    ADM_VDI_READDIAG_T *pReadDiag = (ADM_VDI_READDIAG_T *)pData;

    log_i("pstEcuIdenInfo.requestId = %#x", pReadDiag->pstEcuIdenInfo.requestId);
    log_i("pstEcuIdenInfo.responseId = %#x", pReadDiag->pstEcuIdenInfo.responseId);
    log_i("pstEcuIdenInfo.functionId = %#x", pReadDiag->pstEcuIdenInfo.functionId);
    log_i("pstEcuIdenInfo.swDid = %#x", pReadDiag->pstEcuIdenInfo.swDid);
    log_i("diagScriptPath = %s", pReadDiag->diagScriptPath);
    log_i("eBusType = %d", pReadDiag->eBusType);
    log_i("eChannelType = %d", pReadDiag->eChannelType);

    log_i("didArray[0].did = %d", pReadDiag->didListRequest.didArray[0].did);
    log_i("didArray[0].startByte = %d", pReadDiag->didListRequest.didArray[0].startByte);
    log_i("didArray[0].endByte = %d", pReadDiag->didListRequest.didArray[0].endByte);
    log_i("didArray[0].errorCode = %d", pReadDiag->didListRequest.didArray[0].errorCode);
    log_i("didArray[1].did = %d", pReadDiag->didListRequest.didArray[1].did);
    log_i("didArray[1].startByte = %d", pReadDiag->didListRequest.didArray[1].startByte);
    log_i("didArray[1].endByte = %d", pReadDiag->didListRequest.didArray[1].endByte);
    log_i("didArray[1].errorCode = %d", pReadDiag->didListRequest.didArray[1].errorCode);


    ADM_VDI_READDIAG_RESP_T stResp;
    stResp.errroCode = 22;
    stResp.stEcuIdenInfo.requestId = pReadDiag->pstEcuIdenInfo.requestId;
    stResp.stEcuIdenInfo.responseId = pReadDiag->pstEcuIdenInfo.responseId;
    stResp.stEcuIdenInfo.functionId = pReadDiag->pstEcuIdenInfo.functionId;
    stResp.stEcuIdenInfo.swDid = pReadDiag->pstEcuIdenInfo.swDid;
    adm_nm_response(notifyID, sessionID, (void *)&stResp, sizeof(ADM_VDI_READDIAG_RESP_T));

    return 0;
}

//adm_nm_session----Recv端 收到----adm_nm_response响应----走异步转同步
static D32S dim_req_3_cb(const char *notifyID, D32U sessionID, const void *pData, void *pParam)
{
    log_i("dim_req_3_cb");
    log_i("sessionID=%u", sessionID);

    ADM_VDI_READDIAG_T *pReadDiag = (ADM_VDI_READDIAG_T *)pData;

    log_i("pstEcuIdenInfo.requestId = %#x", pReadDiag->pstEcuIdenInfo.requestId);
    log_i("pstEcuIdenInfo.responseId = %#x", pReadDiag->pstEcuIdenInfo.responseId);
    log_i("pstEcuIdenInfo.functionId = %#x", pReadDiag->pstEcuIdenInfo.functionId);
    log_i("pstEcuIdenInfo.swDid = %#x", pReadDiag->pstEcuIdenInfo.swDid);
    log_i("diagScriptPath = %s", pReadDiag->diagScriptPath);
    log_i("eBusType = %d", pReadDiag->eBusType);
    log_i("eChannelType = %d", pReadDiag->eChannelType);

    log_i("didArray[0].did = %d", pReadDiag->didListRequest.didArray[0].did);
    log_i("didArray[0].startByte = %d", pReadDiag->didListRequest.didArray[0].startByte);
    log_i("didArray[0].endByte = %d", pReadDiag->didListRequest.didArray[0].endByte);
    log_i("didArray[0].errorCode = %d", pReadDiag->didListRequest.didArray[0].errorCode);
    log_i("didArray[1].did = %d", pReadDiag->didListRequest.didArray[1].did);
    log_i("didArray[1].startByte = %d", pReadDiag->didListRequest.didArray[1].startByte);
    log_i("didArray[1].endByte = %d", pReadDiag->didListRequest.didArray[1].endByte);
    log_i("didArray[1].errorCode = %d", pReadDiag->didListRequest.didArray[1].errorCode);

    //异步队列线程睡5s,超时验证
    //wait_sleep(5);

    //响应数据申请
    ADM_VDI_READDIAG_RESP_T *pRespData =
        (ADM_VDI_READDIAG_RESP_T *)adm_malloc(sizeof(ADM_VDI_READDIAG_RESP_T));
    
    D32U respSize = sizeof(ADM_VDI_READDIAG_RESP_T);

    pRespData->stEcuIdenInfo.requestId = 0x111;
    pRespData->stEcuIdenInfo.responseId = 0x222;
    pRespData->stEcuIdenInfo.functionId = 0x333;
    pRespData->stEcuIdenInfo.swDid = 0x444;
    pRespData->errroCode = 11;
    //队列线程等到信号后会释放
    //void * pRespDatatmp = NULL;
    adm_nm_response(notifyID, sessionID, pRespData, respSize);
    adm_free(pRespData);

    return 0;
}

static D32S dim_req_flush_cb(const char *notifyID, D32U sessionID, const void *pData, void *pParam)
{
    log_i("dim_req_flush_cb");
    return 0;
}


//adm_nm_request ---接口带的发送端回调
static D32S dim_req_2_resp(const char *notifyID, const void *pData, D32U respSize, void *pContext)
{
    log_i("dim_req_2_resp");
    ADM_VDI_READDIAG_RESP_T *pResp = (ADM_VDI_READDIAG_RESP_T *)pData;

    log_i("stEcuIdenInfo.requestId = %#x", pResp->stEcuIdenInfo.requestId);
    log_i("stEcuIdenInfo.responseId = %#x", pResp->stEcuIdenInfo.responseId);
    log_i("stEcuIdenInfo.functionId = %#x", pResp->stEcuIdenInfo.functionId);
    log_i("stEcuIdenInfo.swDid = %#x", pResp->stEcuIdenInfo.swDid);
    log_i("errroCode = %d", pResp->errroCode);
    return 0;
}

//发送端直接通知adm_nm_notify
static D32U test_dim_send_1()
{
    log_i("test_dim_send_3 beign");
    
    ADM_VDI_READDIAG_T stReadDiag = {0};

    stReadDiag.pstEcuIdenInfo.requestId = 0x1;
    stReadDiag.pstEcuIdenInfo.responseId = 0x2;
    stReadDiag.pstEcuIdenInfo.functionId = 0x3;
    stReadDiag.pstEcuIdenInfo.swDid = 0x4;
    stReadDiag.diagScriptPath = (DCHAR *)"/data/test/nmm/tmp.txt";
    stReadDiag.eBusType = VEHICLE_BUS_TYPE_CAN;
    stReadDiag.eChannelType = VEHICLE_BUS_PIN_TYPE_B_CAN;
    stReadDiag.isCGWRouted = 1;
    stReadDiag.didListRequest.didArrayLength = 2;
    stReadDiag.didListRequest.didArray = (ADM_DIM_DID_DATA_T *)adm_malloc(
            sizeof(ADM_DIM_DID_DATA_T) * stReadDiag.didListRequest.didArrayLength);
    log_i("didArray=%p", stReadDiag.didListRequest.didArray);
    log_i("didArray.size=%u", sizeof(ADM_DIM_DID_DATA_T) * stReadDiag.didListRequest.didArrayLength);

    stReadDiag.didListRequest.didArray[0].did = 0x11;
    stReadDiag.didListRequest.didArray[0].startByte = 0x12;
    stReadDiag.didListRequest.didArray[0].endByte = 0x13;
    stReadDiag.didListRequest.didArray[0].errorCode = 0x14;
    stReadDiag.didListRequest.didArray[0].didData.dataType = 2;
    stReadDiag.didListRequest.didArray[0].didData.dataLength = 4;
    stReadDiag.didListRequest.didArray[0].didData.pValue = NULL;
    stReadDiag.didListRequest.didArray[1].did = 0x21;
    stReadDiag.didListRequest.didArray[1].startByte = 0x22;
    stReadDiag.didListRequest.didArray[1].endByte = 0x23;
    stReadDiag.didListRequest.didArray[1].errorCode = 0x24;
    stReadDiag.didListRequest.didArray[1].didData.dataType = 2;
    stReadDiag.didListRequest.didArray[1].didData.dataLength = 4;
    stReadDiag.didListRequest.didArray[1].didData.pValue = NULL;

    adm_nm_notify(ADM_NM_DIM_NOTIFY_1, (void *)&stReadDiag, sizeof(ADM_VDI_READDIAG_T));
    adm_free(stReadDiag.didListRequest.didArray);

    log_i("free test_dim_send_1");

    return 0;
}

//发送端 adm_nm_request
static D32U test_dim_send_2()
{
    ADM_VDI_READDIAG_T *pReadDiag = (ADM_VDI_READDIAG_T *)adm_malloc(sizeof(ADM_VDI_READDIAG_T));

    pReadDiag->pstEcuIdenInfo.requestId = 0x1;
    pReadDiag->pstEcuIdenInfo.responseId = 0x2;
    pReadDiag->pstEcuIdenInfo.functionId = 0x3;
    pReadDiag->pstEcuIdenInfo.swDid = 0x4;
    pReadDiag->diagScriptPath = (DCHAR *)"/data/test/nmm/tmp.txt";
    pReadDiag->eBusType = VEHICLE_BUS_TYPE_CAN;
    pReadDiag->eChannelType = VEHICLE_BUS_PIN_TYPE_B_CAN;
    pReadDiag->isCGWRouted = 1;
    pReadDiag->didListRequest.didArrayLength = 2;
    pReadDiag->didListRequest.didArray = (ADM_DIM_DID_DATA_T *)adm_malloc(
            sizeof(ADM_DIM_DID_DATA_T) * pReadDiag->didListRequest.didArrayLength);
    pReadDiag->didListRequest.didArray[0].did = 0x11;
    pReadDiag->didListRequest.didArray[0].startByte = 0x12;
    pReadDiag->didListRequest.didArray[0].endByte = 0x13;
    pReadDiag->didListRequest.didArray[0].errorCode = 0x14;
    pReadDiag->didListRequest.didArray[0].didData.dataType = 2;
    pReadDiag->didListRequest.didArray[0].didData.dataLength = 4;
    pReadDiag->didListRequest.didArray[0].didData.pValue = NULL;
    pReadDiag->didListRequest.didArray[1].did = 0x21;
    pReadDiag->didListRequest.didArray[1].startByte = 0x22;
    pReadDiag->didListRequest.didArray[1].endByte = 0x23;
    pReadDiag->didListRequest.didArray[1].errorCode = 0x24;
    pReadDiag->didListRequest.didArray[1].didData.dataType = 2;
    pReadDiag->didListRequest.didArray[1].didData.dataLength = 4;
    pReadDiag->didListRequest.didArray[1].didData.pValue = NULL;

    void *pContext = DNULL;
    adm_nm_request(ADM_NM_DIM_REQUSET_2, (void *)pReadDiag, sizeof(ADM_VDI_READDIAG_T),
                   dim_req_2_resp, pContext);

    log_i("free test_dim_send_2");

    adm_free(pReadDiag->didListRequest.didArray);

    adm_free(pReadDiag);
    return 0;
}

//发送端 adm_nm_session
static D32U test_dim_send_3()
{
    log_i("test_dim_send_3 beign");
    ADM_VDI_READDIAG_T *pReadDiag = (ADM_VDI_READDIAG_T *)adm_malloc(sizeof(ADM_VDI_READDIAG_T));

    pReadDiag->pstEcuIdenInfo.requestId = 0x1;
    pReadDiag->pstEcuIdenInfo.responseId = 0x2;
    pReadDiag->pstEcuIdenInfo.functionId = 0x3;
    pReadDiag->pstEcuIdenInfo.swDid = 0x4;
    pReadDiag->diagScriptPath = (DCHAR *)"/data/test/nmm/tmp.txt";
    pReadDiag->eBusType = VEHICLE_BUS_TYPE_CAN;
    pReadDiag->eChannelType = VEHICLE_BUS_PIN_TYPE_B_CAN;
    pReadDiag->isCGWRouted = 1;
    pReadDiag->didListRequest.didArrayLength = 2;
    pReadDiag->didListRequest.didArray = (ADM_DIM_DID_DATA_T *)adm_malloc(
            sizeof(ADM_DIM_DID_DATA_T) * pReadDiag->didListRequest.didArrayLength);
    pReadDiag->didListRequest.didArray[0].did = 0x11;
    pReadDiag->didListRequest.didArray[0].startByte = 0x12;
    pReadDiag->didListRequest.didArray[0].endByte = 0x13;
    pReadDiag->didListRequest.didArray[0].errorCode = 0x14;
    pReadDiag->didListRequest.didArray[0].didData.dataType = 2;
    pReadDiag->didListRequest.didArray[0].didData.dataLength = 4;
    pReadDiag->didListRequest.didArray[0].didData.pValue = NULL;
    pReadDiag->didListRequest.didArray[1].did = 0x21;
    pReadDiag->didListRequest.didArray[1].startByte = 0x22;
    pReadDiag->didListRequest.didArray[1].endByte = 0x23;
    pReadDiag->didListRequest.didArray[1].errorCode = 0x24;
    pReadDiag->didListRequest.didArray[1].didData.dataType = 2;
    pReadDiag->didListRequest.didArray[1].didData.dataLength = 4;
    pReadDiag->didListRequest.didArray[1].didData.pValue = NULL;

    D32U msTime = 5000;
    ADM_VDI_READDIAG_RESP_T *pRespData =
        (ADM_VDI_READDIAG_RESP_T *)adm_malloc(sizeof(ADM_VDI_READDIAG_RESP_T));

    D32U sessionId = 0;
    D32U retSession = adm_nm_session(ADM_NM_DIM_SESSION_3, (void *)pReadDiag, sizeof(ADM_VDI_READDIAG_T), msTime,
                   (void *)pRespData, &sessionId);

    log_i("retSession=%u", retSession);
    
    log_i("sessionId=%u", sessionId);
    
    log_i("stEcuIdenInfo.requestId = %#x", pRespData->stEcuIdenInfo.requestId);
    log_i("stEcuIdenInfo.responseId = %#x", pRespData->stEcuIdenInfo.responseId);
    log_i("stEcuIdenInfo.functionId = %#x", pRespData->stEcuIdenInfo.functionId);
    log_i("stEcuIdenInfo.swDid = %#x", pRespData->stEcuIdenInfo.swDid);
    log_i("errroCode = %d", pRespData->errroCode);

    adm_free(pReadDiag->didListRequest.didArray);
    adm_free(pReadDiag);

    adm_free(pRespData);
    adm_nm_respData_Free(sessionId);    //外部session时调用

    log_i("free test_dim_send_3");

    return 0;
}

void test_dim_send_4()
{
    log_i("test_dim_send_4");
    ADM_DIM_VDI_CRC_PARAM_T stReq;
    stReq.polynomial = 1;
    stReq.init_value = 2;
    stReq.initvalue2 = (DCHAR*)("12345");
    stReq.final_xor = 3;
    stReq.is_input_reflect = 1;
    stReq.is_output_reflect = 1;

    adm_nm_notify(ADM_NM_DIM_VDIREFLASH, (void *)&stReq, sizeof(ADM_DIM_VDI_CRC_PARAM_T));
}


int main(int argc, char *argv[])
{

    //adm_debug_logInit("/renzhendi/log/", 1045504, 5);
    adm_debug_logInitWithFileName("/renzhendi/log/", "sample.log", (5 * 1024 * 1024), 2,
                                  ELOG_LVL_VERBOSE, TRUE);

    /** nm初始化,包括锁资源         */
    adm_nm_init();

    wait_sleep(2);  ///< 信号等待

    adm_nm_register(ADM_NM_DIM_NOTIFY_1, dim_req_1_cb, NULL, ADM_NM_FLAG_ASYNC);
    adm_nm_structDescrip(ADM_NM_DIM_NOTIFY_1,
                         ADM_READ_DIAGINFO_DESCRIPTION, NULL);

    adm_nm_register(ADM_NM_DIM_REQUSET_2, dim_req_2_cb, NULL, ADM_NM_FLAG_ASYNC);
    adm_nm_structDescrip(ADM_NM_DIM_REQUSET_2,
                         ADM_READ_DIAGINFO_DESCRIPTION, NULL);

    adm_nm_register(ADM_NM_DIM_SESSION_3, dim_req_3_cb, NULL, ADM_NM_FLAG_ASYNC);
    adm_nm_structDescrip(ADM_NM_DIM_SESSION_3,
                         ADM_READ_DIAGINFO_DESCRIPTION, ADM_VDI_READDIAG_RESP_DES);

    adm_nm_register(ADM_NM_DIM_VDIREFLASH, dim_req_flush_cb, NULL, ADM_NM_FLAG_ASYNC);
    adm_nm_structDescrip(ADM_NM_DIM_VDIREFLASH, ADM_DIM_VDI_CRC_PARAM_DES, NULL);

    /*************** test code below *************/
    test_dim_send_1();    ///< notify
    test_dim_send_2();    ///< request
    test_dim_send_3();    ///< session
    test_dim_send_4();    ///< notify描述bool bool
    /*************** test code above *************/

    /** 延时注销 */
    wait_sleep(3);
    adm_nm_unregister(ADM_NM_DIM_NOTIFY_1, dim_req_1_cb);
    adm_nm_unregister(ADM_NM_DIM_REQUSET_2, dim_req_2_cb);
    adm_nm_unregister(ADM_NM_DIM_SESSION_3, dim_req_3_cb);
    adm_nm_unregister(ADM_NM_DIM_VDIREFLASH, dim_req_flush_cb);

    adm_nm_uninit();

    adm_debug_logDeinit();

    //sleep and break
    for (int i = 0; !s_break; i++)
    {
        log_i("wait...........");
        wait_sleep(5);
    }


    return 0;
}


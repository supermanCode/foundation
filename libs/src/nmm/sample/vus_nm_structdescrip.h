#pragma once

#include "adm_typedefs.h"


//结构体与业务相关 foundation需要上层的业务结构
//描述关联 notifyID

#ifdef __cplusplus
extern "C" {
#endif
/**********************test **********************/
typedef struct
{
    D32S requestId;
    D32S responseId;
    D32S functionId;
    D32S swDid;
} ADM_DIM_ECU_IDEN_INFO_T;
#define ECU_IDEN_INFO \
"OBJECT\
{\
    requestId   INT, \
    responseId  INT, \
    functionId  INT, \
    swDid       INT \
}"


typedef enum VEHICLE_BUS_TYPE_ENUM
{
    VEHICLE_BUS_TYPE_CAN = 1, ///< CAN
    VEHICLE_BUS_TYPE_CANFD,   ///< CANFD
    VEHICLE_BUS_TYPE_ETH,     ///< ETH
    VEHICLE_BUS_TYPE_LIN,     ///< LIN
    VEHICLE_BUS_TYPE_INVALID  ///< INVALID
} VEHICLE_BUS_TYPE_E;
typedef enum VEHICLE_BUS_PIN_TYPE_ENUM
{
    VEHICLE_BUS_PIN_TYPE_A_CAN = 1, ///< ACAN
    VEHICLE_BUS_PIN_TYPE_B_CAN = 2, ///< BCAN
    VEHICLE_BUS_PIN_TYPE_P_CAN = 3, ///< PCAN
    VEHICLE_BUS_PIN_TYPE_INVALID    ///< Invalid
} VEHICLE_BUS_PIN_TYPE_E;

typedef struct
{
    D32U dataType;
    D32U dataLength;
    DCHAR * pValue;
    #if 0
    union
    {
        /**!< Union of DID corresponding values */
        D8U value[1];
        D32U valueU32;
        D32S valueS32;
        D64U valueU64;
        D64S valueS64;
        float valueF;
        D8U *valueS;
    } v;
    #endif
} ADM_DIM_VARIABLE_DATA_T;
#define VARIABLE_DATA \
    "OBJECT\
    {\
        dataType     INT,\
        dataLength   INT,\
        value        STRING OPTIONAL \
    }"

typedef struct
{
    D32S did; 
    D32S startByte; 
    D32S endByte;  
    D32U errorCode;  
    ADM_DIM_VARIABLE_DATA_T didData;
} ADM_DIM_DID_DATA_T;
#define DID_DATA \
"OBJECT\
{\
    did          INT, \
    startByte    INT, \
    endByte      INT, \
    errorCode    INT, \
    didData  " VARIABLE_DATA " \
}"

typedef struct
{
    D32U didArrayLength;
    ADM_DIM_DID_DATA_T *didArray;
} ADM_DIM_DID_DATA_ARRAY_T;
#define DID_DATA_ARRAY \
"ARRAY OF " DID_DATA


/** 结构体对应描述 */
typedef struct
{
    ADM_DIM_ECU_IDEN_INFO_T pstEcuIdenInfo; 
    DCHAR *diagScriptPath;
    VEHICLE_BUS_TYPE_E eBusType;
    VEHICLE_BUS_PIN_TYPE_E eChannelType;
    DBOOL isCGWRouted;
    ADM_DIM_DID_DATA_ARRAY_T didListRequest;
} ADM_VDI_READDIAG_T;

#define ADM_READ_DIAGINFO_DESCRIPTION  \
"OBJECT\
{\
    pstEcuIdenInfo    " ECU_IDEN_INFO ",\
    diagScriptPath    STRING,\
    eBusType          INT,\
    eChannelType      INT, \
    isCGWRouted       INT,\
    didListRequest    " DID_DATA_ARRAY " \
}"

//响应
typedef struct
{
    ADM_DIM_ECU_IDEN_INFO_T stEcuIdenInfo; 
    D32U errroCode;
}ADM_VDI_READDIAG_RESP_T;
#define  ADM_VDI_READDIAG_RESP_DES  \
    "OBJECT\
    { \
        stEcuIdenInfo      " ECU_IDEN_INFO ", \
        errroCode          UINT  \
    }"




#define ADM_NM_DIM_VDIREFLASH         "VdiReflash"

typedef struct
{
    D32U polynomial;         ///< 多项式初始值
    D32U init_value;         ///< crc初始值
    DCHAR *initvalue2;
    D32U final_xor;          ///< crc结果异或值
    DBOOL is_input_reflect;  ///< crc输入值是否反转
    DBOOL is_output_reflect; ///< crc 输出值是否反转
} ADM_DIM_VDI_CRC_PARAM_T;
#define ADM_DIM_VDI_CRC_PARAM_DES \
    "OBJECT \
    { \
        polynomial     UINT, \
        initvalue      UINT, \
        initvalue2     STRING OPTIONAL,\
        finalxor       UINT, \
        isinputreflect   INT SIZE(1), \
        isoutputreflect  INT SIZE(1)  \
    }"



#ifdef __cplusplus  // close out "C" linkage in case of c++ compiling
}
#endif


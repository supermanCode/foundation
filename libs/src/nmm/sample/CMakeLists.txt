cmake_minimum_required (VERSION 3.10)
project(NMMTest C CXX)

set(project_name "NMMTest")

#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -rdynamic -funwind-tables -ffunction-sections -std=c++11  -Wl,-Map,${CMAKE_BINARY_DIR}/output.map")
#set(SYSTEM_LIBRARY_ pthread rt dl stdc++)

add_executable(
    ${project_name}
    nmm_main.cpp
)

target_include_directories(
    ${project_name}
    PRIVATE
    
    ${CMAKE_CURRENT_LIST_DIR}/../../../inc/nmm
    ${CMAKE_CURRENT_LIST_DIR}/../../../inc/datatype
    ${CMAKE_CURRENT_LIST_DIR}/../../../../platform/os/inc
    ${CMAKE_CURRENT_LIST_DIR}/../../../../package/log/log
    ${CMAKE_CURRENT_LIST_DIR}/../../../../package/log/log/EasyLogger/easylogger/inc
)

target_link_libraries(
    ${PROJECT_NAME}
    
    ${CMAKE_CURRENT_LIST_DIR}/../../../../build/libs/libfoundation.so
    #${CMAKE_CURRENT_LIST_DIR}/../../../../build/libs/src/nmm/libNMM.a
    #${CMAKE_INSTALL_LIBDIR}/libadmdebug.a
    ${SYSTEM_LIBRARY}
    dl
)

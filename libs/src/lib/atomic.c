#include <stdio.h>
#include <pthread.h>

#include "atomic.h"

/* init pthread_mutex */
void atomic_init(atomic_t *v)
{
    pthread_mutex_init(&v->mutex, NULL);
    v->counter = 0;
}

/* deinit pthread_mutex */
void atomic_deinit(atomic_t *v)
{
    pthread_mutex_destroy(&v->mutex);
}

/* set atomic value */
void atomic_set(atomic_t *v, int i)
{
    pthread_mutex_lock(&v->mutex);
    v->counter = i;
    pthread_mutex_unlock(&v->mutex);
}

/* return ( ++atomic_value) */
int atomic_inc_return(atomic_t *v)
{
    int count;
    pthread_mutex_lock(&v->mutex);
    v->counter++;
    count = v->counter;
    pthread_mutex_unlock(&v->mutex);
    return count;
}

/* 执行完减操作后检查结果是否为0 */
int atomic_sub_and_test(int i, atomic_t *v)
{
    int c;
    pthread_mutex_lock(&v->mutex);
    v->counter -= i;
    c = v->counter;
    pthread_mutex_unlock(&v->mutex);
    //return (c != 0);
    return (c == 0);
}

/* 递减后检查结果是否为0 */
int atomic_dec_and_test(atomic_t *v)
{
    int count;
    pthread_mutex_lock(&v->mutex);
    v->counter--;
    count = v->counter;
    pthread_mutex_unlock(&v->mutex);
    //return (count != 0);
    return (count == 0);
}

/*
 * 它检查v是否等于u，如果不是则把v的值加上a，返回值表示
 * 相加前v是否等于u。因为在atomic_read || atomic_cmpxchg
 * 中间可能有其他的 写操作，所以要循环检查自己的值是否被
 * 写进去。
 */
int atomic_add_unless(atomic_t *v, int a, int u)
{
    pthread_mutex_lock(&v->mutex);
    int count = v->counter;
    if (v->counter != u) {
        v->counter += a;
    }
    pthread_mutex_unlock(&v->mutex);
    //return (count != u);
    return (count == u);
}

int atomic_read(atomic_t *v)
{
    int count;

    pthread_mutex_lock(&v->mutex);
    count = v->counter;
    pthread_mutex_unlock(&v->mutex);
    return count;
}

#ifndef __GLOB_H
#define __GLOB_H

#include <stdbool.h>

bool glob_match(char const *pat, char const *str); 

#endif

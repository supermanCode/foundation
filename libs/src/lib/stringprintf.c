#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "stringprintf.h"

static char *StringAppendV(char *dst, const char *format, va_list ap)
{
    char *appendstring = NULL;
    int remain_retry = 50;
    size_t dst_len = 0;

    if (dst) {
        dst_len = strnlen(dst, 0x7fffff);
    }

    /* First try with a small fixed size buffer */
    char space[1024];
    /* It's possible for methods that use a va_list to invalidate
     * the data in it upon use.  The fix is to make a copy
     * of the structure before using it and use that copy instead.
     */
    va_list backup_ap;
    va_copy(backup_ap, ap);
    int result = vsnprintf(space, sizeof(space), format, backup_ap);
    va_end(backup_ap);

    if (result < 0) {
        goto out;
    }

    if (result < (int)(sizeof(space))) {
        /* Normal case -- everything fit. */
        /* dst->append(space, result); */
        appendstring = malloc(dst_len + result + 1);

        if (appendstring) {
            appendstring[0] = '\0';

            if (dst) {
                memcpy(appendstring, dst, dst_len);
                appendstring[dst_len] = '\0';
            }

            strcat(appendstring, space);
        }

        goto out;
    }

    do {
        /* Increase the buffer size to the size requested by vsnprintf,
         * plus one for the closing \0.
         */
        int length = result + 64;
        char *buf = malloc(length);

        if (!buf) {
            goto out;
        }

        /* Restore the va_list before we use it again */
        va_copy(backup_ap, ap);
        result = vsnprintf(buf, length, format, backup_ap);
        va_end(backup_ap);

        if (result < 0) {
            free(buf);
            break;
        }

        if (result < length) {
            /* It fit */
            appendstring = malloc(dst_len + result + 1);

            if (appendstring) {
                appendstring[0] = '\0';

                if (dst) {
                    memcpy(appendstring, dst, dst_len);
                    appendstring[dst_len] = '\0';
                }

                strcat(appendstring, buf);
            }

            free(buf);
            break;
        }

        free(buf);
        remain_retry--;
    } while (remain_retry);

out:
    return appendstring;
}

char *StringVPrintf(const char *fmt, va_list ap)
{
    char *result;
    result = StringAppendV(NULL, fmt, ap);
    return result;
}

char *StringPrintf(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    char *result;
    result = StringAppendV(NULL, fmt, ap);
    va_end(ap);
    return result;
}

void StringAppendF(char **dst, const char *format, ...)
{
    va_list ap;
    va_start(ap, format);
    char *result = StringAppendV(*dst, format, ap);

    if (result && dst && *dst) {
        free(*dst);
        *dst = result;
    }

    va_end(ap);
}

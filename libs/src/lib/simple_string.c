#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "simple_string.h"

char *simple_strndup(const char *s, size_t n)
{
    size_t max_len = strnlen(s, n);

    char *dest = malloc(max_len + 1);
    if (dest) {
        memcpy(dest, s, max_len);
        dest[max_len] = '\0';
    }

    return dest;
}

char *simple_strdup(const char *s)
{
    return simple_strndup(s, 0x7fffff);
}

/**
 * strncasecmp - Case insensitive, length-limited string comparison
 * @s1: One string
 * @s2: The other string
 * @len: the maximum number of characters to compare
 */
int simple_strncasecmp(const char *s1, const char *s2, size_t len)
{
    /* Yes, Virginia, it had better be unsigned */
    unsigned char c1, c2;

    if (!len) {
        return 0;
    }

    do {
        c1 = *s1++;
        c2 = *s2++;
        if (!c1 || !c2) {
            break;
        }
        if (c1 == c2) {
            continue;
        }
        c1 = tolower(c1);
        c2 = tolower(c2);
        if (c1 != c2) {
            break;
        }
    } while (--len);
    return (int)c1 - (int)c2;
}

int simple_strcasecmp(const char *s1, const char *s2)
{
    int c1, c2;

    do {
        c1 = tolower(*s1++);
        c2 = tolower(*s2++);
    } while (c1 == c2 && c1 != 0);
    return c1 - c2;
}

/**
 * strreplace - Replace all occurrences of character in string.
 * @s: The string to operate on.
 * @old: The character being replaced.
 * @new: The character @old is replaced with.
 *
 * Returns pointer to the nul byte at the end of @s.
 */
char *strreplace(char *s, char old, char new)
{
    for (; *s; ++s)
        if (*s == old) {
            *s = new;
        }
    return s;
}

/**
 * strchr - Find the first occurrence of a character in a string
 * @s: The string to be searched
 * @c: The character to search for
 */
char *simple_strchr(const char *s, int c)
{
    for (; *s != (char)c; ++s)
        if (*s == '\0') {
            return NULL;
        }
    return (char *)s;
}

/**
 * strlcpy - Copy a C-string into a sized buffer
 * @dest: Where to copy the string to
 * @src: Where to copy the string from
 * @size: size of destination buffer
 *
 * Compatible with *BSD: the result is always a valid
 * NUL-terminated string that fits in the buffer (unless,
 * of course, the buffer size is zero). It does not pad
 * out the result like strncpy() does.
 */
size_t simple_strlcpy(char *dest, const char *src, size_t size)
{
    size_t ret = strlen(src);

    if (size) {
        size_t len = (ret >= size) ? size - 1 : ret;
        memcpy(dest, src, len);
        dest[len] = '\0';
    }
    return ret;
}


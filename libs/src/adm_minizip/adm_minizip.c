#if (!defined(_WIN32)) && (!defined(WIN32)) && (!defined(__APPLE__))
        #ifndef __USE_FILE_OFFSET64
                #define __USE_FILE_OFFSET64
        #endif
        #ifndef __USE_LARGEFILE64
                #define __USE_LARGEFILE64
        #endif
        #ifndef _LARGEFILE64_SOURCE
                #define _LARGEFILE64_SOURCE
        #endif
        #ifndef _FILE_OFFSET_BIT
                #define _FILE_OFFSET_BIT 64
        #endif
#endif

#ifdef __APPLE__
// In darwin and perhaps other BSD variants off_t is a 64 bit value, hence no need for specific 64 bit functions
#define FOPEN_FUNC(filename, mode) fopen(filename, mode)
#define FTELLO_FUNC(stream) ftello(stream)
#define FSEEKO_FUNC(stream, offset, origin) fseeko(stream, offset, origin)
#else
#define FOPEN_FUNC(filename, mode) fopen64(filename, mode)
#define FTELLO_FUNC(stream) ftello64(stream)
#define FSEEKO_FUNC(stream, offset, origin) fseeko64(stream, offset, origin)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>

#ifdef _WIN32
# include <direct.h>
# include <io.h>
#else
# include <unistd.h>
# include <utime.h>
# include <sys/types.h>
# include <sys/stat.h>
#include <dirent.h>
#endif

#include "zip.h"

#ifdef _WIN32
        #define USEWIN32IOAPI
        #include "iowin32.h"
#endif



#define WRITEBUFFERSIZE (16384)
#define MAXFILENAME (256)

#ifdef _WIN32
uLong filetime(f, tmzip, dt)
    char *f;                /* name of file to get info on */
    tm_zip *tmzip;             /* return value: access, modific. and creation times */
    uLong *dt;             /* dostime */
{
  int ret = 0;
  {
      FILETIME ftLocal;
      HANDLE hFind;
      WIN32_FIND_DATAA ff32;

      hFind = FindFirstFileA(f,&ff32);
      if (hFind != INVALID_HANDLE_VALUE)
      {
        FileTimeToLocalFileTime(&(ff32.ftLastWriteTime),&ftLocal);
        FileTimeToDosDateTime(&ftLocal,((LPWORD)dt)+1,((LPWORD)dt)+0);
        FindClose(hFind);
        ret = 1;
      }
  }
  return ret;
}
#else
#ifdef unix || __APPLE__
uLong filetime(f, tmzip, dt)
    char *f;               /* name of file to get info on */
    tm_zip *tmzip;         /* return value: access, modific. and creation times */
    uLong *dt;             /* dostime */
{
  int ret=0;
  struct stat s;        /* results of stat() */
  struct tm* filedate;
  time_t tm_t=0;

  if (strcmp(f,"-")!=0)
  {
    char name[MAXFILENAME+1];
    int len = strlen(f);
    if (len > MAXFILENAME)
      len = MAXFILENAME;

    strncpy(name, f,MAXFILENAME-1);
    /* strncpy doesnt append the trailing NULL, of the string is too long. */
    name[ MAXFILENAME ] = '\0';

    if (name[len - 1] == '/')
      name[len - 1] = '\0';
    /* not all systems allow stat'ing a file with / appended */
    if (stat(name,&s)==0)
    {
      tm_t = s.st_mtime;
      ret = 1;
    }
  }
  filedate = localtime(&tm_t);

  tmzip->tm_sec  = filedate->tm_sec;
  tmzip->tm_min  = filedate->tm_min;
  tmzip->tm_hour = filedate->tm_hour;
  tmzip->tm_mday = filedate->tm_mday;
  tmzip->tm_mon  = filedate->tm_mon ;
  tmzip->tm_year = filedate->tm_year;

  return ret;
}
#else
uLong filetime(f, tmzip, dt)
    char *f;                /* name of file to get info on */
    tm_zip *tmzip;             /* return value: access, modific. and creation times */
    uLong *dt;             /* dostime */
{
    return 0;
}
#endif
#endif

static int isLargeFile(const char* filename)
{
  int largeFile = 0;
  ZPOS64_T pos = 0;
  FILE* pFile = FOPEN_FUNC(filename, "rb");

  if(pFile != NULL)
  {
    int n = FSEEKO_FUNC(pFile, 0, SEEK_END);
    pos = FTELLO_FUNC(pFile);

                printf("File : %s is %lld bytes\n", filename, pos);

    if(pos >= 0xffffffff)
     largeFile = 1;

                fclose(pFile);
  }

 return largeFile;
}

// static int isLargeFile(const char* filename)
// {
//   int largeFile = 0;
//   ZPOS64_T pos = 0;
//   struct stat sb;

//     if (-1 == lstat(filename, &sb)) {
//         printf("lstat %s failed\n", filename);
//     } else {
//         pos = sb.st_size;
//         printf("File : %s is %lld bytes\n", filename, pos);

//         if(pos >= 0xffffffff)
//         largeFile = 1;
//     }

//     return largeFile;
// }

/**
 * @brief 将目录下的所有文件，添加到压缩文件中
 * 
 */
static int adm_compress_zip_filesInZip(zipFile zf, const char* compressedDir)
{
    int err = ZIP_ERRNO;
    DIR* dir = NULL;
    struct dirent* ptr = NULL;
    zip_fileinfo zi;
    const char* filenameinzip = NULL;
    const char *savefilenameinzip = NULL;
    int  zip64 = 0;               /* Add ZIP64 extened information in the extra field */
    char fullfilename[256] = {0};
    int len = 0;
    FILE* fin = NULL;
    int size_read = 0;
    int size_buf=0;
    void* buf=NULL;

    size_buf = WRITEBUFFERSIZE;
    buf = (void*)malloc(size_buf);
    if (buf==NULL)
    {
        printf("Error allocating memory\n");
        return ZIP_INTERNALERROR;
    }

    do {
        //!< 校验入参
        if (NULL == zf || NULL == compressedDir) {
            printf("zf || compressedDir is nil\n");
            break;
        }

        //!< 打开日志文件存储的目录
        dir = opendir(compressedDir);
        if (NULL == dir) {
            printf("opendir %s failed\n", compressedDir);
            break;
        }

        len = strlen(compressedDir);
        while(NULL != (ptr = readdir(dir))) {
            //!< 只能压缩文件
            if (8 != ptr->d_type) {
                printf("ptr->d_type:%d is not file\n", ptr->d_type);
                continue;
            }

            if (NULL == ptr->d_name) {
                printf("ptr->d_name is nil\n");
                continue;
            }

            zi.tmz_date.tm_sec = zi.tmz_date.tm_min = zi.tmz_date.tm_hour =
            zi.tmz_date.tm_mday = zi.tmz_date.tm_mon = zi.tmz_date.tm_year = 0;
            zi.dosDate = 0;
            zi.internal_fa = 0;
            zi.external_fa = 0;
            filenameinzip = ptr->d_name;
            savefilenameinzip = ptr->d_name;

            memset(fullfilename, 0x00, sizeof(fullfilename));
            if ('/' == compressedDir[len - 1]) {
                snprintf(fullfilename, sizeof(fullfilename), "%s%s", compressedDir, filenameinzip);
            } else {
                snprintf(fullfilename, sizeof(fullfilename), "%s/%s", compressedDir, filenameinzip);
            }
            filetime(fullfilename, &zi.tmz_date,&zi.dosDate);
            zip64 = isLargeFile(fullfilename);
            err = zipOpenNewFileInZip3_64(zf, savefilenameinzip, &zi,
                            NULL,0,NULL,0,NULL /* comment*/,
                            Z_DEFLATED,     /** The deflate compression method (the only one supported in this version) */
                            6, 0,           /** 压缩等级：6 */
                            -MAX_WBITS, DEF_MEM_LEVEL, Z_DEFAULT_STRATEGY,
                            NULL, 0, zip64);
            if (err != ZIP_OK) {
                printf("error in opening %s in zipfile\n",filenameinzip);
                break;
            } else {
                fin = FOPEN_FUNC(fullfilename,"rb");
                if (fin==NULL)
                {
                    err=ZIP_ERRNO;
                    printf("error in opening %s for reading\n",fullfilename);
                }
            }


            if (err == ZIP_OK) {
               do
                {
                    err = ZIP_OK;
                    size_read = (int)fread(buf,1,size_buf,fin);
                    if (size_read < size_buf)
                        if (feof(fin)==0)
                    {
                        printf("error in reading %s\n",filenameinzip);
                        err = ZIP_ERRNO;
                    }

                    if (size_read>0)
                    {
                        err = zipWriteInFileInZip (zf,buf,size_read);
                        if (err<0)
                        {
                            printf("error in writing %s in the zipfile\n",
                                                filenameinzip);
                        }

                    }
                } while ((err == ZIP_OK) && (size_read>0));
            }

            if (fin) {
                fclose(fin);
            }

            if (err<0)
                err=ZIP_ERRNO;
            else
            {
                err = zipCloseFileInZip(zf);
                if (err!=ZIP_OK)
                    printf("error in closing %s in the zipfile\n",
                                filenameinzip);
            }
        }
        closedir(dir);
    } while(0);
    free(buf);

    return err;
}

static int adm_compress_zip_internal(const char *compressedDir, const char *zipDir, const char* zipFileName)
{
    int ret = ZIP_ERRNO;
    char fullZipPath[256] = {0};
    zipFile zf;
    int zipDirLen = 0;
    int errclose = ZIP_ERRNO;

    do {
        //!< 校验输入参数
        if (NULL == zipDir || NULL == zipFileName || NULL == compressedDir) {
            printf("zipDir || zipFileName || compressDir is nil\n");
            break;
        }

        //!< 校验被压缩的目录是否存在
        if (0 != access(compressedDir, F_OK)) {
            printf("compressedDir:%s not found\n", compressedDir);
            break;
        }

        //!< 判断目录是否存在，如果不存在需要创建目录
        if (0 != access(zipDir, F_OK)) {
            //!< 755 权限创建目录
            if (0 != mkdir(zipDir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)) {
                printf("mkdir zipDir:%s failed\n", zipDir);
                break;
            }
        }

        //!< 拼接压缩文件全路径
        memset(fullZipPath, 0x00, sizeof(fullZipPath));
        zipDirLen = strlen(zipDir);
        //!< 判断最后一个字符是否为 '/'
        if ('/' == zipDir[zipDirLen - 1]) {
            snprintf(fullZipPath, sizeof(fullZipPath), "%s%s", zipDir, zipFileName);
        } else {
            snprintf(fullZipPath, sizeof(fullZipPath), "%s/%s", zipDir, zipFileName);
        }

        //!< 开始执行压缩
        zf = zipOpen64(fullZipPath, APPEND_STATUS_CREATE);
        if (NULL == zf) {
            printf("error opening %s\n", fullZipPath);
            break;
        }

        printf("creating %s\n", fullZipPath);
        //!< 将待压缩文件，写入压缩包中
        ret = adm_compress_zip_filesInZip(zf, compressedDir);
        if (ZIP_OK != ret) {
            printf("adm_compress_zip_filesInZip failed, [err:%d]\n", ret);
        }
        
        errclose = zipClose(zf, NULL);
        if (ZIP_OK != errclose) {
            printf("error in closing %s\n",fullZipPath);
        }

    } while(0);

    return ret;
}

/**
 * @brief 压缩目录下的所有文件到指定的输出路径
 * 
 * @param compressedDir     [IN]    被压缩的文件目录
 * @param zipDir            [IN]    待输出的压缩文件的目录
 * @param zipFileName       [IN]    待输出的压缩文件名 
 * @return int  0:Successed
 */
int adm_compress_zip(const char *compressedDir, const char *zipDir, const char* zipFileName)
{
    return adm_compress_zip_internal(compressedDir, zipDir, zipFileName);
}
#include "adm_miniunz.h"
#include "adm_minizip.h"

#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

static char* compressDir = NULL;
static char* zipDir = NULL;
static char* zipFileName = NULL;

static void* threadHandler(void* argv)
{
    if (0 == adm_compress_zip(compressDir, zipDir, zipFileName)) {
        printf("adm_compress_zip successed\n");
    }
}

int main(int argc, char *argv[]){
    //if(0 == adm_extract_zip(argv[1], argv[2])){
    // if(0 == adm_extract_zip_onefile(argv[1], argv[2], argv[3])){
	// if (0 == adm_compress_zip(argv[1], argv[2], argv[3])) {
    // printf("success\n");
    // }else{
	// printf("failed\n");
    // }

    compressDir = strdup(argv[1]);
    zipDir = strdup(argv[2]);
    zipFileName = strdup(argv[3]);

    pthread_t threadId = 0;

    do {
        if (0 == pthread_create(&threadId, NULL, threadHandler, NULL)) {
            printf("pthread_create successed\n");
            pthread_detach(threadId);
        }
    } while(0);

    pause();

    return 0;
}

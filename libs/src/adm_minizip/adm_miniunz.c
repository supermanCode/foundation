#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>

# include <unistd.h>
# include <utime.h>

#include "unzip.h"

       #include <sys/stat.h>
       #include <sys/types.h>


#define CASESENSITIVITY (0)
#define WRITEBUFFERSIZE (8192)
#define MAXFILENAME (256)

static 
void change_file_date(filename,dosdate,tmu_date)
    const char *filename;
    uLong dosdate;
    tm_unz tmu_date;
{
  struct utimbuf ut;
  struct tm newdate;
  newdate.tm_sec = tmu_date.tm_sec;
  newdate.tm_min=tmu_date.tm_min;
  newdate.tm_hour=tmu_date.tm_hour;
  newdate.tm_mday=tmu_date.tm_mday;
  newdate.tm_mon=tmu_date.tm_mon;
  if (tmu_date.tm_year > 1900)
      newdate.tm_year=tmu_date.tm_year - 1900;
  else
      newdate.tm_year=tmu_date.tm_year ;
  newdate.tm_isdst=-1;

  ut.actime=ut.modtime=mktime(&newdate);
  utime(filename,&ut);
}

static 
int mymkdir(dirname)
    const char* dirname;
{
    int ret=0;
    ret = mkdir (dirname,0775);
    return ret;
}

static 
int makedir (newdir)
    char *newdir;
{
  char *buffer ;
  char *p;
  int  len = (int)strlen(newdir);

  if (len <= 0)
    return 0;

  buffer = (char*)malloc(len+1);
        if (buffer==NULL)
        {
                printf("Error allocating memory\n");
                return UNZ_INTERNALERROR;
        }
  strcpy(buffer,newdir);

  if (buffer[len-1] == '/') {
    buffer[len-1] = '\0';
  }
  if (mymkdir(buffer) == 0)
    {
      free(buffer);
      return 1;
    }

  p = buffer+1;
  while (1)
    {
      char hold;

      while(*p && *p != '\\' && *p != '/')
        p++;
      hold = *p;
      *p = 0;
      if ((mymkdir(buffer) == -1) && (errno == ENOENT))
        {
          printf("couldn't create directory %s\n",buffer);
          free(buffer);
          return 0;
        }
      if (hold == 0)
        break;
      *p++ = hold;
    }
  free(buffer);
  return 1;
}

static 
int do_extract_currentfile(uf,password, extractdir)
    unzFile uf;
    const char* password;
    const char *extractdir;
{
    char filename_inzip[256];
    char* filename_withoutpath;
    char* p;
    int err=UNZ_OK;
    FILE *fout=NULL;
    void* buf;
    uInt size_buf;

    unz_file_info64 file_info;
    uLong ratio=0;
    err = unzGetCurrentFileInfo64(uf,&file_info,filename_inzip,sizeof(filename_inzip),NULL,0,NULL,0);

    if (err!=UNZ_OK)
    {
        printf("error %d with zipfile in unzGetCurrentFileInfo\n",err);
        return err;
    }

    size_buf = WRITEBUFFERSIZE;
    buf = (void*)malloc(size_buf);
    if (buf==NULL)
    {
        printf("Error allocating memory\n");
        return UNZ_INTERNALERROR;
    }

    p = filename_withoutpath = filename_inzip;
    while ((*p) != '\0')
    {
        if (((*p)=='/') || ((*p)=='\\'))
            filename_withoutpath = p+1;
        p++;
    }

    char fulldir[512] = {0};
    char fullpath[512] = {0};

    if ((*filename_withoutpath)=='\0')
    {
            printf("creating directory: %s\n",filename_inzip);
	    snprintf(fulldir, sizeof(fulldir)/sizeof(fulldir[0]), "%s%s", extractdir, filename_inzip);
            mymkdir(fulldir);
    }
    else
    {
        const char* write_filename;

            write_filename = filename_inzip;
	    snprintf(fullpath, sizeof(fullpath)/sizeof(fullpath[0]), "%s%s", extractdir, write_filename);

        err = unzOpenCurrentFilePassword(uf,password);
        if (err!=UNZ_OK)
        {
            printf("error %d with zipfile in unzOpenCurrentFilePassword\n",err);
        }

        if ((err==UNZ_OK))
        {
            /* some zipfile don't contain directory alone before file */
            if ((filename_withoutpath!=(char*)filename_inzip))
            {
                char c=*(filename_withoutpath-1);
                *(filename_withoutpath-1)='\0';
		snprintf(fulldir, sizeof(fulldir)/sizeof(fulldir[0]), "%s%s", extractdir, write_filename);
                makedir(fulldir);
                *(filename_withoutpath-1)=c;
                /*fout=FOPEN_FUNC(write_filename,"wb");*/
            }

            fout=fopen(fullpath,"wb");

            if (fout==NULL)
            {
                printf("error opening %s\n",fullpath);
		err = UNZ_ERRNO;
            }
        }

        if (fout!=NULL)
        {
            printf(" extracting: %s\n",write_filename);

            do
            {
                err = unzReadCurrentFile(uf,buf,size_buf);
                if (err<0)
                {
                    printf("error %d with zipfile in unzReadCurrentFile\n",err);
                    break;
                }
                if (err>0)
                    if (fwrite(buf,err,1,fout)!=1)
                    {
                        printf("error in writing extracted file\n");
                        err=UNZ_ERRNO;
                        break;
                    }
            }
            while (err>0);
            if (fout)
                    fclose(fout);

            if (err==0)
                change_file_date(fullpath,file_info.dosDate,
                                 file_info.tmu_date);
        }

        if (err==UNZ_OK)
        {
            err = unzCloseCurrentFile (uf);
            if (err!=UNZ_OK)
            {
                printf("error %d with zipfile in unzCloseCurrentFile\n",err);
            }
        }
        else
            unzCloseCurrentFile(uf); /* don't lose the error */
    }

    free(buf);
    return err;
}

static 
int do_extract(uf,password,extractdir)
    unzFile uf;
    const char* password;
    const char *extractdir;
{
    uLong i;
    unz_global_info64 gi;
    int err;

    err = unzGetGlobalInfo64(uf,&gi);
    if (err!=UNZ_OK)
    {
        printf("error %d with zipfile in unzGetGlobalInfo \n",err);
	return err;
    }

    for (i=0;i<gi.number_entry;i++)
    {
        if ((err = do_extract_currentfile(uf, password, extractdir)) != UNZ_OK)
            break;

        if ((i+1)<gi.number_entry)
        {
            err = unzGoToNextFile(uf);
            if (err!=UNZ_OK)
            {
                printf("error %d with zipfile in unzGoToNextFile\n",err);
                break;
            }
        }
    }

    return err;
}

static 
int do_extract_onefile(uf,filename,password,extractdir)
    unzFile uf;
    const char* filename;
    const char* password;
    const char *extractdir;
{
    int err = UNZ_OK;
    if (unzLocateFile(uf,filename,CASESENSITIVITY)!=UNZ_OK)
    {
        printf("file %s not found in the zipfile\n",filename);
        return 2;
    }

    if (do_extract_currentfile(uf, password, extractdir) == UNZ_OK)
        return 0;
    else
        return 1;
}

static int adm_extract_zip_internal(const char *zipfile, const char *extractdir, const char *filename_to_extract){
    int ret = UNZ_ERRNO;
    if(NULL == zipfile || NULL == extractdir){}else{
        if(0 != access(zipfile, F_OK)){
            printf("%s: not found\n", zipfile);
        }else{
	    size_t extractdirlen = strlen(extractdir);
	    if(extractdirlen > 0){
	        char *safeextractdir = malloc((extractdirlen + 2) *sizeof(*safeextractdir));
	        if(NULL != safeextractdir){
	            strcpy(safeextractdir, extractdir);
	            if(safeextractdir[extractdirlen -1] == '/'){}else{
			safeextractdir[extractdirlen] = '/';
			safeextractdir[extractdirlen +1] = '\0';
		    }
                    if(1 == makedir(safeextractdir)){
	                unzFile uf = unzOpen64(zipfile);
	                if(NULL != uf){
			    if(NULL == filename_to_extract){
	                        ret = do_extract(uf, NULL, safeextractdir);
			    }else{
	                        ret = do_extract_onefile(uf, filename_to_extract, NULL, safeextractdir);
			    }
	                    unzClose(uf);
	                }
                    }
	            free(safeextractdir);
	        }
	    }
        }
    }
    return ret;
}

int adm_extract_zip(const char *zipfile, const char *extractdir){
    return adm_extract_zip_internal(zipfile, extractdir, NULL);
}

int adm_extract_zip_onefile(const char *zipfile, const char *extractdir, const char *filename_to_extract){
    return adm_extract_zip_internal(zipfile, extractdir, filename_to_extract);
}

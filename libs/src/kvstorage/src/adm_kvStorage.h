#ifndef ADM_KV_STORAGE_H_
#define ADM_KV_STORAGE_H_

#include <string>
#include <vector>

namespace abup
{
namespace ota
{
namespace kv
{

/// @brief Interface to the common key-value storage functions.
class KeyValueStorage
{
public:
    /// @briefDefault constructor for KeyValueStorage.
    KeyValueStorage(){};

    /// @brief Move constructor for KeyValueStorage.
    KeyValueStorage(KeyValueStorage&&) noexcept = default;

    /// @brief Move assignment operator for KeyValueStorage.
    ///
    /// @returns The moved KeyValueStorage object.
    KeyValueStorage& operator=(KeyValueStorage&&) & noexcept = default;

    /// @brief The copy constructor for KeyValueStorage shall not be used.
    KeyValueStorage(const KeyValueStorage&) = delete;

    /// @brief The copy assignment operator for KeyValueStorage shall not be used.
    KeyValueStorage& operator=(const KeyValueStorage&) = delete;

    /// @brief Destructor for KeyValueStorage.
    virtual ~KeyValueStorage(){};

    /// @brief Creates an instance of KeyValueStorage which configures the storage location.
    ///
    /// @param[in] database The shortName of a PortPrototype typed by a PersistencyKeyValueDatabaseInterface.
    /// @returns A Result, containing an instance of KeyValueStorage, or one of the errors defined for Persistency in
    /// bool.
    virtual bool OpenKeyValueStorage(const std::string& database, std::string* errorStr) noexcept = 0;

    virtual bool DestroyKeyValueStorage(std::string* errorStr) noexcept = 0;

    virtual bool RepareKeyValueStorage(const std::string& database, std::string* errorStr) noexcept = 0;

    /// @brief Returns a list of all currently available keys of the KeyValueStorage.
    ///
    /// @returns A Result, containing a list of available keys, or one of the errors
    /// defined for Persistency in bool.
    virtual bool GetAllKeys(std::vector<std::string>* keys, std::string* errorStr) const noexcept = 0;

    /// @brief Checks if a key exists in the KeyValueStorage.
    ///
    /// @param[in] key The key that shall be checked.
    /// @returns A Result, containing true if the key could be located or false if it couldnot,
    /// or one of the errors defined for Persistency in bool.
    virtual bool HasKey(const std::string& key, std::string* errorStr) const noexcept = 0;

    /// @brief Returns the value assigned to a key of the KeyValueStorage.
    ///
    /// @param[in] key The key to look up.
    /// @param[out] value The retrieved value.
    /// @returns The success of the operation.
    virtual bool GetValue(const std::string& key, std::string* value, std::string* errorStr) const noexcept = 0;

    /// @brief Stores a key in the KeyValueStorage.
    ///
    /// @param[in] key The key to assign the value to.
    /// @param[in] value The value to store.
    /// @returns The success of the operation.
    virtual bool SetValue(const std::string& key, const std::string& value, std::string* errorStr) noexcept = 0;

    /// @brief Removes a key and the associated value from the KeyValueStorage.
    ///
    /// @param[in] key The key to be removed.
    /// @returns The success of the operation.
    virtual bool RemoveKey(const std::string& key, std::string* errorStr) noexcept = 0;

    /// @brief Removes all keys and associated values from the KeyValueStorage.
    ///
    /// @returns The success of the operation.
    virtual bool RemoveAllKeys(std::string* errorStr) noexcept = 0;
};
}  // namespace kv
}  // namespace ota
}  // namespace abup

#endif  // ARA_PER_KEY_VALUE_STORAGE_H_

#ifndef ADM_KV_STORAGE_IMPL_LEVELDB_H_
#define ADM_KV_STORAGE_IMPL_LEVELDB_H_

#include <string>
#include <vector>
#include <memory>
#include <mutex>

#include "leveldb/db.h"
#include "adm_kvStorage.h"

namespace abup
{
namespace ota
{
namespace kv
{

/// @brief key-value-storage internal implementation header
class KeyValueStorageLevelDB final : public KeyValueStorage
{
public:
    /// @briefDefault constructor for KeyValueStorageLevelDB.
    KeyValueStorageLevelDB()
        : mIsOpenDb(false)
        , mKvsLevelDb(nullptr){

          };

    /// @brief Move constructor for KeyValueStorageLevelDB.
    KeyValueStorageLevelDB(KeyValueStorageLevelDB&&) noexcept = default;

    /// @brief Move assignment operator for KeyValueStorageLevelDB.
    ///
    /// @returns The moved KeyValueStorageLevelDB object.
    KeyValueStorageLevelDB& operator=(KeyValueStorageLevelDB&&) & noexcept = default;

    /// @brief The copy constructor for KeyValueStorageLevelDB shall not be used.
    KeyValueStorageLevelDB(const KeyValueStorageLevelDB&) = delete;

    /// @brief The copy assignment operator for KeyValueStorageLevelDB shall not be used.
    KeyValueStorageLevelDB& operator=(const KeyValueStorageLevelDB&) = delete;

    /// @brief Destructor for KeyValueStorageLevelDB.
    virtual ~KeyValueStorageLevelDB()
    {
        mIsOpenDb = false;
        mKvsLevelDb = nullptr;
    }

    /// @brief Creates an instance of KeyValueStorageLevelDB which configures the storage location.
    ///
    /// @param[in] database The shortName of a PortPrototype typed by a PersistencyKeyValueDatabaseInterface.
    /// @returns A Result, containing an instance of KeyValueStorageLevelDB, or one of the errors defined for
    /// Persistency in bool.
    virtual bool OpenKeyValueStorage(const std::string& database, std::string* errorStr) noexcept override;

    virtual bool DestroyKeyValueStorage(std::string* errorStr) noexcept override;

    virtual bool RepareKeyValueStorage(const std::string& database, std::string* errorStr) noexcept override;

    /// @brief Returns a list of all currently available keys of the KeyValueStorageLevelDB.
    ///
    /// @returns A Result, containing a list of available keys, or one of the errors
    /// defined for Persistency in bool.
    virtual bool GetAllKeys(std::vector<std::string>* keys, std::string* errorStr) const noexcept override;

    /// @brief Checks if a key exists in the KeyValueStorageLevelDB.
    ///
    /// @param[in] key The key that shall be checked.
    /// @returns A Result, containing true if the key could be located or false if it couldnot,
    /// or one of the errors defined for Persistency in bool.
    virtual bool HasKey(const std::string& key, std::string* errorStr) const noexcept override;

    /// @brief Returns the value assigned to a key of the KeyValueStorageLevelDB.
    ///
    /// @param[in] key The key to look up.
    /// @param[out] value The retrieved value.
    /// @returns The success of the operation.
    virtual bool GetValue(const std::string& key, std::string* value, std::string* errorStr) const noexcept override;

    /// @brief Stores a key in the KeyValueStorageLevelDB.
    ///
    /// @param[in] key The key to assign the value to.
    /// @param[in] value The value to store.
    /// @returns The success of the operation.
    virtual bool SetValue(const std::string& key, const std::string& value, std::string* errorStr) noexcept override;

    /// @brief Removes a key and the associated value from the KeyValueStorageLevelDB.
    ///
    /// @param[in] key The key to be removed.
    /// @returns The success of the operation.
    virtual bool RemoveKey(const std::string& key, std::string* errorStr) noexcept override;

    /// @brief Removes all keys and associated values from the KeyValueStorageLevelDB.
    ///
    /// @returns The success of the operation.
    virtual bool RemoveAllKeys(std::string* errorStr) noexcept override;

private:
    /// @brief database name
    std::string mDatabase;

    bool mIsOpenDb;

    /// @brief The stored key-value-pairs in their storing order.
    std::unique_ptr<leveldb::DB> mKvsLevelDb;
};
}  // namespace kv
}  // namespace ota
}  // namespace abup

#endif  // ARA_PER_KEY_VALUE_STORAGE_H_

#include "adm_kvStorage_impl_leveldb.h"
#include "leveldb/write_batch.h"
#include "leveldb/c.h"

#include <unistd.h>
#include <stdio.h>

namespace abup
{
namespace ota
{
namespace kv
{

bool KeyValueStorageLevelDB::OpenKeyValueStorage(const std::string& database, std::string* errorStr) noexcept
{
    std::string databaseLockPath;
    bool retVal = false;
    leveldb::Options opts;
    opts.create_if_missing = true;
    leveldb::DB* db;

    auto status = leveldb::DB::Open(opts, database, &db);
    if (status.ok()) {
        mDatabase = database;
        mIsOpenDb = true;
        mKvsLevelDb.reset(db);
        retVal = true;
    } else {
        if ('/' == database.back()) {
            databaseLockPath = database + "LOCK";
        } else {
            databaseLockPath = database + "/LOCK";
        }
        /*prevent app from exiting unexpectedly and database being locked,remove LOCK file*/
        if (access(databaseLockPath.c_str(), F_OK) == 0) {
            remove(databaseLockPath.c_str());
        }
        status = leveldb::DB::Open(opts, database, &db);
        if (status.ok()) {
            mDatabase = database;
            mIsOpenDb = true;
            mKvsLevelDb.reset(db);
            retVal = true;
        } else {
            *errorStr = status.ToString();
            retVal = false;
        }
    }
    return retVal;
}

bool KeyValueStorageLevelDB::DestroyKeyValueStorage(std::string* errorStr) noexcept
{
    if (mIsOpenDb) {
        /*the DB must be closed*/
        mKvsLevelDb = nullptr;
        mIsOpenDb = false;
    }

    bool retVal = false;

    auto status = leveldb::DestroyDB(mDatabase, leveldb::Options());
    if (status.ok()) {
        retVal = true;
    } else {
        *errorStr = status.ToString();
        retVal = false;
    }
    return retVal;
}

bool KeyValueStorageLevelDB::GetAllKeys(std::vector<std::string>* keys, std::string* errorStr) const noexcept
{
    if (false == mIsOpenDb) {
        *errorStr = strdup("DB is close");
        return false;
    }

    bool retVal = false;
    auto iterator = mKvsLevelDb->NewIterator(leveldb::ReadOptions());

    for (iterator->SeekToFirst(); iterator->Valid(); iterator->Next()) {
        (*keys).emplace_back(iterator->key().ToString());
    }

    if (iterator->status().ok()) {
        retVal = true;
    } else {
        *errorStr = iterator->status().ToString();
        retVal = false;
    }

    delete iterator;
    return retVal;
}

bool KeyValueStorageLevelDB::HasKey(const std::string& key, std::string* errorStr) const noexcept
{
    if (false == mIsOpenDb) {
        *errorStr = strdup("DB is close");
        return false;
    }

    bool retVal = false;
    std::string value;

    auto status = mKvsLevelDb->Get(leveldb::ReadOptions(), key, &value);
    if (status.ok()) {
        retVal = true;
    } else {
        *errorStr = status.ToString();
        retVal = false;
    }
    return retVal;
}

bool KeyValueStorageLevelDB::GetValue(const std::string& key, std::string* value, std::string* errorStr) const noexcept
{
    if (false == mIsOpenDb) {
        *errorStr = strdup("DB is close");
        return false;
    }

    bool retVal = false;

    auto status = mKvsLevelDb->Get(leveldb::ReadOptions(), key, value);
    if (status.ok()) {
        retVal = true;
    } else {
        *errorStr = status.ToString();
        retVal = false;
    }
    return retVal;
}

bool KeyValueStorageLevelDB::SetValue(const std::string& key, const std::string& value, std::string* errorStr) noexcept
{
    if (false == mIsOpenDb) {
        *errorStr = strdup("DB is close");
        return false;
    }

    bool retVal = false;
    leveldb::WriteOptions opts;
    opts.sync = true;

    auto status = mKvsLevelDb->Put(opts, key, value);
    if (status.ok()) {
        retVal = true;
    } else {
        *errorStr = status.ToString();
        retVal = false;
    }
    return retVal;
}

bool KeyValueStorageLevelDB::RemoveKey(const std::string& key, std::string* errorStr) noexcept
{
    if (false == mIsOpenDb) {
        *errorStr = strdup("DB is close");
        return false;
    }

    bool retVal = false;
    leveldb::WriteOptions opts;
    opts.sync = true;

    auto status = mKvsLevelDb->Delete(opts, key);
    if (status.ok()) {
        retVal = true;
    } else {
        *errorStr = status.ToString();
        retVal = false;
    }
    return retVal;
}

bool KeyValueStorageLevelDB::RemoveAllKeys(std::string* errorStr) noexcept
{
    if (false == mIsOpenDb) {
        *errorStr = strdup("DB is close");
        return false;
    }

    leveldb::WriteBatch batch;
    auto iterator = mKvsLevelDb->NewIterator(leveldb::ReadOptions());

    for (iterator->SeekToFirst(); iterator->Valid(); iterator->Next()) {
        batch.Delete(iterator->key().ToString());
    }

    if (iterator->status().ok()) {
        delete iterator;
    } else {
        delete iterator;
        *errorStr = iterator->status().ToString();
        return false;
    }

    bool retVal = false;
    leveldb::WriteOptions opts;
    opts.sync = true;

    leveldb::Status status = mKvsLevelDb->Write(opts, &batch);
    if (status.ok()) {
        retVal = true;
    } else {
        *errorStr = status.ToString();
        retVal = false;
    }
    return retVal;
}

bool KeyValueStorageLevelDB::RepareKeyValueStorage(const std::string& database, std::string* errorStr) noexcept
{
    if (nullptr == database.c_str()) {
        *errorStr = strdup("databaseName is NULL");
        return false;
    }

    char* pErr = nullptr;  

    leveldb_options_t* pOptions = leveldb_options_create();
    leveldb_repair_db(pOptions, database.c_str(), &pErr);
    if (pOptions) {
        leveldb_options_destroy(pOptions);
    }
    
    if(nullptr != pErr)
    {
        *errorStr = pErr;
        return false;
    }

    return true;
}


}  // namespace kv
}  // namespace ota
}  // namespace abup
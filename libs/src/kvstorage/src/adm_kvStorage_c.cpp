#include "adm_kvStorage.h"
#include "kvstorage/adm_kvStorage_c.h"
#include "adm_kvStorage_impl_leveldb.h"
#include "leveldb/c.h"

#include <memory>
#include <mutex>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include "adm_string.h"

extern "C" {

struct adm_kvStorage_
{
    std::unique_ptr<abup::ota::kv::KeyValueStorage> mleveldb;
    std::mutex mMutex;
};

adm_kvStorage_t* adm_kvStorage_open(const char* databaseName, char** errorStr)
{
    if (!databaseName) {
        *errorStr = strdup("databaseName is NULL");
        return NULL;
    }

    // if (access(databaseName, F_OK) != 0) {
    //     *errorStr = strdup("database directory does not exist");
    //     return NULL;
    // }

    adm_kvStorage_t* kvs = NULL;
    std::string dbErrorStr;
    bool retVal = false;

    kvs = new adm_kvStorage_t;
    kvs->mleveldb.reset(new abup::ota::kv::KeyValueStorageLevelDB());

    retVal = kvs->mleveldb->OpenKeyValueStorage(databaseName, &dbErrorStr);
    if (retVal) {
        return kvs;
    } else {
        *errorStr = strdup(dbErrorStr.c_str());
        delete kvs;
        kvs = NULL;
        return NULL;
    }
}

bool adm_kvstorage_repare(const char* pDbName, char** errorStr)
{
    if (NULL == pDbName) {
        *errorStr = strdup("databaseName is NULL");
        return false;
    }

    adm_kvStorage_t* kvs = NULL;
    std::string dbErrorStr;
    bool retVal = false;

    kvs = new adm_kvStorage_t;
    kvs->mleveldb.reset(new abup::ota::kv::KeyValueStorageLevelDB());
    retVal = kvs->mleveldb->RepareKeyValueStorage(pDbName, &dbErrorStr);
    if (retVal) 
    {
        delete kvs;
        kvs = NULL;
        return true;
    } 
    else 
    {
        *errorStr = strdup(dbErrorStr.c_str());
        delete kvs;
        kvs = NULL;
        return false;
    }

    return true;

    
}

bool adm_kvStorage_destroy(adm_kvStorage_t* kvs, char** errorStr)
{
    if (!kvs) {
        *errorStr = strdup("kvs is NULL");
        return false;
    }

    std::unique_lock<std::mutex> lock(kvs->mMutex);

    std::string dbErrorStr;
    bool retVal = false;
    retVal = kvs->mleveldb->DestroyKeyValueStorage(&dbErrorStr);
    if (!retVal) {
        *errorStr = strdup(dbErrorStr.c_str());
    }
    return retVal;
}

void adm_kvStorage_close(adm_kvStorage_t* kvs)
{
    if (!kvs) {
        return;
    }

    delete kvs;
    kvs = NULL;
    return;
}

bool adm_kvStorage_has_key(adm_kvStorage_t* kvs, const char* key, char** errorStr)
{
    if (!kvs || !key) {
        *errorStr = strdup("input parameter is invalid");
        return false;
    }

    std::unique_lock<std::mutex> lock(kvs->mMutex);

    std::string dbErrorStr;
    bool retVal = false;
    retVal = kvs->mleveldb->HasKey(key, &dbErrorStr);
    if (!retVal) {
        *errorStr = strdup(dbErrorStr.c_str());
    }
    return retVal;
}

static std::string kvStorageGenerateValueTypeKey(const std::string& key)
{
    return key + "_vt";
}

bool adm_kvStorage_remove_key(adm_kvStorage_t* kvs, const char* key, char** errorStr)
{
    if (!kvs || !key) {
        *errorStr = strdup("input parameter is invalid");
        return false;
    }

    std::unique_lock<std::mutex> lock(kvs->mMutex);

    std::string dbErrorStr;
    bool retVal = false;
    retVal = kvs->mleveldb->RemoveKey(key, &dbErrorStr);
    if (!retVal) {
        *errorStr = strdup(dbErrorStr.c_str());
    }

    std::string keyType = kvStorageGenerateValueTypeKey(key);
    retVal = kvs->mleveldb->RemoveKey(keyType, &dbErrorStr);
    if (!retVal) {
        *errorStr = strdup(dbErrorStr.c_str());
    }
    return retVal;
}

bool adm_kvStorage_remove_allKeys(adm_kvStorage_t* kvs, char** errorStr)
{
    if (!kvs) {
        *errorStr = strdup("input parameter is invalid");
        return false;
    }

    std::unique_lock<std::mutex> lock(kvs->mMutex);

    bool retVal = false;
    std::string dbErrorStr;
    retVal = kvs->mleveldb->RemoveAllKeys(&dbErrorStr);
    if (!retVal) {
        *errorStr = strdup(dbErrorStr.c_str());
    }
    return retVal;
}

bool adm_kvStorage_set_string(adm_kvStorage_t* kvs,
    const char* key,
    const char* val,
    const ADM_KV_STORAGE_VALUE_TYPE_E eValType,
    char** errorStr)
{
    if (!kvs || !key || !val) {
        *errorStr = strdup("input parameter is invalid");
        return false;
    }
    if (ADM_KV_STORAGE_VALUE_TYPE_UINT8 > eValType || ADM_KV_STORAGE_VALUE_TYPE_JSON < eValType) {
        *errorStr = strdup("data type parameter is invalid");
        return false;
    }

    std::unique_lock<std::mutex> lock(kvs->mMutex);

    std::string dbErrorStr;
    bool retVal = false;
    std::string keyType;
    std::string keyTypeVal;

    retVal = kvs->mleveldb->SetValue(key, val, &dbErrorStr);
    if (!retVal) {
        *errorStr = strdup(dbErrorStr.c_str());
        return retVal;
    }

    keyType = kvStorageGenerateValueTypeKey(key);
    keyTypeVal = std::to_string(eValType);

    retVal = kvs->mleveldb->SetValue(keyType, keyTypeVal, &dbErrorStr);
    if (!retVal) {
        *errorStr = strdup(dbErrorStr.c_str());
        return retVal;
    }
    return retVal;
}

bool adm_kvStorage_get_string(adm_kvStorage_t* kvs,
    const char* key,
    char** val,
    ADM_KV_STORAGE_VALUE_TYPE_E* eValType,
    char** errorStr)
{
    if (!kvs || !key) {
        *errorStr = strdup("input parameter is invalid");
        return false;
    }

    std::unique_lock<std::mutex> lock(kvs->mMutex);

    std::string dbErrorStr;
    bool retVal = false;
    std::string stringVal;
    std::string keyType;
    std::string keyTypeVal;
    D32S keyTypeValEValType;

    retVal = kvs->mleveldb->GetValue(key, &stringVal, &dbErrorStr);
    if (!retVal) {
        *errorStr = strdup(dbErrorStr.c_str());
        return retVal;
    }

    keyType = kvStorageGenerateValueTypeKey(key);

    retVal = kvs->mleveldb->GetValue(keyType, &keyTypeVal, &dbErrorStr);
    if (!retVal) {
        *errorStr = strdup(dbErrorStr.c_str());
        return retVal;
    }

    *val = strdup(stringVal.c_str());
    adm_stringToInt32(keyTypeVal.c_str(), &keyTypeValEValType);
    *eValType = (ADM_KV_STORAGE_VALUE_TYPE_E)keyTypeValEValType;

    return retVal;
}

bool adm_kvStorage_get_allKeys(adm_kvStorage_t* kvs, adm_kvStorage_all_key_t** keyList, char** errorStr)
{
    if (!kvs) {
        *errorStr = strdup("input parameter is invalid");
        return false;
    }

    std::unique_lock<std::mutex> lock(kvs->mMutex);

    bool retVal = false;
    std::vector<std::string> allKeys;
    std::string dbErrorStr;

    retVal = kvs->mleveldb->GetAllKeys(&allKeys, &dbErrorStr);
    if (retVal) {
        *keyList
            = (adm_kvStorage_all_key_t*)malloc(sizeof(adm_kvStorage_all_key_t) + sizeof(char*) * (allKeys.size() + 1));
        (*keyList)->num = 0;
        for (auto iter : allKeys) {
            (*keyList)->listKeys[(*keyList)->num] = strdup(iter.c_str());
            (*keyList)->num++;
        }
    } else {
        *errorStr = strdup(dbErrorStr.c_str());
    }
    return retVal;
}
}
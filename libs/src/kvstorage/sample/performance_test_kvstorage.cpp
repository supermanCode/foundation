#include "kvstorage/adm_kvStorage_c.h"
#include <thread>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static const char* g_kDatabaseName = "./kvStorage";

int main()
{
    char* errorStr = NULL;
    adm_kvStorage_t* kvs = adm_kvStorage_open(g_kDatabaseName, &errorStr);
    if (!kvs) {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
        return 0;
    }
    std::thread thread([kvs]() {
        char* errorStr = NULL;
        bool retVal = false;
        std::string key;
        std::string value;
        ADM_KV_STORAGE_VALUE_TYPE_E eValType = ADM_KV_STORAGE_VALUE_TYPE_INVALID;
        char* str = nullptr;
        for (int i = 0; i < 5000; ++i) {
            key = "key" + std::to_string(i);
            value = "value" + std::to_string(i);
            retVal = adm_kvStorage_set_string(
                kvs, key.c_str(), value.c_str(), ADM_KV_STORAGE_VALUE_TYPE_STRING, &errorStr);
            if (!retVal) {
                printf("errorStr = %s\n", errorStr);
                free(errorStr);
                errorStr = NULL;
            }
            printf("set key = %s, value = %s\n", key.c_str(), value.c_str());

            if (adm_kvStorage_get_string(kvs, key.c_str(), &str, &eValType, &errorStr)) {
                printf("get key = %s, value = %s , eValType:%d\n", key.c_str(), value.c_str(), eValType);
                if (str) {
                    free(str);
                    str = NULL;
                }
            } else {
                printf("errorStr = %s\n", errorStr);
                free(errorStr);
                errorStr = NULL;
            }
        }
    });
    std::this_thread::sleep_for(std::chrono::seconds(2));
    adm_kvStorage_close(kvs);
    while (1) {
        std::this_thread::sleep_for(std::chrono::seconds(2));
    }
    return 0;
}
cmake_minimum_required(VERSION 3.13)
project(kvstorage C CXX)

# set(CMAKE_C_FLAGS "-fPIC")
# set(CMAKE_CXX_FLAGS "-fPIC")

add_executable(
	kvstorage_test
	sample_kvstorage.c
)

target_include_directories(
	kvstorage_test
    PUBLIC
	$<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/libs/inc>
)

target_link_libraries(
	kvstorage_test
		-Wl,--whole-archive
		${CMAKE_SOURCE_DIR}/build/libs/libfoundation.so
		-Wl,--no-whole-archive
		stdc++
        ${THREAD_LIBRARY}
		${SSL_LIBRARY}
)

add_executable(
	kvstorage_performance_test
	performance_test_kvstorage.cpp
)

target_include_directories(
	kvstorage_performance_test
    PUBLIC
	$<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/libs/inc>
)

target_link_libraries(
	kvstorage_performance_test
		-Wl,--whole-archive
		${CMAKE_SOURCE_DIR}/build/libs/libfoundation.so
		-Wl,--no-whole-archive
		stdc++
        ${THREAD_LIBRARY}
		${SSL_LIBRARY}
)

#include "kvstorage/adm_kvStorage_c.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static const char* g_kDatabaseName = "./kvStorage";

int main()
{
    char* errorStr = NULL;
    bool retVal = false;
    ADM_KV_STORAGE_VALUE_TYPE_E eValType = ADM_KV_STORAGE_VALUE_TYPE_INVALID;

    adm_kvStorage_t* kvs = adm_kvStorage_open(g_kDatabaseName, &errorStr);
    if (!kvs) {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
        return 0;
    }

    retVal = adm_kvStorage_set_string(kvs, "str", "erabc", ADM_KV_STORAGE_VALUE_TYPE_STRING, &errorStr);
    if (!retVal) {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }

    retVal = adm_kvStorage_set_string(kvs, "str1", "123", ADM_KV_STORAGE_VALUE_TYPE_INT32, &errorStr);
    if (!retVal) {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }

    retVal = adm_kvStorage_set_string(kvs, "str2", "178.45", ADM_KV_STORAGE_VALUE_TYPE_DOUBLE, &errorStr);
    if (!retVal) {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }

    char* str = NULL;
    if (adm_kvStorage_get_string(kvs, "str", &str, &eValType, &errorStr)) {
        printf("str = %s , eValType:%d\n", str, eValType);
        if (str) {
            free(str);
            str = NULL;
        }
    } else {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }

    if (adm_kvStorage_get_string(kvs, "str1", &str, &eValType, &errorStr)) {
        printf("str1 = %s , eValType:%d\n", str, eValType);
        if (str) {
            free(str);
            str = NULL;
        }
    } else {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }

    if (adm_kvStorage_get_string(kvs, "str2", &str, &eValType, &errorStr)) {
        printf("str2 = %s , eValType:%d\n", str, eValType);
        if (str) {
            free(str);
            str = NULL;
        }
    } else {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }

    adm_kvStorage_all_key_t* keyList = NULL;
    if (adm_kvStorage_get_allKeys(kvs, &keyList, &errorStr)) {
        for (int i = 0; i < keyList->num; i++) {
            printf("key[%d]:%s\n", i, keyList->listKeys[i]);
        }
        for (int j = 0; j < keyList->num; ++j) {
            free(keyList->listKeys[j]);
            keyList->listKeys[j] = NULL;
        }
        free(keyList);
        keyList = NULL;
    } else {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }

    retVal = adm_kvStorage_has_key(kvs, "str", &errorStr);
    if (retVal) {
        printf("HasKey str \n");
    } else {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }
    retVal = adm_kvStorage_has_key(kvs, "str1", &errorStr);
    if (retVal) {
        printf("HasKey str1 \n");
    } else {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }
    retVal = adm_kvStorage_has_key(kvs, "str2", &errorStr);
    if (retVal) {
        printf("HasKey str2 \n");
    } else {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }

    retVal = adm_kvStorage_remove_key(kvs, "str", &errorStr);
    if (retVal) {
        printf("remove str success\n");
    } else {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }

    retVal = adm_kvStorage_remove_key(kvs, "str1", &errorStr);
    if (retVal) {
        printf("remove str1 success\n");
    } else {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }

    retVal = adm_kvStorage_remove_key(kvs, "str2", &errorStr);
    if (retVal) {
        printf("remove str2 success\n");
    } else {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }

    retVal = adm_kvStorage_set_string(kvs, "str", "erabc", ADM_KV_STORAGE_VALUE_TYPE_STRING, &errorStr);
    if (!retVal) {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }

    retVal = adm_kvStorage_set_string(kvs, "str1", "123", ADM_KV_STORAGE_VALUE_TYPE_INT32, &errorStr);
    if (!retVal) {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }

    retVal = adm_kvStorage_set_string(kvs, "str2", "178.45", ADM_KV_STORAGE_VALUE_TYPE_DOUBLE, &errorStr);
    if (!retVal) {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }

    retVal = adm_kvStorage_has_key(kvs, "str", &errorStr);
    if (retVal) {
        printf("HasKey str \n");
    } else {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }
    retVal = adm_kvStorage_has_key(kvs, "str1", &errorStr);
    if (retVal) {
        printf("HasKey str1 \n");
    } else {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }
    retVal = adm_kvStorage_has_key(kvs, "str2", &errorStr);
    if (retVal) {
        printf("HasKey str2 \n");
    } else {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }

    retVal = adm_kvStorage_remove_allKeys(kvs, &errorStr);
    if (retVal) {
        printf("remove all key success\n");
    } else {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }

    retVal = adm_kvStorage_has_key(kvs, "str", &errorStr);
    if (retVal) {
        printf("HasKey str \n");
    } else {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }
    retVal = adm_kvStorage_has_key(kvs, "str1", &errorStr);
    if (retVal) {
        printf("HasKey str1 \n");
    } else {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }
    retVal = adm_kvStorage_has_key(kvs, "str2", &errorStr);
    if (retVal) {
        printf("HasKey str2 \n");
    } else {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }

    /*for test destroy*/
    #if 1
    retVal = adm_kvStorage_destroy(kvs, &errorStr);
    if (retVal) {
        printf("destroy db success\n");
    } else {
        printf("errorStr = %s\n", errorStr);
        free(errorStr);
        errorStr = NULL;
    }
    #endif

    adm_kvStorage_close(kvs);
    return 0;
}
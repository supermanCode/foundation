/*
 * Copyright 2018 The ABUP Project Authors. All Rights Reserved.
 *
 * Licensed under the ABUP license (the "License").  You may not use
 * this file except in compliance with the License. 
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include "build_version.h"
#define VERSION_DATA_LEN 256 

char* foundation_getVersion(void)
{
    static char version_data[VERSION_DATA_LEN] = {0};

    memset(version_data,0,VERSION_DATA_LEN);
//    printf("Build version: 0x%08lX\n", OPENSSL_VERSION_NUMBER);
//    printf("Library version: 0x%08lX\n", OpenSSL_version_num());
//    log_i("foundation build info: \n build time=%s\n commit ID=%s\n Tool Link=%s\n ", V_BUILD_TIME,V_GIT_INFO,V_BUILD_TOOLLINK);
    sprintf((char *)version_data,
					"{\"project_name\":\"%s\",\"build_time\":\"%s\",\"git_id\":\"%s\",\"tool_link\":\"%s\"}",
					V_PROJECT,
					V_BUILD_TIME,
					V_GIT_INFO,
					V_BUILD_TOOLLINK);
    return version_data;
}

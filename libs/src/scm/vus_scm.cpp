// vus_serial.cpp :定义控制台应用程序的入口点
//


//#include "./json-c/json_types.h"
//#include "./json-c/json_object.h"
//#include "./json-c/json.h"
#include "json_types.h"
#include "json_object.h"
#include "json.h"




#include "adm_typedefs.h"
#include "adm_memory.h"
#include "adm_string.h"
#include "vus_scm.h"


int main()
{
    return 0;
}

typedef struct vus_convert_ctrl_struct
{
    D32U tableNumber;
    vus_serial_table_cpt pSCTables;
    vus_serial_table_cpt pMain;
    json_object *pMainJsonObj;
} vus_convert_ctrl_st, *vus_convert_ctrl_pt;

//DHANDLE sc_create_handle(const DCHAR* sc_ctrl_name)
//{
//
//}
//
//D32U sc_add_table(DHANDLE ctrl_handle, const DCHAR*  st_name, vus_serial_table_pt serial_tables, D32U number)
//{
//
//}

static D8U *convertJson2Struct(vus_convert_ctrl_pt pCtrl, json_object *pJsonObj,
                               vus_serial_table_cpt pSCTable);


static DBOOL checkConvertTable(vus_serial_table_cpt pSCTable)
{
    DASSERT(pSCTable != DNULL);

    if (pSCTable->name == DNULL || pSCTable->size == 0 || pSCTable->itemNumber == 0)
    {
        return DFALSE;
    }
    else
    {
        return DTRUE;
    }
}

vus_convert_ctrl_pt createSCControl(vus_serial_table_cpt pSCTables, D32U tableNumber,
                                    vus_serial_table_cpt pMainSCTable)
{
    vus_convert_ctrl_pt pRet = DNULL;
    DASSERT(pSCTables != DNULL);
    DASSERT(pMainSCTable != DNULL);
    const D32U convertControlSize = sizeof(vus_convert_ctrl_st);

    pRet = (vus_convert_ctrl_pt)adm_malloc(convertControlSize);

    if (pRet != DNULL)
    {
        adm_memzero(pRet, convertControlSize);
        pRet->pSCTables = pSCTables;
        pRet->pMain = pMainSCTable;
        pRet->tableNumber = tableNumber;
    }
    else {}

    return pRet;
}

void freeSCControl(vus_convert_ctrl_pt pCtrl)
{
    DASSERT(pCtrl != DNULL);

    adm_free(pCtrl);
}


vus_serial_table_cpt findSCTable(vus_convert_ctrl_pt pCtrl, const char *name)
{
    vus_serial_table_cpt pRet = DNULL;

    DASSERT(pCtrl != DNULL);

    for (D32U i = 0; i < pCtrl->tableNumber; i++)
    {
        vus_serial_table_cpt pSCTable = &(pCtrl->pSCTables[i]);

        if (adm_strcmp(name, pSCTable->name) == 0)
        {
            pRet = pSCTable;
            break;
        }
    }

    return pRet;
}

static D32U convertJson2Item(vus_convert_ctrl_pt pCtrl, D8U *pStructBuffer, D32U bufferSize,
                             json_object *pJsonObj, vus_serial_item_cpt pItem)
{
    D32S retCode = VUS_SC_OK;

    switch (pItem->type)
    {
        case VUS_DATA_TYPE_S32:
        {
            D32S Value = json_object_get_int(pJsonObj);
            D32S Size = pItem->size;

            if ((Size == sizeof(D32S)) && ((D32S)(bufferSize - pItem->startByte) >= Size))
            {
                adm_memcpy(pStructBuffer + pItem->startByte, &Value, bufferSize - pItem->startByte, Size);
            }
            else
            {
                DASSERT(DFALSE);
            }
        }

        case VUS_DATA_TYPE_S64:
        {
            D64S Value = json_object_get_int64(pJsonObj);
            D32S Size = pItem->size;

            if ((Size == sizeof(D64S)) && ((D32S)(bufferSize - pItem->startByte) >= Size))
            {
                adm_memcpy(pStructBuffer + pItem->startByte, &Value, bufferSize - pItem->startByte, Size);
            }
            else
            {
                DASSERT(DFALSE);
            }

            break;
        }

        case VUS_DATA_TYPE_STRINGPT:
        {
            const char *pValue = adm_strdup(json_object_get_string(pJsonObj));

            if (pValue != DNULL && (pItem->size == sizeof(char *)))
            {
                adm_memcpy(pStructBuffer + pItem->startByte, &pValue, bufferSize - pItem->startByte, pItem->size);
            }
            else
            {
                DASSERT(DFALSE);
            }

            break;
        }

        case VUS_DATA_TYPE_STRINGARRAY:
        {
            const char *pValue = json_object_get_string(pJsonObj);

            if (pValue != DNULL)
            {
                adm_strcpy((DCHAR *)(pStructBuffer + pItem->startByte), pValue, pItem->size);
            }
            else
            {
                DASSERT(DFALSE);
            }

            break;
        }

        case VUS_DATA_TYPE_OBJ:
        {
            vus_serial_table_cpt pSCSubTable = findSCTable(pCtrl, pItem->keyName);

            if (pSCSubTable != DNULL)
            {
                D8U *pValue = convertJson2Struct(pCtrl, pJsonObj, pSCSubTable);

                if (pValue != DNULL)
                {
                    adm_memcpy(pStructBuffer + pItem->startByte, pValue, bufferSize - pItem->startByte, pItem->size);
                    adm_free(pValue);
                }
                else
                {
                    DASSERT(DFALSE);
                }
            }
            else
            {
                DASSERT(DFALSE);
            }

            break;
        }

        case VUS_DATA_TYPE_OBJPT:
        {
            vus_serial_table_cpt pSCSubTable = findSCTable(pCtrl, pItem->keyName);

            if (pSCSubTable != DNULL)
            {
                D8U *pValue = convertJson2Struct(pCtrl, pJsonObj, pSCSubTable);

                if (pValue != DNULL && (pItem->size == sizeof(void *)))
                {
                    adm_memcpy(pStructBuffer + pItem->startByte, &pValue, bufferSize - pItem->startByte, pItem->size);
                }
                else
                {
                    DASSERT(DFALSE);
                }
            }
            else
            {
                DASSERT(DFALSE);
            }

            break;
        }

        case VUS_DATA_TYPE_ARRAY:
        {
            break;
        }

        case VUS_DATA_TYPE_PT:
        {
            break;
        }

        default:
            break;
    }

    return retCode;
}

static D8U *convertJson2Struct(vus_convert_ctrl_pt pCtrl, json_object *pJsonObj,
                               vus_serial_table_cpt pSCTable)
{
    D8U *pRet = DNULL;
    D32U objStructSize = 0;
    DASSERT(pCtrl != DNULL);
    DASSERT(pJsonObj != DNULL);
    DASSERT(pSCTable != DNULL);
    DASSERT(pSCTable->size != 0);

    do
    {
        if (checkConvertTable(pSCTable) == DFALSE)
        {
            break;
        }

        objStructSize = pSCTable->size;
        pRet = (D8U *)adm_malloc(objStructSize);

        for (D32U i = 0; i < pSCTable->itemNumber; i++)
        {
            vus_serial_item_cpt pItem = pSCTable->items + i;
            json_object *pJsonValue = DNULL;

            if (!json_object_object_get_ex(pJsonObj, pItem->keyName, &pJsonValue))
            {
                break;
            }

        }
    }
    while (DFALSE);

    return pRet;
}

D8U *adm_sc_convertJson2Struct(vus_serial_table_pt pSCTables, D32U tableNumber,
                               vus_serial_table_pt pMainSCTable, const DCHAR *json)
{
    D8U *pRet = DNULL;
    json_object *pJsonObj = DNULL;
    vus_convert_ctrl_pt pCtrl = DNULL;

    do
    {
        if (pSCTables == DNULL || pSCTables->itemNumber == 0)
        {
            break;
        }

        pJsonObj = json_tokener_parse(json);
        pCtrl = createSCControl(pSCTables, tableNumber, pMainSCTable);

        if (pCtrl != DNULL)
        {
            pRet = convertJson2Struct(pCtrl, pJsonObj, pMainSCTable);
            freeSCControl(pCtrl);
        }
        else
        {
            DASSERT(DFALSE);
        }
    }
    while (DFALSE);

    return pRet;
}

DHANDLE adm_sc_createHandle(vus_serial_table_pt pSCTables, D32U tableNumber,
                         vus_serial_table_pt pMainSCTable, DHANDLE *pHandle)
{
    vus_convert_ctrl_pt pCtrl = DNULL;
    pCtrl = createSCControl(pSCTables, tableNumber, pMainSCTable);
    *pHandle = (DHANDLE)pCtrl;

    return 0;
}

D32U adm_sc_getvalue(DHANDLE handle, const char* keyName, D8U* pBuffer, D32U* bufferSize)
{
	return 0;
}


//DHANDLE sc_load_jsonfile(DHANDLE ctrl_handle, const DCHAR* path)
//{
//
//}
//
//DHANDLE sc_convert(DHANDLE ctrl_handle, const DCHAR* json)
//{
//
//}




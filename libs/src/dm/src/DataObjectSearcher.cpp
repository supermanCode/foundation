#include "DataObjectSearcher.h"
#include "Param.h"
#include "DataTree.h"


namespace vus
{
    namespace dm
    {
        ObjectDataSearcher::ObjectDataSearcher(DataTree* pDataTree)
        {
            _pDataTree = pDataTree;
        }

        Value* ObjectDataSearcher::find(const DCHAR* pPath, ParamList* pParamList/* = NULL */)
        {
            Value*          pMember = NULL;
            DataPathNode*   pPathNode = NULL;
            DataPath        path(pPath);
            Value*          pRoot = &_pDataTree->_root;
            ObjectValueDetail   detail;

            pPathNode = path.first();
            if (FALSE == pPathNode->isRoot())
            {
                return NULL;
            }

            while (NULL != pPathNode)
            {
                pMember = search(pRoot, pPathNode, pParamList, &detail);
                if (NULL == pMember)
                {
                    break;
                }

                pRoot = pMember;
                pPathNode = path.next();
            }

            return pMember;
        }

        Value* ObjectDataSearcher::find(Value* pRoot, const DCHAR* pPath, ParamList* pParamList/* = NULL */)
        {
            Value*          pMember = NULL;
            DataPathNode*   pPathNode = NULL;
            DataPath        path(pPath);
            ObjectValueDetail   detail;

            pPathNode = path.first();
            while (NULL != pPathNode)
            {
                pMember = search(pRoot, pPathNode, pParamList, &detail);
                if (NULL == pMember)
                {
                    break;
                }

                pRoot = pMember;
                pPathNode = path.next();
            }

            return pMember;
        }


        Value* ObjectDataSearcher::find(DataPathNode* pPathNode, ParamList* pParamList /*= NULL*/)
        {
            Value* pValue = NULL;
            ObjectValueDetail   detail;

            if (TRUE == pPathNode->isRoot())
            {
                pValue = search(&_pDataTree->_root, pPathNode, pParamList, &detail);
            }

            return pValue;
        }

        Value* ObjectDataSearcher::find(Value* pRoot, DataPathNode* pPathNode, ParamList* pParamList /*= NULL*/)
        {
            ObjectValueDetail   detail;
            return search(pRoot, pPathNode, pParamList, &detail);
        }

        D32S ObjectDataSearcher::find(const DCHAR* pPath, ParamList* pParamList, ObjectValueDetail* pDetail)
        {
            D32S iRet = -1;
            
            DataPathNode*   pPathNode = NULL;
            DataPath        path(pPath);
            Value* pRoot = NULL;
            Value* pMember = &_pDataTree->_root;

            pPathNode = path.first();
            if (FALSE == pPathNode->isRoot())
            {
                return iRet;
            }

            while (NULL != pPathNode)
            {
                pRoot = pMember;
                pMember = search(pRoot, pPathNode, pParamList, pDetail);
                if (NULL == pMember)
                {
                    break;
                }

                pPathNode = path.next();
            }

            if (pMember)
            {
                iRet = 0;
            }

            return iRet;
        }

        DBOOL ObjectDataSearcher::meet(Object* pObject, DataPathNode* pNode, ParamList* pParamList)
        {
            DBOOL bRes = FALSE;
            Value* pValue = NULL;
            DataPathNode::SearchKeyIter iter(pNode);


            if (TRUE != iter.first())
            {
                return bRes;
            }

            do
            {
                bRes = FALSE;

                pValue = pObject->get(iter.getKey());
                if (NULL == pValue)
                {
                    break;
                }

                const DCHAR* pCmpValue;
                if (TRUE == iter.isParam())
                {
                    if (NULL == pParamList || 0 != pParamList->getValue(iter.getParamName(), pCmpValue))
                    {
                        break;
                    }
                }
                else
                {
                    pCmpValue = iter.getValue();
                }

                switch (pValue->getType())
                {
                case VALUE_STRING:
                    if (0 == adm_strcmp(pCmpValue, ((String*)pValue)->asString()))
                    {
                        bRes = TRUE;
                    }
                    break;
                    
                case VALUE_INT:
                    {
                        D64S iValue = (D64S)strtoll(pCmpValue, NULL, 0);
                        if (iValue == ((Number*)pValue)->asInt64())
                        {
                            bRes = TRUE;
                        }
                        break;
                    }
                case VALUE_UINT:
                {
                    D64U uiValue = (D64U)strtoull(pCmpValue, NULL, 0);
                    if (uiValue == ((Number*)pValue)->asUInt64())
                    {
                        bRes = TRUE;
                    }
                    break;
                }
                default:
                    break;
                }
            } while (TRUE == bRes && TRUE == iter.next());

            return bRes;
        }

        Object* ObjectDataSearcher::search(Array* pArray, DataPathNode* pNode, ParamList* pParamList)
        {
            Object* pObject = NULL;
            Value* pMember = NULL;

            for (D32U i = 0; i < pArray->count(); i++)
            {
                pMember = pArray->get(i);
                if (VALUE_OBJECT == pMember->getType())
                {
                    if (TRUE == meet((Object*)pMember, pNode, pParamList))
                    {
                        pObject = (Object*)pMember;
                        break;
                    }
                }
            }

            return pObject;
        }

        Value* ObjectDataSearcher::search(Value* pRoot, DataPathNode* pNode, ParamList* pParamList, ObjectValueDetail* pValueDetail)
        {
            Value* pMember = NULL;

            do 
            {
                if (VALUE_OBJECT != pRoot->getType())
                {
                    break;
                }

                pMember = ((Object*)pRoot)->get(pNode->getName());
                if (NULL == pMember)
                {
                    break;
                }

                if (FALSE == pNode->hasSearchKey())
                {
                    pValueDetail->pParent = pRoot;
                    pValueDetail->pValue = pMember;
                    break;
                }

                switch (pMember->getType())
                {
                case VALUE_ARRAY:
                    pValueDetail->pParent = pMember;
                    pMember = search((Array*)pMember, pNode, pParamList);
                    pValueDetail->pValue = pMember;
                    break;
                case VALUE_OBJECT:
                    if (FALSE == meet((Object*)pMember, pNode, pParamList))
                    {
                        pMember = NULL;
                    }
                    pValueDetail->pParent = pRoot;
                    pValueDetail->pValue = pMember;
                    break;
                default:
                    pMember = NULL;
                    break;
                }
            } while (FALSE);
            
            return pMember;
        }
    }
}
#include "Param.h"

namespace vus
{
    namespace dm
    {

//         D32S ParamList::init(D32U uiCount)
//         {
//             return 0;
//         }

        D32S ParamList::setValue(const DCHAR* pName, const DCHAR* pValue)
        {
            return _dict.add(pName, pValue);
        }
// 
//         D32S ParamList::setValue(const DCHAR* pName, D32U uiValue)
//         {
//             return 0;
//         }
// 
//         D32S ParamList::getValue(const DCHAR* pName, const D64S& iValue)
//         {
//             return 0;
//         }
// 
//         D32S ParamList::getValue(const DCHAR* pName, const D64U& uiValue)
//         {
//             return 0;
//         }

        D32S ParamList::getValue(const DCHAR* pName, const DCHAR*& pValue)
        {
            pValue = _dict.find(pName);
            return NULL != pValue ? 0 : -1;
        }

//         DCHAR* ParamList::getValue(D32U uiIndex)
//         {
//             return NULL;
//         }

    }
}
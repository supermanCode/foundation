#include "adm_typedefs.h"
#include "Object.h"
#include <map>
#include "adm_memory.h"
#include "adm_string.h"

// 
// typedef struct
// {
//     D32S    _iType;
// } ADM_DM_VALUE_T;
// 
// D32S adm_dm_getType(const ADM_DM_VALUE_T* pValue)
// {
//     return pValue->_iType;
// }
// 
// 
// typedef enum
// {
//     ADM_DM_VALUE_INT    = 1,
//     ADM_DM_VALUE_UINT,
//     ADM_DM_VALUE_REAL,
//     ADM_DM_VALUE_STRING,
//     ADM_DM_VALUE_ARRAY,
//     ADM_DM_VALUE_OBJECT
// } ADM_DM_VALUE_TYPE_E;
// 
// 
// typedef struct
// {
//     ADM_DM_VALUE_T  _base;
//     union
//     {
//         D32S    _i32;
//         D32U    _ui32;
//         D64S    _i64;
//         D64U    _ui64;
//         DOUBLE  _d;
//     }_value;
// } ADM_DM_NUMBER_T;
// 
// D32S adm_dm_asInt(const ADM_DM_NUMBER_T* pValue)
// {
//     return ((ADM_DM_NUMBER_T*)pValue)->_value._i32;
// }
// 
// D32U adm_dm_asUInt(const ADM_DM_NUMBER_T* pValue)
// {
//     return ((ADM_DM_NUMBER_T*)pValue)->_value._i32;
// }
// 
// D64S adm_dm_asInt64(const ADM_DM_NUMBER_T* pValue)
// {
//     return ((ADM_DM_NUMBER_T*)pValue)->_value._i64;
// }
// 
// D64U adm_dm_asUInt64(const ADM_DM_NUMBER_T* pValue)
// {
//     return ((ADM_DM_NUMBER_T*)pValue)->_value._ui64;
// }
// 
// 
// 
// 
// 
// typedef struct
// {
//     ADM_DM_VALUE_T  _base;
//     DCHAR* _pValue;
// } ADM_DM_STRING_T;
// 
// typedef struct
// {
//     std::map<const char*, ADM_DM_VALUE_T*>  _map;
// } ADM_DICTIONARY_T;
// 
// typedef struct
// {
//     ADM_DM_VALUE_T      _base;
//     ADM_DICTIONARY_T    _dict;
//     D32U                _uiCount;
// } ADM_DM_ARRAY_T;
// 
// typedef struct
// {
//     ADM_DM_VALUE_T      _base;
//     ADM_DICTIONARY_T    _dict;
// } ADM_DM_OBJECT_T


namespace vus
{
    namespace dm
    {

        Number::Number()
        {
            _iType = VALUE_INT;
            _value.i64 = 0;
        }


        Number::Number(D32S iType)
        {
            _iType = iType;
            _value.i64 = 0;
        }

        Number::~Number()
        {

        }

        D32S Number::asInt32() const
        {
            if (VALUE_REAL != _iType)
            {
                return _value.i32;
            }
            else
            {
                return (D64S)_value.d;
            }
        }

        D32U Number::asUInt32() const
        {
            if (VALUE_REAL != _iType)
            {
                return _value.ui32;
            }
            else
            {
                return (D32U)_value.d;
            }
        }

        D64S Number::asInt64() const
        {
            if (VALUE_REAL != _iType)
            {
                return _value.i64;
            }
            else
            {
                return (D64S)_value.d;
            }
        }

        D64U Number::asUInt64() const
        {
            if (VALUE_REAL != _iType)
            {
                return _value.ui64;
            }
            else
            {
                return (D32U)_value.d;
            }
        }

        DOUBLE Number::asDouble() const
        {
            DOUBLE dValue = 0.0;

            switch (_iType)
            {
            case VALUE_REAL:
                dValue = _value.d;
                break;
            case VALUE_INT:
                dValue = (DOUBLE)_value.i64;
                break;
            case VALUE_UINT:
                dValue = (DOUBLE)_value.ui64;
                break;
            default:
                break;
            }
                
            return dValue;
        }

        void Number::setAsInt32(D32S iValue)
        {
            _iType = VALUE_INT;
            _value.i32 = iValue;
        }

        void Number::setAsUInt32(D32U uiValue)
        {
            _iType = VALUE_UINT;
            _value.ui32 = uiValue;
        }

        void Number::setAsInt64(D64S iValue)
        {
            _iType = VALUE_INT;
            _value.i64 = iValue;
        }

        void Number::setAsUInt64(D64U uiValue)
        {
            _iType = VALUE_UINT;
            _value.ui64 = uiValue;
        }

        void Number::setAsDouble(DOUBLE dValue)
        {
            _iType = VALUE_REAL;
            _value.d = dValue;
        }


        String::String()
        {
            _iType = VALUE_STRING;
            _pValue = NULL;
            _uiSize = 0;
        }


        String::~String()
        {
            adm_free(_pValue);
        }

        const DCHAR* String::asString() const
        {
            return _pValue;
        }

        D32S String::setString(const DCHAR* pValue)
        {
            D32S iRet = -1;
            do
            {
                if (NULL == pValue)
                {
                    break;
                }

                D32U uiLength = (D32U)adm_strlen(pValue) + 1U;
                if (NULL != _pValue && _uiSize < uiLength)
                {
                    adm_free(_pValue);
                    _pValue = NULL;
                    _uiSize = 0;
                }

                if (NULL == _pValue)
                {
                    _pValue = (DCHAR*)adm_malloc(uiLength);
                    if (NULL == _pValue)
                    {
                        break;
                    }
                    _uiSize = uiLength;
                }

                adm_memcpy(_pValue, pValue, uiLength, uiLength);
                iRet = 0;
            } while (FALSE);

            return iRet;
        }



        Array::Array()
        {
            _iType = VALUE_ARRAY;
            // _uiCount = 0;
        }

        Array::~Array()
        {
            for (VectorIter iter = _vector.begin(); iter != _vector.cend(); iter++)
            {
                delete *iter;
            }
        }

        Value* Array::get(D32U uiIndex)
        {
            return uiIndex < _vector.size() ? _vector[uiIndex] : NULL;
        }

        D32S Array::add(D32U uiIndex, Value* pValue)
        {
            if (uiIndex >= _vector.size())
            {
                _vector.resize(uiIndex + 1);
            }

            _vector[uiIndex] = pValue;
            
            return 0;
        }

        D32S Array::append(Value* pValue)
        {
            _vector.push_back(pValue);
            return 0;
        }

        D32S Array::insert(D32U uiIndex, Value* pValue)
        {
            _vector.emplace(_vector.begin() + uiIndex, pValue);
            return 0;
        }

        D32S Array::remove(D32U uiIndex)
        {
            D32S iRet = -1;
            VectorIter iter = _vector.begin() + uiIndex;
            if (_vector.cend() != iter)
            {
                _vector.erase(iter);
                delete  *iter;
                iRet = 0;
            }
            return iRet;
        }

        D32S Array::remove(Value* pValue)
        {
            D32S iRet = -1;
            for (VectorIter iter = _vector.begin(); iter != _vector.cend(); iter++)
            {
                if (*iter == pValue)
                {
                    _vector.erase(iter);
                    iRet = 0;
                    break;
                }
            }

            return iRet;
        }


        D32S Array::remove(Value* pValue, D32U& uiIndex)
        {
            D32S iRet = -1;
            D32S iIndex = 0;
            for (VectorIter iter = _vector.begin(); iter != _vector.cend(); iter++)
            {
                if (*iter == pValue)
                {
                    _vector.erase(iter);
                    uiIndex = iIndex;
                    iRet = 0;
                    break;
                }
                iIndex++;
            }

            return iRet;
        }

        Value* Array::removeEx(D32U uiIndex)
        {
            Value* pValue = NULL;
            VectorIter iter = _vector.begin() + uiIndex;
            if (_vector.cend() != iter)
            {
                pValue = *iter;
                _vector.erase(iter);
            }
            
            return pValue;
        }

        D32U Array::count()
        {
            return (D32U)_vector.size();
        }



        Object::Object()
        {
            _iType = VALUE_OBJECT;
        }

        Object::~Object()
        {
            clear();
        }

        Value* Object::get(const DCHAR* pName)
        {
            MapIter iter = _map.find(pName);
            return iter != _map.cend() ? iter->second : NULL;
        }


        D32S Object::asInt(const DCHAR* pName, D64S& iValue)
        {
            return 0;
        }


        D32S Object::asUInt(const DCHAR* pName, D64U& uiValue)
        {
            return 0;
        }


        D32S Object::asReal(const DCHAR* pName, DOUBLE& dValue)
        {
            return 0;
        }

        Number* Object::asNumber(const DCHAR* pName)
        {
            Value* pValue = get(pName);
            if (NULL != pValue)
            {
                switch (pValue->getType())
                {
                case VALUE_INT:
                case VALUE_UINT:
                case VALUE_REAL:
                    break;
                default:
                    pValue = NULL;
                    break;
                }
            }

            return NULL != pValue ? (Number*)pValue : NULL;
        }

        D32S Object::asString(const DCHAR* pName, const DCHAR*& pStr)
        {
            D32S iRet = -1;
            Value* pValue = get(pName);
            if (NULL != pValue && VALUE_STRING == pValue->getType())
            {
                pStr = ((String*)pValue)->asString();
                iRet = 0;
            }

            return iRet;
        }

        String* Object::asString(const DCHAR* pName)
        {
            Value* pValue = get(pName);
            return NULL != pValue && VALUE_STRING == pValue->getType() ? (String*)pValue : NULL;
        }

        Array* Object::asArray(const DCHAR* pName)
        {
            Value* pValue = get(pName);
            return NULL != pValue && VALUE_ARRAY == pValue->getType() ? (Array*)pValue : NULL;
        }

        Object* Object::asObject(const DCHAR* pName)
        {
            Value* pValue = get(pName);
            return NULL != pValue && VALUE_OBJECT == pValue->getType() ? (Object*)pValue : NULL;
        }

        D32S Object::add(const DCHAR* pName, Value* pValue)
        {
            D32S iRet = -1;
            const DCHAR* pNameStr = adm_strdup(pName);
            if (NULL != pNameStr)
            {
                _map.insert(MapPair(pNameStr, pValue));
                iRet = 0;
            }
            
            return iRet;
        }

        D32S Object::remove(const DCHAR* pName)
        {
            D32S iRet = -1;
            DCHAR* pPtr = NULL;
            MapIter iter = _map.find(pName);
            if (iter != _map.cend())
            {
                _map.erase(iter);
                pPtr = (DCHAR*)iter->first;
                adm_free(pPtr);
                delete iter->second;
                iRet = 0;
            }

            return iRet;
        }

        D32S Object::remove(Value* pValue)
        {
            DCHAR* pPtr = NULL;
            for (MapIter iter = _map.begin(); iter != _map.cend(); iter++)
            {
                if (iter->second == pValue)
                {
                    _map.erase(iter);
                    pPtr = (DCHAR*)iter->first;
                    adm_free(pPtr);
                    break;
                }
            }

            return 0;
        }

        const DCHAR* Object::getName(Value* pValue)
        {
            const DCHAR* pName = NULL;

            for (MapIter iter = _map.begin(); iter != _map.cend(); iter++)
            {
                if (iter->second == pValue)
                {
                    pName = iter->first;
                    break;
                }
            }

            return pName;
        }

        void Object::clear()
        {
            DCHAR* pPtr = NULL;
            MapIter iter = _map.begin();
            while (iter != _map.cend())
            {
                pPtr = (DCHAR*)iter->first;
                adm_free(pPtr);
                delete iter->second;

                _map.erase(iter);
                iter = _map.begin();
            }
        }

        Value* Object::removeEx(const DCHAR* pName)
        {
            Value* pValue = NULL;
            MapIter iter = _map.find(pName);
            if (iter != _map.cend())
            {
                delete iter->first;
                pValue = iter->second;
                _map.erase(iter);
            }

            return pValue;
        }

        D32S Object::add(const DCHAR* pName, D64S iValue)
        {
            D32S iRet = -1;
            Number* pNumber = new Number();
            if (NULL != pNumber)
            {
                pNumber->setAsInt64(iValue);
                iRet = add(pName, pNumber);
                if (0 != iRet)
                {
                    delete pNumber;
                }
            }

            return iRet;
        }

        D32S Object::add(const DCHAR* pName, D64U uiValue)
        {
            D32S iRet = -1;
            Number* pNumber = new Number();
            if (NULL != pNumber)
            {
                pNumber->setAsUInt64(uiValue);
                iRet = add(pName, pNumber);
                if (0 != iRet)
                {
                    delete pNumber;
                }
            }

            return iRet;
        }

        D32S Object::add(const DCHAR* pName, DOUBLE dValue)
        {
            D32S iRet = -1;
            Number* pNumber = new Number();
            if (NULL != pNumber)
            {
                pNumber->setAsDouble(dValue);
                iRet = add(pName, pNumber);
                if (0 != iRet)
                {
                    delete pNumber;
                }
            }

            return iRet;
        }

        D32S Object::add(const DCHAR* pName, const DCHAR* pValue)
        {
            D32S iRet = -1;
            String* pString = new String();
            if (NULL != pString)
            {
                iRet = pString->setString(pValue);
                if (0 == iRet)
                {
                    iRet = add(pName, pString);
                }
                    
                if (0 != iRet)
                {
                    delete pString;
                }
            }

            return iRet;
        }

        Array* Object::addArray(const DCHAR* pName)
        {
            D32S iRet = -1;
            Array* pArray = new Array();
            if (NULL != pArray)
            {
                iRet = add(pName, pArray);
                if (0 != iRet)
                {
                    delete pArray;
                    pArray = NULL;
                }
            }

            return pArray;
        }

        Object* Object::addObject(const DCHAR* pName)
        {
            D32S iRet = -1;
            Object* pObject = new Object();
            if (NULL != pObject)
            {
                iRet = add(pName, pObject);
                if (0 != iRet)
                {
                    delete pObject;
                    pObject = NULL;
                }
            }

            return pObject;
        }


        Number* Object::newNumber()
        {
            return new Number();
        }


        String* Object::newString()
        {
            return new String();
        }

        Array* Object::newArray()
        {
            return new Array();
        }

        Object* Object::newObject()
        {
            return new Object();
        }

//         void test()
//         {
//             Object object;
// 
//             object.add("a", "123");
//             object.add("b", (D64S)123);
// 
//         }

        ObjectSetter::~ObjectSetter()
        {
            cleanup();
        }

        Value* ObjectSetter::create(ValueType type)
        {
            Value* pValue = NULL;

            switch (type)
            {
            case VALUE_INT:
            case VALUE_UINT:
            case VALUE_REAL:
                pValue = new Number(type);
                break;
            case VALUE_STRING:
                pValue = new String();
                break;
            case VALUE_ARRAY:
                pValue = new Array();
                break;
            case VALUE_OBJECT:
                pValue = new Object();
                break;
            default:
                break;
            }

            if (NULL != pValue)
            {

                ObjectDoRecord record;
                record.dataType = type;
                record.doType = OBJECT_DO_CREATE;
                record.v1.pValue = pValue;
                record.v2.pValue = NULL;
                if (0 != addRecord(record))
                {
                    delete pValue;
                    pValue = NULL;
                }
            }

            return pValue;
        }

        D32S ObjectSetter::numberSet(Number* pNumber, D32S iValue)
        {
            D32S iRet = 0;
            ObjectDoRecord record;
            D32S iOldValue = pNumber->asInt32();

            if (iValue != iOldValue)
            {
                record.dataType = VALUE_INT;
                record.doType = OBJECT_DO_MODIFY;
                record.v1.pNumber = pNumber;
                record.v2.i32 = iOldValue;

                iRet = addRecord(record);
                if (0 == iRet)
                {
                    pNumber->setAsInt32(iValue);
                }
            }

            return iRet;
        }

        D32S ObjectSetter::numberSet(Number* pNumber, D32U uiValue)
        {
            D32S iRet = 0;
            ObjectDoRecord record;
            D32U uiOldValue = pNumber->asUInt32();

            if (uiValue != uiOldValue)
            {
                record.dataType = VALUE_UINT;
                record.doType = OBJECT_DO_MODIFY;
                record.v1.pNumber = pNumber;
                record.v2.ui32 = uiOldValue;

                iRet = addRecord(record);
                if (0 == iRet)
                {
                    pNumber->setAsUInt32(uiValue);
                }
            }

            return iRet;
        }

        D32S ObjectSetter::numberSet(Number* pNumber, D64S iValue)
        {
            D32S iRet = 0;
            ObjectDoRecord record;
            D64S iOldValue = pNumber->asInt64();

            if (iValue != iOldValue)
            {
                record.dataType = VALUE_INT;
                record.doType = OBJECT_DO_MODIFY;
                record.v1.pNumber = pNumber;
                record.v2.i64 = pNumber->asInt64();

                iRet = addRecord(record);
                if (0 == iRet)
                {
                    pNumber->setAsInt64(iValue);
                }
            }

            return iRet;
        }

        D32S ObjectSetter::numberSet(Number* pNumber, D64U uiValue)
        {
            D32S iRet = 0;
            ObjectDoRecord record;
            D64U uiOldValue = pNumber->asUInt64();

            if (uiOldValue != uiValue)
            {
                record.dataType = VALUE_UINT;
                record.doType = OBJECT_DO_MODIFY;
                record.v1.pNumber = pNumber;
                record.v2.ui64 = uiOldValue;

                iRet = addRecord(record);
                if (0 == iRet)
                {
                    pNumber->setAsUInt64(uiValue);
                }
            }

            return iRet;
        }

        D32S ObjectSetter::numberSet(Number* pNumber, DOUBLE dValue)
        {
            D32S iRet = 0;
            ObjectDoRecord record;
            DOUBLE dOldValue = pNumber->asDouble();

            if (dOldValue != dValue)
            {
                record.dataType = VALUE_REAL;
                record.doType = OBJECT_DO_MODIFY;
                record.v1.pNumber = pNumber;
                record.v2.d = dOldValue;

                iRet = addRecord(record);
                if (0 == iRet)
                {
                    pNumber->setAsDouble(dValue);
                }
            }

            return iRet;
        }

        D32S ObjectSetter::stringSet(String* pString, const DCHAR* pValue)
        {
            D32S iRet = 0;
            ObjectDoRecord record;
            DCHAR* pPtr = NULL;
            const DCHAR* pOldString = pString->asString();

            if (NULL == pOldString || 0 != adm_strcmp(pOldString, pValue))
            {
                record.dataType = VALUE_STRING;
                record.doType = OBJECT_DO_MODIFY;
                record.v1.pString = pString;
                if (NULL != pString->asString())
                {
                    record.v2.pString = adm_strdup(pOldString);
                }
                else
                {
                    record.v2.pString = NULL;
                }

                iRet = pString->setString(pValue);
                if (0 == iRet)
                {
                    iRet = addRecord(record);
                    if (0 != iRet)
                    {
                        pString->setString(record.v2.pString);
                        pPtr = (DCHAR*)record.v2.pString;
                        adm_free(pPtr);
                    }
                }
            }

            return iRet;
        }

        Value* ObjectSetter::objectAdd(Object* pObject, const DCHAR* pName, ValueType type)
        {
            Value* pValue = create(type);
            if (NULL != pValue)
            {
                if (0 != objectAdd(pObject, pName, pValue))
                {
                    pValue = NULL;
                }
            }

            return pValue;
        }

        D32S ObjectSetter::objectAdd(Object* pObject, const DCHAR* pName, Value* pValue)
        {
            D32S iRet = -1;
            ObjectDoRecord record;


            iRet = pObject->add(pName, pValue);
            if (0 == iRet)
            {
                record.dataType = VALUE_OBJECT;
                record.doType = OBJECT_DO_ADD;
                record.v1.pObject = pObject;
                record.v2.pValue = pValue;

                iRet = addRecord(record);
                if (0 != iRet)
                {
                    pObject->remove(pValue);
                }
            }

            return iRet;
        }


        D32S ObjectSetter::objectRemove(Object* pObject, const DCHAR* pName)
        {
            D32S iRet = -1;
            ObjectDoRecord record;
            DCHAR* pMemberName = adm_strdup(pName);

            Value* pValue = pObject->removeEx(pName);
            if (NULL != pValue)
            {
                record.dataType = VALUE_OBJECT;
                record.doType = OBJECT_DO_DELETE;
                record.v1.pObject = pObject;
                record.v2.pValue = pValue;
                record.v3.pName = pMemberName;

                iRet = addRecord(record);
                if (0 != iRet)
                {
                    pObject->remove(pValue);
                }
            }

            return iRet;
        }


        D32S ObjectSetter::objectRemove(Object* pObject, Value* pValue)
        {
            D32S iRet = -1;
            const DCHAR* pName = pObject->getName(pValue);
            if (NULL != pName)
            {
                iRet = objectRemove(pObject, pName);
            }
            
            return iRet;
        }

        Value* ObjectSetter::arrayAdd(Array* pArray, D32U uiIndex, ValueType type)
        {
            Value* pValue = create(type);
            if (NULL != pValue)
            {
                if (0 != arrayAdd(pArray, uiIndex, pValue))
                {
                    pValue = NULL;
                }
            }

            return pValue;
        }

        D32S ObjectSetter::arrayAdd(Array* pArray, D32U uiIndex, Value* pValue)
        {
            D32S iRet = -1;
            ObjectDoRecord record;

            iRet = pArray->add(uiIndex, pValue);
            if (0 == iRet)
            {
                record.dataType = VALUE_ARRAY;
                record.doType = OBJECT_DO_ADD;
                record.v1.pArray = pArray;
                record.v2.uiIndex = uiIndex;
       
                iRet = addRecord(record);
                if (0 != iRet)
                {
                    pArray->remove(uiIndex);
                }
            }

            return iRet;
        }

        Value* ObjectSetter::arrayAppend(Array* pArray, ValueType type)
        {
            Value* pValue = create(type);
            if (NULL != pValue)
            {
                if (0 != arrayAppend(pArray, pValue))
                {
                    pValue = NULL;
                }
            }

            return pValue;
        }

        D32S ObjectSetter::arrayAppend(Array* pArray, Value* pValue)
        {
            D32S iRet = -1;
            ObjectDoRecord record;

            iRet = pArray->append(pValue);
            if (0 == iRet)
            {
                record.dataType = VALUE_ARRAY;
                record.doType = OBJECT_DO_ADD;
                record.v1.pArray = pArray;
                record.v2.uiIndex = pArray->count() - 1;

                iRet = addRecord(record);
                if (0 != iRet)
                {
                    pArray->remove(record.v2.uiIndex);
                }
            }

            return iRet;
        }

        D32S ObjectSetter::arrayRemove(Array* pArray, D32U uiIndex)
        {
            D32S iRet = -1;
            ObjectDoRecord record;

            Value* pValue = pArray->removeEx(uiIndex);
            if (NULL != pValue)
            {
                record.dataType = VALUE_ARRAY;
                record.doType = OBJECT_DO_DELETE;
                record.v1.pArray = pArray;
                record.v2.pValue = pValue;
                record.v3.uiIndex = uiIndex;

                iRet = addRecord(record);
            }

            return iRet;
        }


        D32S ObjectSetter::arrayRemove(Array* pArray, Value* pValue)
        {
            D32U uiIndex = 0;
            ObjectDoRecord record;

            D32S iRet = pArray->remove(pValue, uiIndex);
            if (0 == iRet)
            {
                record.dataType = VALUE_ARRAY;
                record.doType = OBJECT_DO_DELETE;
                record.v1.pArray = pArray;
                record.v2.pValue = pValue;
                record.v3.uiIndex = uiIndex;

                iRet = addRecord(record);
            }

            return iRet;
        }

        void ObjectSetter::undo()
        {
            while (_doRecords.size())
            {
                ObjectDoRecord record = _doRecords.back();
                switch (record.doType)
                {
                case OBJECT_DO_CREATE:
                    (void)undoCreate(record);
                    break;
                case OBJECT_DO_ADD:
                    (void)undoAdd(record);
                    break;
                case OBJECT_DO_MODIFY:
                    (void)undoModify(record);
                    break;
                case OBJECT_DO_DELETE:
                    (void)undoDelete(record);
                    break;
                default:
                    break;
                }

                _doRecords.pop_back();
            }
        }

        D32S ObjectSetter::undoCreate(ObjectDoRecord& record)
        {
            delete record.v1.pValue;
            return 0;
        }

        D32S ObjectSetter::undoAdd(ObjectDoRecord& record)
        {
            D32S iRet = 0;
            switch (record.dataType)
            {
            case VALUE_ARRAY:
                (void)record.v1.pArray->removeEx(record.v2.uiIndex);
                break;
            case VALUE_OBJECT:
                iRet = record.v1.pObject->remove(record.v2.pValue);
                break;
            default:
                iRet = -1;
                break;
            }

            return iRet;
        }

        D32S ObjectSetter::undoModify(ObjectDoRecord& record)
        {
            D32S iRet = 0;
            switch (record.dataType)
            {
            case VALUE_INT:
                record.v1.pNumber->setAsInt64(record.v2.i64);
                break;
            case VALUE_UINT:
                record.v1.pNumber->setAsUInt64(record.v2.ui64);
                break;
            case VALUE_REAL:
                record.v1.pNumber->setAsDouble(record.v2.d);
                break;
            case VALUE_STRING:
                iRet = record.v1.pString->setString(record.v2.pString);
                adm_free(record.v2.pString);
                break;
            default:
                iRet = -1;
                break;
            }

            return iRet;
        }

        D32S ObjectSetter::undoDelete(ObjectDoRecord& record)
        {
            D32S iRet = 0;
            switch (record.dataType)
            {
            case VALUE_ARRAY:
                record.v1.pArray->insert(record.v3.uiIndex, record.v2.pValue);
                break;
            case VALUE_OBJECT:
                iRet = record.v1.pObject->add(record.v3.pName, record.v2.pValue);
                adm_free(record.v3.pName);
                break;
            default:
                iRet = -1;
                break;
            }

            return iRet;
        }


        void ObjectSetter::cleanupModify(ObjectDoRecord& record)
        {
            if (VALUE_STRING == record.dataType)
            {
                adm_free(record.v2.pString);
            }
        }

        void ObjectSetter::cleanupDelete(ObjectDoRecord& record)
        {
            switch (record.dataType)
            {
            case VALUE_ARRAY:
                delete record.v2.pValue;
                break;
            case VALUE_OBJECT:
                delete record.v2.pValue;
                adm_free(record.v3.pName);
                break;
            default:
                break;
            }
        }

        void ObjectSetter::cleanup()
        {
            while (_doRecords.size())
            {
                ObjectDoRecord record = _doRecords.back();
                switch (record.doType)
                {
                case OBJECT_DO_MODIFY:
                    (void)cleanupModify(record);
                    break;
                case OBJECT_DO_DELETE:
                    (void)cleanupDelete(record);
                    break;
                default:
                    break;
                }

                _doRecords.pop_back();
            }
        }

        const DCHAR* ObjectJsonSerializer::serialize(Value* pValue)
        {
            const DCHAR* pJsonStr = NULL;
            D32S iRet = value2JsonStr(pValue);
            if (0 == iRet)
            {
                pJsonStr = _jsonStr.c_str();
            }

            return pJsonStr;
        }

        D32S ObjectJsonSerializer::int2JsonStr(Number* pNumber)
        {
            _jsonStr.append(std::to_string(pNumber->asInt64()));
            return 0;
        }

        D32S ObjectJsonSerializer::uint2JsonStr(Number* pNumber)
        {
            _jsonStr.append(std::to_string(pNumber->asUInt64()));
            return 0;
        }

        D32S ObjectJsonSerializer::real2JsonStr(Number* pNumber)
        {
            _jsonStr.append(std::to_string(pNumber->asDouble()));
            return 0;
        }

        D32S ObjectJsonSerializer::string2JsonStr(String* pString)
        {
            std::string     str;
            StringUtil util(pString->asString());
            (void)util.addEscape(str);
            _jsonStr.append("\"");
            _jsonStr.append(str);
            _jsonStr.append("\"");
            return 0;
        }

        D32S ObjectJsonSerializer::array2JsonStr(Array* pArray)
        {
            D32S iRet = -1;
            D32U uiCount = pArray->count();

            _jsonStr.append("[");

            do 
            {
                if (0 == uiCount)
                {
                    iRet = 0;
                    break;
                }

                iRet = value2JsonStr(pArray->get(0));
                if (0 != iRet)
                {
                    break;
                }

                for (D32U i = 1; i < uiCount; i++)
                {
                    _jsonStr.append(",");
                    iRet = value2JsonStr(pArray->get(i));
                    if (0 != iRet)
                    {
                        return iRet;
                    }
                }
            } while (FALSE);

            _jsonStr.append("]");

            return iRet;
        }


        D32S ObjectJsonSerializer::objectMember2JsonStr(const DCHAR* pName, Value* pMember)
        {
            _jsonStr.append("\"");
            _jsonStr.append(pName);
            _jsonStr.append("\":");
            return value2JsonStr(pMember);
        }

        D32S ObjectJsonSerializer::object2JsonStr(Object* pObject)
        {
            D32S iRet = -1;

            Object::Iterator iter(*pObject);

            do 
            {
                _jsonStr.append("{");
                if (FALSE == iter.next())
                {
                    break;
                }

                iRet = objectMember2JsonStr(iter.pName, iter.pValue);
                if (0 != iRet)
                {
                    break;
                }

                while (TRUE == iter.next())
                {
                    _jsonStr.append(",");
                    iRet = objectMember2JsonStr(iter.pName, iter.pValue);
                    if (0 != iRet)
                    {
                        return iRet;
                    }
                }

                _jsonStr.append("}");
            } while (FALSE);
            
            return iRet;
        }

        D32S ObjectJsonSerializer::value2JsonStr(Value* pValue)
        {
            D32S iRet = -1;

            if (NULL != pValue)
            {
                switch (pValue->getType())
                {
                case VALUE_INT:
                    iRet = int2JsonStr((Number*)pValue);
                    break;
                case VALUE_UINT:
                    iRet = uint2JsonStr((Number*)pValue);
                    break;
                case VALUE_REAL:
                    iRet = real2JsonStr((Number*)pValue);
                    break;
                case VALUE_STRING:
                    iRet = string2JsonStr((String*)pValue);
                    break;
                case VALUE_ARRAY:
                    iRet = array2JsonStr((Array*)pValue);
                    break;
                case VALUE_OBJECT:
                    iRet = object2JsonStr((Object*)pValue);
                    break;
                default:
                    break;
                }
            }
            else
            {
                _jsonStr.append("null");
                iRet = 0;
            }

            return iRet;
        }


        ObjectJsonParser::ObjectJsonParser(): _util(NULL)
        {

        }

        Value* ObjectJsonParser::parse(const DCHAR* pJsonStr)
        {
            Value* pValue = NULL;

            if (NULL != pJsonStr)
            {
                StringUtil util(pJsonStr);
                _util = util;

                (void)parseValue(pValue);
            }

            return pValue;
        }

        D32S ObjectJsonParser::parseNumber(Value*& pValue)
        {
            D32S iRet = -1;
            Number* pNumber = NULL;
            DBOOL bIsDecimal = FALSE;
            DBOOL bIsNegative = FALSE;
            std::string str;

            do 
            {
                if (0 == _util.popNumberString(str, bIsDecimal, bIsNegative))
                {
                    break;
                }

                pNumber = new Number();
                if (NULL == pNumber)
                {
                    break;
                }

                if (FALSE == bIsDecimal)
                {
                    if (FALSE == bIsNegative)
                    {
                        pNumber->setAsUInt64(std::stoull(str));
                    }
                    else
                    {
                        pNumber->setAsInt64(std::stoll(str));
                    }
                }
                else
                {
                    pNumber->setAsDouble(std::stod(str));
                }

                iRet = 0;
            } while (FALSE);
            
            pValue = pNumber;

            return iRet;
        }

        D32S ObjectJsonParser::parseString(Value*& pValue)
        {
            D32S    iRet = -1;
            String* pString = NULL;
            std::string str;

            do 
            {
                if (0 == _util.popEscapeString(str))
                {
                    break;
                }

                pString = new String();
                if (NULL == pString)
                {
                    break;
                }

                iRet = pString->setString(str.c_str());
                if (0 != iRet)
                {
                    delete pString;
                    pString = NULL;
                }
            } while (FALSE);

            pValue = pString;

            return iRet;
        }


        D32S ObjectJsonParser::parseArray(Array* pArray)
        {
            D32S    iRet = -1;
            Value*  pMember = NULL;
            DCHAR   chr = 0;

            _util.skipWhiteSpace();
            if (']' != _util.getChr())
            {
                do
                {
                    iRet = parseValue(pMember);
                    if (0 != iRet)
                    {
                        break;
                    }

                    iRet = pArray->append(pMember);
                    if (0 != iRet)
                    {
                        delete pMember;
                        break;
                    }

                    _util.skipWhiteSpace();
                    chr = _util.popChr();
                    if (']' == chr)
                    {
                        break;
                    }

                    iRet = -1;
                } while (',' == chr);
            }
            else
            {
                _util.skipChr();
                iRet = 0;
            }
            
            return iRet;
        }

        D32S ObjectJsonParser::parseArray(Value*& pValue)
        {
            D32S    iRet = -1;
            Array*  pArray = NULL;
            
            do 
            {
                pArray = new Array();
                if (NULL == pArray)
                {
                    break;
                }

                iRet = parseArray(pArray);
                if (0 != iRet)
                {
                    delete pArray;
                    pArray = NULL;
                }
            } while (FALSE);

            pValue = pArray;

            return iRet;
        }

        D32S ObjectJsonParser::parseObjectMember(Object* pObject)
        {
            D32S    iRet = -1;
            Value*  pObjectMember = NULL;
            DCHAR   acName[64];

            do 
            {
                _util.skipWhiteSpace();
                if ('"' != _util.popChr())
                {
                    break;
                }

                if (0 == _util.popAlphanumericStr(acName))
                {
                    break;
                }

                if ('"' != _util.popChr())
                {
                    break;
                }

                _util.skipWhiteSpace();
                if (':' != _util.popChr())
                {
                    iRet = -1;
                    break;
                }

                iRet = parseValue(pObjectMember);
                if (0 != iRet)
                {
                    break;
                }

                iRet = pObject->add(acName, pObjectMember);
                if (0 != iRet)
                {
                    delete pObjectMember;
                }
            } while (FALSE);

            return iRet;
        }

        D32S ObjectJsonParser::parseObject(Object* pObject)
        {
            D32S    iRet = -1;
            DCHAR   chr = 0;

            _util.skipWhiteSpace();
            if ('}' != _util.getChr())
            {
                do
                {
                    iRet = parseObjectMember(pObject);
                    if (0 != iRet)
                    {
                        break;
                    }

                    _util.skipWhiteSpace();
                    chr = _util.popChr();
                    if ('}' == chr)
                    {
                        break;
                    }

                    iRet = -1;
                } while (',' == chr);
            }
            else
            {
                _util.skipChr();
                iRet = 0;
            }

            return iRet;
        }

        D32S ObjectJsonParser::parseObject(Value*& pValue)
        {
            D32S    iRet = -1;
            Object* pObject = NULL;
            
            do 
            {
                pObject = new Object();
                if (NULL == pObject)
                {
                    break;
                }

                iRet = parseObject(pObject);
                if (0 != iRet)
                {
                    delete pObject;
                    pObject = NULL;
                }
            } while (FALSE);

            pValue = pObject;

            return iRet;
        }

        D32S ObjectJsonParser::parseValue(Value*& pValue)
        {
            D32S iRet = -1;

            _util.skipWhiteSpace();

            switch (_util.getChr())
            {
            case '{':
                _util.skipChr();
                iRet = parseObject(pValue);
                break;
            case '[':
                _util.skipChr();
                iRet = parseArray(pValue);
                break;
            case '"':
                iRet = parseString(pValue);
                break;
            default:
                if (FALSE == _util.compareString("null"))
                {
                    iRet = parseNumber(pValue);
                }
                else
                {
                    iRet = 0;
                }
                break;
            }

            return iRet;
        }
    }
}

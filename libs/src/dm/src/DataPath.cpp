#include "DataPath.h"
#include "adm_memory.h"
#include "adm_string.h"

namespace vus 
{
    namespace dm
    {


// 
// PathIter::PathIter()
// {
//     _pPath = NULL;
//     _pStart = NULL;
//     _pEnd = NULL;
// }
// 
// PathIter::~PathIter()
// {
//     adm_free(_pPath);
// }
// 
// const DCHAR* PathIter::first(const DCHAR* pPath)
// {
//     if (NULL == pPath || '\0' == pPath[0])
//     {
//         return NULL;
//     }
// 
//     _pPath = adm_strdup(pPath);
//     if (NULL == _pPath)
//     {
//         return NULL;
//     }
// 
//     _pEnd = _pStart = _pPath;
//     if ('/' == _pPath[0])
//     {
//         return "/";
//     }
// 
//     return get();
// }
// 
// const DCHAR* PathIter::first(const DCHAR* pPath, const ParamList& paramList)
// {
// 
// }
// 
// const DCHAR* PathIter::next()
// {
//     if (NULL == _pEnd)
//     {
//         return NULL;
//     }
// 
//     *_pEnd = '/';
//     _pEnd++;
//     return get();
// }
// 
// const DCHAR* PathIter::get()
// {
//     _pStart = _pEnd;
//     while (TRUE)
//     {
//         switch (*_pEnd)
//         {
//         case '\0':
//             if (_pStart == _pEnd)
//             {
//                 _pStart = NULL;
//             }
//             _pEnd = NULL;
//             break;
//         case '/':
//             *_pEnd = '\0';
//             break;
//         default:
//             _pEnd++;
//             break;
//         }
//     }
// 
//     return _pStart;
// }



        DataPathNode::DataPathNode()
        {
            _pName = NULL;
        }

        DataPathNode::SearchKeyIter::SearchKeyIter(DataPathNode* pNode)
        {
            _pNode = pNode;
            _pKey = NULL;
            _pValue = NULL;
        }

        DBOOL DataPathNode::SearchKeyIter::first()
        {
            DBOOL bRet = FALSE;
            _iter = _pNode->_searchKeys.begin();
            if (_iter != _pNode->_searchKeys.cend())
            {
                _pKey = _iter->first;
                _pValue = _iter->second;
                bRet = TRUE;
            }

            return bRet;
        }

        DBOOL DataPathNode::SearchKeyIter::next()
        {
            DBOOL bRet = TRUE;
            _iter++;
            if (_iter != _pNode->_searchKeys.cend())
            {
                _pKey = _iter->first;
                _pValue = _iter->second;
            }
            else
            {
                _pKey = NULL;
                _pValue = NULL;
                bRet = FALSE;
            }

            return bRet;
        }


        DataPath::DataPath(const DCHAR* pPath) : _util(pPath)
        {
            _pPath = pPath;
            _pBuf = NULL;
            _uiOffset = 0;
        }

        DataPath::DataPath(DataPath& rhs): _util(rhs._util.getRemainStr() - adm_strlen(rhs._node._pName) - ('\0' != rhs._util.getChr() ?  1 : 0))
        {
            // _pPath = rhs._util.getRemainStr();
            _pPath = _util.getRemainStr();
            _pBuf = NULL;
            _uiOffset = 0;
        }

        DataPath::~DataPath()
        {
            if (NULL != _pBuf)
            {
                adm_free(_pBuf);
            }
        }

        DataPathNode* DataPath::first()
        {
            DataPathNode* pNode = NULL;

            D32U uiSize = (D32U)adm_strlen(_pPath) + 1U;
            _pBuf = (DCHAR*)adm_malloc(uiSize);
            if (NULL == _pBuf)
            {
                return pNode;
            }

            if ('/' == _util.getChr())
            {
                _pBuf[_uiOffset++] = '/';
                _util.skipChr();
                if (0 == parse())
                {
                    pNode = &_node;
                }
            }
            else
            {
                pNode = next();
            }

            return pNode;
        }

        DataPathNode* DataPath::next()
        {
            DataPathNode* pNode = NULL;

            switch (_util.getChr())
            {
            case '/':
            case '\0':
                break;
            default:
                _uiOffset = 0;
                _node._searchKeys.clear();
                if (0 == parse())
                {
                    pNode = &_node;
                }
                break;
            }

            return pNode;
        }


//         D32S DataPath::getRootPath(const DCHAR* pPath, DataPath* pDataPath)
//         {
//             D32U uiOffset = adm_strlen(pPath);
// 
//             if ('/' == pPath[uiOffset])
//             {
//                 uiOffset++;
//             }
// 
//             while ('/' != pPath[uiOffset])
//             {
//                 uiOffset--;
//             }
// 
//             pDataPath
//         }

        D32S DataPath::parseSearchKey()
        {
            D32S iRet = -1;
            DCHAR* pKey = NULL;
            DCHAR* pValue = NULL;
            D32U uiOffset = 0;


            do
            {
                pKey = &_pBuf[_uiOffset];
                uiOffset = _util.popAlphanumericStr(pKey);
                if (0 == uiOffset)
                {
                    break;
                }
                _uiOffset += uiOffset;

                if ('=' != _util.popChr())
                {
                    break;
                }

                pValue = &_pBuf[_uiOffset];
                if (':' == _util.getChr())
                {
                    _pBuf[_uiOffset++] = ':';
                    _util.skipChr();
                }

                uiOffset = _util.popAlphanumericStr(&_pBuf[_uiOffset]);
                if (0 == uiOffset)
                {
                    break;
                }
                _uiOffset += uiOffset;

                _node._searchKeys.insert(DataPathNode::MapPair(pKey, pValue));
                iRet = 0;
            } while (FALSE);

            return iRet;
        }

        D32S DataPath::parseSearchKeys()
        {
            D32S iRet = 0;

            iRet = parseSearchKey();
            if (0 == iRet)
            {
                while ('&' == _util.getChr())
                {
                    _util.skipChr();
                    iRet = parseSearchKey();
                    if (0 != iRet)
                    {
                        break;
                    }
                }
            }

            return iRet;
        }

        D32S DataPath::parse()
        {
            D32U uiOffset = 0;
            D32S iRet = -1;

            do 
            {
                // �����ڵ���
                // pName = &_pBuf[_uiOffset];
                if ('$' == _util.getChr())
                {
                    _pBuf[_uiOffset++] = '$';
                    _util.skipChr();
                }

                uiOffset = _util.popAlphanumericStr(&_pBuf[_uiOffset]);
                if (0 == uiOffset)
                {
                    break;
                }
                _node._pName = _pBuf;
                _uiOffset += uiOffset;

                if ('?' == _util.getChr())
                {
                    _util.skipChr();
                    iRet = parseSearchKeys();
                    if (0 != iRet)
                    {
                        break;
                    }
                }

                switch (_util.getChr())
                {
                case '/':
                    _util.skipChr();
                    break;
                case '\0':
                    break;
                default:
                    iRet = -1;
                    break;
                }

                iRet = 0;
            } while (FALSE);
    
            return iRet;
        }
}
}




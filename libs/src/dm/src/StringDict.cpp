#include "StringDict.h"
#include "adm_memory.h"
#include "adm_string.h"

namespace vus
{
    namespace dm
    {
        StringDict::~StringDict()
        {
            DCHAR* pPtr = NULL;
            for (MapIter iter = _map.begin(); iter != _map.cend(); iter++)
            {
                pPtr = (DCHAR*)iter->first;
                adm_free(pPtr);
            }
        }

        D32S StringDict::add(const DCHAR* pKey, const DCHAR* pValue)
        {
            if (NULL == pKey || NULL == pValue)
            {
                return -1;
            }

            D32U uiKeyLength = (D32U)adm_strlen(pKey) + 1U;
            D32U uiValueLength = (D32U)adm_strlen(pValue) + 1U;

            DCHAR* pKeyTmp = (DCHAR*)adm_malloc(uiKeyLength + uiValueLength);
            if (NULL == pKeyTmp)
            {
                return -1;
            }

            adm_memcpy(pKeyTmp, pKey, uiKeyLength, uiKeyLength);

            DCHAR* pValueTmp = &pKeyTmp[uiKeyLength];
            adm_memcpy(pValueTmp, pValue, uiValueLength, uiValueLength);

            _map.insert(MapPair(pKeyTmp, pValueTmp));

            return 0;
        }

        const DCHAR* StringDict::find(const DCHAR* pKey)
        {
            MapIter iter = _map.find(pKey);
            return iter != _map.cend() ? iter->second : NULL;
        }

        D32S StringDict::remove(const DCHAR* pKey)
        {
            MapIter iter = _map.find(pKey);
            if (iter != _map.cend())
            {
                _map.erase(iter);
                free((void*)iter->first);
            }
            return 0;
        }


        StringDict::Iterator::Iterator(StringDict* pDict)
        {
            _pMap = &pDict->_map;
            pKey = NULL;
            pValue = NULL;
        }


        DBOOL StringDict::Iterator::first()
        {
            DBOOL bRet = FALSE;
            _iter = _pMap->begin();
            if (_iter != _pMap->cend())
            {
                pKey = _iter->first;
                pValue = _iter->second;
                bRet = TRUE;
            }

            return bRet;
        }


        DBOOL StringDict::Iterator::next()
        {
            DBOOL bRet = FALSE;
            _iter++;
            if (_iter != _pMap->cend())
            {
                pKey = _iter->first;
                pValue = _iter->second;
                bRet = TRUE;
            }

            return bRet;
        }
    }
}

#include "JsonDataRule.h"
#include "DataSearcher.h"
#include "json-c/json.h"


namespace vus
{
    namespace dm
    {
        JsonDataRule::~JsonDataRule()
        {

        }


//         D32S vus::dm::JsonDataRule::addVar(const DCHAR* pName, const DCHAR* pValue)
//         {
//             return _vars.add(pName, pValue);
//         }
// 
//         const DCHAR* JsonDataRule::getVarValue(const DCHAR* pName)
//         {
//             return _vars.find(pName);
//         }


//         D32S vus::dm::JsonDataRule::addItem(const DCHAR* pDataPath, const DCHAR* pJsonPath)
//         {
//             return _vars.add(pDataPath, pJsonPath);
//         }

        D32S JsonDataRule::setItems(const DCHAR* pDescriptionStr)
        {
            JsonDataRuleParser parser(pDescriptionStr);

            return parser.parse(&_items);
        }



        D32S JsonDataRuleParser::parse(StringDict* pDict)
        {
            D32S iRet = -1;
            _util.skipWhiteSpace();

            do 
            {
                if ('[' != _util.popChr())
                {
                    break;
                }

                iRet = parseAll(pDict);
                if (0 != iRet)
                {
                    break;
                }

                _util.skipWhiteSpace();
                if (']' != _util.popChr())
                {
                    iRet = -1;
                    break;
                }
            } while (FALSE);

            return 0;
        }

        D32S JsonDataRuleParser::parseOne(StringDict* pDict)
        {
            D32S iRet = -1;
            DCHAR acDataPath[256];
            DCHAR acJsonPath[256];

            _util.skipWhiteSpace();

            do 
            {
                if (0 == _util.popUriString(acDataPath))
                {
                    break;
                }

                _util.skipSpace();
                if (0 == _util.popUriString(acJsonPath))
                {
                    break;
                }

                iRet = pDict->add(acDataPath, acJsonPath);
            } while (FALSE);

            return iRet;
        }

        D32S JsonDataRuleParser::parseAll(StringDict* pDict)
        {
            D32S iRet = -1;
            do 
            {
                iRet = parseOne(pDict);

                _util.skipSpace();
                if (',' != _util.getChr())
                {
                    break;
                }
                
                _util.skipChr();
            } while (TRUE);

            return iRet;
        }



        JsonDataMapObjectGetterHelper::JsonDataMapObjectGetterHelper(ObjectDataSetter* pDataSetter)
        {
            _pDataSetter = pDataSetter;
        }

        D32S JsonDataMapObjectGetterHelper::handle(DataPathNode* pNode, void* pInData, void*& pOutData)
        {
            D32S iRet = -1;
            const DataTypeDef* pType = NULL;
            Value* pValue = NULL;

            pValue = _pDataSetter->get(pNode, _pType, _pValue, pType);
            if (NULL != pValue)
            {
                if (DM_DATA_ARRAY == pType->uiType)
                {
                    over();
                }

                _pValue = pValue;
                _pType = pType;
                iRet = 0;
            }

            return iRet;
        }

        JsonDataMapObjectGetter::JsonDataMapObjectGetter(ObjectDataSetter* pDataSetter) : _worker(&_helper), _helper(pDataSetter)
        {

        }

        Value* JsonDataMapObjectGetter::get(DataPath* pDataPath, Value* pRoot, const DataTypeDef* pRootType, const DataTypeDef*& pType)
        {
            Value* pValue = NULL;

            _helper._pValue = pRoot;
            _helper._pType = pRootType;
            if (0 == _worker.foreach(pDataPath, pRoot))
            {
                pValue = _helper._pValue;
                pType = _helper._pType;

            }

            return pValue;
        }



        D32S JsonDataMapObjectConvertHelper::handle(DataPathNode* pNode, void* pInData, void*& pOutData)
        {
            D32S iRet = -1;
            struct json_object* pRoot = (struct json_object*)_pJsonObject;
            struct json_object* pMember = NULL;

            do 
            {
                if (json_type_object != json_object_get_type(pRoot))
                {
                    break;
                }

                pMember = json_object_object_get(_pJsonObject, pNode->getName());
                if (NULL != pMember)
                {
                    switch (json_object_get_type(pMember))
                    {
                    case json_type_null:
                        over();
                        break;
                    case json_type_array:
                        over();
                        break;
                    default:
                        break;
                    }
                }
                else
                {
                    over();
                }

                _pJsonObject = pMember;
                iRet = 0;
            } while (FALSE);

            return iRet;
        }



        JsonDataMapObjectConverter::JsonDataMapObjectConverter(ObjectDataSetter* pDataSetter) : _worker(&_helper)
        {
            _pJsonDataPath = NULL;
            _pDataPath = NULL;
            _pJsonObject = NULL;
            _pVarSet = NULL;
            _pDataSetter = pDataSetter;
        }


        D32S JsonDataMapObjectConverter::convert(const DCHAR* pJsonDataPath, const DCHAR* pDataPath, struct json_object* pRoot)
        {
            DataPath dataPath(pDataPath);
            DataPath jsonDataPath(pJsonDataPath);
            dataPath.first();
            jsonDataPath.first();
            return convert(&jsonDataPath, &dataPath, pRoot);
        }

        D32S JsonDataMapObjectConverter::convert(DataPath* pJsonDataPath, DataPath* pDataPath, struct json_object* pRoot)
        {
            _pJsonDataPath = pJsonDataPath;
            _pDataPath = pDataPath;
            return convert(pRoot, NULL, NULL);
        }


        D32S JsonDataMapObjectConverter::convert(struct json_object* pRoot, Value* pRootValue, const DataTypeDef* pRootType)
        {
            D32S iRet = -1;
            struct json_object* pMember = NULL;

            if (json_type_array != json_object_get_type(pRoot))
            {
                _helper._pJsonObject = pRoot;
                iRet = _worker.foreach(_pJsonDataPath, (void*)pRoot);
                if (0 == iRet)
                {
                    _pJsonDataPath->next();
                    pMember = _helper._pJsonObject;
                }
            }
            else
            {
                pMember = pRoot;
            }

            if (NULL != pMember)
            {
                const DataTypeDef* pType = NULL;
                JsonDataMapObjectGetter objectGetter(_pDataSetter);
                objectGetter.setVarSet(_pVarSet);

                Value* pValue = objectGetter.get(_pDataPath, pRootValue, pRootType, pType);
                if (NULL != pValue)
                {
                    _pDataPath->next();
                    iRet = covertJsonMember(pMember, pValue, pType);
                }
            }

            return iRet;
        }

        D32S JsonDataMapObjectConverter::covertJsonBoolean(struct json_object* pRoot, Value* pValue)
        {
            D32S iRet = -1;
            if (VALUE_INT == pValue->getType() || VALUE_UINT == pValue->getType())
            {
                iRet = _pDataSetter->set(pValue, (D64S)json_object_get_boolean(pRoot));
            }

            return iRet;
        }

        D32S JsonDataMapObjectConverter::covertJsonInt(struct json_object* pRoot, Value* pValue)
        {
            D32S iRet = -1;
            if (VALUE_INT == pValue->getType())
            {
                iRet = _pDataSetter->set(pValue, (D64S)json_object_get_int64(pRoot));
            }
            else if (VALUE_UINT == pValue->getType())
            {
                iRet = _pDataSetter->set(pValue, (D64U)json_object_get_uint64(pRoot));
            }

            return iRet;
        }

        D32S JsonDataMapObjectConverter::covertJsonDouble(struct json_object* pRoot, Value* pValue)
        {
            D32S iRet = -1;
            if (VALUE_REAL == pValue->getType())
            {
                iRet = _pDataSetter->set(pValue, (DOUBLE)json_object_get_double(pRoot));
            }

            return iRet;
        }

        D32S JsonDataMapObjectConverter::covertJsonString(struct json_object* pRoot, Value* pValue)
        {
            D32S iRet = -1;
            if (VALUE_STRING == pValue->getType())
            {
                iRet = _pDataSetter->set(pValue, json_object_get_string(pRoot));
            }

            return iRet;
        }

        D32S JsonDataMapObjectConverter::covertJsonArray(struct json_object* pRoot, Value* pArray, const DataTypeDef* pArrayType)
        {
            D32S iRet = -1;
            struct json_object* pMember = NULL;
            const DataTypeDef* pType = NULL;
            Value* pValue = NULL;

            do
            {
                if (VALUE_ARRAY != pArray->getType())
                {
                    break;
                }
                pType = pArrayType->pSubType;

                JsonDataMapObjectConverter converter(_pDataSetter);
                converter._worker.setVarSet(_pVarSet);

                D32U uiCount = (D32U)json_object_array_length(pRoot);
                for (D32U i = 0; i < uiCount; i++)
                {
                    pMember = json_object_array_get_idx(pRoot, i);
                    if (NULL == pMember)
                    {
                        break;
                    }

                    pValue = _pDataSetter->get((Array*)pArray, i, dataType2ObjectType(pType->uiType));

                    DataPath dataPath(*_pDataPath);
                    DataPath jsonDataPath(*_pJsonDataPath);
                    dataPath.first();
                    jsonDataPath.first();

                    converter._pJsonDataPath = &jsonDataPath;
                    converter._pDataPath = &dataPath;
                    iRet = converter.convert(pMember, pValue, pType);
                    if (0 != iRet)
                    {
                        break;
                    }
                }
            } while (FALSE);

            return iRet;
        }

        D32S JsonDataMapObjectConverter::covertJsonMember(struct json_object* pRoot, Value* pValue, const DataTypeDef* pRootType)
        {
            D32S iRet = -1;
//             if (NULL == pRoot || NULL == pValue)
//             {
//                 return iRet;
//             }

            switch (json_object_get_type(pRoot))
            {
            case json_type_boolean:
                iRet = covertJsonBoolean(pRoot, pValue);
                break;
            case json_type_double:
                iRet = covertJsonDouble(pRoot, pValue);
                break;
            case json_type_int:
                iRet = covertJsonInt(pRoot, pValue);
                break;
            case json_type_string:
                iRet = covertJsonString(pRoot, pValue);
                break;
            case json_type_array:
                iRet = covertJsonArray(pRoot, pValue, pRootType);
                break;
            default:
                break;
            }
 
            return iRet;
        }


        

        JsonDataConverter::JsonDataConverter(DataTree* pDataTree)
        {
            _pDataTree = pDataTree;
        }

        D32S JsonDataConverter::convert(struct json_object* pRoot, JsonDataRule* pRule)
        {
            D32S iRet = -1;
            ObjectDataSetter dataSetter(_pDataTree);

            StringDict::Iterator iter(pRule->getRuleItems());
            iter.first();
            do 
            {
                JsonDataMapObjectConverter converter(&dataSetter);
                converter.setVarSet(pRule->getVarSet());

                iRet = converter.convert(iter.pValue, iter.pKey, pRoot);
                if (0 != iRet)
                {
                    dataSetter.undo();
                    break;
                }
            } while (TRUE == iter.next());
            
            return iRet;
        }

        D32S JsonDataConverter::convert(const DCHAR* pJsonStr, JsonDataRule* pRule)
        {
            D32S iRet = -1;
            struct json_object* pRoot = json_tokener_parse(pJsonStr);
            if (NULL != pRoot)
            {
                iRet = convert(pRoot, pRule);
            }

            return iRet;
        }

    }
}

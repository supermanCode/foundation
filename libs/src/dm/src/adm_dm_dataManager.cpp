﻿#include "adm_dm_dataManager.h"
#include "datatype/DataType.h"
//#include "StaticDataRule.h"
#include "JsonDataRule.h"
#include "DataTree.h"


using namespace vus::dm;


#ifdef __cplusplus
extern "C"
{
#endif  // __cplusplus

//    struct ADM_DM_STATIC_DATA_RULE_ITEM_T
//    {
//        StaticDataRuleItem  _ruleItem;
//    };
//
//    D32S adm_dm_sdrItemInit(ADM_DM_STATIC_DATA_RULE_ITEM_T* pRuleItem, const ADM_DM_STATIC_DATA_DESC_T* pDescription)
//    {
//        return pRuleItem->_ruleItem.init(*(StaticDataDescription*)pDescription);
//    }
//
//    D32S adm_dm_sdrItemAddSub(ADM_DM_STATIC_DATA_RULE_ITEM_T* pRuleItem, const ADM_DM_STATIC_DATA_DESC_T* pDescription)
//    {
//        return pRuleItem->_ruleItem.addSub(*(StaticDataDescription*)pDescription);
//    }
//
//    D32S adm_dm_sdrItemAddSubEx(ADM_DM_STATIC_DATA_RULE_ITEM_T* pRuleItem, const ADM_DM_STATIC_DATA_DESC_T* pDescription, ADM_DM_STATIC_DATA_RULE_ITEM_T** ppSubRuleItem)
//    {
//        return pRuleItem->_ruleItem.addSub(*(StaticDataDescription*)pDescription, *(StaticDataRuleItem**)ppSubRuleItem);
//    }
//
//
//    struct ADM_DM_STATIC_DATA_RULE_T
//    {
//        StaticDataRule  _rule;
//    };
//
//    ADM_DM_STATIC_DATA_RULE_T* adm_dm_sdrCreate()
//    {
//        return new ADM_DM_STATIC_DATA_RULE_T();
//    }
//
//    void adm_dm_sdrDestroy(ADM_DM_STATIC_DATA_RULE_T* pRule)
//    {
//        if (NULL == pRule)
//        {
//            delete pRule;
//        }
//    }
//
////     D32S adm_dm_sdrAddVar(ADM_DM_STATIC_DATA_RULE_T* pRule, const DCHAR* pName, const DCHAR* pValue)
////     {
////         return pRule->_rule.addVar(pName, pValue);
////     }
//// 
////     const DCHAR* adm_dm_sdrGetVar(ADM_DM_STATIC_DATA_RULE_T* pRule, const DCHAR* pName)
////     {
////         return pRule->_rule.getVar(pName);
////     }
//
//    D32S adm_dm_sdrSetRule(ADM_DM_STATIC_DATA_RULE_T* pRule, const ADM_DM_STATIC_DATA_DESC_T* pDescription, ADM_DM_STATIC_DATA_RULE_ITEM_T** ppRuleItem)
//    {
//        return pRule->_rule.setRule(*(StaticDataDescription*)pDescription, *(StaticDataRuleItem**)ppRuleItem);
//    }
//
//    D32S adm_dm_sdrSetRuleByDesc(ADM_DM_STATIC_DATA_RULE_T* pRule, const DCHAR* pDescriptionStr)
//    {
//        return pRule->_rule.setRule(pDescriptionStr);
//    }
//
//    const ADM_DM_STATIC_DATA_RULE_ITEM_T* adm_dm_sdrGetRule(ADM_DM_STATIC_DATA_RULE_T* pRule)
//    {
//        return (const ADM_DM_STATIC_DATA_RULE_ITEM_T*)pRule->_rule.getRule();
//    }


 
     struct ADM_DM_JSON_DATA_RULE_T
     {
         JsonDataRule  _rule;
     };
 
     ADM_DM_JSON_DATA_RULE_T* adm_dm_jdrCreate()
     {
         return new ADM_DM_JSON_DATA_RULE_T();
     }
 
     void adm_dm_jdrDestroy(ADM_DM_JSON_DATA_RULE_T* pRule)
     {
         if (NULL == pRule)
         {
             delete pRule;
         }
     }
 
     D32S adm_dm_jdrAddVar(ADM_DM_JSON_DATA_RULE_T* pRule, const DCHAR* pName, const DCHAR* pValue)
     {
         return pRule->_rule.getVarSet()->add(pName, pValue);
     }

//      D32S adm_dm_jdrAddItem(ADM_DM_JSON_DATA_RULE_T* pRule, const DCHAR* pDataPath, const DCHAR* pJsonPath)
//      {
//          return pRule->_rule.addItem(pDataPath, pJsonPath);
//      }
 
     D32S adm_dm_jdrAddItemsByDesc(ADM_DM_JSON_DATA_RULE_T* pRule, const DCHAR* pDescriptionStr)
     {
         return pRule->_rule.setItems(pDescriptionStr);
     }

    struct ADM_DM_PARAM_LIST_T
    {
        ParamList   _list;
    };

    ADM_DM_PARAM_LIST_T* adm_dm_paramListCreate()
    {
        return new ADM_DM_PARAM_LIST_T();
    }

    void adm_dm_paramListDestory(ADM_DM_PARAM_LIST_T* pParamList)
    {
        if (NULL != pParamList)
        {
            delete pParamList;
        }
    }

    D32S adm_dm_paramListAdd(ADM_DM_PARAM_LIST_T* pList, const DCHAR* pName, const DCHAR* pValue)
    {
        return pList->_list.setValue(pName, pValue);
    }


//     struct ADM_DM_DATA_RULE_ITEM_T
//     {
//         DataRuleItem  _ruleItem;
//     };
// 
//     D32S adm_dm_drItemAddSub(ADM_DM_DATA_RULE_ITEM_T* pRuleItem, const ADM_DM_DATA_TYPE_DESC_T* pDescription)
//     {
//         return pRuleItem->_ruleItem.addSub(*(DataTypeDescription*)pDescription);
//     }
// 
//     D32S adm_dm_drItemAddSubEx(ADM_DM_DATA_RULE_ITEM_T* pRuleItem, const ADM_DM_DATA_TYPE_DESC_T* pDescription, ADM_DM_DATA_RULE_ITEM_T** ppSubRuleItem)
//     {
//         return pRuleItem->_ruleItem.addSub(*(DataTypeDescription*)pDescription, *(DataRuleItem**)ppSubRuleItem);
//     }


    struct ADM_DM_DATA_MANAGER_T
    {
        DataTree  _dataTree;
    };

    ADM_DM_DATA_MANAGER_T* adm_dm_create()
    {
        ADM_DM_DATA_MANAGER_T* pManager = new ADM_DM_DATA_MANAGER_T();
        if (NULL != pManager)
        {
            pManager->_dataTree.init();
        }

        return pManager;
    }

    void adm_dm_destroy(ADM_DM_DATA_MANAGER_T* pManager)
    {
        if (NULL != pManager)
        {
            delete pManager;
        }
    }

    D32S adm_dm_init(ADM_DM_DATA_MANAGER_T* pManager, ADM_KV_PERSIST_STORAGE_T* pStorage)
    {
        return pManager->_dataTree.init((IPersistStorage*)pStorage);
    }

    D32S adm_dm_load(ADM_DM_DATA_MANAGER_T* pManager)
    {
        return pManager->_dataTree.load();
    }


//     D32S adm_dm_addDataRule(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pRootName, ADM_DM_DATA_RULE_ITEM_T** ppRuleItem)
//     {
//         return pManager->_dataTree.addDataRule(pRootName, *(DataRuleItem**)ppRuleItem);
//     }
// 
//     D32S adm_dm_addDataRuleByDef(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pRootName, const ADM_DM_DATA_TYPE_DEF_T* pTypeDefine)
//     {
//         return pManager->_dataTree.addDataRule(pRootName, *(DataTypeDef*)pTypeDefine);
//     }

    D32S adm_dm_addDataRuleByDesc(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pDescriptionStr)
    {
        return pManager->_dataTree.getDescriptionTree()->add(pDescriptionStr);
    }

//     Value* adm_dm_queryData(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pDataPath)
//     {
//         return pManager->_manager.queryData(pDataPath);
//     }
// 
//     Value* adm_dm_queryDataWithParam(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pDataPath, const ADM_DM_PARAM_LIST_T* pParamList)
//     {
//         return pManager->_manager.queryData(pDataPath, pParamList->_list);
//     }

    D32S adm_dm_setData(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pDataPath, void* pInData, D32U uiInDataSize)
    {
        return pManager->_dataTree.setData(pDataPath, pInData, uiInDataSize);
    }

    D32S adm_dm_setDataWithParam(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pDataPath, const ADM_DM_PARAM_LIST_T* pParamList, void* pInData, D32U uiInDataSize)
    {
        return pManager->_dataTree.setData(pDataPath, *(ParamList*)&pParamList->_list, pInData, uiInDataSize);
    }

    D32S adm_dm_queryData(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pDataPath, void* pOutBuf, D32U uiOutBufSize)
    {
        return pManager->_dataTree.queryData(pDataPath, pOutBuf, uiOutBufSize);
    }

    D32S adm_dm_queryDataWithParam(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pDataPath, const ADM_DM_PARAM_LIST_T* pParamList, void* pOutBuf, D32U uiOutBufSize)
    {
        return pManager->_dataTree.queryData(pDataPath, *(ParamList*)&pParamList->_list, pOutBuf, uiOutBufSize);
    }

    D32S adm_dm_destroyData(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pDataPath, void* pData, D32U uiDataSize)
    {
        return pManager->_dataTree.destroyData(pDataPath, pData, uiDataSize);
    }

    D32S adm_dm_remove(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pDataPath)
    {
        return pManager->_dataTree.removeData(pDataPath);
    }

    D32S adm_dm_removeWithParam(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pDataPath, const ADM_DM_PARAM_LIST_T* pParamList)
    {
        return pManager->_dataTree.removeData(pDataPath, *(ParamList*)&pParamList->_list);
    }

    ADM_KV_PERSIST_STORAGE_T* adm_dm_kvStorageGet(ADM_DM_DATA_MANAGER_T* pManager)
    {
        return (ADM_KV_PERSIST_STORAGE_T*)pManager->_dataTree.getKvStorage();
    }

    D32S adm_dm_setDataBySdr(ADM_DM_DATA_MANAGER_T* pManager, const ADM_DM_STATIC_DATA_RULE_T* pRule, void* pInData, D32U uiInDataSize)
    {
        // return pManager->_dataTree.setData(*(StaticDataRule*)&pRule->_rule, pInData, uiInDataSize);
        return -1;
    }

    D32S adm_dm_setDataWithParamBySdr(ADM_DM_DATA_MANAGER_T* pManager, const ADM_DM_STATIC_DATA_RULE_T* pRule, const ADM_DM_PARAM_LIST_T* pParamList, void* pInData, D32U uiInDataSize)
    {
        // return pManager->_dataTree.setData(*(StaticDataRule*)&pRule->_rule, *(ParamList*)&pParamList->_list, pInData, uiInDataSize);
        return -1;
    }

    D32S adm_dm_queryDataBySdr(ADM_DM_DATA_MANAGER_T* pManager, const ADM_DM_STATIC_DATA_RULE_T* pRule, void* pOutBuf, D32U uiOutBufSize)
    {
        // return pManager->_dataTree.queryData(*(StaticDataRule*)&pRule->_rule, pOutBuf, uiOutBufSize);
        return -1;
    }

    D32S adm_dm_queryDataWithParamBySdr(ADM_DM_DATA_MANAGER_T* pManager, const ADM_DM_STATIC_DATA_RULE_T* pRule, const ADM_DM_PARAM_LIST_T* pParamList, void* pOutBuf, D32U uiOutBufSize)
    {
        // return pManager->_dataTree.queryData(*(StaticDataRule*)&pRule->_rule, *(ParamList*)&pParamList->_list, pOutBuf, uiOutBufSize);
        return -1;
    }

    D32S adm_dm_destroyDataBySdr(ADM_DM_DATA_MANAGER_T* pManager, const ADM_DM_STATIC_DATA_RULE_T* pRule, void* pData, D32U uiDataSize)
    {
        // return pManager->_dataTree.destroyData(*(StaticDataRule*)&pRule->_rule, pData, uiDataSize);
        return -1;
    }

    D32S adm_dm_setDataByJdr(ADM_DM_DATA_MANAGER_T* pManager, const ADM_DM_JSON_DATA_RULE_T* pRule, const DCHAR* pJsonstr)
    {
        return pManager->_dataTree.setData(pJsonstr , (JsonDataRule*)&pRule->_rule);
    }

#ifdef __cplusplus
}
#endif  // __cplusplus
#include "DataSearcher.h"



namespace vus
{
    namespace dm
    {
        DataSearcher::DataSearcher(void* pData, VarSet* pVarSet, DataSearchHelper* pHelper)
        {
            _pData = pData;
            _pVarSet = pVarSet;
            _pHepler = pHelper;
        }

        void* DataSearcher::search(const DCHAR* pPath)
        {
            void* pRoot = NULL;
            void* pObject = NULL;
            DataPath dataPath(pPath);

            DataPathNode* pNode = dataPath.first();
            if (pNode->isRoot())
            {
                _pHepler->getRoot(pNode, _pData, pRoot);
                pNode = dataPath.next();
            }
            else
            {
                pRoot = _pData;
            }

            while (NULL != pNode)
            {
                pObject = searchByPathNode(pNode, pRoot);
                pRoot = pObject;
                pNode = dataPath.next();
            }

            return pObject;
        }


        void* DataSearcher::search(const DCHAR* pPath, void* pRoot)
        {
            // void* pRoot = NULL;
            void* pObject = NULL;
            DataPath dataPath(pPath);

            DataPathNode* pNode = dataPath.first();
            if (pNode->isRoot())
            {
                pNode = dataPath.next();
                _pHepler->getRoot(pNode, _pData, pRoot);
                pNode = dataPath.next();
            }

            while (NULL != pNode)
            {
                pObject = searchByPathNode(pNode, pRoot);
                pRoot = pObject;
                pNode = dataPath.next();
            }

            return pObject;
        }

        void* DataSearcher::searchByVar(const DCHAR* pVarName, void* pRoot)
        {
            void* pObject = NULL;
            const DCHAR* pVarValue = _pVarSet->find(pVarName);

            DataMapIter iter = _dataMap.find(pVarValue);
            if (iter != _dataMap.cend())
            {
                pObject = iter->second;
            }
            else
            {
                pObject = searchByDataPath(pVarValue, pRoot);
                if (NULL != pObject)
                {
                    _dataMap.insert(DataPair(pVarValue, pObject));
                }
            }

            return pObject;
        }

        void* DataSearcher::searchByPathNode(DataPathNode* pPathNode, void* pRoot)
        {
            void* pObject = NULL;

            if (FALSE == pPathNode->isVar())
            {
                _pHepler->getNode(pPathNode, pRoot, pObject);
            }
            else
            {
                pObject = searchByVar(pPathNode->getName(), pRoot);
            }

            return pObject;
        }

        void* DataSearcher::searchByDataPath(const DCHAR* pPath, void* pRoot)
        {
            void* pObject = NULL;
            DataPath dataPath(pPath);

            DataPathNode* pNode = dataPath.first();
            while (NULL != pNode)
            {
                pObject = searchByPathNode(pNode, pRoot);
                pRoot = pObject;
                pNode = dataPath.next();
            }

            return pObject;
        }


        D32S DataPathWorker::foreach(const DCHAR* pDataPath, void* pInData)
        {
            void* pOutData = NULL;
            return foreach(pDataPath, pInData, pOutData);
        }

        D32S DataPathWorker::foreach(DataPath* pDataPath, void* pInData)
        {
            void* pOutData = NULL;
            return foreach(pDataPath, pInData, pOutData);
        }

        D32S DataPathWorker::foreach(const DCHAR* pDataPath, void* pInData, void*& pOutData)
        {
            DataPath dataPath(pDataPath);
            (void*)dataPath.first();
            return foreach(&dataPath, pInData, pOutData);
        }

        D32S DataPathWorker::foreach(DataPath* pDataPath, void* pInData, void*& pOutData)
        {
            D32S iRet = -1;
            void* pInDataTmp = pInData;
            void* pData = NULL;

            DataPathNode* pNode = pDataPath->current();
            // DataPathNode* pNextNode = pDataPath->next();
            while (NULL != pNode)
            {
                iRet = handleByNode(pNode, pInDataTmp, pData);
                if (0 != iRet || TRUE == _pHelper->isOver())
                {
                    break;
                }

                pInDataTmp = pData;
  
                pNode = pDataPath->next();
            }

            pOutData = pData;

            return iRet;
        }


        DataPathWorker::DataPathWorker(DataPathWokerHelper* pHelper)
        {
            _pHelper = pHelper;
        }

        D32S DataPathWorker::handleByNode(DataPathNode* pPathNode, void* pInData, void*& pOutData)
        {
            D32S iRet = -1;

            if (FALSE == pPathNode->isVar())
            {
                iRet = _pHelper->handle(pPathNode, pInData, pOutData);
            }
            else
            {
                iRet = handleByVar(pPathNode, pInData, pOutData);
            }

            return iRet;
        }

        D32S DataPathWorker::handleByVar(DataPathNode* pPathNode, void* pInData, void*& pOutData)
        {
            D32S iRet = -1;
            const DCHAR* pVarValue = _pVarSet->find(pPathNode->getVarName());

            DataMapIter iter = _dataMap.find(pVarValue);
            if (iter != _dataMap.cend())
            {
                pOutData = iter->second;
                iRet = 0;
            }
            else
            {
                iRet = foreach(pVarValue, pInData, pOutData);
                if (0 == iRet && NULL != pOutData)
                {
                    _dataMap.insert(DataPair(pVarValue, pOutData));
                }
            }

            return iRet;
        }

    }
}

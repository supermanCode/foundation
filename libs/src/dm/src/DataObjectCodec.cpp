#include "DataObjectCodec.h"



namespace vus
{
    namespace dm
    {
        extern ValueType dataType2ObjectType(D32S iType);

        Value* DataObjectDecodeHelper::newValueNode(D32S iType)
        {
            Value* pValue = NULL;

            switch (iType)
            {
            case DM_DATA_INT:
            case DM_DATA_UINT:
            case DM_DATA_REAL:
                pValue = Object::newNumber();
                break;
            case DM_DATA_STRING:
                pValue = Object::newString();
                break;
            case DM_DATA_ARRAY:
                pValue = Object::newArray();
                break;
            case DM_DATA_OBJECT:
                pValue = Object::newObject();
                break;
            }

            return pValue;
        }

        D32S DataObjectDecodeHelper::init(const DataTypeDef* pTypeDef, void* pData, void*& pOutData)
        {
            Value* pValue = NULL;
            if (NULL == *(void**)pData)
            {
                pValue = _pSetter->create(dataType2ObjectType(pTypeDef->uiType));
                *(void**)pData = pValue;
            }
            else
            {
                pValue = *(Value**)pData;
            }

            pOutData = pValue;

            return NULL != pValue ? 0 : -1;
        }

        D32S DataObjectDecodeHelper::setAsInt(const DataTypeDef* pTypeDef, void* pData, D64S iValue)
        {
            return _pSetter->numberSet((Number*)pData, iValue);
        }

        D32S DataObjectDecodeHelper::setAsUInt(const DataTypeDef* pTypeDef, void* pData, D64U uiValue)
        {
            return _pSetter->numberSet((Number*)pData, uiValue);
        }

        D32S DataObjectDecodeHelper::setAsReal(const DataTypeDef* pTypeDef, void* pData, DOUBLE dValue)
        {
            return _pSetter->numberSet((Number*)pData, dValue);
        }

        D32S DataObjectDecodeHelper::setAsString(const DataTypeDef* pTypeDef, void* pData, const DCHAR* pValue)
        {
            return _pSetter->stringSet((String*)pData, pValue);
        }

        D32S DataObjectDecodeHelper::setNull(const DataTypeDef* pTypeDef, void* pData)
        {
            // return _pSetter->objectAdd((Object*)pData, pTypeDef->pName, (Value*)NULL);
            return 0;
        }

        D32S DataObjectDecodeHelper::setArrayCount(const DataTypeDef* pTypeDef, void* pData, D32U uiCount)
        {
            D32S iRet = -1;
            Array* pArray = (Array*)pData;
            D32U uiDestCount = pArray->count();

            if (uiDestCount > uiCount)
            {
                for (D32U i = uiDestCount - 1; i >= uiCount; i--)
                {
                    _pSetter->arrayRemove(pArray, i);
                }
            }

            return iRet;
        }

        D32S DataObjectDecodeHelper::getArrayMember(const DataTypeDef* pTypeDef, D32U uiIndex, void* pData, void*& pOutData)
        {
            D32S iRet = -1;
            Array* pArray = (Array*)pData;
            Value* pValue = NULL;

            do
            {
                pValue = pArray->get(uiIndex);
                if (NULL == pValue)
                {
                    pValue = _pSetter->arrayAdd(pArray, uiIndex, dataType2ObjectType(pTypeDef->uiType));
                    if (NULL == pValue)
                    {
                        break;
                    }
                }

                pOutData = pValue;
                iRet = 0;
            } while (FALSE);

            return iRet;
        }

        D32S DataObjectDecodeHelper::getObjectMember(const DataTypeDef* pTypeDef, void* pData, void*& pOutData)
        {
            D32S iRet = -1;
            Object* pRoot = (Object*)pData;
            Value* pValue = NULL;

            do
            {
                pValue = pRoot->get(pTypeDef->pName);
                if (NULL == pValue)
                {
                    pValue = _pSetter->objectAdd(pRoot, pTypeDef->pName, dataType2ObjectType(pTypeDef->uiType));
                    if (NULL == pValue)
                    {
                        break;
                    }
                }

                pOutData = pValue;
                iRet = 0;
            } while (FALSE);

            return iRet;
        }



        D32S DataObjectEncodeHelper::init(const DataTypeDef* pTypeDef, void* pData, void*& pOutData)
        {
            D32S iRet = -1;
            if (TRUE == checkValueNode((Value*)pData, pTypeDef))
            {
                pOutData = pData;
                iRet = 0;
            }

            return iRet;
        }
        
        D32S DataObjectEncodeHelper::asInt(const DataTypeDef* pTypeDef, void* pData, D64S& iValue)
        {
            Number* pNumber = (Number*)pData;
            iValue = pNumber->asInt64();
            return 0;
        }

        D32S DataObjectEncodeHelper::asUInt(const DataTypeDef* pTypeDef, void* pData, D64U& uiValue)
        {
            Number* pNumber = (Number*)pData;
            uiValue = pNumber->asUInt64();
            return 0;
        }

        D32S DataObjectEncodeHelper::asReal(const DataTypeDef* pTypeDef, void* pData, DOUBLE& dValue)
        {
            Number* pNumber = (Number*)pData;
            dValue = pNumber->asDouble();
            return 0;
        }

        D32S DataObjectEncodeHelper::asString(const DataTypeDef* pTypeDef, void* pData, const DCHAR*& pValue)
        {
            String* pString = (String*)pData;
            pValue = pString->asString();
            return 0;
        }

        D32S DataObjectEncodeHelper::getArrayCount(const DataTypeDef* pTypeDef, void* pData, D32U& uiCount)
        {
            uiCount = ((Array*)pData)->count();
            return 0;
        }

        D32S DataObjectEncodeHelper::getArray(const DataTypeDef* pTypeDef, void* pData, void*& pOutData)
        {
            pOutData = pData;
            return 0;
        }

        // D32S DataObjectEncodeHelper::asArray(const DataTypeDef* pTypeDef, void* pData, void*& pOutData, D32U& uiCount)
        // {
        //     Object* pRoot = (Object*)pData;
        //     Array* pArray = pRoot->asArray(pTypeDef->pName);
        //     pOutData = pArray;
        //     return NULL != pArray ? 0 : -1;
        // }

        D32S DataObjectEncodeHelper::getArrayMember(const DataTypeDef* pTypeDef, D32U uiIndex, void* pData, void*& pOutData)
        {
            D32S iRet = -1;
            Array* pArray = (Array*)pData;
            Value* pValue = pArray->get(uiIndex);

            if (NULL != pValue && TRUE == checkValueNode(pValue, pTypeDef))
            {
                pOutData = pValue;
                iRet = 0;
            }

            return iRet;
        }

        D32S DataObjectEncodeHelper::getObjectMember(const DataTypeDef* pTypeDef, void* pData, void*& pOutData)
        {
            D32S iRet = -1;
            Object* pObject = (Object*)pData;
            Value* pValue = pObject->get(pTypeDef->pName);

            do 
            {
                if (0 != pTypeDef->uiOptional && NULL == pValue)
                {
                    pOutData = NULL;
                    iRet = 0;
                    break;
                }

                if (NULL != pValue && TRUE == checkValueNode(pValue, pTypeDef))
                {
                    pOutData = pValue;
                    iRet = 0;
                }
            } while (FALSE);

            return iRet;
        }

        DBOOL DataObjectEncodeHelper::checkValueNode(const Value* pValue, const DataTypeDef* pTypeDef)
        {
            DBOOL bRet = FALSE;
            switch (pValue->getType())
            {
            case VALUE_INT:
            case VALUE_UINT:
                bRet = DM_DATA_UINT == pTypeDef->uiType || DM_DATA_INT == pTypeDef->uiType ? TRUE : FALSE;
                break;
            case VALUE_REAL:
                bRet = DM_DATA_REAL == pTypeDef->uiType ? TRUE : FALSE;
                break;
            case VALUE_STRING:
                bRet = DM_DATA_STRING == pTypeDef->uiType ? TRUE : FALSE;
                break;
            case VALUE_ARRAY:
                bRet = DM_DATA_ARRAY == pTypeDef->uiType ? TRUE : FALSE;
                break;
            case VALUE_OBJECT:
                bRet = DM_DATA_OBJECT == pTypeDef->uiType ? TRUE : FALSE;
                break;
            }

            return bRet;
        }
    }
}
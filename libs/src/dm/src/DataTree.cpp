#include "DataTree.h"
#include "DataObjectSearcher.h"
#include "DataObjectCodec.h"
#include "JsonDataRule.h"
#include "datatype/DataTypeDescription.h"
#include "adm_memory.h"
#include "adm_string.h"

namespace vus
{
    namespace dm
    {
        ValueType dataType2ObjectType(D32S iType)
        {
            ValueType type = VALUE_INVALID;

            switch (iType)
            {
            case DM_DATA_INT:
                type = VALUE_INT;
                break;
            case DM_DATA_UINT:
                type = VALUE_UINT;
                break;
            case DM_DATA_REAL:
                type = VALUE_REAL;
                break;
            case DM_DATA_STRING:
                type = VALUE_STRING;
                break;
            case DM_DATA_ARRAY:
                type = VALUE_ARRAY;
                break;
            case DM_DATA_OBJECT:
                type = VALUE_OBJECT;
                break;
            default:
                break;
            }

            return type;
        }


        D32S DataDescriptionTreeParser::parse(DataTypeDef* pType, const DCHAR* pDescriptionStr)
        {
            D32S iRet = -1;
            DCHAR acStr[64];
            DCHAR* pName = NULL;
            StringUtil  _util(pDescriptionStr);

            do
            {
                _util.skipWhiteSpace();
                if (0 == _util.popAlphanumericStr(acStr))
                {
                    break;
                }

                pName = adm_strdup(acStr);
                if (NULL == pName)
                {
                    break;
                }

                DataTypeDescriptionParser parser(_util.getRemainStr());
                parser.init();
                iRet = parser.parse(pType);
                if (0 != iRet)
                {
                    adm_free(pName);
                    break;
                }

                pType->pName = pName;
            } while (FALSE);

            return iRet;
        }

        void DataDescriptionTreeParser::destroyType(DataTypeDef* pType)
        {
            DataTypeDescriptionParser::destroyType(pType);
        }

        DataDescriptionTree::~DataDescriptionTree()
        {
            for (TypeMapIter iter = _typeMap.begin(); iter != _typeMap.cend(); iter++)
            {
                DataDescriptionTreeParser::destroyType(iter->second);
            }
        }

        D32S DataDescriptionTree::add(const DCHAR* pDescriptionStr)
        {
            D32S iRet = -1;

            do 
            {
                DataTypeDef* pType = new DataTypeDef();
                if (NULL == pType)
                {
                    break;
                }

                iRet = DataDescriptionTreeParser::parse(pType, pDescriptionStr);
                if (0 != iRet && NULL != pType->pName)
                {
                    delete pType;
                    break;
                }

                _typeMap.insert(TypePair("/" + std::string(pType->pName), pType));
            } while (FALSE);

            return iRet;
        }

        const DataTypeDef* DataDescriptionTree::checkout(DataPathNode* pDataPathNode, const DataTypeDef* pType)
        {
            if (TRUE == pDataPathNode->hasSearchKey() && DM_DATA_ARRAY == pType->uiType)
            {
                pType = pType->pSubType;
            }

            return pType;
        }

        const DataTypeDef* DataDescriptionTree::find(DataPathNode* pDataPathNode)
        {
            const DataTypeDef* pType = NULL;
            if (TRUE == pDataPathNode->isRoot())
            {
                TypeMapIter iter = _typeMap.find(pDataPathNode->getName());
                if (iter != _typeMap.cend())
                {
                    pType = checkout(pDataPathNode, iter->second);
                }
            }

            return pType;
        }

        const DataTypeDef* DataDescriptionTree::find(DataPathNode* pDataPathNode, const DataTypeDef* pType)
        {
            const DataTypeDef* pSubType = NULL;


            if (DM_DATA_ARRAY == pType->uiType)
            {
                pType = pType->pSubType;
            }

            if (DM_DATA_OBJECT == pType->uiType)
            {
                for (D32U i = 0; i < pType->uiSize; i++)
                {
                    pSubType = &pType->pSubType[i];
                    if (0 == adm_strcmp(pDataPathNode->getName(), pSubType->pName))
                    {
                        pSubType = checkout(pDataPathNode, pSubType);
                        break;
                    }

                    pSubType = NULL;
                }
            }

            return pSubType;
        }

        const DataTypeDef* DataDescriptionTree::find(DataPath* pDataPath, const DataTypeDef* pType)
        {
            DataPathNode* pNode = pDataPath->next();
            const DataTypeDef* pSubType = pType;

            while (NULL != pNode)
            {
                pSubType = find(pNode, pSubType);
                if (NULL == pSubType)
                {
                    break;
                }

                pNode = pDataPath->next();
            }

            return pSubType;
        }

        const vus::DataTypeDef* DataDescriptionTree::find(const DCHAR* pDataPath, const DataTypeDef* pType)
        {
            DataPath    dataPath(pDataPath);
            DataPathNode* pNode = NULL;
            const DataTypeDef* pSubType = NULL;

            do 
            {
                pNode = dataPath.first();
                if (NULL == pNode)
                {
                    break;
                }

                pSubType = find(pNode, pType);
                if (NULL == pSubType)
                {
                    break;
                }

                pSubType = find(&dataPath, pSubType);
            } while (FALSE);

            return pSubType;
        }

        const DataTypeDef* DataDescriptionTree::find(const DCHAR* pDataPath)
        {
            const DataTypeDef*  pType = NULL;
            DataPathNode*       pNode = NULL;
            DataPath            dataPath(pDataPath);

            do 
            {
                pNode = dataPath.first();
                if (NULL == pNode || FALSE == pNode->isRoot())
                {
                    break;
                }

                TypeMapIter iter = _typeMap.find(pNode->getName());
                if (iter != _typeMap.cend())
                {
                    pType = find(&dataPath, checkout(pNode, iter->second));
                }
            } while (FALSE);

            return pType;
        }

        class DataDescriptionTreeIterator
        {
        public:
            DataDescriptionTreeIterator(DataDescriptionTree* pDescriptionTree)
            {
                _pTypeMap = &pDescriptionTree->_typeMap;
                _iter = _pTypeMap->begin();
            }

            const DCHAR* getName()
            {
                return _iter->first.c_str();
            }

            void next()
            {
                _iter++;
            }

            DBOOL isEnd()
            {
                return _iter != _pTypeMap->cend() ? FALSE : TRUE;
            }

        private:
            DataDescriptionTree::TypeMap* _pTypeMap;
            DataDescriptionTree::TypeMapIter _iter;
        };



        DataTree::DataTree()
        {
            _pStorage = NULL;
        }

        DataTree::~DataTree()
        {
            if (NULL != _pStorage)
            {
                delete _pStorage;
            }
        }

        D32S DataTree::init(IPersistStorage* pKvStorage/* = NULL*/)
        {
            D32S iRet = -1;

            if (NULL != pKvStorage)
            {
                _pStorage = new DataTreeStorage(this, pKvStorage);
                if (NULL != _pStorage)
                {
                    iRet = 0;
                }
            }
            else
            {
                iRet = 0;
            }

            return iRet;
        }

        D32S DataTree::setData(const DCHAR* pDataPath, const void* pData, D32U uiDataSize)
        {
            D32S iRet = -1;
            ObjectDataSetter dataSetter(this);
            iRet = dataSetter.set(pDataPath, pData, uiDataSize);
            if (0 == iRet && NULL != _pStorage)
            {
                iRet = _pStorage->save(pDataPath);
                if (0 != iRet)
                {
                    dataSetter.undo();
                }
            }

            return iRet;
        }

        D32S DataTree::setData(const DCHAR* pDataPath, ParamList& paramList, const void* pData, D32U uiDataSize)
        {
            D32S iRet = -1;
            ObjectDataSetter dataSetter(this);
            iRet = dataSetter.set(pDataPath, paramList, pData, uiDataSize);
            if (0 == iRet && NULL != _pStorage)
            {
                iRet = _pStorage->save(pDataPath);
                if (0 != iRet)
                {
                    dataSetter.undo();
                }
            }

            return iRet;
        }


        D32S DataTree::load()
        {
            return NULL != _pStorage ? _pStorage->load() : 0;
        }

        D32S DataTree::clear()
        {
            _root.clear();
            if (NULL != _pStorage)
            {
                _pStorage->cleanup();
            }

            return 0;
        }

        D32S DataTree::queryData(const DCHAR* pDataPath, ParamList* pParamList, void* pOut, D32U uiOutMaxSize)
        {
            D32S iRet = -1;

            do
            {
                ObjectDataSearcher searcher(this);
                Value* pValue = searcher.find(pDataPath, pParamList);
                if (NULL == pValue)
                {
                    break;
                }

                const DataTypeDef* pDataType = _descriptionTree.find(pDataPath);
                if (NULL == pDataType)
                {
                    break;
                }

                DataObjectEncoder encoder;
                iRet = encoder.encode(pDataType, pValue, pOut, uiOutMaxSize);
            } while (FALSE);

            return iRet;
        }

        D32S DataTree::queryData(const DCHAR* pDataPath, void* pOut, D32U uiOutMaxSize)
        {
            return queryData(pDataPath, NULL, pOut, uiOutMaxSize);
        }

        D32S DataTree::queryData(const DCHAR* pDataPath, ParamList& paramList, void* pOut, D32U uiOutMaxSize)
        {
            return queryData(pDataPath, &paramList, pOut, uiOutMaxSize);
        }

        Value* DataTree::queryData(const DCHAR* pDataPath)
        {
            return queryData(pDataPath, (ParamList*)NULL);
        }

        Value* DataTree::queryData(const DCHAR* pDataPath, ParamList& paramList)
        {
            return queryData(pDataPath, &paramList);
        }

        Value* DataTree::queryData(const DCHAR* pDataPath, ParamList* pParamList)
        {
            ObjectDataSearcher searcher(this);
            return searcher.find(pDataPath, pParamList);
        }

        D32S DataTree::destroyData(const DCHAR* pDataPath, void* pData, D32U uiDataSize)
        {
            const DataTypeDef* pDataType = _descriptionTree.find(pDataPath);
            DataStructDestructor destructor(pDataType);
            return destructor.destroy(pData, uiDataSize);
        }

        D32S DataTree::removeData(const DCHAR* pDataPath)
        {
            D32S iRet = -1;
            ObjectDataSetter dataSetter(this);
            iRet = dataSetter.remove(pDataPath, NULL);
            if (0 == iRet && NULL != _pStorage)
            {
                iRet = _pStorage->save(pDataPath);
                if (0 != iRet)
                {
                    dataSetter.undo();
                }
            }

            return iRet;
        }


        D32S DataTree::removeData(const DCHAR* pDataPath, ParamList& paramList)
        {
            D32S iRet = -1;
            ObjectDataSetter dataSetter(this);
            iRet = dataSetter.remove(pDataPath, &paramList);
            if (0 == iRet && NULL != _pStorage)
            {
                iRet = _pStorage->save(pDataPath);
                if (0 != iRet)
                {
                    dataSetter.undo();
                }
            }

            return iRet;
        }

        D32S DataTree::queryData(const DCHAR* pDataPath, DataStruct* pDataStruct)
        {
            return queryData(pDataPath, NULL, pDataStruct);
        }

        D32S DataTree::queryData(const DCHAR* pDataPath, ParamList* pParamList, DataStruct* pDataStruct)
        {
            D32S iRet = -1;

            do
            {
                ObjectDataSearcher searcher(this);
                Value* pValue = searcher.find(pDataPath, pParamList);
                if (NULL == pValue)
                {
                    break;
                }

                const DataTypeDef* pDataType = _descriptionTree.find(pDataPath);
                if (NULL == pDataType)
                {
                    break;
                }

                DataObjectEncoder encoder;
                iRet = encoder.encode(pDataType, pValue, pDataStruct->getDataBuf(), pDataStruct->getDataBufSize());
                if (0 == iRet)
                {
                    DataStructInitializer initializer(pDataStruct);
                    initializer.setType(pDataType);
                }
            } while (FALSE);

            return iRet;
        }

        D32S DataTree::destroyData(DataStruct* pDataStruct)
        {
            return pDataStruct->destroy();
        }

        D32S DataTree::setData(const DCHAR* pJsonStr, JsonDataRule* pRule)
        {
            JsonDataConverter converter(this);
            return converter.convert(pJsonStr, pRule);
        }


//         void* DataTypeSearchHelper::getRoot(DataPathNode* pPathNode, void* pData, void*& pRoot)
//         {
//             return NULL;
//         }
// 
//         void* DataTypeSearchHelper::getNode(DataPathNode* pPathNode, void* pRoot, void*& pData)
//         {
//             return NULL;
//         }


        ObjectDataGetter::ObjectDataGetter(DataTree* pDataTree)
        {
            _pDataTree = pDataTree;
            _pVarSet = NULL;
            _pParamList = NULL;
        }

        Value* ObjectDataGetter::get(const DCHAR* pDataPath, const DataTypeDef*& pType)
        {
            return getByDataPath(NULL, NULL, pDataPath, pType);
        }

        Value* ObjectDataGetter::get(const DCHAR* pDataPath, ParamList& paramList, const DataTypeDef*& pType)
        {
            _pParamList = &paramList;
            return getByDataPath(NULL, NULL, pDataPath, pType);
        }

        vus::dm::Value* ObjectDataGetter::get(const DCHAR* pDataPath, const DataTypeDef* pRootType, Value* pRoot, const DataTypeDef*& pType)
        {
            _pParamList = NULL;
            return getByDataPath(pRootType, pRoot, pDataPath, pType);
        }

        vus::dm::Value* ObjectDataGetter::get(const DCHAR* pDataPath, const DataTypeDef* pRootType, Value* pRoot, ParamList& paramList, const DataTypeDef*& pType)
        {
            _pParamList = &paramList;
            return getByDataPath(pRootType, pRoot, pDataPath, pType);
        }

        Value* ObjectDataGetter::get(DataPathNode* pPathNode, const DataTypeDef* pRootType, Value* pRoot, const DataTypeDef*& pType)
        {
            Value*  pValue = getByPathNode(pRootType, pRoot, pPathNode, pType);
            if (NULL == pValue)
            {
                if (TRUE == pPathNode->isRoot())
                {
                    pValue = createRootData(pPathNode, pType);
                }
                else
                {
                    pValue = addDataByPathNode(pPathNode, pRootType, pRoot, pType);
                }
            }

            return pValue;
        }

        Value* ObjectDataGetter::get(const DCHAR* pDataPath, D32U uiIndex, const DataTypeDef*& pType)
        {
            Value* pValue = NULL;

            do 
            {
                pValue = getByDataPath(NULL, NULL, pDataPath, pType);
                if (NULL == pValue)
                {
                    break;
                }

                if (VALUE_ARRAY != pValue->getType())
                {
                    pValue = NULL;
                    break;
                }

                pValue = get((Array*)pValue, uiIndex, dataType2ObjectType(pType->uiType));
            } while (FALSE);

            return pValue;
        }

        Value* ObjectDataGetter::get(const DCHAR* pDataPath, D32U uiIndex, ParamList& paramList, const DataTypeDef*& pType)
        {
            _pParamList = &paramList;
            return get(pDataPath, uiIndex, pType);
        }


        Value* ObjectDataGetter::get(Array* pArray, D32U uiIndex, ValueType type)
        {
            Value* pValue = NULL;

            do 
            {
                if (NULL == pArray)
                {
                    break;
                }

                pValue  = pArray->get(uiIndex);
                if (NULL == pValue)
                {
                    pValue = _objectSetter.arrayAdd(pArray, uiIndex, type);
                }
            } while (FALSE);
     
            return pValue;
        }

        Value* ObjectDataGetter::get(Object* pObject, const DCHAR* pName, ValueType type)
        {
            Value* pValue = NULL;
            do 
            {
                if (NULL == pObject)
                {
                    break;
                }

                pValue = pObject->get(pName);
                if (NULL == pValue)
                {
                    pValue = _objectSetter.objectAdd(pObject, pName, type);
                }
            } while (FALSE);

            return pValue;
        }

        

// 
//         Array* ObjectDataGetter::findArray(DataPath* pDataPath, DataPathNode* pPathNode)
//         {
//             Value* pObject = NULL;
// 
//             if (FALSE == pPathNode->isVar())
//             {
//                 ObjectDataSearcher searcher(_pDataTree);
// 
//                 if (pPathNode->isRoot())
//                 {
//                     pObject = searcher.find(pPathNode, _pParamList);
//                     if (NULL != pObject)
//                     {
//                         pType = _pDataTree->getDescriptionTree()->find(pPathNode);
//                     }
//                 }
//                 else
//                 {
//                     pObject = searcher.find(pRoot, pPathNode, _pParamList);
//                     if (NULL != pObject)
//                     {
//                         pType = _pDataTree->getDescriptionTree()->find(pPathNode, pRootType);
//                     }
//                 }
//             }
//             else
//             {
//                 pObject = getByVar(pRootType, pRoot, pPathNode, pType);
//             }
// 
//             if (NULL == pObject)
//             {
//                 _objectSetter.redo();
//             }
// 
//             return pObject;
//         }
// 
//         Array* ObjectDataGetter::findArray(DataPath* pDataPath)
//         {
//             Array* pArray = NULL;
// 
//             DataPathNode* pNode = pDataPath->current();
//             while (NULL != pNode)
//             {
//                 pObject = getByPathNode(pRootType, pRoot, pNode, pType);
//                 if (NULL == pObject)
//                 {
//                     pObject = addDataByDataPath(&dataPath, pRootType, pRoot, pType);
//                     break;
//                 }
// 
//                 pRoot = pObject;
//                 pRootType = pType;
//                 pNode = dataPath.next();
//             }
// 
//             return pArray;
//         }

        Value* ObjectDataGetter::getByPathNode(const DataTypeDef* pRootType, Value* pRoot, DataPathNode* pPathNode, const DataTypeDef*& pType)
        {
            Value* pObject = NULL;

            if (FALSE == pPathNode->isVar())
            {
                ObjectDataSearcher searcher(_pDataTree);

                if (pPathNode->isRoot())
                {
                    pObject = searcher.find(pPathNode, _pParamList);
                    if (NULL != pObject)
                    {
                        pType = _pDataTree->getDescriptionTree()->find(pPathNode);
                    }
                }
                else
                {
                    pObject = searcher.find(pRoot, pPathNode, _pParamList);
                    if (NULL != pObject)
                    {
                        pType = _pDataTree->getDescriptionTree()->find(pPathNode, pRootType);
                    }
                }
            }
            else
            {
                pObject = getByVar(pRootType, pRoot, pPathNode, pType);
            }

            //if (NULL == pObject)
            //{
            //    _objectSetter.undo();
            //}

            return pObject;
        }


        Value* ObjectDataGetter::getByDataPath(const DataTypeDef* pRootType, Value* pRoot, DataPath* pDataPath, const DataTypeDef*& pType)
        {
            Value* pObject = NULL;

            DataPathNode* pNode = pDataPath->current();
            while (NULL != pNode)
            {
                pObject = getByPathNode(pRootType, pRoot, pNode, pType);
                if (NULL == pObject)
                {
                    break;
                }

                pRoot = pObject;
                pRootType = pType;
                pNode = pDataPath->next();
            }

            return pObject;
        }

        Value* ObjectDataGetter::getByDataPath(const DataTypeDef* pRootType, Value* pRoot, const DCHAR* pPath, const DataTypeDef*& pType)
        {
            Value* pObject = NULL;

            DataPath dataPath(pPath);

            DataPathNode* pNode = dataPath.first();
            while (NULL != pNode)
            {
                pObject = getByPathNode(pRootType, pRoot, pNode, pType);
                if (NULL == pObject)
                {
                    pObject = addDataByDataPath(&dataPath, pRootType, pRoot, pType);
                    break;
                }

                pRoot = pObject;
                pRootType = pType;
                pNode = dataPath.next();
            }

            return pObject;
        }

        Value* ObjectDataGetter::getByVar(const DataTypeDef* pRootType, Value* pRoot, DataPathNode* pPathNode, const DataTypeDef*& pType)
        {
            Value* pObject = NULL;
            const DCHAR* pVarValue = _pVarSet->find(pPathNode->getVarName());

            if (NULL != pVarValue)
            {
                DataMapIter iter = _dataMap.find(pVarValue);
                if (iter != _dataMap.cend())
                {
                    pObject = iter->second;
                }
                else
                {
                    pObject = getByDataPath(pRootType, pRoot, pVarValue, pType);
                    if (NULL != pObject)
                    {
                        _dataMap.insert(DataPair(pVarValue, pObject));
                    }
                }
            }

            return pObject;
        }

        const DataTypeDef* findType(const DataTypeDef* pType, const DCHAR* pName)
        {
            const DataTypeDef* pSubType = NULL;

            for (D32U i = 0; i < pType->uiSize; i++)
            {
                pSubType = &pType->pSubType[i];
                if (0 == adm_strcmp(pName, pSubType->pName))
                {
                    break;
                }
                pSubType = NULL;
            }

            return pSubType;
        }

        Value* ObjectDataGetter::addObjectMember(DataPathNode* pPathNode, const DataTypeDef* pRootType, Object* pRoot, const DataTypeDef*& pType)
        {
            Value* pValue = NULL;

            do 
            {
                pType = findType(pRootType, pPathNode->getName());
                if (NULL == pType)
                {
                    break;
                }

                pValue = _objectSetter.objectAdd(pRoot, pPathNode->getName(), dataType2ObjectType(pType->uiType));
                if (NULL == pValue)
                {
                    break;
                }

                pValue = fixDataByPathNode(pValue, pPathNode, pType);
            } while (FALSE);

            return pValue;
        }


        Value* ObjectDataGetter::addArrayMember(DataPathNode* pPathNode, const DataTypeDef* pRootType, Array* pRoot, const DataTypeDef*& pType)
        {
            Value* pValue = NULL;

            do
            {
                pType = pRootType->pSubType;
                if (NULL == pType)
                {
                    break;
                }

                pValue = _objectSetter.arrayAppend(pRoot, dataType2ObjectType(pType->uiType));
                if (NULL == pValue)
                {
                    break;
                }

                pValue = fixDataByPathNode(pValue, pPathNode, pType);
            } while (FALSE);

            return pValue;
        }

        Value* ObjectDataGetter::addDataByPathNode(DataPathNode* pPathNode, const DataTypeDef* pRootType, Value* pRoot, const DataTypeDef*& pType)
        {
            Value* pValue = NULL;

            if (FALSE == pPathNode->isVar())
            {
                if (DM_DATA_OBJECT == pRootType->uiType)
                {
                    pValue = addObjectMember(pPathNode, pRootType, (Object*)pRoot, pType);
                }
                else if (DM_DATA_ARRAY == pRootType->uiType)
                {
                    pValue = addArrayMember(pPathNode, pRootType, (Array*)pRoot, pType);
                }
            }
            else
            {
                const DCHAR* pVarValue = _pVarSet->find(pPathNode->getName());
                if (NULL != pVarValue)
                {
                    pValue = addDataByDataPath(pVarValue, pRootType, pRoot);
                    // _dataMap.insert(DataPair(pVarValue, pValue));
                }
            }
            
            return pValue;
        }

        Value* ObjectDataGetter::addDataByDataPath(DataPath* pDataPath, const DataTypeDef* pRootType, Value* pRoot, const DataTypeDef*& pType)
        {
            Value* pValue = NULL;
            DataPathNode* pPathNode = NULL;

            pPathNode = pDataPath->current();
            if (TRUE == pPathNode->isRoot())
            {
                if (TRUE == pPathNode->hasSearchKey())
                {
                    ObjectDataSearcher searcher(_pDataTree);
                    pValue = searcher.find(pPathNode->getName());
                    if (NULL != pValue)
                    {
                        pType = _pDataTree->getDescriptionTree()->find(pPathNode->getName());
                    }
                }
                
                if (NULL == pValue)
                {
                    pValue = createRootData(pPathNode, pType);
                    pPathNode = pDataPath->next();
                }

                pRootType = pType;
            }
            else
            {
                if (TRUE == pPathNode->hasSearchKey())
                {
                    ObjectDataSearcher searcher(_pDataTree);
                    pValue = searcher.find(pRoot, pPathNode->getName());
                    if (NULL != pValue)
                    {
                        pRootType = _pDataTree->getDescriptionTree()->find(pPathNode->getName(), pRootType);
                    }
                }
                
                if (NULL == pValue)
                {
                    pValue = pRoot;
                }
            }

            while (NULL != pPathNode)
            {
                pValue = addDataByPathNode(pPathNode, pRootType, pValue, pType);
                pRootType = pType;
                pPathNode = pDataPath->next();
            }

            return pValue;
        }

        Value* ObjectDataGetter::addDataByDataPath(const DCHAR* pDataPath, const DataTypeDef* pRootType, Value* pRoot)
        {
            Value* pValue = pRoot;
            const DataTypeDef* pType = NULL;
            DataPathNode* pPathNode = NULL;
            DataPath dataPath(pDataPath);

            pPathNode = dataPath.first();
            if (TRUE == pPathNode->isRoot())
            {
                pPathNode = dataPath.next();
                pValue = createRootData(pPathNode, pType);
                pRootType = pType;

                pPathNode = dataPath.next();
            }

            while (NULL != pPathNode)
            {
                pValue = addDataByPathNode(pPathNode, pRootType, pValue, pType);
                pRootType = pType;
                pPathNode = dataPath.next();
            }

            return pValue;
        }

        Value* ObjectDataGetter::fixDataByPathNode(Value* pRoot, DataPathNode* pPathNode, const DataTypeDef*& pType)
        {
            Value* pValue = pRoot;

            do 
            {
                if (FALSE == pPathNode->hasSearchKey())
                {
                    break;
                }

                if (DM_DATA_ARRAY == pType->uiType)
                {
                    pType = pType->pSubType;
                    pValue = _objectSetter.arrayAppend((Array*)pRoot, dataType2ObjectType(pType->uiType));
                }
                else if (DM_DATA_OBJECT != pType->uiType)
                {
                    pValue = NULL;
                    break;
                }

                DataPathNode::SearchKeyIter iter(pPathNode);
                iter.first();
                do
                {
                    const DCHAR* pStr = NULL;
                    const DataTypeDef* pMemberType = NULL;
                    Value* pMember = NULL;

                    if (TRUE == iter.isParam())
                    {
                        if (NULL == _pParamList || 0 != _pParamList->getValue(iter.getParamName(), pStr))
                        {
                            pValue = NULL;
                            break;
                        }
                    }
                    else
                    {
                        pStr = iter.getValue();
                    }

                    pMemberType = findType(pType, iter.getKey());
                    if (NULL == pMemberType)
                    {
                        pValue = NULL;
                        break;
                    }

                    pMember = _objectSetter.objectAdd((Object*)pValue, iter.getKey(), dataType2ObjectType(pMemberType->uiType));
                    switch (pMember->getType())
                    {
                    case VALUE_INT:
                        _objectSetter.numberSet((Number*)pMember, (D64S)strtoll(pStr, NULL, 0));
                        break;
                    case VALUE_UINT:
                        _objectSetter.numberSet((Number*)pMember, (D64U)strtoull(pStr, NULL, 0));
                        break;
                    case VALUE_STRING:
                        _objectSetter.stringSet((String*)pMember, pStr);
                        break;
                    }
                } while (TRUE == iter.next());
            } while (FALSE);
            
            return pValue;
        }
// 
//         Value* ObjectDataGetter::addDataByPathNode(Value* pRoot, DataPathNode* pPathNode, const DataTypeDef*& pType)
//         {
//             Value* pMember = NULL;
//             Value* pValue = NULL;
//             
//             do 
//             {
//                 if (TRUE == pPathNode->hasSearchKey())
//                 {
//                     if (DM_DATA_ARRAY != pType->uiType && DM_DATA_OBJECT != pType->uiType)
//                     {
//                         break;
//                     }
//                 }
// 
//                 pValue = _objectSetter.create(dataType2ObjectType(pType->uiType));
//                 if (TRUE == pPathNode->hasSearchKey())
//                 {
//                     if (DM_DATA_ARRAY == pType->uiType)
//                     {
//                         pType = pType->pSubType;
//                         pValue = _objectSetter.arrayAppend((Array*)pValue, dataType2ObjectType(pType->uiType));
//                     }
// 
//                     
//                     DataPathNode::SearchKeyIter iter(pPathNode);
//                     
//                     iter.first();
//                     do
//                     {
//                         const DCHAR* pStr = NULL;
//                         const DataTypeDef* pMemberType = NULL;
//                         Value* pMember = NULL;
// 
//                         if (TRUE == iter.isParam())
//                         {
//                             if (NULL == _pParamList || 0 != _pParamList->getValue(iter.getParamName(), pStr))
//                             {
//                                 break;
//                             }
//                         }
//                         else
//                         {
//                             pStr = iter.getValue();
//                         }
// 
//                         pMemberType = findType(pType, iter.getKey());
//                         if (NULL == pMemberType)
//                         {
//                             pValue = NULL;
//                             break;
//                         }
// 
//                         pMember = _objectSetter.objectAdd((Object*)pValue, iter.getKey(), dataType2ObjectType(pMemberType->uiType));
//                         _objectSetter.stringSet((String*)pMember, pStr);
//                     } while (TRUE == iter.next());
//                 }
//             } while (FALSE);
// 
//             return pValue;
//         }

        Value* ObjectDataGetter::createRootData(DataPathNode* pPathNode, const DataTypeDef*& pType)
        {
            Value* pValue = NULL;

            do 
            {
                pType = _pDataTree->getDescriptionTree()->find(pPathNode->getName());
                if (NULL == pType)
                {
                    break;
                }

                pValue = _objectSetter.objectAdd(&_pDataTree->_root, pPathNode->getName(), dataType2ObjectType(pType->uiType));
                if (NULL == pValue)
                {
                    break;
                }

                pValue = fixDataByPathNode(pValue, pPathNode, pType);
            } while (FALSE);

            return pValue;
        }


        ObjectDataSetter::ObjectDataSetter(DataTree* pDataTree): ObjectDataGetter(pDataTree)
        {
        }

        D32S ObjectDataSetter::set(const DCHAR* pDataPath, const void* pData, D32U uiDataSize)
        {
            const DataTypeDef* pType = NULL;
            Value* pValue = get(pDataPath, pType);

            return setData(pValue, pType, pData, uiDataSize);
        }


        D32S ObjectDataSetter::set(const DCHAR* pDataPath, ParamList& paramList, const void* pData, D32U uiDataSize)
        {
            const DataTypeDef* pType = NULL;
            Value*  pValue = get(pDataPath, paramList, pType);

            return setData(pValue, pType, pData, uiDataSize);
        }

        D32S ObjectDataSetter::remove(const DCHAR* pDataPath, ParamList* pParamList)
        {
            D32S iRet = -1;
            ObjectValueDetail detail;
            ObjectDataSearcher searcher(_pDataTree);

            iRet = searcher.find(pDataPath, pParamList, &detail);
            if (0 == iRet)
            {
                
                switch (detail.pParent->getType())
                {
                case VALUE_ARRAY:
                    iRet = _objectSetter.arrayRemove((Array*)detail.pParent, detail.pValue);
                    break;
                case VALUE_OBJECT:
                    iRet = _objectSetter.objectRemove((Object*)detail.pParent, detail.pValue);
                    break;
                default:
                    break;
                }
            }

            return iRet;
        }

        void ObjectDataSetter::undo()
        {
            _objectSetter.undo();
        }

        D32S ObjectDataSetter::setData(Value* pValue, const DataTypeDef* pType, const void* pData, D32U uiDataSize)
        {
            D32S iRet = -1;

            if (NULL != pValue)
            {
                DataObjectDecoder decoder(&_objectSetter);

                iRet = decoder.decode(pType, &pValue, (void*)pData, uiDataSize);
                if (0 != iRet)
                {
                    _objectSetter.undo();
                    pValue = NULL;
                }
            }

            return iRet;
        }


        D32S ObjectDataSetter::set(Value* pValue, D64S iValue)
        {
            return _objectSetter.numberSet((Number*)pValue, iValue);
        }

        D32S ObjectDataSetter::set(Value* pValue, D64U uiValue)
        {
            return _objectSetter.numberSet((Number*)pValue, uiValue);
        }

        D32S ObjectDataSetter::set(Value* pValue, DOUBLE dValue)
        {
            return _objectSetter.numberSet((Number*)pValue, dValue);
        }

        D32S ObjectDataSetter::set(Value* pValue, const DCHAR* pStr)
        {
            return _objectSetter.stringSet((String*)pValue, pStr);
        }

// 
//         ObjectDataRemover::ObjectDataRemover(DataTree* pDataTree)
//         {
//             _pDataTree = pDataTree;
//         }
// 
//         D32S ObjectDataRemover::remove(const DCHAR* pDataPath)
//         {
//             return remove(pDataPath, NULL);
//         }
// 
//         D32S ObjectDataRemover::remove(const DCHAR* pDataPath, ParamList& paramList)
//         {
//             return remove(pDataPath, &paramList);
//         }
// 
//         D32S ObjectDataRemover::remove(const DCHAR* pDataPath, ParamList* pParamList)
//         {
//             D32S iRet = -1;
//             ObjectValueDetail detail;
//             ObjectDataSearcher searcher(_pDataTree);
// 
//             iRet = searcher.find(pDataPath, pParamList, &detail);
//             if (0 == iRet)
//             {
//                 switch (detail.pParent->getType())
//                 {
//                 case VALUE_ARRAY:
//                     (void)((Array*)detail.pParent)->remove(detail.pValue);
//                     delete detail.pValue;
//                     break;
//                 case VALUE_OBJECT:
//                     (void)((Object*)detail.pParent)->remove(detail.pValue);
//                     delete detail.pValue;
//                     break;
//                 default:
//                     break;
//                 }
//             }
// 
//             return iRet;
//         }


        DataTreeStorage::DataTreeStorage(DataTree* pDataTree, IPersistStorage* pKvStorage)
        {
            _pDataTree = pDataTree;
            _pKvPersistStorage = pKvStorage;
        }

        D32S DataTreeStorage::save(const DCHAR* pDataPath)
        {
            D32S iRet = -1;
            DataPath dataPath(pDataPath);

            do 
            {
                DataPathNode* pNode = dataPath.first();
                Value* pValue = _pDataTree->queryData(pNode->getName());
                if (NULL == pValue)
                {
                    break;
                }

                ObjectJsonSerializer serializer;
                const DCHAR* pJsonStr = serializer.serialize(pValue);
                if (NULL == pJsonStr)
                {
                    break;
                }
                
                iRet = _pKvPersistStorage->setString(pNode->getRootName(), pJsonStr);
            } while (FALSE);

            return iRet;
        }

        D32S DataTreeStorage::load(const DCHAR* pRootName)
        {
            D32S iRet = -1;
            const DCHAR* pJsonStr = NULL;
            Value* pValue = NULL;

            do 
            {
                pJsonStr = _pKvPersistStorage->getString(&pRootName[1]);
                if (NULL == pJsonStr)
                {
                    iRet = 0;
                    break;
                }

                ObjectJsonParser parser;
                pValue = parser.parse(pJsonStr);
                if (NULL == pValue)
                {
                    break;
                }

                iRet = _pDataTree->_root.add(pRootName, pValue);
            } while (FALSE);

            return iRet;
        }
        
        D32S DataTreeStorage::load()
        {
            D32S iRet = -1;
            DataDescriptionTreeIterator iter(&_pDataTree->_descriptionTree);

            while (TRUE != iter.isEnd())
            {
                iRet = load(iter.getName());
                if (-1 == iRet)
                {
                    break;
                }

                iter.next();
            }

            return iRet;
        }

        void DataTreeStorage::cleanup()
        {
            _pKvPersistStorage->clear();
        }

    }
}

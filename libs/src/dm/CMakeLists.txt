cmake_minimum_required(VERSION 3.13)
project(admdm C CXX)

set(LIBRARY_NAME "admdm")

find_package(json REQUIRED)
add_definitions(-fPIC)

add_library(
    ${LIBRARY_NAME}
    # STATIC
    src/adm_dm_dataManager.cpp
    src/DataObjectCodec.cpp
    src/DataObjectSearcher.cpp
    src/DataPath.cpp
    src/DataSearcher.cpp
    src/DataTree.cpp
    src/JsonDataRule.cpp
    src/Object.cpp
    src/Param.cpp
    src/StringDict.cpp
    src/VarSet.cpp
)

target_include_directories(
    ${LIBRARY_NAME}
    PRIVATE
    ../../inc
    ../../inc/dm
    ../../../platform   /os/inc
    ../../../package/json
    ${CMAKE_INSTALL_INCLUDEDIR}
    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/libs/inc>
)

target_link_libraries(
    ${LIBRARY_NAME}
    PRIVATE
    -Wl,--whole-archive
    ${CMAKE_INSTALL_LIBDIR}/libjson-c.a
    # ${THREAD_LIBRARY}
    -Wl,--no-whole-archive
)

add_dependencies(${LIBRARY_NAME} json)


#add_subdirectory(sample)


#ifndef __WORKQUEUE_H
#define __WORKQUEUE_H

#include <pthread.h>

/*
 * Structure to keep track of work queue requests.
 */
typedef struct workq_ele_tag {
    struct workq_ele_tag        *next;
    void                        *data;
} workq_ele_t;

/*
 * Structure describing a work queue.
 */
typedef struct workq_tag {
    pthread_mutex_t     mutex;
    pthread_cond_t      cv;             /* wait for work */
    pthread_attr_t      attr;           /* create detached threads */
    workq_ele_t         *first, *last;  /* work queue */
    int                 valid;          /* set when valid */
    int                 quit;           /* set when workq should quit */
    int                 parallelism;    /* number of threads required */
    int                 counter;        /* current number of threads */
    int                 idle;           /* number of idle threads */
    void (*engine)(void *arg);                  /* user engine */
} workq_t;

#define WORKQ_VALID     0xdec1992

/*
 * Define work queue functions
 */
extern int workq_init(
    workq_t     *wq,
    int         threads,                /* maximum threads */
    void (*engine)(void *));            /* engine routine */
extern int workq_destroy(workq_t *wq);
extern int workq_add(workq_t *wq, void *data);


/* The follow work queue infterface for using simply */
struct work_struct;

struct workqueue {
    char name[64];
    struct workq_tag tag;
};

typedef void (*work_func_t)(struct work_struct *work);

struct work_struct {
    work_func_t func;
};

#define INIT_QUEUEWORK(w, f) \
    do {                     \
        (w)->func = f;       \
    } while (0)

int init_workqueue(struct workqueue *wq, const char *name);
int deinit_workqueue(struct workqueue *wq);
struct workqueue *alloc_workqueue(const char *name);
int destory_workqueue(struct workqueue *wq);

/**
 * queue_work - queue work on a workqueue
 * @wq: workqueue to use
 * @work: work to queue
 *
 * Returns %error if @work was already on a queue, %0 otherwise.
 */
int queue_work(struct workqueue *wq, struct work_struct *work);

/**
 * schedule_work - put work task in global workqueue
 * @work: job to be done
 *
 * Returns %error if @work was already on the kernel-global workqueue and
 * %0 otherwise.
 *
 * This puts a job in the kernel-global workqueue if it was not already
 * queued and leaves it in the same position on the kernel-global
 * workqueue otherwise.
 */
int schedule_work(struct work_struct *work);

#endif

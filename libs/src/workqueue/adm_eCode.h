#ifndef __ADM_E_CODE_H_
#define	__ADM_E_CODE_H_


#include "adm_typedefs.h"

#define	ADM_PHASE_SHIFT	10000
#define	ADM_PHASE_E_OK	1000

/* error code number */
#define    		ADM_E_OK		0	/*	成功	*/
#define    		ADM_E_OTHER_START		100	/*	其他错误段开始（100-199）	*/
#define    		ADM_E_UNKNOWN		101	/*	未知错误	*/
#define    		ADM_E_START_FSERVER_ERROR		102	/*	启动文件传输服务失败	*/
#define    		ADM_E_IO_START		200	/*	设备I/O错误段开始（200-299）	*/
#define    		ADM_E_FILE_NOT_EXIST		201	/*	文件不存在	*/
#define    		ADM_E_OPEN_FILE_FAIL		202	/*	打开文件失败	*/
#define    		ADM_E_READ_FILE_FAIL		203	/*	读文件失败	*/
#define    		ADM_E_WRITE_FILE_FAIL		204	/*	写文件失败	*/
#define    		ADM_E_NO_MEMORY		205	/*	RAM不足	*/
#define    		ADM_E_SPACE_NOT_ENOUGH		206	/*	磁盘空间不足	*/
#define    		ADM_E_EXTRACT_PKG_FAIL		207	/*	解压缩失败	*/
#define    		ADM_E_NET_START		300	/*	网络错误开始（300-399）	*/
#define    		ADM_E_NET_UNKNOWN		301	/*	未知的网络错误	*/
#define    		ADM_E_URL_MALFORMAT		302	/*	URL格式不正确	*/
#define    		ADM_E_COULDNT_RESOLVE_HOST		303	/*	无法解析主机	*/
#define    		ADM_E_COULDNT_CONNECT		304	/*	无法连接	*/
#define    		ADM_E_HTTP_RETURNED_ERROR		305	/*	HTTP协议返回错误	*/
#define    		ADM_E_UPLOAD_FAILED		306	/*	启动上传失败	*/
#define    		ADM_E_OPERATION_TIMEDOUT		307	/*	操作超时	*/
#define    		ADM_E_RANGE_ERROR		308	/*	服务器不支持或接受范围请求	*/
#define    		ADM_E_HTTP_POST_ERROR		309	/*	HTTP的post请求失败	*/
#define    		ADM_E_BAD_DOWNLOAD_RESUME		310	/*	无法恢复下载	*/
#define    		ADM_E_ABORTED_BY_CALLBACK		311	/*	被回调中止	*/
#define    		ADM_E_TOO_MANY_REDIRECTS		312	/*	重定向太多	*/
#define    		ADM_E_GOT_NOTHING		313	/*	没有从服务器返回任何内容	*/
#define    		ADM_E_SEND_ERROR		314	/*	发送网络数据失败	*/
#define    		ADM_E_RECV_ERROR		315	/*	接收网络数据失败	*/
#define    		ADM_E_BAD_CONTENT_ENCODING		316	/*	无法识别的传输编码	*/
#define    		ADM_E_FILESIZE_EXCEEDED		317	/*	超出最大文件大小	*/
#define    		ADM_E_LOGIN_DENIED		318	/*	远程服务器拒绝登录	*/
#define    		ADM_E_REMOTE_DISK_FULL		319	/*	服务器上的磁盘空间不足	*/
#define    		ADM_E_REMOTE_FILE_NOT_FOUND		320	/*	URL中引用的资源不存在	*/
#define    		ADM_E_SSL_CONNECT_ERROR		321	/*	SSL / TLS握手中出现问题	*/
#define    		ADM_E_PEER_FAILED_VERIFICATION		322	/*	远程服务器的SSL证书异常	*/
#define    		ADM_E_SSL_ENGINE_NOTFOUND		323	/*	找不到指定的加密引擎	*/
#define    		ADM_E_SSL_ENGINE_SETFAILED		324	/*	将所选SSL加密引擎设置为默认值失败	*/
#define    		ADM_E_SSL_CERTPROBLEM		325	/*	本地客户端证书的问题	*/
#define    		ADM_E_SSL_CIPHER		326	/*	无法使用指定的密码	*/
#define    		ADM_E_SSL_CACERT		327	/*	同侪凭证不能与已知的CA证书进行身份验证	*/
#define    		ADM_E_SSL_ENGINE_INITFAILED		328	/*	启动SSL引擎失败	*/
#define    		ADM_E_SSL_CACERT_BADFILE		329	/*	读取SSL CA证书时出现问题	*/
#define    		ADM_E_FTP_CONNECT_FAILED		330	/*	FTP连接失败	*/
#define    		ADM_E_UP_START		400	/*	UP错误开始（400-499）	*/
#define    		ADM_E_UP_INVAL_TOPIC		401	/*	无效的主题	*/
#define    		ADM_E_UP_TIMEOUT		402	/*	请求超时	*/
#define    		ADM_E_UP_SEND_MSG		403	/*	发送消息失败	*/
#define    		ADM_E_UP_RECEV_MSG		404	/*	接收消息失败	*/
#define    		ADM_E_PARAM_START		500	/*	入参检查开始（500-599）	*/
#define    		ADM_E_INVAL_PARAM		501	/*	无效的参数	*/
#define    		ADM_E_INVAL_DEVINFO		502	/*	无效的设备信息	*/
#define    		ADM_E_INVAL_TASK		503	/*	无效的任务	*/
#define    		ADM_E_INVAL_CFG_FORM		504	/*	无效的配置表	*/
#define    		ADM_E_SYSI_START		600	/*	sysi错误开始（600-699）	*/
#define    		ADM_E_LACK_ECU_MEMBER		601	/*	ecu信息不全	*/
#define    		ADM_E_OTA_SERVER_START		700	/*	ota服务器返回开始（700-799）	*/
#define    		ADM_E_OTA_SERVER_LACK_INFORMATION		701	/*	服务器返回缺少信息	*/
#define    		ADM_E_OTA_SERVER_UNKNOWN_STATUS		702	/*	服务器未知状态	*/
#define    		ADM_E_OTA_SERVER_INTERNAL_ERROR		703	/*	服务器内部异常	*/
#define    		ADM_E_OTA_SERVER_NO_NEW_VERSION		704	/*	已经是最新版本	*/
#define    		ADM_E_OTA_SERVER_UNREGISTER		705	/*	车辆未注册	*/
#define    		ADM_E_OTA_SERVER_VEHICLE_NOT_EXIST		706	/*	车辆不存在	*/
#define    		ADM_E_OTA_SERVER_INVAL_JSON		707	/*	参数Json格式转换异常	*/
#define    		ADM_E_OTA_SERVER_CHECK_INVAL_PARAM		708	/*	参数校验失败	*/
#define    		ADM_E_OTA_SERVER_VERIFY_SIGN_ERROR		709	/*	签名验证失败	*/
#define    		ADM_E_OTA_SERVER_DECRYPT_ERROR		710	/*	请求报文解密失败	*/
#define    		ADM_E_OTA_SERVER_SAVE_FILE_ERROR		711	/*	文件保存异常	*/
#define    		ADM_E_OTA_SERVER_ILLEGAL_FILE_FORMAT		712	/*	文件格式非法	*/
#define    		ADM_E_OTA_SERVER_REQ_TOO_FREQUENT		713	/*	同一车辆频繁请求	*/
#define    		ADM_E_OTA_SERVER_TASK_STOP		714	/*	任务终止	*/
#define			ADM_E_OTA_SERVER_CFG_NEED_UPDATE		715 /*	配置信息需要更新	*/
#define			ADM_E_OTA_SERVER_CFG_NOT_EXIST		716		/* 配置信息不存在 */
#define    		ADM_E_CONDITION_START		800	/*	条件校验开始（800-899）	*/
#define    		ADM_E_FSM_ABNORMAL		801	/*	状态机异常	*/
#define    		ADM_E_VERIFY_SIZE_FAIL		802	/*	文件大小校验失败	*/
#define    		ADM_E_VERIFY_SHA256_FAIL		803	/*	文件sha256校验失败	*/
#define    		ADM_E_INSTALL_CONDITION_DISABEL		804	/*	安装前置条件不满足	*/
#define    		ADM_E_INVAL_DEPENDENCE		805	/*	无效的安装依赖	*/
#define    		ADM_E_INVAL_ECU_VERSION		806	/*	安装前校验ECU版本不符合安装策略	*/
#define    		ADM_E_INSTALL_ROLLBACK_FAIL		807	/*	安装失败回滚失败	*/
#define    		ADM_E_INSTALL_ROLLBACK_OK		808	/*	安装失败回滚成功	*/
#define    		ADM_E_INSTALL_FAIL		809	/*	安装失败	*/
#define    		ADM_E_RESERVATION_EXPIRED		810	/*	预约安装失效	*/
#define    		ADM_E_INVAL_HMI_STATE		811	/*	HMI持有的状态异常	*/
#define    		ADM_E_NOT_COMPLETED_INIT		812	/*	SDK未初始化完成	*/
#define 		ADM_E_PACKAGE_VERIFY_SIGN_FAIL	813 /*  升级包签名验证失败 */
#define			ADM_E_PACKAGE_DECRYPT_FAIL 		814 /* 升级包解密失败  */
#define 		ADM_E_PACKAGE_DECRYPT_KEY_QUERY_FAIL 815  /* 升级包解密KEY查询失败 */
#define    		ADM_E_END_START		900	/*	主动终止开始（900-999）	*/
#define    		ADM_E_PAUSE_DOWNLOAD		901	/*	暂停下载	*/
#define    		ADM_E_CANCEL_DOWNLOAD		902	/*	取消下载	*/
#define    		ADM_E_INSTALL_CANCEL		903	/*	取消安装	*/
#define    		ADM_E_OFFLINE				904	/*  设备已下线  */

/* error code desc */
#define    		ADM_E_DESC_OK								"ok"
#define    		ADM_E_DESC_OTHER_START						"other start"
#define    		ADM_E_DESC_UNKNOWN							"unknown"
#define    		ADM_E_DESC_START_FSERVER_ERROR				"start fserver error"
#define    		ADM_E_DESC_IO_START							"io start"
#define    		ADM_E_DESC_FILE_NOT_EXIST					"file not exist"
#define    		ADM_E_DESC_OPEN_FILE_FAIL					"open file fail"
#define    		ADM_E_DESC_READ_FILE_FAIL					"read file fail"
#define    		ADM_E_DESC_WRITE_FILE_FAIL					"write file fail"
#define    		ADM_E_DESC_NO_MEMORY						"no memory"
#define    		ADM_E_DESC_SPACE_NOT_ENOUGH					"space not enough"
#define    		ADM_E_DESC_EXTRACT_PKG_FAIL					"extract pkg fail"
#define    		ADM_E_DESC_NET_START						"net start"
#define    		ADM_E_DESC_NET_UNKNOWN						"net unknown"
#define    		ADM_E_DESC_URL_MALFORMAT					"url malformat"
#define    		ADM_E_DESC_COULDNT_RESOLVE_HOST				"couldnt resolve host"
#define    		ADM_E_DESC_COULDNT_CONNECT					"couldnt connect"
#define    		ADM_E_DESC_HTTP_RETURNED_ERROR				"http returned error"
#define    		ADM_E_DESC_UPLOAD_FAILED					"upload failed"
#define    		ADM_E_DESC_OPERATION_TIMEDOUT				"operation timedout"
#define    		ADM_E_DESC_RANGE_ERROR						"range error"
#define    		ADM_E_DESC_HTTP_POST_ERROR					"http post error"
#define    		ADM_E_DESC_BAD_DOWNLOAD_RESUME				"bad download resume"
#define    		ADM_E_DESC_ABORTED_BY_CALLBACK				"aborted by callback"
#define    		ADM_E_DESC_TOO_MANY_REDIRECTS				"too many redirects"
#define    		ADM_E_DESC_GOT_NOTHING						"got nothing"
#define    		ADM_E_DESC_SEND_ERROR						"send error"
#define    		ADM_E_DESC_RECV_ERROR						"recv error"
#define    		ADM_E_DESC_BAD_CONTENT_ENCODING				"bad content encoding"
#define    		ADM_E_DESC_FILESIZE_EXCEEDED				"filesize exceeded"
#define    		ADM_E_DESC_LOGIN_DENIED						"login denied"
#define    		ADM_E_DESC_REMOTE_DISK_FULL					"remote disk full"
#define    		ADM_E_DESC_REMOTE_FILE_NOT_FOUND			"remote file not found"
#define    		ADM_E_DESC_SSL_CONNECT_ERROR				"ssl connect error"
#define    		ADM_E_DESC_PEER_FAILED_VERIFICATION			"peer failed verification"
#define    		ADM_E_DESC_SSL_ENGINE_NOTFOUND				"ssl engine notfound"
#define    		ADM_E_DESC_SSL_ENGINE_SETFAILED				"ssl engine setfailed"
#define    		ADM_E_DESC_SSL_CERTPROBLEM					"ssl certproblem"
#define    		ADM_E_DESC_SSL_CIPHER						"ssl cipher"
#define    		ADM_E_DESC_SSL_CACERT						"ssl cacert"
#define    		ADM_E_DESC_SSL_ENGINE_INITFAILED			"ssl engine initfailed"
#define    		ADM_E_DESC_SSL_CACERT_BADFILE				"ssl cacert badfile"
#define    		ADM_E_DESC_FTP_CONNECT_FAILED				"ftp connect failed"
#define    		ADM_E_DESC_UP_START							"UP start"
#define    		ADM_E_DESC_UP_INVAL_TOPIC					"UP inval topic"
#define    		ADM_E_DESC_UP_TIMEOUT						"UP timeout"
#define    		ADM_E_DESC_UP_SEND_MSG						"UP send msg"
#define    		ADM_E_DESC_UP_RECEV_MSG						"UP recev msg"
#define    		ADM_E_DESC_PARAM_START						"param start"
#define    		ADM_E_DESC_INVAL_PARAM						"inval param"
#define    		ADM_E_DESC_INVAL_DEVINFO					"inval devinfo"
#define    		ADM_E_DESC_INVAL_TASK						"inval task"
#define    		ADM_E_DESC_INVAL_CFG_FORM					"inval cfg form"
#define    		ADM_E_DESC_SYSI_START						"sysi start"
#define    		ADM_E_DESC_LACK_ECU_MEMBER					"lack ecu member"
#define    		ADM_E_DESC_OTA_SERVER_START					"ota server start"
#define    		ADM_E_DESC_OTA_SERVER_LACK_INFORMATION		"ota server lack information"
#define    		ADM_E_DESC_OTA_SERVER_UNKNOWN_STATUS		"ota server unknown status"
#define    		ADM_E_DESC_OTA_SERVER_INTERNAL_ERROR		"ota server internal error"
#define    		ADM_E_DESC_OTA_SERVER_NO_NEW_VERSION		"ota server no new version"
#define    		ADM_E_DESC_OTA_SERVER_UNREGISTER			"ota server unregister"
#define    		ADM_E_DESC_OTA_SERVER_VEHICLE_NOT_EXIST		"ota server vehicle not exist"
#define    		ADM_E_DESC_OTA_SERVER_INVAL_JSON			"ota server inval json"
#define    		ADM_E_DESC_OTA_SERVER_CHECK_INVAL_PARAM		"ota server check inval param"
#define    		ADM_E_DESC_OTA_SERVER_VERIFY_SIGN_ERROR		"ota server verify sign error"
#define    		ADM_E_DESC_OTA_SERVER_DECRYPT_ERROR			"ota server decrypt error"
#define    		ADM_E_DESC_OTA_SERVER_SAVE_FILE_ERROR		"ota server save file error"
#define    		ADM_E_DESC_OTA_SERVER_ILLEGAL_FILE_FORMAT	"ota server illegal file format"
#define    		ADM_E_DESC_OTA_SERVER_REQ_TOO_FREQUENT		"ota server req too frequent"
#define    		ADM_E_DESC_OTA_SERVER_TASK_STOP				"ota server task stop"
#define			ADM_E_DESC_OTA_SERVER_CFG_NEED_UPDATE		"ota server cfg need update"
#define			ADM_E_DESC_OTA_SERVER_CFG_NOT_EXIST			"ota server cfg not exist"
#define    		ADM_E_DESC_CONDITION_START					"condition start"
#define    		ADM_E_DESC_FSM_ABNORMAL						"fsm abnormal"
#define    		ADM_E_DESC_VERIFY_SIZE_FAIL					"verify size fail"
#define    		ADM_E_DESC_VERIFY_SHA256_FAIL				"verify sha256 fail"
#define    		ADM_E_DESC_INSTALL_CONDITION_DISABEL		"install condition disabel"
#define    		ADM_E_DESC_INVAL_DEPENDENCE					"inval dependence"
#define    		ADM_E_DESC_INVAL_ECU_VERSION				"inval ecu version"
#define    		ADM_E_DESC_INSTALL_ROLLBACK_FAIL			"install rollback fail"
#define    		ADM_E_DESC_INSTALL_ROLLBACK_OK				"install rollback ok"
#define    		ADM_E_DESC_INSTALL_FAIL						"install fail"
#define    		ADM_E_DESC_RESERVATION_EXPIRED				"reservation expired"
#define    		ADM_E_DESC_INVAL_HMI_STATE					"inval hmi state"
#define    		ADM_E_DESC_NOT_COMPLETED_INIT				"not completed init"
#define 		ADM_E_DESC_PACKAGE_VERIFY_SIGN_FAIL			"package verify sign fail"
#define			ADM_E_DESC_PACKAGE_DECRYPT_FAIL 			"package decrypt fail"
#define 		ADM_E_DESC_PACKAGE_DECRYPT_KEY_QUERY_FAIL 	"package decrypt key query fail"
#define    		ADM_E_DESC_END_START						"end start"
#define    		ADM_E_DESC_PAUSE_DOWNLOAD					"pause download"
#define    		ADM_E_DESC_CANCEL_DOWNLOAD					"cancel download"
#define    		ADM_E_DESC_INSTALL_CANCEL					"install cancel"
#define    		ADM_E_DESC_OFFLINE							"UP client offline"

/* error code phase */
#define	ADM_PHASE_REGISTER	1
#define	ADM_PHASE_CHECK   	2
#define	ADM_PHASE_DOWNLOAD	3
#define	ADM_PHASE_INSTALL 	4
#define	ADM_PHASE_REPORT  	5
#define	ADM_PHASE_OTHER   	9

extern D32S   adm_eCode(D32S phase, D32S errCode); /* TODO move to ota client */
extern D32S   adm_eUncode(D32S err);
extern DCHAR *adm_eCodeDesc(D32S errCode);






#endif//__ADM_E_CODE_H_

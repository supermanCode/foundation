#include "workqueue.h"
#include "adm_typedefs.h"
#include "adm_memory.h"
#include "adm_debug.h"
#include "adm_eCode.h"


typedef struct ADM_Workqueue_ {
    workq_t     wq;
} ADM_Workqueue_T;

ADM_Workqueue_T *adm_WorkqueueNew(D32S maxThreadCount,
        void (*engine)(void *param)) {
    ADM_Workqueue_T *wq = NULL;

    wq = adm_malloc(sizeof(ADM_Workqueue_T));
    if(wq) {
        if(workq_init(&(wq->wq), maxThreadCount, engine)) {
            log_e("workq_init error");
            adm_free(wq);
            wq = NULL;
        }
    } else {
        log_e("malloc error");
    }

    return wq;
}

D32S adm_WorkqueueAdd(ADM_Workqueue_T *wq, void *param) {
    D32S ret;

    if(workq_add(&(wq->wq), param)) {
        log_e("workq_add error");
        ret = ADM_E_NO_MEMORY;
    } else {
        ret = ADM_E_OK;
    }

    return ret;
}

D32S adm_WorkqueueDel(ADM_Workqueue_T *wq) {
    D32S ret;

    if(workq_destroy(&(wq->wq))) {
        log_e("workq_destroy error");
        ret = ADM_E_NO_MEMORY;
    } else {
        ret = ADM_E_OK;
    }

    adm_free(wq);

    return ret;
}

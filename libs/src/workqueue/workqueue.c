#include <pthread.h>
#include <stdlib.h>
#include <time.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>

#include "workqueue.h"
#include "adm_debug.h"

/*
 * Thread start routine to serve the work queue.
 */
static void *workq_server(void *arg)
{
    struct timespec timeout;
    workq_t *wq = (workq_t *)arg;
    workq_ele_t *we;
    int status, timedout;

    /*
     * We don't need to validate the workq_t here... we don't
     * create server threads until requests are queued (the
     * queue has been initialized by then!) and we wait for all
     * server threads to terminate before destroying a work
     * queue.
     */
    //log_i("%s", "A worker is starting\n");
    status = pthread_mutex_lock(&wq->mutex);
    if (status != 0) {
        return NULL;
    }

    while (1) {
        timedout = 0;
        //log_i("%s", "Worker waiting for work\n");
        clock_gettime(CLOCK_MONOTONIC, &timeout);
        timeout.tv_sec += 2;

        while (wq->first == NULL && !wq->quit) {
            /*
             * Server threads time out after spending 2 seconds
             * waiting for new work, and exit.
             */
            status = pthread_cond_timedwait(
                         &wq->cv, &wq->mutex, &timeout);
            if (status == ETIMEDOUT) {
                //log_i("%s", "Worker wait timed out\n");
                timedout = 1;
                break;
            } else if (status != 0) {
                /*
                 * This shouldn't happen, so the work queue
                 * package should fail. Because the work queue
                 * API is asynchronous, that would add
                 * complication. Because the chances of failure
                 * are slim, I choose to avoid that
                 * complication. The server thread will return,
                 * and allow another server thread to pick up
                 * the work later. Note that, if this was the
                 * only server thread, the queue won't be
                 * serviced until a new work item is
                 * queued. That could be fixed by creating a new
                 * server here.
                 */
                log_e("Worker wait failed, %d (%s)\n",
                     status, strerror(status));
                wq->counter--;
                pthread_mutex_unlock(&wq->mutex);
                return NULL;
            }
        }
        if (wq->first) {
            //log_i("Work queue: %p, quit: %d\n",
            //     (void *)wq->first, wq->quit);
        } else {
            //log_i("Work queue: 0x(nil), quit: %d\n", wq->quit);
        }

        we = wq->first;

        if (we != NULL) {
            wq->first = we->next;
            if (wq->last == we) {
                wq->last = NULL;
            }
            status = pthread_mutex_unlock(&wq->mutex);
            if (status != 0) {
                return NULL;
            }
            //log_i("%s", "Worker calling engine\n");
            wq->engine(we->data);
            free(we);
            status = pthread_mutex_lock(&wq->mutex);
            if (status != 0) {
                return NULL;
            }
        }

        /*
         * If there are no more work requests, and the servers
         * have been asked to quit, then shut down.
         */
        if (wq->first == NULL && wq->quit) {
            //log_i("%s", "Worker shutting down\n");
            wq->counter--;

            /*
             * NOTE: Just to prove that every rule has an
             * exception, I'm using the "cv" condition for two
             * separate predicates here.  That's OK, since the
             * case used here applies only once during the life
             * of a work queue -- during rundown. The overhead
             * is minimal and it's not worth creating a separate
             * condition variable that would be waited and
             * signaled exactly once!
             */
            if (wq->counter == 0) {
                pthread_cond_broadcast(&wq->cv);
            }
            pthread_mutex_unlock(&wq->mutex);
            return NULL;
        }

        /*
         * If there's no more work, and we wait for as long as
         * we're allowed, then terminate this server thread.
         */
        if (wq->first == NULL && timedout) {
            //log_i("%s", "engine terminating due to timeout.\n");
            wq->counter--;
            break;
        }
    }

    pthread_mutex_unlock(&wq->mutex);
    //log_i("%s", "Worker exiting\n");
    return NULL;
}

/*
 * Initialize a work queue.
 */
int workq_init(workq_t *wq, int threads, void (*engine)(void *arg))
{
    int status;

    status = pthread_attr_init(&wq->attr);
    if (status != 0) {
        return status;
    }
    status = pthread_attr_setdetachstate(
                 &wq->attr, PTHREAD_CREATE_DETACHED);
    if (status != 0) {
        pthread_attr_destroy(&wq->attr);
        return status;
    }
    status = pthread_mutex_init(&wq->mutex, NULL);
    if (status != 0) {
        pthread_attr_destroy(&wq->attr);
        return status;
    }
    status = pthread_cond_init(&wq->cv, NULL);
    if (status != 0) {
        pthread_mutex_destroy(&wq->mutex);
        pthread_attr_destroy(&wq->attr);
        return status;
    }
    wq->quit = 0;                       /* not time to quit */
    wq->first = wq->last = NULL;        /* no queue entries */
    wq->parallelism = threads;          /* max servers */
    wq->counter = 0;                    /* no server threads yet */
    wq->idle = 0;                       /* no idle servers */
    wq->engine = engine;
    wq->valid = WORKQ_VALID;
    return 0;
}

/*
 * Destroy a work queue.
 */
int workq_destroy(workq_t *wq)
{
    int status, status1, status2;

    if (wq->valid != WORKQ_VALID) {
        return EINVAL;
    }
    status = pthread_mutex_lock(&wq->mutex);
    if (status != 0) {
        return status;
    }
    wq->valid = 0;                 /* prevent any other operations */

    /*
     * Check whether any threads are active, and run them down:
     *
     * 1.       set the quit flag
     * 2.       broadcast to wake any servers that may be asleep
     * 4.       wait for all threads to quit (counter goes to 0)
     *          Because we don't use join, we don't need to worry
     *          about tracking thread IDs.
     */
    if (wq->counter > 0) {
        wq->quit = 1;
        /* if any threads are idling, wake them. */
        if (wq->idle > 0) {
            status = pthread_cond_broadcast(&wq->cv);
            if (status != 0) {
                pthread_mutex_unlock(&wq->mutex);
                return status;
            }
        }

        /*
         * Just to prove that every rule has an exception, I'm
         * using the "cv" condition for two separate predicates
         * here. That's OK, since the case used here applies
         * only once during the life of a work queue -- during
         * rundown. The overhead is minimal and it's not worth
         * creating a separate condition variable that would be
         * waited and signalled exactly once!
         */
        while (wq->counter > 0) {
            status = pthread_cond_wait(&wq->cv, &wq->mutex);
            if (status != 0) {
                pthread_mutex_unlock(&wq->mutex);
                return status;
            }
        }
    }
    status = pthread_mutex_unlock(&wq->mutex);
    if (status != 0) {
        return status;
    }
    status = pthread_mutex_destroy(&wq->mutex);
    status1 = pthread_cond_destroy(&wq->cv);
    status2 = pthread_attr_destroy(&wq->attr);
    return (status ? status : (status1 ? status1 : status2));
}

/*
 * Add an item to a work queue.
 */
int workq_add(workq_t *wq, void *element)
{
    workq_ele_t *item;
    pthread_t id;
    int status;

    if (wq->valid != WORKQ_VALID) {
        return EINVAL;
    }

    /*
     * Create and initialize a request structure.
     */
    item = (workq_ele_t *)malloc(sizeof(workq_ele_t));
    if (item == NULL) {
        return ENOMEM;
    }
    item->data = element;
    item->next = NULL;
    status = pthread_mutex_lock(&wq->mutex);
    if (status != 0) {
        free(item);
        return status;
    }

    /*
     * Add the request to the end of the queue, updating the
     * first and last pointers.
     */
    if (wq->first == NULL) {
        wq->first = item;
    } else {
        wq->last->next = item;
    }
    wq->last = item;

    /*
     * if any threads are idling, wake one.
     */
    if (wq->idle > 0) {
        status = pthread_cond_signal(&wq->cv);
        if (status != 0) {
            pthread_mutex_unlock(&wq->mutex);
            return status;
        }
    } else if (wq->counter < wq->parallelism) {
        /*
         * If there were no idling threads, and we're allowed to
         * create a new thread, do so.
         */
        //log_i("%s", "Creating new worker\n");
        status = pthread_create(
                     &id, &wq->attr, workq_server, (void *)wq);
        if (status != 0) {
            pthread_mutex_unlock(&wq->mutex);
            return status;
        }
        wq->counter++;
    }
    pthread_mutex_unlock(&wq->mutex);
    return 0;
}

/*
 * This is the routine called by the work queue servers to
 * perform operations in parallel.
 */
static void engine_routine(void *arg)
{
    struct work_struct *work = (struct work_struct *)arg;
    if (work) {
        work->func(work);
    }
}

int init_workqueue(struct workqueue *wq, const char *name)
{
    int status;

    strncpy(wq->name, name, sizeof(wq->name));

    status = workq_init(&wq->tag, 4, engine_routine);

    if (status == 0) {
        return 0;
    } else {
        log_e("Pthread init failed, %d %s\n", status, strerror(status));
        return -1;
    }
}

int deinit_workqueue(struct workqueue *wq)
{
    int ret_value;

    ret_value = workq_destroy(&wq->tag);
    if (ret_value != 0) {
        log_e("Failed to destory work queue, %d %s\n",
             ret_value,
             strerror(ret_value));
        return -1;
    }

    return 0;
}

struct workqueue *alloc_workqueue(const char *name)
{
    struct workqueue *wq = malloc(sizeof(*wq));
    if (wq) {
        int status = init_workqueue(wq, name);
        if (status < 0) {
            free(wq);
            return NULL;
        }
    }

    return wq;
}

int destory_workqueue(struct workqueue *wq)
{
    int ret_value;

    if (!wq) {
        return -1;
    }

    ret_value = deinit_workqueue(wq);
    free(wq);

    return ret_value;
}

int queue_work(struct workqueue *wq, struct work_struct *work)
{
    int status;

    status = workq_add(&wq->tag, (void *)work);
    if (status == 0) {
        return 0;
    } else {
        log_e("Failed to queue work, %d\n", status);
        return -1;
    }
}

int schedule_work(struct work_struct *work)
{
    int ret_value;

    static struct workqueue __system_wq;
    static int __state_initialized = 0;

    if (!__state_initialized) {
        if ((ret_value = init_workqueue(&__system_wq, "sysq")) < 0) {
            return ret_value;
        }

        __state_initialized = 1;
    }

    return queue_work(&__system_wq, work);
}

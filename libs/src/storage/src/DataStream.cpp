#include "DataStream.h"
#include "adm_memory.h"
#include "adm_string.h"

DataStream::DataStream()
{
    _pData = NULL;
    _uiDataLen = 0;
    _uiDataBufSize = 0;
}

DataStream::~DataStream()
{
    if (NULL != _pData)
    {
        adm_free(_pData);
    }
}

D32S DataStream::setData(const D8U* pData, D32U uiDataLen)
{
    D32S iRet = -1;
    D8U* pDestData = _pData;

    do
    {
        if (NULL != pDestData)
        {
            if (uiDataLen > _uiDataBufSize)
            {
                adm_free(pDestData);
                pDestData = NULL;
            }
        }

        if (NULL == pDestData)
        {
            pDestData = (D8U*)adm_malloc(uiDataLen);
            if (NULL == pDestData)
            {
                break;
            }

            _pData = pDestData;
            _uiDataBufSize = uiDataLen;
        }

        adm_memcpy(pDestData, pData, uiDataLen, uiDataLen);
        _uiDataLen = uiDataLen;

        iRet = 0;
    } while (FALSE);

    return iRet;
}

D32S DataStream::setAsString(const DCHAR* pStr)
{
    return setData((const D8U*)pStr, (D32U)adm_strlen(pStr));
}


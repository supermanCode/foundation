#include "KvPersistStorage.h"
#include "adm_string.h"
#include "adm_memory.h"

KvPersistStorage::KvPersistStorage()
{
    _pStorage = NULL;
    _pKvSecurity = NULL;
}

KvPersistStorage::~KvPersistStorage()
{
    if (NULL != _pStorage)
    {
        adm_kvStorage_close(_pStorage);
    }
}

D32S KvPersistStorage::init(const DCHAR* pDbName, ADM_STORAGE_SECURITY_SUITE_T* pSecuritySuite/* = NULL*/)
{
    D32S    iRet = -1;
    DCHAR*  pErrStr = NULL;

    do 
    {
        _pStorage = adm_kvStorage_open(pDbName, &pErrStr);
        if (NULL == _pStorage)
        {
            adm_free(pErrStr);
            break;
        }

        KvStorageSecurity* pKvSecurity = NULL;
        if (NULL == pSecuritySuite)
        {
            iRet = 0;
            break;
        }

        pKvSecurity = new KvStorageSecurity();
        if (NULL != pKvSecurity)
        {
            iRet = pKvSecurity->init(pSecuritySuite);
        }

        if (0 != iRet)
        {
            adm_kvStorage_close(_pStorage);
            _pStorage = NULL;
            break;
        }

        _pKvSecurity = pKvSecurity;
    } while (FALSE);

    return iRet;
}

D32S KvPersistStorage::setString(const DCHAR* pKey, const DCHAR* pStr)
{
    D32S iRet = 0;
    DataStream stream;
    const DCHAR* pSrc = NULL;
    DCHAR* pErrStr = NULL;

    if (NULL != _pKvSecurity)
    {
        
        iRet = _pKvSecurity->encryptAsStr((const D8U*)pStr, adm_strlen(pStr), stream);
        if (0 != iRet)
        {
            pSrc = stream.getAsString();
        }
    }
    else
    {
        pSrc = pStr;
    }

    if (false == adm_kvStorage_set_string(_pStorage, pKey, pSrc, ADM_KV_STORAGE_VALUE_TYPE_JSON, &pErrStr))
    {
        adm_free(pErrStr);
        iRet = -1;
    }

    return iRet;
}

const DCHAR* KvPersistStorage::getString(const DCHAR* pKey)
{
    D32S iRet = 0;
    DCHAR* pStr = NULL;
    const DCHAR* pOut = NULL;
    DCHAR* pErrStr = NULL;
    ADM_KV_STORAGE_VALUE_TYPE_E type = ADM_KV_STORAGE_VALUE_TYPE_INVALID;

    do 
    {
        if (true == adm_kvStorage_get_string(_pStorage, pKey, &pStr, &type, &pErrStr))
        {
            if (NULL != _pKvSecurity)
            {
                iRet = _pKvSecurity->decryptAsStr((const D8U*)pStr, adm_strlen(pStr), _stream);
            }
            else
            {
                iRet = _stream.setAsString(pStr);
            }
            
            adm_free(pStr);
            if (0 == iRet)
            {
                pOut = _stream.getAsString();
            }
        }
        else
        {
            adm_free(pErrStr);
            iRet = -1;
        }
    } while (FALSE);

    return pOut;
}

D32S KvPersistStorage::remove(const DCHAR* pKey)
{
    DCHAR* pErrStr = NULL;
    return FALSE != adm_kvStorage_remove_key(_pStorage, pKey, &pErrStr) ? 0 : -1;
}

void KvPersistStorage::clear()
{
    DCHAR* pErrStr = NULL;
    (void)adm_kvStorage_remove_allKeys(_pStorage, &pErrStr);
}


// D32S adm_com_cryptoAesCbcPkcs7Encrypt(DHANDLE handle, const D8U* inData, const size_t inLen, const D8U* key, const D8U* iv, const DBOOL isB64Enc);
// D32S adm_com_cryptoAesCbcPkcs7Decrypt(DHANDLE handle, const D8U* inData, const size_t inLen, const D8U* key, const D8U* iv, const DBOOL isB64Enc);
// 

D32S KvStorageSecurity::init(const ADM_STORAGE_SECURITY_SUITE_T* pSuite)
{
    D32S iRet = -1;
    if (NULL != pSuite->fnEncrypt && NULL != pSuite->fnDecrypt)
    {
        _suite = *pSuite;
        iRet = 0;
    }
    
    return iRet;
}

D32S KvStorageSecurity::decryptAsStr(const D8U* pInData, D32U uiInDataLen, DataStream& dataStream)
{
    return _suite.fnDecrypt(_suite.pSecurity, pInData, uiInDataLen, (ADM_DATA_STREAM_T*)&dataStream);
}

D32S KvStorageSecurity::encryptAsStr(const D8U* pInData, D32U uiInDataLen, DataStream& dataStream)
{
    return _suite.fnEncrypt(_suite.pSecurity, pInData, uiInDataLen, (ADM_DATA_STREAM_T*)&dataStream);
}

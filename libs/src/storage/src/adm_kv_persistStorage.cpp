#include "adm_kv_persistStorage.h"
#include "KvPersistStorage.h"


D32S adm_kv_persistStorageInit(ADM_KV_PERSIST_STORAGE_T* pStorage, const DCHAR* pDbName, ADM_STORAGE_SECURITY_SUITE_T* pSecuritySuite)
{
    new((KvPersistStorage*)pStorage) KvPersistStorage();
    return ((KvPersistStorage*)pStorage)->init(pDbName, pSecuritySuite);
}

void adm_kv_persistStorageDestroy(ADM_KV_PERSIST_STORAGE_T* pStorage)
{
    ((KvPersistStorage*)pStorage)->~KvPersistStorage();
}
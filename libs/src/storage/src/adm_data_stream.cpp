#include "adm_data_stream.h"
#include "DataStream.h"
#include <new>


void adm_dataStream_init(ADM_DATA_STREAM_T* pDataStream)
{
    new((DataStream*)pDataStream) DataStream();
}

void adm_dataStream_destroy(ADM_DATA_STREAM_T* pDataStream)
{
    ((DataStream*)pDataStream)->~DataStream();
}

D32S adm_dataStream_setData(ADM_DATA_STREAM_T* pDataStream, const D8U* pData, D32U uiDataLen)
{
    return ((DataStream*)pDataStream)->setData(pData, uiDataLen);
}

const D8U* adm_dataStream_getData(ADM_DATA_STREAM_T* pDataStream, D32U* puiDataLen)
{
    *puiDataLen = ((DataStream*)pDataStream)->getDataLength();
    return ((DataStream*)pDataStream)->getData();
}

D32S adm_dataStream_setString(ADM_DATA_STREAM_T* pDataStream, const DCHAR* pStr)
{
    return ((DataStream*)pDataStream)->setAsString(pStr);
}

const DCHAR* adm_dataStream_getString(ADM_DATA_STREAM_T* pDataStream)
{
    return ((DataStream*)pDataStream)->getAsString();
}

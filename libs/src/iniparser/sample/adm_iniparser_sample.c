#include "iniparser/adm_iniparser.h"
#include <stdio.h>
#define  LOG_TAG        "[INI]"
#include "adm_debug.h"


#define ADM_INIPARSER_NAME	"./initest.ini"

int main()
{
	Dictionary_t* dict = NULL;

	int blanks_a = 0;
	int errorCode = 0;
	long int multi_d = 0;
	const char* multi_a = NULL;
	const char* logDir = NULL;
    long long logMaxSize = 0;
    unsigned int logMaxRotate = 0;

    logDir = "/tmp/vus/";
    logMaxSize = 1024 * 1024 * 10;
    logMaxRotate = 1;


	do {
		adm_debug_logInit(logDir, logMaxSize, logMaxRotate);

		log_e("xxxxxxxxxxxxxxxxxxxxxxx");

		dict = adm_iniparser_load(ADM_INIPARSER_NAME);
		if (NULL == dict) {
			log_e("adm_iniparser_load %s failed\n", ADM_INIPARSER_NAME);
			break;
		} else {
			
		}

		// adm_iniparser_dump(dict, stderr);

		blanks_a = adm_iniparser_getint(dict, "blanks:a", errorCode);
		if (0 == errorCode) {
			printf("blanks:a get successed\n");
			printf("blanks_a:%d\n", blanks_a);
		}

		multi_a = adm_iniparser_getstring(dict, "multi:a", "UNDEF");
		if (0 == errorCode) {
			printf("multi:a get successed\n");
			printf("multi_a %s\n", multi_a);
		}

		multi_d = adm_iniparser_getlongint(dict, "multi:d", errorCode);
		if (0 == errorCode) {
			printf("multi:d get successed\n");
			printf("multi_d:%ld\n", multi_d);
		}
	} while(0);

	adm_iniparser_freedict(dict);
	return 0;
}

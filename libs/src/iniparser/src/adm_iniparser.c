#include "iniparser/adm_iniparser.h"
#include "iniparser/iniparser.h"

void adm_iniparser_set_error_callback(int (*errback)(const char*, ...))
{
    iniparser_set_error_callback(errback);
}

int adm_iniparser_getnsec(const Dictionary_t* d)
{
    return iniparser_getnsec(d);
}

const char* adm_iniparser_getsecname(const Dictionary_t* d, int n)
{
    return iniparser_getsecname(d, n);
}

void adm_iniparser_dump_ini(const Dictionary_t * d, FILE * f)
{
    iniparser_dump_ini(d, f);
}

void adm_iniparser_dumpsection_ini(const Dictionary_t * d, const char * s, FILE * f)
{
    iniparser_dumpsection_ini(d, s, f);
}

void adm_iniparser_dump(const Dictionary_t * d, FILE * f)
{
    iniparser_dump(d, f);
}

int adm_iniparser_getsecnkeys(const Dictionary_t * d, const char * s)
{
    return iniparser_getsecnkeys(d, s);
}

const char ** adm_iniparser_getseckeys(const Dictionary_t * d, const char * s, const char ** keys)
{
    return iniparser_getseckeys(d, s, keys);
}

const char * adm_iniparser_getstring(const Dictionary_t * d, const char * key, const char * def)
{
    return iniparser_getstring(d, key, def);
}

int adm_iniparser_getint(const Dictionary_t * d, const char * key, int notfound)
{
    return iniparser_getint(d, key ,notfound);
}

long int adm_iniparser_getlongint(const Dictionary_t * d, const char * key, long int notfound)
{
    return iniparser_getlongint(d, key, notfound);
}

double adm_iniparser_getdouble(const Dictionary_t * d, const char * key, double notfound)
{
    return iniparser_getdouble(d, key, notfound);
}

int adm_iniparser_getboolean(const Dictionary_t * d, const char * key, int notfound)
{
    return iniparser_getboolean(d, key, notfound);
}

int adm_iniparser_set(Dictionary_t * ini, const char * entry, const char * val)
{
    return iniparser_set(ini, entry, val);
}

void adm_iniparser_unset(Dictionary_t * ini, const char * entry)
{
    iniparser_unset(ini, entry);
}

int adm_iniparser_find_entry(const Dictionary_t * ini, const char * entry)
{
    return iniparser_find_entry(ini, entry);
}

Dictionary_t * adm_iniparser_load(const char * ininame)
{
    return iniparser_load(ininame);
}

void adm_iniparser_freedict(Dictionary_t * d)
{
    iniparser_freedict(d);
}
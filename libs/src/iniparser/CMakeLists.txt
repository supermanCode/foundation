cmake_minimum_required(VERSION 3.13)
project(iniparser C CXX)

set(LIBRARY_NAME "adminiparser")

find_package(iniparser REQUIRED)

add_library(
	${LIBRARY_NAME}
	#SHARED
	#STATIC
	src/adm_iniparser.c
)

target_include_directories(
	${LIBRARY_NAME}
	PRIVATE
		${CMAKE_INSTALL_INCLUDEDIR}
		$<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/libs/inc/>
)

if(${CUSTOM_CMAKE_SYSTEM_NAME} MATCHES "Linux")
    set(THREAD_LIBRARY pthread z)
elseif(${CUSTOM_CMAKE_SYSTEM_NAME} MATCHES "Android")
    set(THREAD_LIBRARY c z)
elseif(${CUSTOM_CMAKE_SYSTEM_NAME} MATCHES "Alios")
    set(THREAD_LIBRARY c)
elseif(${CUSTOM_CMAKE_SYSTEM_NAME} MATCHES "QNX7")
    set(THREAD_LIBRARY c)
else()
    set(THREAD_LIBRARY pthread z)
endif()

if(USE_SYSTEM_SSL)
    message("+++++++++++use system ssl USE_SYSTEM_SSL = ${USE_SYSTEM_SSL}")
    set(SSL_LIBRARY crypto ssl)
else()
    message("+++++++++++use project compile ssl USE_SYSTEM_SSL = ${USE_SYSTEM_SSL}")
    if(USE_DYNAMIC_SSL_LIB)
        message("+++++++++++use project compile dynamic ssl USE_DYNAMIC_SSL_LIB = ${USE_DYNAMIC_SSL_LIB}")
        set(SSL_LIBRARY ${CMAKE_INSTALL_LIBDIR}/libcrypto.so  ${CMAKE_INSTALL_LIBDIR}/libssl.so)
    else()
        message("+++++++++++use project compile static ssl USE_DYNAMIC_SSL_LIB = ${USE_DYNAMIC_SSL_LIB}")
        set(SSL_LIBRARY "")
    endif()
endif()

target_link_libraries(
	${LIBRARY_NAME}
	PRIVATE
	${CMAKE_INSTALL_LIBDIR}/libiniparser.a 
	${THREAD_LIBRARY}
	${SSL_LIBRARY}
)

add_dependencies(${LIBRARY_NAME} iniparser)

add_subdirectory(sample)

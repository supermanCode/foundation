#include "JsonDataSerialization.h"
#include "adm_memory.h"







D32S JsonDataEncoder::JsonDataEncodeHelper::init(const DataTypeDef* pTypeDef, void* pData, void*& pOutData)
{
    D32S iRet = -1;
    struct json_object* pRoot = *(struct json_object**)pData;
    if (NULL == pRoot)
    {
        pRoot = createJsonObject(pTypeDef);
        if (NULL != pRoot)
        {
            *(struct json_object**)pData = pRoot;
            pOutData = pRoot;
            iRet = 0;
        }
    }
    else
    {
        pOutData = pData;
        iRet = 0;
    }

    return iRet;
}

D32S JsonDataEncoder::JsonDataEncodeHelper::setAsInt(const DataTypeDef* pTypeDef, void* pData, D64S iValue)
{
    return 1 == json_object_set_int64((struct json_object*)pData, iValue) ? 0 : -1;
}

D32S JsonDataEncoder::JsonDataEncodeHelper::setAsUInt(const DataTypeDef* pTypeDef, void* pData, D64U uiValue)
{
    return 1 == json_object_set_uint64((struct json_object*)pData, uiValue) ? 0 : -1;
}


D32S JsonDataEncoder::JsonDataEncodeHelper::setAsReal(const DataTypeDef* pTypeDef, void* pData, DOUBLE dValue)
{
    return 1 == json_object_set_double((struct json_object*)pData, dValue) ? 0 : -1;
}

D32S JsonDataEncoder::JsonDataEncodeHelper::setAsString(const DataTypeDef* pTypeDef, void* pData, const DCHAR* pValue)
{
    return 1 == json_object_set_string((struct json_object*)pData, pValue) ? 0 : -1;
}

D32S JsonDataEncoder::JsonDataEncodeHelper::setNull(const DataTypeDef* pTypeDef, void* pData)
{
    D32S iRet = -1;

    do
    {
        struct json_object* pMember = json_object_new_null();
        if (NULL == pMember)
        {
            iRet = 0;
            break;
        }

        if (0 != json_object_object_add((struct json_object*)pData, pTypeDef->pName, pMember))
        {
            break;
        }

        iRet = 0;
    } while (FALSE);

    return iRet;
}

D32S JsonDataEncoder::JsonDataEncodeHelper::setArrayCount(const DataTypeDef* pTypeDef, void* pData, D32U uiCount)
{
    return 0;
}

D32S JsonDataEncoder::JsonDataEncodeHelper::getArrayMember(const DataTypeDef* pTypeDef, D32U uiIndex, void* pData, void*& pOutData)
{
    D32S iRet = -1;

    do 
    {
        struct json_object* pMember = createJsonObject(pTypeDef);
        if (NULL == pMember)
        {
            break;
        }

        if (0 != json_object_array_put_idx((struct json_object*)pData, uiIndex, pMember))
        {
            break;
        }

        pOutData = pMember;
        iRet = 0;
    } while (FALSE);

    return iRet;
}

D32S JsonDataEncoder::JsonDataEncodeHelper::getObjectMember(const DataTypeDef* pTypeDef, void* pData, void*& pOutData)
{
    D32S iRet = -1;

    do
    {
        struct json_object* pMember = createJsonObject(pTypeDef);
        if (NULL == pMember)
        {
            break;
        }

        if (0 != json_object_object_add((struct json_object*)pData, pTypeDef->pName, pMember))
        {
            break;
        }

        pOutData = pMember;
        iRet = 0;
    } while (FALSE);

    return iRet;
}

struct json_object* JsonDataEncoder::JsonDataEncodeHelper::createJsonObject(const DataTypeDef* pTypeDef)
{
    struct json_object* pObject = NULL;
    switch (pTypeDef->uiType)
    {
    case DM_DATA_INT:
        pObject = json_object_new_int64(0);
        break;
    case DM_DATA_UINT:
        pObject = json_object_new_uint64(0);
        break;
    case DM_DATA_REAL:
        pObject = json_object_new_double(0);
        break;
    case DM_DATA_STRING:
        pObject = json_object_new_string("");
        break;
    case DM_DATA_ARRAY:
        pObject = json_object_new_array();
        break;
    case DM_DATA_OBJECT:
        pObject = json_object_new_object();
        break;
    default:
        break;
    }

    return pObject;
}



JsonDataEncoder::JsonDataEncoder(const DataTypeDef* pType): _decoder(&_helper)
{
    _pType = pType;
    _pRoot = NULL;
}

JsonDataEncoder::~JsonDataEncoder()
{
    if (NULL != _pRoot)
    {
        json_object_put(_pRoot);
    }
}

const DCHAR* JsonDataEncoder::encode(void* pData, D32U uiDataSize)
{
    const DCHAR* pStr = NULL;
    if (0 == _decoder.decode(_pType, &_pRoot, pData, uiDataSize))
    {
        pStr = json_object_to_json_string(_pRoot);
    }
    return pStr;
}




D32S JsonDataDecoder::JsonDataDecodeHelper::init(const DataTypeDef* pTypeDef, void* pData, void*& pOutData)
{
    pOutData = pData;
    return 0;
}

D32S JsonDataDecoder::JsonDataDecodeHelper::asInt(const DataTypeDef* pTypeDef, void* pData, D64S& iValue)
{
    iValue = json_object_get_int64((struct json_object*)pData);
    return 0;
}

D32S JsonDataDecoder::JsonDataDecodeHelper::asUInt(const DataTypeDef* pTypeDef, void* pData, D64U& uiValue)
{
    uiValue = json_object_get_uint64((struct json_object*)pData);
    return 0;
}

D32S JsonDataDecoder::JsonDataDecodeHelper::asReal(const DataTypeDef* pTypeDef, void* pData, DOUBLE& dValue)
{
    dValue = json_object_get_double((struct json_object*)pData);
    return 0;
}

D32S JsonDataDecoder::JsonDataDecodeHelper::asString(const DataTypeDef* pTypeDef, void* pData, const DCHAR*& pValue)
{
    pValue = json_object_get_string((struct json_object*)pData);
    return 0;
}

D32S JsonDataDecoder::JsonDataDecodeHelper::getArrayCount(const DataTypeDef* pTypeDef, void* pData, D32U& uiCount)
{
    uiCount = (D32U)json_object_array_length((struct json_object*)pData);
    return 0;
}

D32S JsonDataDecoder::JsonDataDecodeHelper::getArray(const DataTypeDef* pTypeDef, void* pData, void*& pOutData)
{
    pOutData = pData;
    return 0;
}

D32S JsonDataDecoder::JsonDataDecodeHelper::getArrayMember(const DataTypeDef* pTypeDef, D32U uiIndex, void* pData, void*& pOutData)
{
    D32S iRet = -1;
    struct json_object* pMember = json_object_array_get_idx((struct json_object*)pData, uiIndex);
    if (json_type_null == json_object_get_type(pMember))
    {
        pMember = NULL;
    }

    if (TRUE == checkJsonObject(pMember, pTypeDef))
    {
        pOutData = pMember;
        iRet = 0;
    }
    
    return iRet;
}

D32S JsonDataDecoder::JsonDataDecodeHelper::getObjectMember(const DataTypeDef* pTypeDef, void* pData, void*& pOutData)
{
    D32S iRet = -1;
    do 
    {
        struct json_object* pMember = json_object_object_get((struct json_object*)pData, pTypeDef->pName);
        if (json_type_null == json_object_get_type(pMember))
        {
            pMember = NULL;
        }

        if (TRUE == checkJsonObject(pMember, pTypeDef))
        {

            pOutData = pMember;
            iRet = 0;
        }
    } while (FALSE);

    return iRet;
}

DBOOL JsonDataDecoder::JsonDataDecodeHelper::checkJsonObject(struct json_object* pObject, const DataTypeDef* pTypeDef)
{
    DBOOL bRes = FALSE;

    do 
    {
        if (NULL == pObject)
        {
            if (0 != pTypeDef->uiOptional)
            {
                bRes = TRUE;
            }
            break;
        }

        switch (json_object_get_type(pObject))
        {
        case json_type_int:
            bRes = (DM_DATA_INT == pTypeDef->uiType || DM_DATA_UINT == pTypeDef->uiType) ? TRUE : FALSE;
            break;
        case json_type_double:
            bRes = DM_DATA_REAL == pTypeDef->uiType ? TRUE : FALSE;
            break;
        case json_type_string:
            bRes = DM_DATA_STRING == pTypeDef->uiType ? TRUE : FALSE;
            break;
        case json_type_array:
            bRes = DM_DATA_ARRAY == pTypeDef->uiType ? TRUE : FALSE;
            break;
        case json_type_object:
            bRes = DM_DATA_OBJECT == pTypeDef->uiType ? TRUE : FALSE;
            break;
        default:
            break;
        }
    } while (FALSE);
    
    return bRes;
}



JsonDataDecoder::JsonDataDecoder(const DataTypeDef* pType) : _encoder(&_helper)
{
    _pType = pType;
    _pData = NULL;
}

JsonDataDecoder::~JsonDataDecoder()
{
    if (NULL != _pData)
    {
        DataStructDestructor destructor(_pType);
        destructor.destroy(_pData, _pType->uiMemorySize);
        adm_free(_pData);
    }
}

D32S JsonDataDecoder::decode(const DCHAR* pJsonStr, void*& pOutData, D32U& uiOutDataSize)
{
    D32S iRet = -1;
    void* pOut = NULL;

    do 
    {
        D32U uiOutMaxSize = _pType->uiMemorySize;
        pOut = adm_malloc(uiOutMaxSize);
        if (NULL == pOut)
        {
            break;
        }
        
        struct json_object* pRoot = json_tokener_parse(pJsonStr);
        if (NULL == pRoot)
        {
            adm_free(pOut);
            pOut = NULL;
            break;
        }
        
        _pData = pOut;
        iRet = _encoder.encode(_pType, pRoot, pOut, uiOutMaxSize);
        json_object_put(pRoot);
        if (0 != iRet)
        {
            break;
        }

        pOutData = pOut;
        uiOutDataSize = uiOutMaxSize;
    } while (FALSE);

    return iRet;
}


D32S JsonDataDecoder::decodeEx(const DCHAR* pJsonStr, void* pOutData, D32U uiOutDataSize)
{
    D32S iRet = -1;

    do
    {
        if (uiOutDataSize < _pType->uiMemorySize)
        {
            break;
        }

        struct json_object* pRoot = json_tokener_parse(pJsonStr);
        if (NULL == pRoot)
        {
            break;
        }

        iRet = _encoder.encode(_pType, pRoot, pOutData, uiOutDataSize);
        json_object_put(pRoot);
    } while (FALSE);

    return iRet;
}


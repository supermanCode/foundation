#pragma once

#include "adm_typedefs.h"


//结构体与业务相关 foundation需要上层的业务结构
//描述关联 notifyID

#ifdef __cplusplus
extern "C" {
#endif
/**********************test **********************/
typedef struct
{
    D32S requestId;
    D32S responseId;
    D32S functionId;
    D32S swDid;
} JDS_ECU_IDEN_INFO_T;
#define JDS_ECU_IDEN_INFO_DES \
"OBJECT\
{\
    requestId   INT, \
    responseId  INT, \
    functionId  INT, \
    swDid       INT \
}"


typedef enum
{
    JDS_VEHICLE_BUS_TYPE_CAN = 1, ///< CAN
    JDS_VEHICLE_BUS_TYPE_CANFD,   ///< CANFD
    JDS_VEHICLE_BUS_TYPE_ETH,     ///< ETH
    JDS_VEHICLE_BUS_TYPE_LIN,     ///< LIN
    JDS_VEHICLE_BUS_TYPE_INVALID  ///< INVALID
} JDS_VEHICLE_BUS_TYPE_E;
typedef enum
{
    JDS_VEHICLE_BUS_PIN_TYPE_A_CAN = 1, ///< ACAN
    JDS_VEHICLE_BUS_PIN_TYPE_B_CAN = 2, ///< BCAN
    JDS_VEHICLE_BUS_PIN_TYPE_P_CAN = 3, ///< PCAN
    JDS_VEHICLE_BUS_PIN_TYPE_INVALID    ///< Invalid
} JDS_VEHICLE_BUS_PIN_TYPE_E;

typedef struct
{
    D32U dataType;
    D32U dataLength;
    DCHAR * pValue;
    #if 0
    union
    {
        /**!< Union of DID corresponding values */
        D8U value[1];
        D32U valueU32;
        D32S valueS32;
        D64U valueU64;
        D64S valueS64;
        float valueF;
        D8U *valueS;
    } v;
    #endif
} JDS_VARIABLE_DATA_T;
#define JDS_VARIABLE_DATA_DES \
    "OBJECT\
    {\
        dataType     INT,\
        dataLength   INT,\
        value        STRING OPTIONAL \
    }"

typedef struct
{
    D32S did; 
    D32S startByte; 
    D32S endByte;  
    D32U errorCode;  
    JDS_VARIABLE_DATA_T didData;
} JDS_DID_DATA_T;
#define JDS_DID_DATA_DES \
"OBJECT\
{\
    did          INT, \
    startByte    INT, \
    endByte      INT, \
    errorCode    INT, \
    didData  " JDS_VARIABLE_DATA_DES " \
}"

typedef struct
{
    D32U didArrayLength;
    JDS_DID_DATA_T *didArray;
} JDS_DID_DATA_ARRAY_T;
#define JDS_DID_DATA_ARRAY_DES \
"ARRAY OF " JDS_DID_DATA_DES


/** 结构体对应描述 */
typedef struct
{
    JDS_ECU_IDEN_INFO_T pstEcuIdenInfo; 
    DCHAR *diagScriptPath;
    JDS_VEHICLE_BUS_TYPE_E eBusType;
    JDS_VEHICLE_BUS_PIN_TYPE_E eChannelType;
    DBOOL isCGWRouted;
    JDS_DID_DATA_ARRAY_T didListRequest;
} JDS_READDIAG_REQ_T;

#define JDS_READDIAG_REQ_DES  \
"OBJECT\
{\
    pstEcuIdenInfo    " JDS_ECU_IDEN_INFO_DES ",\
    diagScriptPath    STRING,\
    eBusType          INT,\
    eChannelType      INT, \
    isCGWRouted       INT,\
    didListRequest    " JDS_DID_DATA_ARRAY_DES " \
}"

//响应
typedef struct
{
    JDS_ECU_IDEN_INFO_T stEcuIdenInfo; 
    D32U errroCode;
}JDS_READDIAG_RESP_T;
#define  JDS_READDIAG_RESP_DES  \
    "OBJECT\
    { \
        stEcuIdenInfo      " JDS_ECU_IDEN_INFO_DES ", \
        errroCode          UINT  \
    }"


#ifdef __cplusplus  // close out "C" linkage in case of c++ compiling
}
#endif


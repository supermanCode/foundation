
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <ctype.h>


#include "adm_string.h"
#include "adm_memory.h"
#include "adm_debug.h"

#include "jds_sampleDescrip.h"
#include "DataTypeDescription.h"
#include "JsonDataSerialization.h"

static int s_break = 0;

static void wait_sleep(int secs)
{
    //sleep(secs);
    struct timeval tv;
    tv.tv_sec  = secs;
    tv.tv_usec = 0;

    (void)select(0, NULL, NULL, NULL, &tv);
}


#define ADM_NM_DIM_NOTIFY_1 "dim_norify_1"
#define ADM_NM_DIM_REQUSET_2 "dim_request_2"
#define ADM_NM_DIM_SESSION_3 "dim_session_2"


//序列化
static D32U test_Serialize(const void *pData, D32U dataSize)
{
    log_i("dataSize = %u", dataSize);
    log_i("ADM_VDI_READDIAG_T dataSize = %u", sizeof(JDS_READDIAG_REQ_T));

    JDS_READDIAG_REQ_T *pReadDiag = (JDS_READDIAG_REQ_T *)pData;
    log_i("pReadDiag=%p", pReadDiag);

    log_i("pstEcuIdenInfo.requestId = %#x", pReadDiag->pstEcuIdenInfo.requestId);
    log_i("pstEcuIdenInfo.responseId = %#x", pReadDiag->pstEcuIdenInfo.responseId);
    log_i("pstEcuIdenInfo.functionId = %#x", pReadDiag->pstEcuIdenInfo.functionId);
    log_i("pstEcuIdenInfo.swDid = %#x", pReadDiag->pstEcuIdenInfo.swDid);
    log_i("diagScriptPath = %s", pReadDiag->diagScriptPath);
    log_i("eBusType = %d", pReadDiag->eBusType);
    log_i("eChannelType = %d", pReadDiag->eChannelType);

    log_i("didArray=%p", pReadDiag->didListRequest.didArray);

    log_i("didArray[0].did = %d", pReadDiag->didListRequest.didArray[0].did);
    log_i("didArray[0].startByte = %d", pReadDiag->didListRequest.didArray[0].startByte);
    log_i("didArray[0].endByte = %d", pReadDiag->didListRequest.didArray[0].endByte);
    log_i("didArray[0].errorCode = %d", pReadDiag->didListRequest.didArray[0].errorCode);
    log_i("didArray[0].didData.dataType = %u", pReadDiag->didListRequest.didArray[0].didData.dataType);
    log_i("didArray[0].didData.dataLength = %u", pReadDiag->didListRequest.didArray[0].didData.dataLength);
    log_i("didArray[0].didData.pValue = %s", pReadDiag->didListRequest.didArray[0].didData.pValue);
    
    log_i("didArray[1].did = %d", pReadDiag->didListRequest.didArray[1].did);
    log_i("didArray[1].startByte = %d", pReadDiag->didListRequest.didArray[1].startByte);
    log_i("didArray[1].endByte = %d", pReadDiag->didListRequest.didArray[1].endByte);
    log_i("didArray[1].errorCode = %d", pReadDiag->didListRequest.didArray[1].errorCode);
    log_i("didArray[1].didData.dataType = %u", pReadDiag->didListRequest.didArray[1].didData.dataType);
    log_i("didArray[1].didData.dataLength = %u", pReadDiag->didListRequest.didArray[1].didData.dataLength);
    log_i("didArray[1].didData.pValue = %s", pReadDiag->didListRequest.didArray[1].didData.pValue);


    return 0;
}


//发送端直接通知adm_nm_notify
static D32U test_jds_send()
{
    D32U retCode = 1;

    JDS_READDIAG_REQ_T stReadDiag = {0};

    stReadDiag.pstEcuIdenInfo.requestId = 0x1;
    stReadDiag.pstEcuIdenInfo.responseId = 0x2;
    stReadDiag.pstEcuIdenInfo.functionId = 0x3;
    stReadDiag.pstEcuIdenInfo.swDid = 0x4;
    stReadDiag.diagScriptPath = (DCHAR *)"/data/test/nmm/tmp.txt";
    stReadDiag.eBusType = JDS_VEHICLE_BUS_TYPE_CAN;
    stReadDiag.eChannelType = JDS_VEHICLE_BUS_PIN_TYPE_B_CAN;
    stReadDiag.isCGWRouted = 1;
    stReadDiag.didListRequest.didArrayLength = 2;
    stReadDiag.didListRequest.didArray = (JDS_DID_DATA_T *)adm_malloc(
            sizeof(JDS_DID_DATA_T) * stReadDiag.didListRequest.didArrayLength);
    log_i("didArray=%p", stReadDiag.didListRequest.didArray);
    log_i("didArray.size=%u", sizeof(JDS_DID_DATA_T) * stReadDiag.didListRequest.didArrayLength);

    stReadDiag.didListRequest.didArray[0].did = 0x11;
    stReadDiag.didListRequest.didArray[0].startByte = 0x12;
    stReadDiag.didListRequest.didArray[0].endByte = 0x13;
    stReadDiag.didListRequest.didArray[0].errorCode = 0x14;
    stReadDiag.didListRequest.didArray[0].didData.dataType = 2;
    stReadDiag.didListRequest.didArray[0].didData.dataLength = 4;
    stReadDiag.didListRequest.didArray[0].didData.pValue = NULL;
    stReadDiag.didListRequest.didArray[1].did = 0x21;
    stReadDiag.didListRequest.didArray[1].startByte = 0x22;
    stReadDiag.didListRequest.didArray[1].endByte = 0x23;
    stReadDiag.didListRequest.didArray[1].errorCode = 0x24;
    stReadDiag.didListRequest.didArray[1].didData.dataType = 2;
    stReadDiag.didListRequest.didArray[1].didData.dataLength = 4;
    stReadDiag.didListRequest.didArray[1].didData.pValue = NULL;

    //序列化
    //需要增加接口
    vus::DataTypeDef *pType = DNULL;
    vus::DataTypeDescriptionParser parser(JDS_READDIAG_REQ_DES);
    parser.init();
    pType = new vus::DataTypeDef;
    retCode = parser.parse(pType);

    if (0 != retCode)
    {
        log_e("DataTypeDef failed");
        delete pType;
        pType = DNULL;
    }

    //strJson随对象toJsonObj销毁而释放
    JsonDataEncoder toJsonObj(pType);
    const DCHAR *strJson = toJsonObj.encode((void *)&stReadDiag, sizeof(JDS_READDIAG_REQ_T));
    log_i("strJson=%s", strJson);


    //反序列化
    if (DNULL != strJson)
    {
        void *pOutData = NULL;
        D32U uiOutDataSize = 0;
        //pOutData随对象toStruct销毁而释放
        JsonDataDecoder toStruct(pType);
        toStruct.decode(strJson, pOutData, uiOutDataSize);

        test_Serialize(pOutData, uiOutDataSize);
    }

    adm_free(stReadDiag.didListRequest.didArray);

    return 0;
}



int main(int argc, char *argv[])
{

    adm_debug_logInit("/renzhendi/log/", 1045504, 5);

    /*************** test code below *************/
    test_jds_send();    ///<
    /*************** test code above *************/


    /** 延时注销 */
    wait_sleep(3);


    //sleep and break
    for (int i = 0; !s_break; i++)
    {
        log_i("wait...........");
        wait_sleep(5);
    }


    return 0;
}


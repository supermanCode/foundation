#include "adm_jds.h"
#include "JsonDataSerialization.h"


ADM_JDS_DECODER_T* adm_jds_decoderCreate(const ADM_DM_DATA_TYPE_DEF_T* pType)
{
    return new JsonDataDecoder((const DataTypeDef*)pType);
}

void adm_jds_decoderDestroy(ADM_JDS_DECODER_T* pDecoder)
{
    delete (JsonDataDecoder*)pDecoder;
}

D32S adm_jds_decoderDecode(ADM_JDS_DECODER_T* pDecoder, const DCHAR* pJsonStr, void** pOutData, D32U* puiOutDataSize)
{
    return ((JsonDataDecoder*)pDecoder)->decode(pJsonStr, *pOutData, *puiOutDataSize);
}

ADM_JDS_ENCODER_T* adm_jds_encoderCreate(const ADM_DM_DATA_TYPE_DEF_T* pType)
{
    return new JsonDataEncoder((const DataTypeDef*)pType);
}

void adm_jds_encoderDestroy(ADM_JDS_ENCODER_T* pEncoder)
{
    delete (JsonDataEncoder*)pEncoder;
}

const DCHAR* adm_jds_encoderEncode(ADM_JDS_ENCODER_T* pEncoder, void* pInData, D32U uiUDataSize)
{
    return ((JsonDataEncoder*)pEncoder)->encode(pInData, uiUDataSize);
}

D32S adm_jds_decodeJsonStr(const ADM_DM_DATA_TYPE_DEF_T* pType, const DCHAR* pJsonStr, void* pOutData, D32U puiOutDataSize)
{
    JsonDataDecoder decoder((const DataTypeDef*)pType);
    return decoder.decodeEx(pJsonStr, pOutData, puiOutDataSize);
}

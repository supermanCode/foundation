﻿#include "DataTypeDescription.h"
#include "DataStruct.h"
#include <list>
#include "adm_memory.h"
#include "adm_string.h"


namespace vus
{
    D32S DataTypeDescriptionParser::init()
    {
        _typeMap.insert(TypeMapPair("INT", &DataTypeDescriptionParser::parseInt));
        _typeMap.insert(TypeMapPair("UINT", &DataTypeDescriptionParser::parseUInt));
        _typeMap.insert(TypeMapPair("REAL", &DataTypeDescriptionParser::parseReal));
        _typeMap.insert(TypeMapPair("STRING", &DataTypeDescriptionParser::parseString));
        _typeMap.insert(TypeMapPair("ARRAY", &DataTypeDescriptionParser::parseArray));
        _typeMap.insert(TypeMapPair("OBJECT", &DataTypeDescriptionParser::parseObject));

        return 0;
    }

    DataTypeDescriptionParser::DataTypeDescriptionParser(const DCHAR* pDescriptionStr) : _util(pDescriptionStr)
    {

    }

    D32S DataTypeDescriptionParser::parse(DataTypeDef* pType)
    {
        D32S iRet = -1;

        _util.skipSpace();
        pType->pName = NULL;
        iRet = parseType(pType);
        if (0 == iRet)
        {
            DataStructTypeInitializer initializer;
            iRet = initializer.init(pType);
        }
        if (0 != iRet)
        {
            destroyType(pType);
        }

        return iRet;
    }

    D32S DataTypeDescriptionParser::parseSize(D32U& uiSize)
    {
        D32S iRet = -1;
        D32S iSize = 0;

        do
        {
            _util.skipSpace();
            if ('S' != _util.getChr())
            {
                iRet = 0;
                break;
            }

            if (FALSE == _util.compareString("SIZE"))
            {
                break;
            }

            if ('(' != _util.popChr())
            {
                break;
            }

            if (0 == _util.popInteger(iSize))
            {
                break;
            }

            if (')' != _util.popChr())
            {
                break;
            }

            uiSize = (D32U)iSize;
            iRet = 0;
        } while (FALSE);

        return iRet;
    }

    D32S DataTypeDescriptionParser::parseInt(DataTypeDef* pType)
    {
        D32S iRet = 0;
        D32U uiSize = 4;

        do
        {
            iRet = parseSize(uiSize);
            if (0 != iRet)
            {
                break;
            }

            if (1 != uiSize && 2 != uiSize && 4 != uiSize && 8 != uiSize)
            {
                break;
            }

            pType->uiType = DM_DATA_INT;
            pType->uiSize = uiSize;
            pType->pSubType = NULL;
        } while (FALSE);

        return iRet;
    }

    D32S DataTypeDescriptionParser::parseUInt(DataTypeDef* pType)
    {
        D32S iRet = 0;
        D32U uiSize = 4;

        do
        {
            iRet = parseSize(uiSize);
            if (0 != iRet)
            {
                break;
            }

            if (1 != uiSize && 2 != uiSize && 4 != uiSize && 8 != uiSize)
            {
                break;
            }

            pType->uiType = DM_DATA_UINT;
            pType->uiSize = uiSize;
            pType->pSubType = NULL;
        } while (FALSE);

        return iRet;
    }

    D32S DataTypeDescriptionParser::parseReal(DataTypeDef* pType)
    {
        D32S iRet = 0;
        D32U uiSize = 8;

        do
        {
            iRet = parseSize(uiSize);
            if (0 != iRet)
            {
                break;
            }

            if (4 != uiSize && 8 != uiSize)
            {
                break;
            }

            pType->uiType = DM_DATA_REAL;
            pType->uiSize = uiSize;
            pType->pSubType = NULL;
        } while (FALSE);

        return iRet;
    }

    D32S DataTypeDescriptionParser::parseString(DataTypeDef* pType)
    {
        D32S iRet = 0;
        D32U uiSize = 0;

        do
        {
            iRet = parseSize(uiSize);
            if (0 != iRet)
            {
                break;
            }

            pType->uiType = DM_DATA_STRING;
            pType->uiSize = uiSize;
            pType->pSubType = NULL;
        } while (FALSE);

        return iRet;
    }

    D32S DataTypeDescriptionParser::parseArray(DataTypeDef* pType)
    {
        D32S iRet = 0;
        D32U uiSize = 0;

        do
        {
            iRet = parseSize(uiSize);
            if (0 != iRet)
            {
                break;
            }

            _util.skipSpace();

            if (FALSE == _util.compareString("OF"))
            {
                break;
            }

            DataTypeDef* pSubType = (DataTypeDef*)adm_malloc(sizeof(DataTypeDef));
            if (NULL == pSubType)
            {
                break;
            }
            pSubType->pName = NULL;
            pSubType->pSubType = NULL;

            iRet = parseType(pSubType);
            if (0 != iRet)
            {
                adm_free(pSubType);
                break;
            }

            pType->uiType = DM_DATA_ARRAY;
            pType->uiSize = uiSize;
            pType->pSubType = pSubType;
        } while (FALSE);

        return iRet;
    }


    D32S DataTypeDescriptionParser::parseObject(DataTypeDef* pType)
    {
        D32S iRet = -1;

        do
        {
            _util.skipSpace();
            if ('{' != _util.popChr())
            {
                break;
            }

            _util.skipSpace();
            D32U uiCount = 0;
            std::list<DataTypeDef*> typeList;
            DCHAR chr = 0;
            DataTypeDef* pSubType = NULL;

            do
            {
                DataTypeDef* pSubType = (DataTypeDef*)adm_malloc(sizeof(DataTypeDef));
                if (NULL == pSubType)
                {
                    break;
                }
                pSubType->pName = NULL;
                pSubType->pSubType = NULL;
                pType->uiSize = 0;

                iRet = parseMember(pSubType);
                if (0 != iRet)
                {
                    adm_free(pSubType);
                    break;
                }

                typeList.push_back(pSubType);
                uiCount++;

                _util.skipSpace();

                chr = _util.popChr();
                if ('}' == chr)
                {
                    break;
                }

                iRet = -1;
                _util.skipWhiteSpace();
            } while (',' == chr);

            if (0 != iRet)
            {
                break;
            }

            DataTypeDef* pSubTypes = (DataTypeDef*)adm_malloc(sizeof(DataTypeDef) * uiCount);
            if (NULL == pSubTypes)
            {
                break;
            }

            for (D32U i = 0; i < uiCount; i++)
            {
                pSubType = typeList.front();
                typeList.pop_front();

                pSubTypes[i].pName = pSubType->pName;
                pSubTypes[i].uiType = pSubType->uiType;
                pSubTypes[i].uiSize = pSubType->uiSize;
                pSubTypes[i].pSubType = pSubType->pSubType;
                pSubTypes[i].uiOptional = pSubType->uiOptional;
                pSubTypes[i].uiHasDefault = pSubType->uiHasDefault;
                pSubTypes[i].pDefault = pSubType->pDefault;

                adm_free(pSubType);
            }

            pType->uiType = DM_DATA_OBJECT;
            pType->pSubType = pSubTypes;
            pType->uiSize = uiCount;
            iRet = 0;
        } while (FALSE);

        return iRet;
    }

    D32S DataTypeDescriptionParser::parseType(DataTypeDef* pType)
    {
        D32S iRet = -1;
        DCHAR acStr[64];

        do
        {
            _util.skipSpace();
            if (0 == _util.popAlphanumericStr(acStr))
            {
                break;
            }

            TypeMapIter iter = _typeMap.find(acStr);
            if (iter == _typeMap.cend())
            {
                break;
            }

            iRet = (this->*iter->second)(pType);
            if (0 != iRet)
            {
                break;
            }

            iRet = parseDefault(pType);
            if (0 != iRet)
            {
                break;
            }

            iRet = parseOptional(pType);
            if (0 != iRet)
            {
                break;
            }

            _util.skipSpace();
            _util.getChr();
        } while (FALSE);

        return iRet;
    }

    D32S DataTypeDescriptionParser::parseMember(DataTypeDef* pType)
    {
        D32S iRet = -1;
        DCHAR acStr[64];

        do
        {
            if (0 == _util.popAlphanumericStr(acStr))
            {
                break;
            }

            pType->pName = adm_strdup(acStr);
            if (NULL == pType->pName)
            {
                break;
            }

            iRet = parseType(pType);
//             if (0 == iRet)
//             {
//                 parseOptional(pType);
//             }
        } while (FALSE);

        return iRet;
    }

    D32S DataTypeDescriptionParser::parseOptional(DataTypeDef* pType)
    {
        D32S iRet = -1;
        D32U uiOptional = 0;

        do 
        {
            _util.skipSpace();
            if (NULL == pType->pName || 'O' != _util.getChr())
            {
                iRet = 0;
                break;
            }

            if (FALSE == _util.compareString("OPTIONAL"))
            {
                break;
            }

            uiOptional = 1;
            iRet = 0;
        } while (FALSE);


        pType->uiOptional = uiOptional & 0x01;

        return iRet;
    }

    D32S DataTypeDescriptionParser::parseDefault(DataTypeDef* pType)
    {
        D32S iRet = -1;
        D32S iValue = 0;
        D32U uiHasDefalt = 0;

        do
        {
            _util.skipSpace();
            if ('D' != _util.getChr())
            {
                pType->pDefault = NULL;
                iRet = 0;
                break;
            }

            if (FALSE == _util.compareString("DEFAULT"))
            {
                break;
            }

            if ('(' != _util.popChr())
            {
                break;
            }

            if (0 == _util.popInteger(iValue))
            {
                break;
            }

            if (')' != _util.popChr())
            {
                break;
            }

            uiHasDefalt = 1;
            pType->pDefault = (void*)iValue;
            iRet = 0;
        } while (FALSE);

        pType->uiHasDefault = uiHasDefalt & 0x01;

        return iRet;
    }

    void DataTypeDescriptionParser::destroyArrayType(const DataTypeDef* pType)
    {
        void* pPtr = (void*)pType->pSubType;
        destroyType(pType->pSubType);
        adm_free(pPtr);
    }

    void DataTypeDescriptionParser::destroyObjectType(const DataTypeDef* pType)
    {
        DataTypeDef* pSubTypes = (DataTypeDef*)pType->pSubType;
        for (D32U i = 0; i < pType->uiSize; i++)
        {
            destroyType(&pSubTypes[i]);
        }

        adm_free(pSubTypes);
    }

    void DataTypeDescriptionParser::destroyType(const DataTypeDef* pType)
    {
        if (NULL != pType)
        {
            switch (pType->uiType)
            {
            case DM_DATA_INT:
            case DM_DATA_UINT:
            case DM_DATA_REAL:
            case DM_DATA_STRING:
                break;
            case DM_DATA_ARRAY:
                destroyArrayType(pType);
                break;
            case DM_DATA_OBJECT:
                destroyObjectType(pType);
                break;
            }

            DCHAR* pPtr = (DCHAR*)pType->pName;
            if (NULL != pType->pName)
            {
                adm_free(pPtr);
            }
        }
    }
}
﻿#include "adm_data_struct.h"
#include "DataStruct.h"
#include <new>


using namespace vus;

void adm_dataStruct_init(ADM_DATA_STRUCT_T* pStruct, const ADM_DM_DATA_TYPE_DEF_T* pTypeDef, void* pData, D32S uiDataSize)
{
    new((DataStruct*)pStruct) DataStruct((DataTypeDef*)pTypeDef, pData, uiDataSize);
}

D32S adm_dataStruct_clone(ADM_DATA_STRUCT_T* pStruct, ADM_DATA_STRUCT_T* pDestStruct)
{
    return ((DataStruct*)pStruct)->clone((DataStruct*)pDestStruct);
}

D32S adm_dataStruct_copy(ADM_DATA_STRUCT_T* pStruct, const void* pData, D32S uiDataSize)
{
    return ((DataStruct*)pStruct)->copy(pData, uiDataSize);
}

D32S adm_dataStruct_destroy(ADM_DATA_STRUCT_T* pStruct)
{
    return ((DataStruct*)pStruct)->destroy();
}

﻿#include "adm_datatype.h"
#include "DataTypeDescription.h"

using namespace vus;

D32S adm_datatype_parse(ADM_DM_DATA_TYPE_DEF_T* pType, const DCHAR* pDescription)
{
    DataTypeDescriptionParser parser(pDescription);
    parser.init();
    return parser.parse((DataTypeDef*)pType);
}

D32S adm_datatype_destroy(const ADM_DM_DATA_TYPE_DEF_T* pType)
{
    DataTypeDescriptionParser::destroyType((const DataTypeDef*)pType);
    return 0;
}

﻿#include "DataStructCodec.h"
#include "adm_memory.h"
#include "adm_string.h"

namespace vus
{
    DataStructEncoder::DataStructEncoder(DataStructEncodeHelper* pHelper)
    {
        _pHelper = pHelper;
    }

    D32S DataStructEncoder::encode(const DataTypeDef* pTypeDef, void* pData, void* pOutData, D32U uiOutMaxSize)
    {
        void* pInData = NULL;

        _writer.setBuffer(pOutData, uiOutMaxSize);
        _pHelper->init(pTypeDef, pData, pInData);
        return encode(pTypeDef, pInData);
    }

    D32S DataStructEncoder::encodeInt(const DataTypeDef* pTypeDef, void* pData)
    {
        D32S iRet = -1;
        D64S iValue = 0;

        if (NULL != pData)
        {
            iRet = _pHelper->asInt(pTypeDef, pData, iValue);
        }
        else if (0 != pTypeDef->uiHasDefault)
        {
            iValue = (D64S)pTypeDef->pDefault;
            iRet = 0;
        }
        
        if (0 == iRet)
        {
            switch (pTypeDef->uiSize)
            {
            case 1:
                iRet = _writer.writeInt8((D8S)iValue);
                break;
            case 2:
                iRet = _writer.writeInt16((D16S)iValue);
                break;
            case 4:
                iRet = _writer.writeInt32((D32S)iValue);
                break;
            case 8:
                iRet = _writer.writeInt64(iValue);
                break;
            default:
                iRet = -1;
                break;
            }
        }

        return iRet;
    }

    D32S DataStructEncoder::encodeUInt(const DataTypeDef* pTypeDef, void* pData)
    {
        D32S iRet = -1;
        D64U uiValue = 0;

        if (NULL != pData)
        {
            iRet = _pHelper->asUInt(pTypeDef, pData, uiValue);
        }
        else if (0 != pTypeDef->uiHasDefault)
        {
            uiValue = (D64U)pTypeDef->pDefault;
            iRet = 0;
        }

        if (0 == iRet)
        {
            switch (pTypeDef->uiSize)
            {
            case 1:
                iRet = _writer.writeUInt8((D8U)uiValue);
                break;
            case 2:
                iRet = _writer.writeUInt16((D16U)uiValue);
                break;
            case 4:
                iRet = _writer.writeUInt32((D32U)uiValue);
                break;
            case 8:
                iRet = _writer.writeUInt64(uiValue);
                break;
            default:
                iRet = -1;
                break;
            }
        }

        return iRet;
    }


    D32S DataStructEncoder::encodeReal(const DataTypeDef* pTypeDef, void* pData)
    {
        D32S    iRet = -1;
        DOUBLE  dValue = 0;

        if (NULL != pData)
        {
            iRet = _pHelper->asReal(pTypeDef, pData, dValue);
        }
//         else if (0 != pTypeDef->uiHasDefault)
//         {
//             dValue = *(DOUBLE*)&pTypeDef->pDefault;
//             iRet = 0;
//         }

        if (0 == iRet)
        {
            switch (pTypeDef->uiSize)
            {
            case 4:
                iRet = _writer.writeFloat((DFLOAT)dValue);
                break;
            case 8:
                iRet = _writer.writeDouble(dValue);
                break;
            default:
                iRet = -1;
                break;
            }
        }

        return iRet;
    }

    D32S DataStructEncoder::encodeString(const DataTypeDef* pTypeDef, void* pData)
    {
        D32S iRet = -1;
        const DCHAR* pStr = NULL;

        if (NULL != pData)
        {
            iRet = _pHelper->asString(pTypeDef, pData, pStr);
        }
//         else if (0 != pTypeDef->uiHasDefault)
//         {
//             pStr = (const DCHAR*)pTypeDef->pDefault;
//             iRet = 0;
//         }

        if (0 == iRet)
        {
            if (0 == pTypeDef->uiSize)
            {
                iRet = _writer.writeString(pStr);
            }
            else
            {
                iRet = _writer.writeFixedString(pStr, pTypeDef->uiSize);
            }
        }

        return iRet;
    }


    D32S DataStructEncoder::encodeArrayMember(const DataTypeDef* pTypeDef, void* pData, D32U uiIndex)
    {
        D32S iRet = -1;
        void* pArrayMemberData = NULL;

        if (NULL != pData)
        {
            _writer.writeArrayMember(uiIndex, pTypeDef->uiMemorySize);
            iRet = _pHelper->getArrayMember(pTypeDef, uiIndex, pData, pArrayMemberData);
            if (0 == iRet)
            {
                iRet = encode(pTypeDef, pArrayMemberData);
            }
        }

        return iRet;
    }

    D32S DataStructEncoder::encodeArray(const DataTypeDef* pSubType, void* pData, D32U uiCount)
    {
        D32S iRet = -1;

        do 
        {
            for (D32U i = 0; i < uiCount; i++)
            {
                iRet = encodeArrayMember(pSubType, pData, i);
                if (0 != iRet)
                {
                    break;
                }
            }
        } while (FALSE);

        return iRet;
    }

    D32S DataStructEncoder::encodeArray(const DataTypeDef* pTypeDef, void* pData)
    {
        D32S iRet = -1;
        D32U uiSize = pTypeDef->uiSize;
        D32U uiCount = 0;
        const DataTypeDef* pSubType = pTypeDef->pSubType;
        void* pArrayData = NULL;

        do
        {
            if (NULL == pData)
            {
                break;
            }

            iRet = _pHelper->getArrayCount(pTypeDef, pData, uiCount);
            if (0 != iRet)
            {
                break;
            }

            _pHelper->getArray(pTypeDef, pData, pArrayData);
            if (0 == uiSize)
            {
                DataStructEncoder encoder(_pHelper);
                iRet = _writer.writeArray(&encoder._writer, uiCount, pSubType->uiMemorySize);
                if (0 == iRet && uiCount > 0)
                {
                    
                    iRet = encoder.encodeArray(pSubType, pArrayData, uiCount);
                }
            }
            else
            {
                if (uiSize != uiCount)
                {
                    iRet = -1;
                    break;
                }

                DataStructEncoder encoder(_pHelper);
                _writer.writeFixedArray(&encoder._writer, uiCount, pSubType->uiMemorySize);
                iRet = encoder.encodeArray(pSubType, pArrayData, uiCount);
            }
        } while (FALSE);

        return iRet;
    }

    D32S DataStructEncoder::encodeObject(const DataTypeDef* pTypeDef, void* pData)
    {
        D32S iRet = -1;
        D32U uiSize = pTypeDef->uiSize;
        const DataTypeDef* pSubType = NULL;
        void* pObjectData = NULL;

        do
        {
            if (NULL == pData)
            {
                break;
            }

            DataStructEncoder encoder(_pHelper);
            iRet = _writer.writeObject(&encoder._writer);
            if (0 != iRet)
            {
                break;
            }

            // _pHelper->getObject(pTypeDef, pData, pObject);
            for (D32U i = 0; i < uiSize; i++)
            {
                pObjectData = NULL;
                pSubType = &pTypeDef->pSubType[i];
                iRet = _pHelper->getObjectMember(pSubType, pData, pObjectData);
                if (0 != iRet && 0 == pSubType->uiHasDefault)
                {
                    break;
                }

                encoder._writer.anchor(pSubType->uiOffset);
                if (0 == pSubType->uiOptional)
                {
                    iRet = encoder.encode(pSubType, pObjectData);
                    if (0 != iRet)
                    {
                        break;
                    }
                }
                else
                {
                    encoder.encodeOption(pSubType, pObjectData);
                }
            }
        } while (FALSE);

        return iRet;
    }

    D32S DataStructEncoder::encodeMember(const DataTypeDef* pTypeDef, void* pData)
    {
        D32S iRet = -1;

        switch (pTypeDef->uiType)
        {
        case DM_DATA_INT:
            iRet = encodeInt(pTypeDef, pData);
            break;
        case DM_DATA_UINT:
            iRet = encodeUInt(pTypeDef, pData);
            break;
        case DM_DATA_REAL:
            iRet = encodeReal(pTypeDef, pData);
            break;
        case DM_DATA_STRING:
            iRet = encodeString(pTypeDef, pData);
            break;
        case DM_DATA_ARRAY:
            iRet = encodeArray(pTypeDef, pData);
            break;
        case DM_DATA_OBJECT:
            iRet = encodeObject(pTypeDef, pData);
            break;
        default:
            break;
        }

        return iRet;
    }


    D32S DataStructEncoder::encodeOption(const DataTypeDef* pTypeDef, void* pData)
    {
        D32S iRet = -1;
        D32U uiSize = 0;
        
        do 
        {
            if (NULL == pData)
            {
                iRet = 0;
                _writer.writePointer(NULL);
                break;
            }

            if (DM_DATA_STRING != pTypeDef->uiType)
            {
                uiSize = pTypeDef->uiMemorySize;
                void* pPtr = adm_malloc(uiSize);
                if (NULL == pPtr)
                {
                    break;
                }
                iRet = _writer.writePointer(pPtr);
                if (0 != iRet)
                {
                    break;
                }

                DataStructEncoder encoder(_pHelper);
                encoder._writer.setBuffer(pPtr, uiSize);
                iRet = encoder.encodeMember(pTypeDef, pData);
            }
            else
            {
                iRet = encodeString(pTypeDef, pData);
            }
        } while (FALSE);

        return iRet;
    }

    D32S DataStructEncoder::encode(const DataTypeDef* pTypeDef, void* pData)
    {
//         if (0 == pTypeDef->uiOptional)
//         {
//             return encodeMember(pTypeDef, pData);
//         }
//         else
//         {
//             return encodeOption(pTypeDef, pData);
//         }

        return encodeMember(pTypeDef, pData);
    }


    DataStructDecoder::DataStructDecoder(DataStructDecodeHelper* pHelper)
    {
        _pHelper = pHelper;
    }

    D32S DataStructDecoder::decode(const DataTypeDef* pTypeDef, void* pData, void* pDataBuf, D32U uiDataBufSize)
    {
        void* pInData = NULL;
        _reader.setBuffer(pDataBuf, uiDataBufSize);
        _pHelper->init(pTypeDef, pData, pInData);
        return decode(pTypeDef, pInData);
    }

    D32S DataStructDecoder::decodeInt(const DataTypeDef* pTypeDef, void* pData)
    {
        D32S iRet = -1;
        D64S iValue = 0;

        switch (pTypeDef->uiSize)
        {
        case 1:
            iRet = _reader.readInt8(*(D8S*)&iValue);
            break;
        case 2:
            iRet = _reader.readInt16(*(D16S*)&iValue);
            break;
        case 4:
            iRet = _reader.readInt32(*(D32S*)&iValue);
            break;
        case 8:
            iRet = _reader.readInt64(*(D64S*)&iValue);
            break;
        default:
            break;
        }

        if (0 == iRet)
        {
            iRet = _pHelper->setAsInt(pTypeDef, pData, iValue);
        }

        return iRet;
    }

    D32S DataStructDecoder::decodeUInt(const DataTypeDef* pTypeDef, void* pData)
    {
        D32S iRet = -1;
        D64U uiValue = 0;

        switch (pTypeDef->uiSize)
        {
        case 1:
            iRet = _reader.readUInt8(*(D8U*)&uiValue);
            break;
        case 2:
            iRet = _reader.readUInt16(*(D16U*)&uiValue);
            break;
        case 4:
            iRet = _reader.readUInt32(*(D32U*)&uiValue);
            break;
        case 8:
            iRet = _reader.readUInt64(*(D64U*)&uiValue);
            break;
        default:
            break;
        }

        if (0 == iRet)
        {
            iRet = _pHelper->setAsUInt(pTypeDef, pData, uiValue);
        }

        return iRet;
    }


    D32S DataStructDecoder::decodeReal(const DataTypeDef* pTypeDef, void* pData)
    {
        D32S    iRet = -1;
        DOUBLE  dValue = 0.0;
        DFLOAT  fValue = 0.0;

        switch (pTypeDef->uiSize)
        {
        case 4:
            iRet = _reader.readFloat(fValue);
            dValue = fValue;
            break;
        case 8:
            iRet = _reader.readDouble(dValue);
            break;
        default:
            break;
        }

        if (0 == iRet)
        {
            iRet = _pHelper->setAsReal(pTypeDef, pData, dValue);
        }

        return iRet;
    }

    D32S DataStructDecoder::decodeString(const DataTypeDef* pTypeDef, void* pData)
    {
        D32S iRet = -1;
        const DCHAR* pStr = NULL;
        D32U uiStrSize = 0;

        do 
        {
            if (0 == pTypeDef->uiSize)
            {
                pStr = _reader.readString(uiStrSize);
            }
            else
            {
                pStr = _reader.readFixedString(pTypeDef->uiSize, uiStrSize);
            }

            if (NULL == pStr)
            {
                break;
            }

            iRet = _pHelper->setAsString(pTypeDef, pData, pStr);
        } while (FALSE);

        return iRet;
    }


    D32S DataStructDecoder::decodeArrayMember(const DataTypeDef* pTypeDef, void* pData, D32U uiIndex)
    {
        D32S iRet = -1;
        void* pArrayMemberData = NULL;

        iRet = _pHelper->getArrayMember(pTypeDef, uiIndex, pData, pArrayMemberData);
        if (0 == iRet)
        {
            iRet = decodeMember(pTypeDef, pArrayMemberData);
        }

        return iRet;
    }

    D32S DataStructDecoder::decodeArray(const DataTypeDef* pSubType, void* pData, D32U uiCount)
    {
        D32S iRet = -1;

        do
        {
            for (D32U i = 0; i < uiCount; i++)
            {
                _reader.readArrayMember(i, pSubType->uiMemorySize);
                iRet = decodeArrayMember(pSubType, pData, i);
                if (0 != iRet)
                {
                    break;
                }
            }
        } while (FALSE);

        return iRet;
    }

    D32S DataStructDecoder::decodeArray(const DataTypeDef* pTypeDef, void* pData)
    {
        D32S iRet = 0;
        void* pArray = NULL;
        D32U uiCount = pTypeDef->uiSize;
        const DataTypeDef* pSubType = pTypeDef->pSubType;

        if (0 == uiCount)
        {
            

//             _reader.readArrayCount(uiCount);
//             iRet = _reader.readArray(&decoder._reader, pSubType->uiMemorySize);
            // iRet = _reader.readArray(&decoder._reader, uiCount, pSubType->uiMemorySize);
            iRet = _reader.readArray(pArray, uiCount);
            if (0 == iRet)
            {
                _pHelper->setArrayCount(pTypeDef, pData, uiCount);

                if (uiCount > 0)
                {
                    DataStructDecoder decoder(_pHelper);
                    decoder._reader.setBuffer(pArray, pSubType->uiMemorySize * uiCount);
                    iRet = decoder.decodeArray(pSubType, pData, uiCount);
                }
            }
        }
        else
        {
            _pHelper->setArrayCount(pTypeDef, pData, uiCount);

            DataStructDecoder decoder(_pHelper);
            _reader.readFixedArray(&decoder._reader, uiCount, pSubType->uiMemorySize);
            iRet = decoder.decodeArray(pSubType, pData, uiCount);
        }

        return iRet;
    }

    D32S DataStructDecoder::decodeObject(const DataTypeDef* pTypeDef, void* pData)
    {
        D32S iRet = 0;
        D32U uiSize = pTypeDef->uiSize;
        const DataTypeDef* pSubType = NULL;
        void* pObjectData = NULL;

        do
        {
            DataStructDecoder decoder(_pHelper);
            iRet = _reader.readObject(&decoder._reader);
            if (0 != iRet)
            {
                break;
            }

            for (D32U i = 0; i < uiSize; i++)
            {
                pSubType = &pTypeDef->pSubType[i];
                decoder._reader.anchor(pSubType->uiOffset);

                if (0 == pSubType->uiOptional)
                {
                    iRet = _pHelper->getObjectMember(pSubType, pData, pObjectData);
                    if (0 != iRet)
                    {
                        break;
                    }

                    iRet = decoder.decodeMember(pSubType, pObjectData);
                    if (0 != iRet)
                    {
                        break;
                    }
                }
                else
                {
                    iRet = decoder.decodeOption(pSubType, pData);
                }
            }
        } while (FALSE);

        return iRet;
    }

    D32S DataStructDecoder::decodeMember(const DataTypeDef* pTypeDef, void* pData)
    {
        D32S iRet = -1;
        switch (pTypeDef->uiType)
        {
        case DM_DATA_INT:
            iRet = decodeInt(pTypeDef, pData);
            break;
        case DM_DATA_UINT:
            iRet = decodeUInt(pTypeDef, pData);
            break;
        case DM_DATA_REAL:
            iRet = decodeReal(pTypeDef, pData);
            break;
        case DM_DATA_STRING:
            iRet = decodeString(pTypeDef, pData);
            break;
        case DM_DATA_ARRAY:
            iRet = decodeArray(pTypeDef, pData);
            break;
        case DM_DATA_OBJECT:
            iRet = decodeObject(pTypeDef, pData);
            break;
        default:
            break;
        }

        return iRet;
    }


    D32S DataStructDecoder::decodeOption(const DataTypeDef* pTypeDef, void* pData)
    {
        D32S iRet = 0;
        void* pPtr = NULL;
        void* pMemberData = NULL;

        do 
        {
            iRet = _reader.readPointer(pPtr);
            if (0 != iRet)
            {
                break;
            }

            if (NULL != pPtr)
            {
                iRet = _pHelper->getObjectMember(pTypeDef, pData, pMemberData);
                if (0 != iRet)
                {
                    break;
                }

                if (DM_DATA_STRING != pTypeDef->uiType)
                {
                    DataStructDecoder decoder(_pHelper);
                    decoder._reader.setBuffer(pPtr, pTypeDef->uiMemorySize);

                    iRet = decoder.decodeMember(pTypeDef, pMemberData);
                }
                else
                {
                    iRet = decodeString(pTypeDef, pMemberData);
                }
            }
            else
            {
                iRet = _pHelper->setNull(pTypeDef, pData);
            }
        } while (FALSE);
        
            
        return iRet;
    }

    D32S DataStructDecoder::decode(const DataTypeDef* pTypeDef, void* pData)
    {
//         if (0 == pTypeDef->uiOptional)
//         {
//             return decodeMember(pTypeDef, pData);
//         }
//         else
//         {
//             return decodeOption(pTypeDef, pData);
//         }
        return decodeMember(pTypeDef, pData);
    }
}

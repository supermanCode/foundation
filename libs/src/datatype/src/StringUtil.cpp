﻿#include "StringUtil.h"
#include <stdlib.h>



StringUtil& StringUtil::operator=(StringUtil& util)
{
    _pPos = util._pPos;
    return *this;
}

DBOOL StringUtil::isSpace(DCHAR chr)
{
    DBOOL bRet = TRUE;

    switch (chr)
    {
    case ' ':
    case '\t':
        break;
    default:
        bRet = FALSE;
        break;
    }

    return bRet;
}

void StringUtil::skipSpace()
{
    while (isSpace(*_pPos))
    {
        _pPos++;
    }
}

DBOOL StringUtil::isWhiteSpace(DCHAR chr)
{
    DBOOL bRet = TRUE;

    switch (chr)
    {
    case ' ':
    case '\t':
    case '\r':
    case '\n':
        break;
    default:
        bRet = FALSE;
        break;
    }

    return bRet;
}

void StringUtil::skipWhiteSpace()
{
    while (isWhiteSpace(*_pPos))
    {
        _pPos++;
    }
}

D32U StringUtil::popAlphanumericStr(DCHAR* pStr)
{
    DCHAR chr = 0;
    D32U uiOffset = 0;

    do
    {
        chr = *_pPos;
        if (('0' <= chr && chr <= '9') || ('A' <= chr && chr <= 'Z') || ('a' <= chr && chr <= 'z'))
        {
            pStr[uiOffset++] = chr;
            _pPos++;
        }
        else
        {
            pStr[uiOffset++] = '\0';
            break;
        }
    } while (TRUE);

    return uiOffset > 1 ? uiOffset : 0;
}


// D32U StringUtil::popString(std::string& str)
// {
// 
// }

D32U StringUtil::popEscapeString(std::string& str)
{
    DCHAR chr = 0;
    D32U uiOffset = 0;
    const DCHAR* pStart = NULL;

    if ('"' == getChr())
    {
        skipChr();
        pStart = _pPos;
        
        while(TRUE)
        {
            chr = popChr();
            if ('\\' == chr)
            {
                chr = popChr();
                if ('"' == chr || '\\' == chr)
                {
                    str.append(1, chr);
                    chr = popChr();
                }
                else
                {
                    pStart = NULL;
                    break;
                }
            }
            else if ('\0' == chr)
            {
                pStart = NULL;
                break;
            }
            if ('"' == chr)
            {
                break;
            }

            str.append(1, chr);
        }
    }

    if (NULL != pStart)
    {
        uiOffset = (D32U)(_pPos - pStart);
    }

    return uiOffset;
}


D32U StringUtil::popNumberString(std::string& str, DBOOL& bIsDecimal, DBOOL& bIsNegative)
{
    D32U uiOffset = 0;
    const DCHAR* pPointPos = NULL;
    const DCHAR* pStart = _pPos;
    DCHAR chr = getChr();


    if ('0' <= chr && chr <= '9')
    {
        skipChr();
    }
    else if ('-' == chr)
    {
        bIsNegative = TRUE;
        skipChr();
    }
    else
    {
        return 0;
    }

    chr = getChr();
    while ('\0' != chr)
    {
        if ('0' <= chr && chr <= '9')
        {
            (void)NULL;
        }
        else if ('.' == chr)
        {
            if (NULL == pPointPos)
            {
                pPointPos = _pPos;
                bIsDecimal = TRUE;
            }
            else
            {
                pStart = NULL;
                break;
            }
        }
        else
        {
            break;
        }

        skipChr();
        chr = getChr();
    }

    if (pPointPos + 1 == _pPos)
    {
        pStart = NULL;
    }

    if (NULL != pStart)
    {
        str.append(pStart, _pPos);
        uiOffset = (D32U)(_pPos - pStart);
    }

    return uiOffset;
}

D32U StringUtil::popInteger(D32S& iValue)
{
    DCHAR chr = 0;
    DCHAR acStr[64];
    D32U uiOffset = 0;

    do
    {
        chr = *_pPos;
        if ('0' <= chr && chr <= '9')
        {
            acStr[uiOffset++] = chr;
            _pPos++;
        }
        else
        {
            acStr[uiOffset] = '\0';
            break;
        }
    } while (uiOffset < sizeof(acStr));

    iValue = atoi(acStr);
    return uiOffset;
}

DBOOL StringUtil::compareString(const DCHAR* pStr)
{
    DBOOL bRet = FALSE;
    while ('\0' != *_pPos)
    {
        if (*_pPos != *pStr)
        {
            break;
        }

        _pPos++;
        pStr++;
        if ('\0' == *pStr)
        {
            bRet = TRUE;
            break;
        }
    }

    return bRet;
}


D32S StringUtil::addEscape(std::string& str)
{
    DCHAR chr = *_pPos;

    while ('\0' != chr)
    {
        if ('"' == chr || '\\' == chr)
        {
            str.append(1, '\\');
        }
        str.append(1, chr);
        _pPos++;
        chr = *_pPos;
    }

    return 0;
}

D32U StringUtilEx::popUriString(DCHAR* pStr)
{
    D32U uiOffset = 0;
    D32U uiSize = 0;

    do
    {
        DCHAR chr = *_pPos;
        if ('/' == chr)
        {
            pStr[uiOffset++] = '/';
            _pPos++;

            chr = *_pPos;
        }

        if ('$' == chr)
        {
            pStr[uiOffset++] = '$';
            _pPos++;
        }

        uiSize = popAlphanumericStr(&pStr[uiOffset]);
        uiOffset += uiSize - 1;
    } while (uiSize > 1);

    return uiOffset;
}

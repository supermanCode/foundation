﻿#include "DataStruct.h"
#include "adm_memory.h"
#include "adm_string.h"

#define DATA_MEMORY_ALIGN(ptr, type)                ((D32U)sizeof(type) - ((D32U)ptr & ((D32U)sizeof(type) - 1U)) % (D32U)sizeof(type))
#define DATA_STRUCT_OFFSET_ALIGN(offset, type)      offset += ((D32U)sizeof(type) - ((D32U)offset & ((D32U)sizeof(type) - 1U)) % (D32U)sizeof(type))

#define DATA_STRUCT_MEMBER_ASSIGN(ptr, type, value) *((type*)ptr) = value

#define DATA_STRUCT_MEMBER_SKIP(offset, type)   DATA_STRUCT_OFFSET_ALIGN(offset, type) + sizeof(type)
#define DATA_STRUCT_MEMBER_SKIP_BY_SIZE(offset, size)   offset += (size - ((D32U)offset & (size - 1U)) % (D32U)size) + size

namespace vus
{ 

// 
// 
//     class DataNumber: public DataStructType
//     {
//     public:
//         D32S size()
//         {
//             return _uiMemorySize;
//         }
// 
//         
//     };
// 
//     class DataString: public DataStructType
//     {
// 
//     };
// 
//     class DataArray: public DataStructType
//     {
// 
//     };
// 
//     class DataStruct: public DataStructType
//     {
//         
//     };



    DataStructTypeInitializer::DataStructTypeInitializer()
    {
        _uiSize = 0;
    }

    D32S DataStructTypeInitializer::init(DataTypeDef* pType)
    {
        D32U uiAlign = 1;
        return initType(pType, uiAlign);
    }

    void DataStructTypeInitializer::initNumber(DataTypeDef* pType, D32U& uiAlign)
    {
        pType->uiMemorySize = pType->uiSize;
        uiAlign = pType->uiSize;
    }

    void DataStructTypeInitializer::initString(DataTypeDef* pType, D32U& uiAlign)
    {
        if (0 == pType->uiSize)
        {
            initPointer(pType, uiAlign);
        }
        else
        {
            pType->uiMemorySize = pType->uiSize;
            uiAlign = 1;
        }
    }

    void DataStructTypeInitializer::initPointer(DataTypeDef* pType, D32U& uiAlign)
    {
        pType->uiMemorySize = sizeof(void*);
        uiAlign = sizeof(void*);
    }

    D32S DataStructTypeInitializer::initArray(DataTypeDef* pType, D32U& uiAlign)
    {
        D32S iRet = -1;
        D32U uiMemberAlign = 0;
        D32U uiOffset = 0;

        do 
        {
            DataTypeDef* pSubType = pType->pSubType;
            if (NULL == pSubType)
            {
                break;
            }

            iRet = initType(pSubType, uiMemberAlign);
            if (0 != iRet)
            {
                break;
            }

            uiOffset = (uiMemberAlign - ((uiMemberAlign - 1) & pSubType->uiMemorySize)) % uiMemberAlign;
            pSubType->uiOffset = 0;
            pSubType->uiMemorySize += uiOffset;

            if (0 == pType->uiSize)
            {
                initPointer(pType, uiAlign);
                pType->uiMemorySize += (D32U)sizeof(void*);
            }
            else
            {
                pType->uiMemorySize = pSubType->uiMemorySize * pType->uiSize;
                uiAlign = uiMemberAlign;
            }
        } while (FALSE);

        return iRet;
    }

    D32S DataStructTypeInitializer::initObject(DataTypeDef* pType, D32U& uiAlign)
    {
        D32S iRet = 0;
        D32U uiOffset = 0;
        D32U uiSize = 0;
        D32U uiObjectAlign = 1;
        D32U uiMemberAlign = 0;
        D32U uiMemberSize = 0;
        DataTypeDef* pSubType = NULL;

        do 
        {
            for (D32U i = 0; i < pType->uiSize; i++)
            {
                pSubType = &pType->pSubType[i];
                iRet = initType(pSubType, uiMemberAlign);
                if (0 != iRet)
                {
                    break;
                }

                if (0 == pSubType->uiOptional)
                {
                    uiMemberSize = pSubType->uiMemorySize;

                }
                else
                {
                    uiMemberSize = sizeof(void*);
                    uiMemberAlign = sizeof(void*);
                }

                if (uiObjectAlign < uiMemberAlign)
                {
                    uiObjectAlign = uiMemberAlign;
                }

                uiOffset = uiSize + (uiMemberAlign - ((uiMemberAlign - 1) & uiSize)) % uiMemberAlign;
                pSubType->uiOffset = uiOffset;
                uiSize = uiOffset + uiMemberSize;
            }

            uiAlign = uiObjectAlign;
            pType->uiMemorySize = uiSize + (uiObjectAlign - ((uiObjectAlign - 1) & uiSize)) % uiObjectAlign;
        } while (FALSE);
        
        return iRet;
    }


    D32S DataStructTypeInitializer::initType(DataTypeDef* pType, D32U& uiAlign)
    {
        D32S iRet = 0;

        switch (pType->uiType)
        {
        case DM_DATA_INT:
        case DM_DATA_UINT:
        case DM_DATA_REAL:
            initNumber(pType, uiAlign);
            break;
        case DM_DATA_STRING:
            initString(pType, uiAlign);
            break;
        case DM_DATA_ARRAY:
            iRet = initArray(pType, uiAlign);
            break;
        case DM_DATA_OBJECT:
            iRet = initObject(pType, uiAlign);
            break;
        default:
            iRet = -1;
            break;
        }

        return iRet;
    }

//     D32S DataStructTypeInitializer::initType(DataTypeDef* pType, D32U& uiAlign)
//     {
//         return initMember(pType, uiAlign);
//     }



// 
//     D32S DataStructTypeBuilder::initStructType(DataStructType* pStructType, const DataTypeDef* pType)
//     {
//         D32S iRet = -1;
// 
//         do
//         {
//             if (NULL != pType->pName && '\0' != pType->pName[0])
//             {
//                 pStructType->pName = adm_strdup(pType->pName);
//                 if (NULL == pStructType->pName)
//                 {
//                     break;
//                 }
//             }
//             else
//             {
//                 pStructType->pName = NULL;
//             }
// 
//             pStructType->uiType = pType->uiType;
//             pStructType->uiSize = pType->uiSize;
//             pStructType->pSubType = NULL;
//             iRet = 0;
//         } while (FALSE);
// 
//         return iRet;
//     }
// 
//     D32S DataStructTypeBuilder::initStructNumber(DataStructType* pStructType, const DataTypeDef* pType)
//     {
//         D32S iRet = initStructType(pStructType, pType);
//         if (0 == iRet)
//         {
//             pStructType->uiMemorySize = pType->uiSize;
//         }
// 
//         return iRet;
//     }
// 
//     D32S DataStructTypeBuilder::initStructString(DataStructType* pStructType, const DataTypeDef* pType)
//     {
//         D32S iRet = initStructType(pStructType, pType);
//         if (0 == iRet)
//         {
//             if (0 == pType->uiSize)
//             {
//                 pStructType->uiMemorySize = sizeof(void*);
//             }
//             else
//             {
//                 pStructType->uiMemorySize = pType->uiSize;
//             }
//         }
// 
//         return 0;
//     }
// 
//     D32S DataStructTypeBuilder::initStructArray(DataStructType* pStructType, const DataTypeDef* pType)
//     {
//         D32S iRet = -1;
// 
//         do
//         {
//             iRet = initStructType(pStructType, pType);
//             if (0 != iRet)
//             {
//                 break;
//             }
// 
//             DataStructType* pStructSubType = new DataStructType();
//             if (NULL == pStructSubType)
//             {
//                 break;
//             }
// 
//             iRet = initStructMember(pStructSubType, pType->pSubType);
//             if (0 != iRet)
//             {
//                 break;
//             }
// 
//             pStructType->pSubType = pStructSubType;
//             if (0 == pType->uiSize)
//             {
//                 pStructType->uiMemorySize = sizeof(D32U) + sizeof(void*);
//             }
//             else
//             {
//                 DataStructSizeGauge gauge;
//                 pStructType->uiMemorySize = gauge.figureSize(pType->pSubType);
//             }
//         } while (FALSE);
// 
//         return 0;
//     }
// 
//     D32S DataStructTypeBuilder::initStructObject(DataStructType* pStructType, const DataTypeDef* pType)
//     {
//         D32S iRet = -1;
// 
//         do
//         {
//             iRet = initStructType(pStructType, pType);
//             if (0 != iRet)
//             {
//                 break;
//             }
// 
//             DataStructType* pStructSubTypes = new DataStructType[pType->uiSize];
//             if (NULL == pStructSubTypes)
//             {
//                 break;
//             }
// 
//             for (D32U i = 0; i < pType->uiSize; i++)
//             {
//                 D32S iRet = initStructMember(&pStructSubTypes[i], &pType->pSubType[i]);
//                 if (0 != iRet)
//                 {
//                     break;
//                 }
//             }
// 
//             DataStructSizeGauge gauge;
//             pStructType->pSubType = pStructSubTypes;
//             pStructType->uiMemorySize = gauge.figureSize(pType);
//         } while (FALSE);
// 
//         return iRet;
//     }
// 
//     D32S DataStructTypeBuilder::initStructMember(DataStructType* pStructType, const DataTypeDef* pType)
//     {
//         D32S iRet = -1;
// 
//         switch (pType->uiType)
//         {
//         case DM_DATA_INT:
//         case DM_DATA_UINT:
//         case DM_DATA_REAL:
//             iRet = initStructNumber(pStructType, pType);
//             break;
//         case DM_DATA_STRING:
//             iRet = initStructString(pStructType, pType);
//             break;
//         case DM_DATA_ARRAY:
//             iRet = initStructArray(pStructType, pType);
//             break;
//         case DM_DATA_OBJECT:
//             iRet = initStructObject(pStructType, pType);
//             break;
//         default:
//             break;
//         }
// 
//         return iRet;
//     }
// 
//     D32S DataStructTypeBuilder::build(DataStructType* pStructType, const DataTypeDef* pType)
//     {
//         return 0;
//     }

    void* DataStructBuilder::createStruct(D32U uiSize)
    {
        return adm_malloc(uiSize);
    }

    void DataStructBuilder::destroyStruct(void* pData)
    {
        adm_free(pData);
    }

    void* DataStructBuilder::createString(D32U uiSize)
    {
        return adm_malloc(uiSize);
    }

    void DataStructBuilder::destroyString(void* pPtr)
    {
        adm_free(pPtr);
    }

    void* DataStructBuilder::createArray(D32U uiSize)
    {
//         void* pPtr = adm_malloc(uiSize + sizeof(void*));
//         if (NULL != pPtr)
//         {
//             *(D32U*)pPtr = uiSize;
//         }
// 
//         return (D8U*)pPtr + sizeof(void*);

        void* pPtr = adm_malloc(uiSize);
        return pPtr;
    }

    D32U DataStructBuilder::getArrayTotalSize(void* pPtr)
    {
        return *(D32U*)((D8U*)pPtr - sizeof(void*));
    }

    void DataStructBuilder::destroyArray(void* pPtr)
    {
        // adm_free((D8U*)pPtr - sizeof(void*));
        adm_free(pPtr);
    }


    D32S DataStructBuilder::clone(const DataTypeDef* pType, const void* pData, D32U uiDataSize, void* pOut, D32U uiSize)
    {
        //DataStruct dataStruct(pData, uiDataSize);
        //return dataStruct.clone(pType, pOut, uiSize);
        return 0;
    }

    DataStructWriter::DataStructWriter()
    {
        _pBuf = NULL;
        _uiSize = 0;
        _uiOffset = 0;
    }

    DataStructWriter::DataStructWriter(const void* pBuf, D32U uiBufSize)
    {
        _pBuf = (D8U*)pBuf;
        _uiSize = uiBufSize;
        _uiOffset = 0;
    }


    void DataStructWriter::setBuffer(const void* pBuf, D32U uiBufSize)
    {
        _pBuf = (D8U*)pBuf;
        _uiSize = uiBufSize;
        _uiOffset = 0;
    }

    D32S DataStructWriter::writeInt8(D8S iValue)
    {
        DATA_STRUCT_MEMBER_ASSIGN(&_pBuf[_uiOffset], D8S, iValue);
        // _uiOffset++;
        return 0;
    }

    D32S DataStructWriter::writeInt16(D16S iValue)
    {
        // DATA_STRUCT_OFFSET_ALIGN(_uiOffset, D16S);
        DATA_STRUCT_MEMBER_ASSIGN(&_pBuf[_uiOffset], D16S, iValue);
        // _uiOffset += sizeof(D16S);
        return 0;
    }

    D32S DataStructWriter::writeInt32(D32S iValue)
    {
        // DATA_STRUCT_OFFSET_ALIGN(_uiOffset, D32S);
        DATA_STRUCT_MEMBER_ASSIGN(&_pBuf[_uiOffset], D32S, iValue);
        // _uiOffset += sizeof(D32S);
        return 0;
    }

    D32S DataStructWriter::writeInt64(D64S iValue)
    {
        // DATA_STRUCT_OFFSET_ALIGN(_uiOffset, D64S);
        DATA_STRUCT_MEMBER_ASSIGN(&_pBuf[_uiOffset], D64S, iValue);
        // _uiOffset += sizeof(D64S);
        return 0;
    }

    D32S DataStructWriter::writeUInt8(D8U uiValue)
    {
        DATA_STRUCT_MEMBER_ASSIGN(&_pBuf[_uiOffset], D8U, uiValue);
        // _uiOffset++;
        return 0;
    }

    D32S DataStructWriter::writeUInt16(D16U uiValue)
    {
        // DATA_STRUCT_OFFSET_ALIGN(_uiOffset, D16U);
        DATA_STRUCT_MEMBER_ASSIGN(&_pBuf[_uiOffset], D16U, uiValue);
        // _uiOffset += sizeof(D16U);
        return 0;
    }

    D32S DataStructWriter::writeUInt32(D32U uiValue)
    {
        // DATA_STRUCT_OFFSET_ALIGN(_uiOffset, D32U);
        DATA_STRUCT_MEMBER_ASSIGN(&_pBuf[_uiOffset], D32U, uiValue);
        // _uiOffset += sizeof(D32U);
        return 0;
    }

    D32S DataStructWriter::writeUInt64(D64U uiValue)
    {
        // DATA_STRUCT_OFFSET_ALIGN(_uiOffset, D64U);
        DATA_STRUCT_MEMBER_ASSIGN(&_pBuf[_uiOffset], D64U, uiValue);
        // _uiOffset += sizeof(D64U);
        return 0;
    }


    D32S DataStructWriter::writeFloat(DFLOAT fValue)
    {
        // DATA_STRUCT_OFFSET_ALIGN(_uiOffset, DFLOAT);
        DATA_STRUCT_MEMBER_ASSIGN(&_pBuf[_uiOffset], DFLOAT, fValue);
        // _uiOffset += sizeof(DFLOAT);
        return 0;
    }


    D32S DataStructWriter::writeDouble(DOUBLE dValue)
    {
        // DATA_STRUCT_OFFSET_ALIGN(_uiOffset, DOUBLE);
        DATA_STRUCT_MEMBER_ASSIGN(&_pBuf[_uiOffset], DOUBLE, dValue);
        // _uiOffset += sizeof(DOUBLE);
        return 0;
    }

    D32S DataStructWriter::writePointer(void* pPtr)
    {
        // DATA_STRUCT_OFFSET_ALIGN(_uiOffset, void*);
        DATA_STRUCT_MEMBER_ASSIGN(&_pBuf[_uiOffset], void*, pPtr);
        // _uiOffset += sizeof(void*);

        return 0;
    }

    D32S DataStructWriter::writeBytes(const D8U* pBytes, D32U uiLength)
    {
        D32S iRet = -1;
        if (_uiSize - _uiOffset >= uiLength)
        {
            adm_memcpy(&_pBuf[_uiOffset], pBytes, uiLength, uiLength);
            _uiOffset += uiLength;
            iRet = 0;
        }

        return iRet;
    }

    D32S DataStructWriter::writeString(const DCHAR* pStr)
    {
        D32S iRet = -1;
        D32U uiStrLength = 0;
        void* pPtr = NULL;
    
        do 
        {
            if (NULL == pStr)
            {
                break;
            }

            uiStrLength = (D32U)adm_strlen(pStr) + 1U;
            pPtr = adm_malloc(uiStrLength);
            if (NULL == pPtr)
            {
                break;
            }

            adm_memcpy(pPtr, pStr, uiStrLength, uiStrLength);
            iRet = writePointer(pPtr);
        } while (FALSE);

        return iRet;
    }

    D32S DataStructWriter::writeFixedString(const DCHAR* pStr, D32U uiFixedSize)
    {
        D32S iRet = -1;
        D32U uiLength = (D32U)adm_strlen(pStr) + 1U;
        if (uiLength <= uiFixedSize && _uiSize - _uiOffset >= uiFixedSize)
        {
            adm_memcpy(&_pBuf[_uiOffset], pStr, uiLength, uiLength);
            _uiOffset += uiFixedSize;
            iRet = 0;
        }

        return iRet;
    }

    D32S DataStructWriter::writeArray(DataStructWriter* pWriter, D32U uiCount, D32U uiMemberSize)
    {
        D32S iRet = -1;
        do
        {
            iRet = writeUInt32(uiCount);
            if (0 != iRet)
            {
                break;
            }

            _uiOffset += (D32U)sizeof(void*);

            void* pPtr = NULL;
            D32U uiSize = 0;
            if (uiCount > 0)
            {
                uiSize = uiCount * uiMemberSize;
                pPtr = DataStructBuilder::createArray(uiSize);
                if (NULL == pPtr)
                {
                    iRet = -1;
                    break;
                }
            }

            iRet = writePointer(pPtr);
            if (0 != iRet)
            {
                DataStructBuilder::destroyArray(pPtr);
                break;
            }

            pWriter->_pBuf = (D8U*)pPtr;
            pWriter->_uiSize = uiSize;
            pWriter->_uiOffset = 0;
        } while (FALSE);

        return 0;
    }

    D32S DataStructWriter::writeArrayMember(D32U uiIndex, D32U uiMemberSize)
    {
        anchor(uiIndex * uiMemberSize);
        return 0;
    }

    D32S DataStructWriter::writeFixedArray(DataStructWriter* pWriter, D32U uiCount, D32U uiMemberSize)
    {
        pWriter->_pBuf = &_pBuf[_uiOffset];
        pWriter->_uiSize = _uiSize - _uiOffset;
        pWriter->_uiOffset = 0;
        _uiOffset += uiCount * uiMemberSize;

        return 0;
    }

    D32S DataStructWriter::writeObject(DataStructWriter* pWriter)
    {
        pWriter->_pBuf = &_pBuf[_uiOffset];
        pWriter->_uiSize = _uiSize - _uiOffset;
        pWriter->_uiOffset = 0;

        return 0;
    }

    D32S DataStructWriter::writeInt8(DataStructReader* pReader)
    {
        D8S uiValue = 0;
        pReader->readInt8(uiValue);
        return writeInt8(uiValue);
    }

    D32S DataStructWriter::writeInt16(DataStructReader* pReader)
    {
        D16S iValue = 0;
        pReader->readInt16(iValue);
        return writeInt16(iValue);
    }

    D32S DataStructWriter::writeInt32(DataStructReader* pReader)
    {
        D32S iValue = 0;
        pReader->readInt32(iValue);
        return writeInt32(iValue);
    }

    D32S DataStructWriter::writeInt64(DataStructReader* pReader)
    {
        D64S iValue = 0;
        pReader->readInt64(iValue);
        return writeInt64(iValue);
    }

    D32S DataStructWriter::writeUInt8(DataStructReader* pReader)
    {
        D8U uiValue = 0;
        pReader->readUInt8(uiValue);
        return writeUInt8(uiValue);
    }

    D32S DataStructWriter::writeUInt16(DataStructReader* pReader)
    {
        D16U uiValue = 0;
        pReader->readUInt16(uiValue);
        return writeUInt16(uiValue);
    }
    D32S DataStructWriter::writeUInt32(DataStructReader* pReader)
    {
        D32U uiValue = 0;
        pReader->readUInt32(uiValue);
        return writeUInt32(uiValue);
    }
    D32S DataStructWriter::writeUInt64(DataStructReader* pReader)
    {
        D64U uiValue = 0;
        pReader->readUInt64(uiValue);
        return writeUInt64(uiValue);
    }
    D32S DataStructWriter::writeFloat(DataStructReader* pReader)
    {
        DFLOAT fValue = 0;
        pReader->readFloat(fValue);
        return writeFloat(fValue);
    }
    D32S DataStructWriter::writeDouble(DataStructReader* pReader)
    {
        DOUBLE dValue = 0;
        pReader->readDouble(dValue);
        return writeDouble(dValue);
    }
    D32S DataStructWriter::writeString(DataStructReader* pReader)
    {
        D32U uiSize = 0;
        const DCHAR* pValue = NULL;
        pValue = pReader->readString(uiSize);
        return NULL != pValue ? writeString(pValue) : -1;
    }
    D32S DataStructWriter::writeFixedString(DataStructReader* pReader, D32U uiFixedSize)
    {
        D32U uiSize = 0;
        const DCHAR* pValue = NULL;
        pValue = pReader->readFixedString(uiFixedSize, uiSize);
        return NULL != pValue ? writeFixedString(pValue, uiFixedSize) : -1;
    }


    DataStructReader::DataStructReader()
    {
        _pData = NULL;
        _uiSize = 0;
        _uiOffset = 0;
    }

    DataStructReader::DataStructReader(const void* pData, D32U uiDataSize)
    {
        _pData = (D8U*)pData;
        _uiSize = uiDataSize;
        _uiOffset = 0;
    }

    void DataStructReader::setBuffer(const void* pData, D32U uiDataSize)
    {
        _pData = (D8U*)pData;
        _uiSize = uiDataSize;
        _uiOffset = 0;
    }

    D32S DataStructReader::readInt8(D8S& iValue)
    {
        D32S iRet = -1;
        if (_uiSize - _uiOffset >= 1)
        {
            iValue = *((D8S*)&_pData[_uiOffset]);
            iRet = 0;
        }
        return iRet;
    }

    D32S DataStructReader::readInt16(D16S& iValue)
    {
        D32S iRet = -1;

        //_uiOffset += _uiOffset & 1;
        if (_uiSize - _uiOffset >= 2)
        {
            iValue = *((D16S*)&_pData[_uiOffset]);
            // _uiOffset += 2;
            iRet = 0;
        }

        return iRet;
    }

    D32S DataStructReader::readInt32(D32S& iValue)
    {
        D32S iRet = -1;

        //_uiOffset += (4 - (_uiOffset & 3)) % 4;
        if (_uiSize - _uiOffset >= 4)
        {
            iValue = *((D32S*)&_pData[_uiOffset]);
            //_uiOffset += 4;
            iRet = 0;
        }

        return iRet;
    }

    D32S DataStructReader::readInt64(D64S& iValue)
    {
        D32S iRet = -1;

        //_uiOffset += (8 - (_uiOffset & 7)) % 8;
        if (_uiSize - _uiOffset >= 8)
        {
            iValue = *((D64S*)&_pData[_uiOffset]);
            // _uiOffset += 8;
            iRet = 0;
        }

        return iRet;
    }

    D32S DataStructReader::readUInt8(D8U& uiValue)
    {
        uiValue = _pData[_uiOffset];
        return 0;
    }

    D32S DataStructReader::readUInt16(D16U& uiValue)
    {
        // _uiOffset += _uiOffset & 1;
        uiValue = *((D16U*)&_pData[_uiOffset]);
        // _uiOffset += 2;

        return 0;
    }

    D32S DataStructReader::readUInt32(D32U& uiValue)
    {
        // _uiOffset += (4 - (_uiOffset & 3)) % 4;
        uiValue = *((D32U*)&_pData[_uiOffset]);
        // _uiOffset += 4;

        return 0;
    }

    D32S DataStructReader::readUInt64(D64U& uiValue)
    {
        // _uiOffset += (8 - (_uiOffset & 7)) % 8;
        uiValue = *((D64U*)&_pData[_uiOffset]);
        // _uiOffset += 8;

        return 0;
    }


    D32S DataStructReader::readFloat(DFLOAT& fValue)
    {
        // _uiOffset += (4 - (_uiOffset & 3)) % 4;
        fValue = *((DFLOAT*)&_pData[_uiOffset]);
        // _uiOffset += 4;

        return 0;
    }


    D32S DataStructReader::readDouble(DOUBLE& dValue)
    {
        // _uiOffset += (8 - (_uiOffset & 7)) % 8;
        dValue = *((DOUBLE*)&_pData[_uiOffset]);
        // _uiOffset += 8;

        return 0;
    }

    D32S DataStructReader::readBytes(D8U bytes[], D32U uiSize)
    {
        adm_memcpy(bytes, &_pData[_uiOffset], uiSize, uiSize);
        _uiOffset += uiSize;

        return 0;
    }

    D32S DataStructReader::readPointer(void*& pPtr)
    {
        // _uiOffset += (sizeof(void*) - _uiOffset & ((sizeof(void*) - 1))) % sizeof(void*);
        pPtr = *((void**)&_pData[_uiOffset]);
        // _uiOffset += sizeof(void*);

        return 0;
    }

    const DCHAR* DataStructReader::readFixedString(D32U uiFixedSize, D32U& uiSize)
    {
        const DCHAR* pStr = (DCHAR*)&_pData[_uiOffset];
        D32U uiStrLen = (D32U)adm_strlen(pStr);
        uiSize = uiStrLen;
        // _uiOffset += uiFixedSize;
        return pStr;
    }

    const DCHAR* DataStructReader::readString(D32U& uiSize)
    {
        void* pPtr = NULL;
        (void)readPointer(pPtr);

        return (const DCHAR*)pPtr;
    }

//     D32S DataStructReader::readArray(DataStructReader* pReader, D32U& uiCount, D32U uiMemberSize)
//     {
//         D32S iRet = -1;
// 
//         do 
//         {
//             iRet = readUInt32(uiCount);
//             if (0 != iRet)
//             {
//                 break;
//             }
//             
//             _uiOffset += sizeof(void*);
//             void* pPtr = NULL;
//             iRet = readPointer(pPtr);
//             if (NULL == pPtr)
//             {
//                 if (0 != uiCount)
//                 {
//                     iRet = -1;
//                 }
//                 break;
//             }
// 
//             pReader->_pData = (D8U*)pPtr;
//             pReader->_uiSize = uiCount * uiMemberSize;
//             pReader->_uiOffset = 0;
// 
//         } while (FALSE);
// 
//         return 0;
//     }
// 
// 
//     D32S DataStructReader::readArray(DataStructReader* pReader, D32U uiMemberSize)
//     {
//         D32S iRet = -1;
//         D32U uiCount = 0;
// 
//         do
//         {
//             iRet = readUInt32(uiCount);
//             if (0 != iRet)
//             {
//                 break;
//             }
// 
//             _uiOffset += sizeof(void*);
//             void* pPtr = NULL;
//             iRet = readPointer(pPtr);
//             if (NULL == pPtr)
//             {
//                 if (0 != uiCount)
//                 {
//                     iRet = -1;
//                 }
//                 break;
//             }
// 
//             pReader->_pData = (D8U*)pPtr;
//             pReader->_uiSize = uiCount * uiMemberSize;
//             pReader->_uiOffset = 0;
// 
//         } while (FALSE);
// 
//         return 0;
//     }


   D32S DataStructReader::readArray(void*& pArray, D32U& uiCount)
    {
        D32S iRet = -1;
        void* pPtr = NULL;

        do
        {
            iRet = readUInt32(uiCount);
            if (0 != iRet)
            {
                break;
            }

            _uiOffset += (D32U)sizeof(void*);
            iRet = readPointer(pPtr);
            if (NULL == pPtr && 0 != uiCount)
            {
                iRet = -1;
            }
        } while (FALSE);

        pArray = pPtr;

        return iRet;
    }

    D32S DataStructReader::readArrayCount(D32U& uiCount)
    {
        return readUInt32(uiCount);
    }

    D32S DataStructReader::readArrayMember(D32U uiIndex, D32U uiSize)
    {
        anchor(uiSize * uiIndex);
        return 0;
    }

    D32S DataStructReader::readFixedArray(DataStructReader* pReader, D32U uiCount, D32U uiMemberSize)
    {
        pReader->_pData = &_pData[_uiOffset];
        pReader->_uiSize = _uiSize - _uiOffset;
        pReader->_uiOffset = 0;
        _uiOffset += uiMemberSize * uiCount;
        return 0;
    }

    D32S DataStructReader::readObject(DataStructReader* pReader)
    {
        pReader->_pData = &_pData[_uiOffset];
        pReader->_uiSize = _uiSize - _uiOffset;
        pReader->_uiOffset = 0;

        return 0;
    }


    D32S DataStructReader::readObject(void*& pObject)
    {
        pObject = (void*)&_pData[_uiOffset];
        return 0;
    }

    D32S DataStructReader::skipByType(D32U uiSize)
    {
        DATA_STRUCT_MEMBER_SKIP_BY_SIZE(_uiOffset, uiSize);
        return 0;
    }

    D32S DataStructReader::skipBySize(D32U uiSize)
    {
        _uiOffset += uiSize;
        return 0;
    }



    DataStructDestructor::DataStructDestructor(const DataTypeDef* pType)
    {
        _pType = pType;
    }

    D32S DataStructDestructor::destroyNumber(const DataTypeDef* pType)
    {
        return _reader.skipByType(pType->uiSize);
    }

    D32S DataStructDestructor::destroyString(const DataTypeDef* pType)
    {
        D32S iRet = 0;
        if (0 == pType->uiSize)
        {
            // _reader.anchor(pType->uiOffset);

            void* pPtr = NULL;
            iRet = _reader.readPointer(pPtr);
            if (0 == iRet && NULL != pPtr)
            {
                adm_free(pPtr);
            }
        }

        return iRet;
    }

    D32S DataStructDestructor::destroyArray(const DataTypeDef* pType)
    {
        D32S iRet = 0;
        D32U uiCount = 0;
        void* pArray = NULL;
        const DataTypeDef* pSubType = pType->pSubType;

        do
        {
            if (0 == pType->uiSize)
            {
                // _reader.readArray(&destructor._reader, uiCount, pSubType->uiMemorySize);
                iRet = _reader.readArray(pArray, uiCount);

                DataStructDestructor destructor(pSubType);
                destructor._reader.setBuffer(pArray, uiCount * pSubType->uiMemorySize);

                for (D32U i = 0; i < uiCount; i++)
                {
                    destructor._reader.readArrayMember(i, pSubType->uiMemorySize);
                    iRet = destructor.destroyMember(pSubType);
                    if (0 != iRet)
                    {
                        break;
                    }
                }

                DataStructBuilder::destroyArray(pArray);
            }
            else
            {
                uiCount = pType->uiSize;
                for (D32U i = 0; i < uiCount; i++)
                {
                    _reader.readArrayMember(i, pSubType->uiMemorySize);
                    iRet = destroyMember(pType->pSubType);
                    if (0 != iRet)
                    {
                        break;
                    }
                }
            }
        } while (FALSE);

        return iRet;
    }

    D32S DataStructDestructor::destroyObject(const DataTypeDef* pType)
    {
        D32S iRet = -1;
        D32U uiCount = pType->uiSize;
        const DataTypeDef* pSubType = NULL;
        DataStructDestructor    destructor(pType);


        _reader.readObject(&destructor._reader);

        for (D32U i = 0; i < uiCount; i++)
        {
            pSubType = &pType->pSubType[i];
            destructor._reader.anchor(pSubType->uiOffset);
            if (0 == pSubType->uiOptional)
            {
                iRet = destructor.destroyMember(pSubType);
                if (0 != iRet)
                {
                    break;
                }
            }
            else
            {
                iRet = destructor.destroyOptional(pSubType);
            }
        }

        return iRet;
    }

    D32S DataStructDestructor::destroyMember(const DataTypeDef* pType)
    {
        D32S iRet = -1;
        switch (pType->uiType)
        {
        case DM_DATA_INT:
        case DM_DATA_UINT:
        case DM_DATA_REAL:
            iRet = destroyNumber(pType);
            break;
        case DM_DATA_STRING:
            iRet = destroyString(pType);
            break;
        case DM_DATA_ARRAY:
            iRet = destroyArray(pType);
            break;
        case DM_DATA_OBJECT:
            iRet = destroyObject(pType);
            break;
        default:
            break;
        }

        return iRet;
    }

    D32S DataStructDestructor::destroyOptional(const DataTypeDef* pType)
    {
        D32S iRet = 0;
        void* pPtr = NULL;

        _reader.readPointer(pPtr);
        if (NULL != pPtr)
        {
            if (DM_DATA_STRING != pType->uiType)
            {
                DataStructDestructor destructor(pType);
                destructor._reader.setBuffer(pPtr, pType->uiMemorySize);
                iRet = destructor.destroyMember(pType);

                DataStructBuilder::destroyStruct(pPtr);
            }
            else
            {
                iRet = destroyString(pType);
            }
        }

        return iRet;
    }

    D32S DataStructDestructor::destroy(const void* pData, D32U uiDataSize)
    {
        _reader.setBuffer(pData, uiDataSize);
        return destroyMember(_pType);
    }



    DataStructCloner::DataStructCloner()
    {
        _pType = NULL;
    }

    DataStructCloner::DataStructCloner(const DataTypeDef* pType)
    {
        _pType = pType;
    }

    D32S DataStructCloner::clone(const void* pData, D32U uiDataSize, void* pOut, D32U uiOutMaxSize)
    {
        _reader.setBuffer(pData, uiDataSize);
        _writer.setBuffer(pOut, uiOutMaxSize);
        return cloneMember(_pType);
    }

    D32S DataStructCloner::cloneInt(const DataTypeDef* pType)
    {
        D32S iRet = -1;
        switch (pType->uiSize)
        {
        case 1:
            iRet = _writer.writeInt8(&_reader);
            break;
        case 2:
            iRet = _writer.writeInt16(&_reader);
            break;
        case 4:
            iRet = _writer.writeInt32(&_reader);
            break;
        case 8:
            iRet = _writer.writeInt64(&_reader);
            break;
        default:
            break;
        }

        return iRet;
    }

    D32S DataStructCloner::cloneUInt(const DataTypeDef* pType)
    {
        D32S iRet = -1;
        switch (pType->uiSize)
        {
        case 1:
            iRet = _writer.writeUInt8(&_reader);
            break;
        case 2:
            iRet = _writer.writeUInt16(&_reader);
            break;
        case 4:
            iRet = _writer.writeUInt32(&_reader);
            break;
        case 8:
            iRet = _writer.writeUInt64(&_reader);
            break;
        default:
            break;
        }

        return iRet;
    }

    D32S DataStructCloner::cloneReal(const DataTypeDef* pType)
    {
        DOUBLE dValue = 0;
        _reader.readDouble(dValue);
        _writer.writeDouble(dValue);

        return 0;
    }

    D32S DataStructCloner::cloneString(const DataTypeDef* pType)
    {
        D32S iRet = -1;

        if (0 == pType->uiSize)
        {
            iRet = _writer.writeString(&_reader);
        }
        else
        {
            iRet = _writer.writeFixedString(&_reader, pType->uiSize);
        }

        return iRet;
    }

    D32S DataStructCloner::cloneArray(const DataTypeDef* pType, D32U uiCount)
    {
        D32S iRet = 0;
        D32U uiPos = 0;
        for (D32U i = 0; i < uiCount; i++)
        {
            _reader.anchor(uiPos);
            _writer.anchor(uiPos);

            iRet = cloneMember(pType);
            if (0 != iRet)
            {
                break;
            }

            uiPos += pType->uiMemorySize;
        }

        return iRet;
    }

    D32S DataStructCloner::cloneArray(const DataTypeDef* pType)
    {
        D32S iRet = -1;
        D32U uiCount = pType->uiSize;
        void* pArray = NULL;
        const DataTypeDef* pSubType = NULL;

        do
        {
            pSubType = pType->pSubType;
            if (0 == uiCount)
            {
                DataStructCloner cloner;

                //iRet = _reader.readArray(&cloner._reader, uiCount, pSubType->uiMemorySize);
                iRet = _reader.readArray(pArray, uiCount);
                if (0 != iRet)
                {
                    break;
                }

                cloner._reader.setBuffer(pArray, uiCount * pSubType->uiMemorySize);

                iRet = _writer.writeArray(&cloner._writer, uiCount, pSubType->uiMemorySize);
                if (0 != iRet)
                {
                    break;
                }

                iRet = cloner.cloneArray(pSubType, uiCount);
            }
            else
            {
                iRet = cloneArray(pSubType, uiCount);
            }
        } while (FALSE);

        return iRet;
    }



    D32S DataStructCloner::cloneObject(const DataTypeDef* pType)
    {
        D32S iRet = -1;
        DataStructCloner    cloner;
        D32U uiCount = pType->uiSize;
        const DataTypeDef* pSubType = NULL;

        do
        {
            iRet = _reader.readObject(&cloner._reader);
            if (0 != iRet)
            {
                break;
            }

            iRet = _writer.writeObject(&cloner._writer);
            if (0 != iRet)
            {
                break;
            }

            for (D32U i = 0; i < uiCount; i++)
            {
                pSubType = &pType->pSubType[i];
                cloner._reader.anchor(pSubType->uiOffset);
                cloner._writer.anchor(pSubType->uiOffset);

                if (0 == pSubType->uiOptional)
                {
                    iRet = cloner.cloneMember(pSubType);
                    if (0 != iRet)
                    {
                        break;
                    }
                }
                else
                {
                    iRet = cloner.cloneOptional(pSubType);
                }
            }
        } while (FALSE);

        return iRet;
    }

    D32S DataStructCloner::cloneMember(const DataTypeDef* pType)
    {
        D32S iRet = -1;

        switch (pType->uiType)
        {
        case DM_DATA_INT:
            iRet = cloneInt(pType);
            break;
        case DM_DATA_UINT:
            iRet = cloneUInt(pType);
            break;
        case DM_DATA_REAL:
            iRet = cloneReal(pType);
            break;
        case DM_DATA_STRING:
            iRet = cloneString(pType);
            break;
        case DM_DATA_ARRAY:
            iRet = cloneArray(pType);
            break;
        case DM_DATA_OBJECT:
            iRet = cloneObject(pType);
            break;
        default:
            break;
        }

        return iRet;
    }

    D32S DataStructCloner::cloneOptional(const DataTypeDef* pType)
    {
        D32S iRet = -1;

        do 
        {
            void* pPtr = NULL;
            iRet = _reader.readPointer(pPtr);
            if (0 != iRet)
            {
                break;
            }

            if (NULL == pPtr)
            {
                iRet = _writer.writePointer(pPtr);
                break;
            }

            if (DM_DATA_STRING != pType->uiType)
            {
                D32U uiSize = pType->uiMemorySize;
                DataStructCloner    cloner;
                cloner._reader.setBuffer(pPtr, uiSize);

                void* pPtr = adm_malloc(uiSize);
                if (NULL == pPtr)
                {
                    break;
                }
                iRet = _writer.writePointer(pPtr);
                if (0 != iRet)
                {
                    break;
                }

                cloner._writer.setBuffer(pPtr, uiSize);
                iRet = cloner.cloneMember(pType);
            }
            else
            {
                iRet = cloneString(pType);
            }
        } while (FALSE);

        return iRet;
    }



//     DataStruct::DataStruct(void* pData, D32U uiDataSize)
//     {
//         _pType = NULL;
//         _pData = pData;
//         _uiSize = uiDataSize;
//     }


    DataStruct::DataStruct()
    {
        _pType = NULL;
        _pData = NULL;
        _uiSize = 0;
    }

    DataStruct::DataStruct(const DataTypeDef* pType, void* pData, D32U uiDataSize)
    {
        _pType = pType;
        _pData = pData;
        _uiSize = uiDataSize;
    }

    D32S DataStruct::clone(void* pOut, D32U uiOutMaxSize)
    {
        D32S iRet = -1;
        if (NULL != _pType)
        {
            DataStructCloner cloner(_pType);
            iRet = cloner.clone(_pData, _uiSize, pOut, uiOutMaxSize);
        }

        return iRet;
    }

    D32S DataStruct::clone(DataStruct* pDataStruct)
    {
        D32S iRet = -1;
        if (NULL != _pType)
        {
            DataStructCloner cloner(_pType);
            pDataStruct->_pType = _pType;
            iRet = cloner.clone(_pData, _uiSize, pDataStruct->_pData, pDataStruct->_uiSize);
        }

        return iRet;
    }


    D32S DataStruct::copy(const void* pData, D32U uiDataSize)
    {
        D32S iRet = -1;
        if (NULL != _pType)
        {
            DataStructCloner cloner(_pType);
            iRet = cloner.clone(pData, uiDataSize, _pData, _uiSize);
        }

        return iRet;
    }

    D32S DataStruct::destroy()
    {
        D32S iRet = -1;
        if (NULL != _pType)
        {
            DataStructDestructor destructor(_pType);
            iRet = destructor.destroy(_pData, _uiSize);
        }

        return iRet;
    }

    D32S DataStruct::setInt(D32S iValue)
    {
        DataStructWriter writer(_pData, _uiSize);
        return writer.writeInt32(iValue);
    }

    D32S DataStruct::setInt64(D64S iValue)
    {
        DataStructWriter writer(_pData, _uiSize);
        return writer.writeInt64(iValue);
    }

    D32S DataStruct::setUInt(D32U uiValue)
    {
        DataStructWriter writer(_pData, _uiSize);
        return writer.writeUInt32(uiValue);
    }

    D32S DataStruct::setUInt64(D64U uiValue)
    {
        DataStructWriter writer(_pData, _uiSize);
        return writer.writeUInt64(uiValue);
    }

    D32S DataStruct::setString(const DCHAR* pValue)
    {
        DataStructWriter writer(_pData, _uiSize);
        return writer.writeString(pValue);
    }

    D32S DataStruct::setFixedString(D32U uiFixedSize, const DCHAR* pValue)
    {
        DataStructWriter writer(_pData, _uiSize);
        return writer.writeFixedString(pValue, uiFixedSize);
    }
}
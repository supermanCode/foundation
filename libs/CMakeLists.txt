cmake_minimum_required(VERSION 3.5.0)
project(libs C CXX)

find_package(gtest)
find_package(json)
find_package(iniparser)
find_package(vsomeip)
find_package(openssl)
find_package(curl)
find_package(zlib)
find_package(log)
find_package(leveldb)
find_package(paho.mqtt)
# find_package(lwm2m)
# if (CUSTOM_CMAKE_XML_NAME)
#     find_package(xml)
# endif()

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/inc)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src/lib)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src/base64)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src/hmac)
include_directories(${CMAKE_INSTALL_INCLUDEDIR})
include_directories(${CMAKE_INSTALL_INCLUDEDIR}/log)
include_directories(${CMAKE_SOURCE_DIR}/platform/os/inc)



set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} \
    -lm   \
    -Wall \
    -g \
    -O0 \
    -Wno-unused-parameter \
    -std=gnu11\
    -Werror=return-type \
    -fPIC\
    ")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} \
    -lm   \
    -Wall \
    -g \
    -O0 \
    -Wno-unused-parameter \
    -std=c++11\
    -fPIC\
    -Werror=return-type \
    -fpermissive\
    ")
    
add_library(
    admlist
    STATIC
    src/adm_list.c
)

install(TARGETS admlist
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}/libs)

add_library(
    admvector
    STATIC
    src/adm_vector.c
)

install(TARGETS admvector
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})

add_library(
    admsbuf
    STATIC
    src/adm_sbuf.c
)

install(TARGETS admsbuf
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})

add_library(
    admmap
    STATIC
    src/map/adm_map.c
    src/map/adm_rbtree.c
)

install(TARGETS admmap
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})

add_library(
    foundation_version
    STATIC
    src/version/version.c
)

install(TARGETS foundation_version
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})

add_library(
    admqueue
    STATIC
    src/adm_queue.c
)
install(TARGETS admqueue
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})

add_library(
    admworkqueue
    STATIC
    src/workqueue/adm_workqueue.c
    src/workqueue/workqueue.c
)
add_dependencies(admworkqueue log admos)

install(TARGETS admworkqueue
    ARCHIVE DESTINATION ${INSTALL_DIR}/lib
    LIBRARY DESTINATION ${INSTALL_DIR}/lib)

add_subdirectory(src/kvstorage)
add_subdirectory(src/iniparser)
add_subdirectory(src/async)
add_subdirectory(src/adm_minizip)
#add_subdirectory(gtest)

add_subdirectory(src/common)
add_subdirectory(src/datatype)
add_subdirectory(src/nmm)
# add_subdirectory(src/scm)
add_subdirectory(src/smm)
add_subdirectory(src/dm)
add_subdirectory(src/storage)
add_subdirectory(src/jds)

add_library(
    admlib
    STATIC
    src/lib/base64/base64.c
    src/lib/hmac/hmac_sha2.c
    src/lib/hmac/sha2.c
    src/lib/atomic.c
    src/lib/glob.c
    src/lib/kref.c
    src/lib/md5.c
    src/lib/simple_string.c
    src/lib/stringprintf.c
)
install(TARGETS admlib
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})

add_library(
    foundation
    SHARED
    src/adm_getFoundationVersion.c
)

target_include_directories(foundation
    PRIVATE
    ${CMAKE_SOURCE_DIR}/libs/inc
)

message("+++++++++++###########foundation compile CUSTOM_CMAKE_SYSTEM_NAME = ${CUSTOM_CMAKE_SYSTEM_NAME}")

if(${CUSTOM_CMAKE_SYSTEM_NAME} MATCHES "Linux")
    set(SYSTEM_LIBRARY pthread rt)
elseif(${CUSTOM_CMAKE_SYSTEM_NAME} MATCHES "Android")
    set(SYSTEM_LIBRARY c)
elseif(${CUSTOM_CMAKE_SYSTEM_NAME} MATCHES "Alios")
    set(SYSTEM_LIBRARY c)
elseif(${CUSTOM_CMAKE_SYSTEM_NAME} MATCHES "QNX7")
    set(SYSTEM_LIBRARY c)
else()
    set(SYSTEM_LIBRARY pthread rt) 
endif()

if(USE_SYSTEM_SSL)
    message("+++++++++++use system ssl USE_SYSTEM_SSL = ${USE_SYSTEM_SSL}")
    set(SSL_LIBRARY crypto ssl)
else()
    message("+++++++++++use project compile ssl USE_SYSTEM_SSL = ${USE_SYSTEM_SSL}")
    if(USE_DYNAMIC_SSL_LIB)
        message("+++++++++++use project compile dynamic ssl USE_DYNAMIC_SSL_LIB = ${USE_DYNAMIC_SSL_LIB}")
        set(SSL_LIBRARY ${CMAKE_INSTALL_LIBDIR}/libcrypto.so  ${CMAKE_INSTALL_LIBDIR}/libssl.so)
    else()
        message("+++++++++++use project compile static ssl USE_DYNAMIC_SSL_LIB = ${USE_DYNAMIC_SSL_LIB}")
        set(SSL_LIBRARY ${CMAKE_INSTALL_LIBDIR}/libcrypto-static.a  ${CMAKE_INSTALL_LIBDIR}/libssl-static.a)
    endif()
endif()

target_link_libraries (foundation
        PRIVATE
        -Wl,--whole-archive
        ${SSL_LIBRARY}
	${CMAKE_INSTALL_LIBDIR}/libz.a
        ${CMAKE_INSTALL_LIBDIR}/libcurl-d.a 
        ${CMAKE_INSTALL_LIBDIR}/libadmlog.a    
        ${CMAKE_INSTALL_LIBDIR}/libadmdebug.a      
        ${CMAKE_INSTALL_LIBDIR}/libjson-c.a
        #${CMAKE_INSTALL_LIBDIR}/libjsoncpp.a
        ${CMAKE_INSTALL_LIBDIR}/libiniparser.a         
        ${CMAKE_INSTALL_LIBDIR}/libleveldb.a      
        #${CMAKE_INSTALL_LIBDIR}/libprotobufd.a
        ${CMAKE_SOURCE_DIR}/build/platform/os/libadmos.a
        ${CMAKE_SOURCE_DIR}/build/libs/libadmlist.a  
        ${CMAKE_SOURCE_DIR}/build/libs/libadmmap.a  
        ${CMAKE_SOURCE_DIR}/build/libs/libfoundation_version.a  
        ${CMAKE_SOURCE_DIR}/build/libs/libadmqueue.a  
        ${CMAKE_SOURCE_DIR}/build/libs/libadmsbuf.a  
        ${CMAKE_SOURCE_DIR}/build/libs/libadmvector.a  
        ${CMAKE_SOURCE_DIR}/build/libs/libadmworkqueue.a
        ${CMAKE_SOURCE_DIR}/build/libs/libadmlib.a
        ${CMAKE_SOURCE_DIR}/build/libs/src/kvstorage/libadmkvstorage.a
        ${CMAKE_SOURCE_DIR}/build/libs/src/iniparser/libadminiparser.a
        ${CMAKE_SOURCE_DIR}/build/libs/src/async/libadmasync.a
        ${CMAKE_SOURCE_DIR}/build/libs/src/adm_minizip/libadm_minizip.a
        ${CMAKE_SOURCE_DIR}/build/libs/src/common/libcommon.a
        ${CMAKE_SOURCE_DIR}/build/libs/src/datatype/libdatatype.a
        ${CMAKE_SOURCE_DIR}/build/libs/src/dm/libadmdm.a
        ${CMAKE_SOURCE_DIR}/build/libs/src/storage/libadmstorage.a
        ${CMAKE_SOURCE_DIR}/build/libs/src/jds/libjds.a
        ${CMAKE_SOURCE_DIR}/build/libs/src/nmm/libNMM.a
        # ${CMAKE_SOURCE_DIR}/build/libs/src/scm/libSCM.a
        ${CMAKE_SOURCE_DIR}/build/libs/src/smm/libSMM.a
        -Wl,--no-whole-archive
        ${SYSTEM_LIBRARY}
    )

# 下面填写依赖的时候，需要将不被后面其他库链接的库放在第一个，否则会出现 cycle dependencies 的问题
add_dependencies(${foundation} curl json iniparser openssl log leveldb admlist admvector admsbuf admmap foundation_version admqueue admworkqueue admlib admos admkvstorage adminiparser admasync adm_minizip common admdm NMM SMM datatype admstorage jds)

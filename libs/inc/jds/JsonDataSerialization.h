#ifndef JSON_DATA_SERIALIZATION_H_
#define JSON_DATA_SERIALIZATION_H_


#include "datatype/DataStructCodec.h"
#include "json-c/json.h"


using namespace vus;



class JsonDataEncoder
{
public:
    JsonDataEncoder(const DataTypeDef* pType);
    ~JsonDataEncoder();
    const DCHAR* encode(void* pData, D32U uiDataSize);

private:
    class JsonDataEncodeHelper : public DataStructDecodeHelper
    {
    public:
        D32S init(const DataTypeDef* pTypeDef, void* pData, void*& pOutData);
        D32S setAsInt(const DataTypeDef* pTypeDef, void* pData, D64S iValue);
        D32S setAsUInt(const DataTypeDef* pTypeDef, void* pData, D64U uiValue);
        D32S setAsReal(const DataTypeDef* pTypeDef, void* pData, DOUBLE dValue);
        D32S setAsString(const DataTypeDef* pTypeDef, void* pData, const DCHAR* pValue);
        D32S setNull(const DataTypeDef* pTypeDef, void* pData);
        D32S setArrayCount(const DataTypeDef* pTypeDef, void* pData, D32U uiCount);
        D32S getArrayMember(const DataTypeDef* pTypeDef, D32U uiIndex, void* pData, void*& pOutData);
        D32S getObjectMember(const DataTypeDef* pTypeDef, void* pData, void*& pOutData);

    private:
        struct json_object* createJsonObject(const DataTypeDef* pTypeDef);
    };

private:
    JsonDataEncodeHelper    _helper;
    DataStructDecoder       _decoder;
    const DataTypeDef* _pType;
    struct json_object* _pRoot;
};


class JsonDataDecoder
{
public:
    JsonDataDecoder(const DataTypeDef* pType);
    ~JsonDataDecoder();
    D32S decode(const DCHAR* pJsonStr, void*& pOutData, D32U& uiOutDataSize);
    D32S decodeEx(const DCHAR* pJsonStr, void* pOutData, D32U uiOutDataSize);

private:
    class JsonDataDecodeHelper : public DataStructEncodeHelper
    {
    public:
        D32S init(const DataTypeDef* pTypeDef, void* pData, void*& pOutData);
        D32S asInt(const DataTypeDef* pTypeDef, void* pData, D64S& iValue);
        D32S asUInt(const DataTypeDef* pTypeDef, void* pData, D64U& uiValue);
        D32S asReal(const DataTypeDef* pTypeDef, void* pData, DOUBLE& dValue);
        D32S asString(const DataTypeDef* pTypeDef, void* pData, const DCHAR*& pValue);
        D32S getArrayCount(const DataTypeDef* pTypeDef, void* pData, D32U& uiCount);
        D32S getArray(const DataTypeDef* pTypeDef, void* pData, void*& pOutData);
        D32S getArrayMember(const DataTypeDef* pTypeDef, D32U uiIndex, void* pData, void*& pOutData);
        // D32S getObject(const DataTypeDef* pTypeDef, void* pData, void*& pOutData);
        D32S getObjectMember(const DataTypeDef* pTypeDef, void* pData, void*& pOutData);

    private:
        DBOOL checkJsonObject(struct json_object* pObject, const DataTypeDef* pTypeDef);
    };

private:
    JsonDataDecodeHelper    _helper;
    DataStructEncoder       _encoder;
    const DataTypeDef* _pType;
    void* _pData;
};

#endif  // JSON_DATA_SERIALIZATION_H_

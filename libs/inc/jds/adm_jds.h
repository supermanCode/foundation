#ifndef ADM_JDS_H_
#define ADM_JDS_H_


#include "adm_typedefs.h"
#include "dm/adm_dm_dataType.h"


#ifdef __cplusplus
extern "C"
{
#endif  // __cplusplus

    typedef void ADM_JDS_DECODER_T;

    ADM_JDS_DECODER_T* adm_jds_decoderCreate(const ADM_DM_DATA_TYPE_DEF_T* pType);
    void adm_jds_decoderDestroy(ADM_JDS_DECODER_T* pDecoder);
    D32S adm_jds_decoderDecode(ADM_JDS_DECODER_T* pDecoder, const DCHAR* pJsonStr, void** pOutData, D32U* puiOutDataSize);


    typedef void ADM_JDS_ENCODER_T;

    ADM_JDS_ENCODER_T* adm_jds_encoderCreate(const ADM_DM_DATA_TYPE_DEF_T* pType);
    void adm_jds_encoderDestroy(ADM_JDS_ENCODER_T* pDecoder);
    const DCHAR* adm_jds_encoderEncode(ADM_JDS_ENCODER_T* pDecoder, void* pInData, D32U uiUDataSize);

    D32S adm_jds_decodeJsonStr(const ADM_DM_DATA_TYPE_DEF_T* pType, const DCHAR* pJsonStr, void* pOutData, D32U puiOutDataSize);

#ifdef __cplusplus
}
#endif  // __cplusplus


#endif // ADM_JDS_H_

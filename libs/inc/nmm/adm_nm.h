/**
 * @file    adm_nm.h
 * @brief   Brief description in Chinese
 * @details 通知事件,事件定义  ADM_NM_XXX_YYYY, XXX为通知模块, YYYY为通知内容
 * @version v0.1.0
 * @author
 * @date    Created in the 2023-3-21
 * @copyright  Copyright (C) 2018 All rights Abup Technology Co., Ltd.
 *
 * @par     Function List :
 *
 * @history
  1.Date        : 2023-3-21
    Author      :
    Modification: modification record
 *
 */

#pragma once

#include "adm_typedefs.h"
#include "adm_memory.h"


#ifdef __cplusplus
extern "C" {
#endif

/** 事件错误码 */
typedef enum ADM_NM_ERRORCODE_ENUM
{
    ADM_NM_OK,                 ///< 成功
    ADM_NM_INVALIDNOTIFYID,    ///< 无效事件ID
    ADM_NM_INVALIDCBFN,        ///< 无效回调函数
    ADM_NM_INVALIDPARAM,
    ADM_NM_INVALIDOBJDEF,      ///< 通知结构定义表
    ADM_NM_HASREGISTED,        ///< 已经注册
    ADM_NM_NOTREGISTED,        ///< 未注册
    ADM_NM_THREADCREATEERROR,  ///< 线程创建失败
    ADM_NM_REQUEST_TIMEOUT,    ///< session请求超时
    ADM_NM_EXPIRED,
} ADM_NM_ERRORCODE_EN;

enum ADM_NM_FLAG_ENUM
{
    ADM_NM_FLAG_SYNC,
    ADM_NM_FLAG_ASYNC   = 0x01,  ///< 异步通知
    ADM_NM_FLAG_KV      = 0x02,  ///< 注册通知处理，是否需要进行KV保存，如果需要则对应通知需通过DM进行存储
};


/**
 * @function    通知类型
 * @brief       接收端注册
 * @details    detail description
 *
 * @param    [in] const char *  通知id
 * @param    [in] const D32U  request 相关的会话ID，NM自动生成, 普通Notify 忽略
 * @param    [in] const void *  通知数据
 * @param    [in] void * 参数
 * @return   D32S
 * @calls        :
 * @called By    : nm模块
 * @note   :
 */
typedef D32S(*ADM_NOTIFY_CB)(const char *notifyID, D32U sessionID, const void *pData, void *pParam);

/**
 * @function    响应类型
 * @brief       发送端携带该回调
 * @details    detail description
 *
 * @param    [in] const char *  通知id
 * @param    [in] const void  request 以及 session的响应数据
 * @param    [in] D32U  响应数据大小
 * @param    [in] void * 发送携带信息
 * @return   D32S
 * @calls        :
 * @called By    : nm模块
 * @note   :
 */

typedef D32S(*ADM_NOTIFYRESP_CB)(const char *notifyID, const void *pData, D32U respSize, void *pContext);


/** 事件ID：DM初始化完成 */
#define ADM_NID_DMINITED            "ADM_NID_DMINITED"

extern D32S adm_nm_init();
extern D32S adm_nm_uninit();

/**
 * @function    adm_nm_register
 * @brief
 * @details    接收端注册函数
 *
 * @param    [in] const char * 事件ID
 * @param    [in] ADM_NOTIFY_CB 回调函数
 * @param    [in] void * 参数
 * @param    [in] D32U  ADM_NM_FLAG_ENUM:通知类型为同步或异步或KV
 * @return
 * @calls        :
 * @called By    :
 * @note   :
 */
extern D32S adm_nm_register(const char *notifyID, ADM_NOTIFY_CB fnCB, void *pParam, D32U flag);


extern D32S adm_nm_unregister(const char *notifyID, ADM_NOTIFY_CB fncb);

/**
 * @function    adm_nm_notify
 * @brief
 * @details    通知
 *
 * @param    [in] const char * 事件ID
 * @param    [in] void * 通知数据
 * @param    [in] void * 通知数据大小
 * @return
 * @calls        :
 * @called By    :
 * @note   :
 */
extern D32S adm_nm_notify(const char *notifyID, void *pData, D32U dataSize);

/**
 * @function   adm_nm_request
 * @brief
 * @details    detail description
 *
 * @param    [in] const char* 通知ID
 * @param    [in] void*  数据
 * @param    [in] void*  数据大小
 * @param    [in] ADM_NOTIFYRESP_CB 业务请求时需要响应, 带回调
 * @param    [in] void*  请求时携带业务信息
 * @return
 * @calls        :
 * @called By    :
 * @note   :
 */
extern D32S adm_nm_request(const char *notifyID, void *pData, D32U dataSize,
                           ADM_NOTIFYRESP_CB fnResp, void *pContext);

/**
 * @function   vus_nm_session
 * @brief      请求时异步转同步 等价 adm_nm_request
 * @details    深拷贝描述对象外部传入?  删除参数  const vus::DataTypeDef *pObjDef
 *             接收端注册时需注册为异步，同步会超时
 *
 * @param    [in] const char* 通知ID
 * @param    [in] void*  数据
 * @param    [in] void*  数据大小
 * @param    [in] void*  nm超时时间
 * @param    [out]   void *pRespData    响应数据
 * @param    [out]   D32U *sessionId    会话id,外部调用adm_nm_respData_Free释放深拷贝内容
 * @return
 * @calls        :
 * @called By    :
 * @date    Created in the 2023-3-24
 */
extern D32S adm_nm_session(const char *notifyID, void *pData, D32U dataSize, D32U msTime,
                           void *pRespData,D32U *sessionId);

/**
 * @function    adm_nm_respData_Free
 * @brief    
 * @details    与   adm_nm_session配套使用 释放深拷贝的响应数据
 *
 * @param    [in]  sessionId  adm_nm_session带出的会话id
 * @return   
 * @calls        : 
 * @called By    : 
 * @note   : 
 */
extern D32S adm_nm_respData_Free(D32U sessionId);



/**
 * @function    adm_nm_response
 * @brief
 * @details    接收端响应
 *
 * @param    [in] const char* 通知ID
 * @param    [in] D32U  request 相关的会话ID
 * @param    [in] void * 响应数据
 * @param    [in] D32U  响应数据大小
 * @return
 * @calls        :
 * @called By    :
 * @note   :
 */

extern D32S adm_nm_response(const char *notifyID, D32U sessionID, const void *pData, D32U respSize);

/**
 * @function    adm_nm_notifyack
 * @brief
 * @details    需要做持久化处理的通知,通知接收者调用该接口，告知nmm清除KV
 *
 * @param    [in] const char * 事件ID
 * @param    [in] 回调函数
 * @return
 * @calls        :
 * @called By    :
 * @note   :
 */
extern D32S adm_nm_notifyack(const char *notifyID, ADM_NOTIFY_CB fnCB);


/**
 * @function    adm_nm_structDescrip
 * @brief
 * @details    注册数据描述 异步时深拷贝使用.   与注册统一成一个??????
 *
 * @param    [in]   char *notifyID  事件
 * @param    [in]   char *pStructDescrip      接收端接收数据结构的描述
 * @param    [in]   char *pRespDescrip        接收端响应数据结构的描述
 * @return
 * @calls        :
 * @called By    :
 * @note   :
 */
extern D32S adm_nm_structDescrip(const char *notifyID,
                                 const char *pStructDescrip, const char *pRespDescrip);


#ifdef __cplusplus  // close out "C" linkage in case of c++ compiling
}
#endif


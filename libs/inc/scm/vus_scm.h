#pragma once

#include "adm_typedefs.h"

#ifdef __cplusplus
extern "C" {
#endif

#define VUS_SERIAL_TABLE_NAME_LEN   128
#define VUS_SERIAL_BYTE_FOLLOW      0xFFFF

#define VUS_SERIAL_BUFFER_LEN       1024


typedef enum VUS_CONVERT_ERROR_ENUM
{
    VUS_SC_OK = 0,
    VUS_SC_INVALIDKEY = 1,
    VUS_SC_INVALIDITEM,
    VUS_SC_ERRORTABLE,
} VUS_CONVERT_ERROR_EN;

typedef enum VUS_SERIAL_TYPE_ENUM
{
    VUS_SERIAL_TYPE_JSON,
    VUS_SERIAL_TYPE_XML,

} VUS_SERIAL_TYPE_EN;

typedef enum VUS_SERIAL_DATA_TYPE_ENUM
{
    VUS_DATA_TYPE_S32,
    VUS_DATA_TYPE_U32,
    VUS_DATA_TYPE_U64,
    VUS_DATA_TYPE_S64,
    VUS_DATA_TYPE_STRINGARRAY,  // 字符串数组 char[]
    VUS_DATA_TYPE_STRINGPT,     // 字符串指针 char*
    VUS_DATA_TYPE_OBJ,          // 结构体数组
    VUS_DATA_TYPE_OBJPT,        // 结构体指针
    VUS_DATA_TYPE_ARRAY,        // 数组         StructA[]
    VUS_DATA_TYPE_ARRAYPT,      // 数组         StructA*
    VUS_DATA_TYPE_PT,
} VUS_SERIAL_DATA_TYPE_EN;

typedef enum VUS_SERIAL_DATA_FORMAT_ENUM
{
    VUS_SDF_INTHEX,
    VUS_SDF_INTDEC,
    VUS_SDF_FLOAT,
} VUS_SERIAL_DATA_FORMAT_EN;

typedef struct vus_serial_item_struct
{
    DCHAR *keyName; //< 当前字段关键字
    DCHAR *refName;
    DCHAR *subTableName;
    VUS_SERIAL_DATA_TYPE_EN type;
    D16U startByte;
    D16U size;
} vus_serial_item_st, *vus_serial_item_pt;
typedef const vus_serial_item_st *vus_serial_item_cpt;


typedef struct vus_serial_table_struct
{
    DCHAR *name;
    D32U size;
    D32U itemNumber;
    vus_serial_item_st items[20];
} vus_serial_table_st, *vus_serial_table_pt;
typedef const vus_serial_table_st *vus_serial_table_cpt;

#ifdef __cplusplus  // close out "C" linkage in case of c++ compiling
}
#endif


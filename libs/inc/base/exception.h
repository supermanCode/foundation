#ifndef APD_ARA_CORE_EXCEPTIONS_H
#define APD_ARA_CORE_EXCEPTIONS_H

#include "error_code.h"

#include <exception>
#include <system_error>
#include <utility>
#include <ostream>

namespace abup
{
namespace ota
{
namespace base
{

class Exception : public std::exception
{
    ErrorCode const mErrorCode;

public:
    explicit Exception(ErrorCode err) noexcept
        : mErrorCode(std::move(err))
    {}

    ErrorCode const& Error() const noexcept
    {
        return mErrorCode;
    }

    char const* what() const noexcept override
    {
        return std::exception::what();
    }
};

}  // namespace base
}  // namespace ota
}  // namespace abup

#endif  // APD_ARA_CORE_EXCEPTIONS_H

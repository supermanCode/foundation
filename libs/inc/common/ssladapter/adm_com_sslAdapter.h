/**
 * @file adm_com_ssl_sslAdapter.h
 * @brief ssl adapter service for openssl and mbedtls
 * @details provide ssl api for establish SSL connection
 * @author xupeng
 * @version v0.1.0
 * @date 2022-6-8
 * @copyright www.abupdate.com
 */
#ifndef ADM_COM_SSLADAPTER_H
#define ADM_COM_SSLADAPTER_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "adm_typedefs.h"

#define EXPORT_FUN __attribute__((visibility("default")))

    typedef void ADM_COM_SSL_CTX_T; ///> SSL_CTX
    typedef void ADM_COM_SSL_T;     ///> SSL

    //这些信息是否可以直接放到ini文件中，在初始化接口中自己做。同时暴露接
    //口，如果外部不做就自己做(判定NULL)。不行，如果要做SSL抽象层，那业务相关的得拿出去
    typedef struct ADM_COM_SSL_CTX_OPT
    {
        const DCHAR *ciphers;
        const DCHAR *version;
        const DCHAR *crt_file;
        const DCHAR *key_file;
        const DCHAR *ca_file;
        const DCHAR *ca_path;
        DCHAR *password;
        DBOOL verify_peer;
        D32S  endpoint;
    } ADM_COM_SSL_CTX_OPT_T;

    enum
    {
        ADM_SSL_OK = 0,
        ADM_SSL_ERROR = -1,
        ADM_SSL_WANT_READ = -2,
        ADM_SSL_WANT_WRITE = -3,
    };

    /**
     * @brief 创建SSL context上下文配置和管理SSL连接
     * @note 此接口用于创建context上下文，配置证书密钥，算法套件和协议版本，是否启用双向认证。
     *
     * @param opt    [In] ADM_COM_SSL_CTX_OPT_T*     相关配置信息
     *
     * @return ADM_COM_SSL_CTX_T*     NULL:failed, Non-NULL:successed
     */
    ADM_COM_SSL_CTX_T *adm_com_sslCtxNew(const ADM_COM_SSL_CTX_OPT_T *param);

    /**
     * @brief 释放context上下文
     * @note 此接口用于释放context上下文，清理释放资源。
     *
     * @param opt    [In] ADM_COM_SSL_CTX_T*     SSL连接上下文
     *
     * @return void
     */
    void adm_com_sslCtxFree(ADM_COM_SSL_CTX_T *cssl_ctx);

    /**
     * @brief 创建SSL连接，并存储管理需要进行保存的SSL连接数据
     * @note 此接口用于创建SSL连接，根据SSL context上下文初始化当前SSL连接信息。
     *
     * @param cssl_ctx    [In] ADM_COM_SSL_CTX_T*     进行SSL连接初始化的SSL context上下文
     * @param fd         [In] *     建立TCP连接成功创建的socket套接字
     * @return ADM_COM_SSL_T*     NULL:failed, Non-NULL:successed
     */
    ADM_COM_SSL_T *adm_com_sslNew(ADM_COM_SSL_CTX_T *cssl_ctx, D32S fd);

    /**
     * @brief 释放SSL连接
     * @note 此接口用于释放SSL连接，清理释放指定SSL连接资源。
     *
     * @param cssl    [In] ADM_COM_SSL_T*     需要进行释放的SSL连接
     * @return void
     */
    void adm_com_sslFree(ADM_COM_SSL_T *cssl);

    /**
     * @brief 服务端监听等待客户端连接，初始化SSL握手
     * @note  依赖于TCP连接的建立并配置成功。
     *        IO配置影响：
     *        如果是非阻塞IO，需要根据返回值来重复调用该接口进行完整的握手。
     *        返回值为ADM_SSL_WANT_READ或ADM_SSL_WANT_WRITE时，需要重复调用。
     *        如果是阻塞IO，只需要调用一次即可完成完整的握手动作。
     * @param cssl    [In] ADM_COM_SSL_T*     监听客户端进行连接的服务端SSL连接
     * @return D32S
     * ADM_SSL_OK  握手流程结束，握手成功，
     * ADM_SSL_WANT_READ 阻塞IO情况下，表示需要接收报文以完成握手
     * ADM_SSL_WANT_WRITE 阻塞IO情况下，表示报文未完成发送，需要继续调用以完成握手
     * other       握手发生异常，可调用adm_com_ssl_strerror接口来获取详细信息
     */
    D32S adm_com_sslAccept(ADM_COM_SSL_T *cssl);

    /**
     * @brief 客户端初始化SSL握手连接服务端
     * @note  依赖于TCP连接的建立并配置成功。
     *        IO配置影响：
     *        如果是非阻塞IO，需要根据返回值来重复调用该接口进行完整的握手。
     *        返回值为ADM_SSL_WANT_READ或ADM_SSL_WANT_WRITE时，需要重复调用。
     *        如果是阻塞IO，只需要调用一次即可完成完整的握手动作。
     * @param cssl    [In] ADM_COM_SSL_T*     监听客户端进行连接的服务端SSL连接
     * @return D32S
     * ADM_SSL_OK  握手流程结束，握手成功，
     * ADM_SSL_WANT_READ 阻塞IO情况下，表示还需要继续进行调用完成握手
     * ADM_SSL_WANT_WRITE 阻塞IO情况下，表示报文未完成发送，需要继续调用以完成握手
     * other       握手发生异常，可调用adm_com_ssl_strerror接口来获取详细信息
     */
    D32S adm_com_sslConnect(ADM_COM_SSL_T *cssl);

    /**
     * @brief 建立握手成功后，此接口用于接收数据
     * @note  依赖于握手成功，读取指定长度的数据，并返回成功读取的数据长度。
     * @param cssl    [In] ADM_COM_SSL_T*     握手成功的SSL连接
     * @param buf    [In] DCHAR*     用来接收数据的buffer
     * @param len    [In] D32S*     指定读取数据长度
     * @return D32S
     * >0  读取成功，返回实际读到的数据长度
     * <=0 读取失败，可调用adm_com_ssl_strerror接口来获取详细信息
     */
    D32S adm_com_sslRead(ADM_COM_SSL_T *cssl, DCHAR *buf, D32S len);

    /**
     * @brief 建立握手成功后，此接口用于发送数据
     * @note  依赖于握手成功，发送指定长度的数据，并返回成功发送的数据长度。
     * @param cssl    [In] ADM_COM_SSL_T*     握手成功的SSL连接
     * @param buf    [In] DCHAR*     用来发送数据的buffer
     * @param len    [In] D32S*     指定发送数据长度
     * @return D32S
     * >0  发送成功，返回实际发送的数据长度
     * <=0 发送失败，可调用adm_com_ssl_strerror接口来获取详细信息
     */
    D32S adm_com_sslWrite(ADM_COM_SSL_T *cssl, const DCHAR *buf, D32S len);

    /**
     * @brief 关闭SSL连接
     * @note 此接口用于主动关闭SSL连接，发送close_notify的alert到对端。
     *       对于阻塞IO，需要判断返回值来确定是否关闭完成。
     * @param cssl    [In] ADM_COM_SSL_T*     需要进行关闭的SSL连接
     * @warning 异常的SSL连接状态时，不能调用此函数进行关闭
     * @return D32S
     * 0  关闭没完成，需要调用读取接口获取对端的关闭请求
     * 1  关闭完成，发送和接收到关闭请求。
     * <0 关闭异常，非阻塞情况需要根据该值确定是否需要再次调用
     */
    D32S adm_com_sslClose(ADM_COM_SSL_T *cssl);

#ifdef __cplusplus
}
#endif
#endif // ADM_COM_SSLADAPTER_H

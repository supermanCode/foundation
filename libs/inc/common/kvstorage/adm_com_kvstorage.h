/**
 * @file adm_com_kvstorage.h
 * @author mengwei@abupdate.com
 * @brief 定义通用的kvstorage接口，包括：是否加密存储
 * @version 0.1
 * @date 2022-01-10
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef __ADM_COM_KV_STORAGE_H__
#define __ADM_COM_KV_STORAGE_H__

#include "adm_typedefs.h"
#include "adm_kvStorage_c.h"
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

#define EXPORT_FUN __attribute__((visibility("default")))

/**
 * @brief kvstorage 公共接口错误码
 * 
 */
typedef enum ADM_COM_KVSTORAGE_ERR_
{
    ADM_COM_KVSTORAGE_ERR_DEFAULT = -1,     //!< 默认错误
    ADM_COM_KVSTORAGE_ERR_SUCCESSED = 0     //!< 成功
} ADM_COM_KVSTORAGE_ERR_E;


typedef struct adm_kvStorage_ KvStorage_t;

/**
 * @brief 定义加密的回调接口
 * @note 
 *   1. Return: 0-Successed, none-0-Failed;
 *   2. in: 待加密的字符串， inLen: 待加密的字符串长度
 *   3. out: 加密后的字符串， outLen: 加密后的字符串长度
 *   4. out: 需要在回调函数内部分配内存，调用者释放内存
 */
typedef D32S (*ADM_COM_KVSTORAGE_ENCRYPT_CALLBACK_F)(const DCHAR* in, const D32U inLen, DCHAR** out, D32U* outLen);  //!<  加密接口

/**
 * @brief 定义解密的回调接口
 * @note 
 *   1. Return: 0-Successed, none-0-Failed;
 *   2. in: 待解密的字符串， inLen: 待解密的字符串长度
 *   3. out: 解密后的字符串， outLen: 解密后的字符串长度
 *   4. out: 需要在回调函数内部分配内存，调用者释放内存
 */
typedef D32S (*ADM_COM_KVSTORAGE_DECRYPT_CALLBACK_F)(const DCHAR* in, const D32U inLen, DCHAR** out, size_t* outLen);  //!<  解密接口

/**
 * @brief 用于打开kvstorage数据库
 * 
 * @param databaseName  [IN]    kvstorage数据库存储目录，绝对路径
 * @return KvStorage_t*     NULL:Failed, None-NULL: kvstorage句柄
 */
extern KvStorage_t* adm_com_kvstorage_open(const char* databaseName);

/**
 * @brief 修复kvstorage数据库
 * 
 * @param pDbName  [IN]    kvstorage数据库存储目录，绝对路径
 * @return DBOOL 
 */
extern DBOOL adm_com_kvstorage_repare(const char* pDbName);

/**
 * @brief 用于摧毁kvstorage数据库，即删除数据库
 * 
 * @param kvs   [IN]    kvStorage 数据库句柄
 * @return DBOOL 
 */
extern DBOOL adm_com_kvstorage_destroy(KvStorage_t* kvs);

/**
 * @brief 用于关闭打开的kvstorage数据库句柄
 * 
 * @param kvs   [IN]    kvstorage数据库句柄
 */
extern void adm_com_kvstorage_close(KvStorage_t* kvs);

/**
 * @brief 用于判断此kvstorage数据库中是否有此key
 * 
 * @param kvs [IN]  kvstorage数据库句柄
 * @param key [IN]  key字段
 * @return DBOOL    TRUE: 存在，FALSE: 不存在
 */
extern DBOOL adm_com_kvstorage_has_key(KvStorage_t* kvs, const char* key);

/**
 * @brief 用于删除kvstorage数据库中key对应的信息
 * 
 * @param kvs [IN]  kvstorage数据库句柄
 * @param key [IN]  key字段
 * @return DBOOL    TRUE: 成功，FALSE: 失败
 */
extern DBOOL adm_com_kvstorage_remove_key(KvStorage_t* kvs, const char* key);

/**
 * @brief 用于存储key对应的value信息
 * 
 * @param kvs [IN]  kvstorage数据库句柄
 * @param key [IN]  key字段
 * @param val [IN]  key字段对应的value
 * @param eValType [IN] key字段的存储的value数据类型
 * @return DBOOL    TRUE: 成功，FALSE: 失败
 */
extern DBOOL adm_com_kvstorage_set_string(KvStorage_t* kvs, const char* key, const char* val, const ADM_KV_STORAGE_VALUE_TYPE_E eValType);

/**
 * @brief 用于获取key对应的value信息
 * 
 * @param kvs [IN]  kvstorage数据库句柄
 * @param key [IN]  key字段
 * @param val [OUT] key字段对应的value值
 * @param length [OUT]  key字段对应的value值的长度
 * @param eValType [OUT]  key字段对应的value值的类型
 * @return DBOOL    TRUE: 成功，FALSE: 失败
 */
extern DBOOL adm_com_kvstorage_get_string(KvStorage_t* kvs, const char* key, char** val, size_t* length, ADM_KV_STORAGE_VALUE_TYPE_E* eValType);

/**
 * @brief 获取所有的key列表
 * @note 
 *  1. 返回值为true时，key_list 使用结束后需要释放内存空间
 * 
 * @param kvs       [IN]    kv句柄
 * @param keyList  [OUT]   key 数组
 * @return true     获取成功
 * @return false    获取失败
 */
extern DBOOL adm_com_kvstorage_list_key(KvStorage_t* kvs, adm_kvStorage_all_key_t** keyList);

/**
 * @brief 通用的kv存储初始化接口
 * @note 
 *   1. 设置kvstorage使用的加解密函数;
 *   2. 加解密函数可以为空或同时不为空，且为空时二者必须同时为空;
 *   3. 加解密函数为空时，默认不加密;
 *   4. 加解密函数不为空时，默认需要加密
 * @param encCb [IN]    加密的回调接口
 * @param decCb [IN]    解密的回调接口
 */
extern void adm_com_kvstorageInit(const ADM_COM_KVSTORAGE_ENCRYPT_CALLBACK_F encCb, const ADM_COM_KVSTORAGE_DECRYPT_CALLBACK_F decCb);

/**
 * @brief 通用的kvstorage反初始化接口
 * @note 
 *   1. 主要将kvstorage需要的加解密函数置空
 * 
 */
extern void adm_com_kvstorageDeinit(void);


#ifdef __cplusplus  // close out "C" linkage in case of c++ compiling
}
#endif

#endif//__ADM_COM_KV_STORAGE_H__
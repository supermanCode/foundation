#ifndef ADM_PERSIST_STORAGE_H__
#define ADM_PERSIST_STORAGE_H__

#include "adm_data_stream.h"


#ifdef __cplusplus
extern "C"
{
#endif  // __cplusplus


typedef void ADM_STORAGE_SECURITY_T;

typedef struct ADM_STORAGE_SECURITY_SUITE_
{
    ADM_STORAGE_SECURITY_T* pSecurity;
    D32S(*fnEncrypt)(ADM_STORAGE_SECURITY_T* pSecurity, const D8U* pInData, D32U uiInDataLen, ADM_DATA_STREAM_T* pOut);
    D32S(*fnDecrypt)(ADM_STORAGE_SECURITY_T* pSecurity, const D8U* pInData, D32U uiInDataLen, ADM_DATA_STREAM_T* pOut);
} ADM_STORAGE_SECURITY_SUITE_T;


#ifdef __cplusplus
}
#endif  // __cplusplus

#endif // ADM_PERSIST_STORAGE_H__

#ifndef ADM_KV_PERSIST_STORAGE_H__
#define ADM_KV_PERSIST_STORAGE_H__

#include "adm_persist_storage.h"


#ifdef __cplusplus
extern "C"
{
#endif  // __cplusplus


typedef struct
{
    void* _v[3];
    ADM_DATA_STREAM_T   _s;
} ADM_KV_PERSIST_STORAGE_T;

D32S adm_kv_persistStorageInit(ADM_KV_PERSIST_STORAGE_T* pStorage, const DCHAR* pDbName, ADM_STORAGE_SECURITY_SUITE_T* pSecuritySuite);
void adm_kv_persistStorageDestroy(ADM_KV_PERSIST_STORAGE_T* pStorage);

#ifdef __cplusplus
}
#endif  // __cplusplus

#endif // ADM_KV_PERSIST_STORAGE_H__
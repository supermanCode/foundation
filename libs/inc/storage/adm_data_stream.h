#ifndef ADM_DATA_STREAM_H__
#define ADM_DATA_STREAM_H__

#include "adm_typedefs.h"


#ifdef __cplusplus
extern "C"
{
#endif  // __cplusplus


typedef struct
{
    void*   _d;
    D32U    _u[2];
} ADM_DATA_STREAM_T;


void adm_dataStream_init(ADM_DATA_STREAM_T* pDataStream);
void adm_dataStream_destroy(ADM_DATA_STREAM_T* pDataStream);
D32S adm_dataStream_setData(ADM_DATA_STREAM_T* pDataStream, const D8U* pData, D32U uiDataLen);
const D8U* adm_dataStream_getData(ADM_DATA_STREAM_T* pDataStream, D32U* puiDataLen);
D32S adm_dataStream_setString(ADM_DATA_STREAM_T* pDataStream, const DCHAR* pStr);
const DCHAR* adm_dataStream_getString(ADM_DATA_STREAM_T* pDataStream);


#ifdef __cplusplus
}
#endif  // __cplusplus


#endif // ADM_DATA_STREAM_H__

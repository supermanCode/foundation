#pragma once

#include "adm_typedefs.h"
#include "DataStream.h"




class IPersistStorage
{
public:
    virtual D32S setString(const DCHAR* pKey, const DCHAR* pValue) = 0;
    virtual const DCHAR* getString(const DCHAR* pKey) = 0;
    virtual void clear() = 0;
    // virtual D32S remove(const DCHAR* pKey) = 0;
};


class IStorgeSecurity
{
public:
    virtual D32S decryptAsStr(const D8U* pInData, D32U uiInDataLen, DataStream& dataStream) = 0;
    virtual D32S encryptAsStr(const D8U* pInData, D32U uiInDataLen, DataStream& dataStream) = 0;
};


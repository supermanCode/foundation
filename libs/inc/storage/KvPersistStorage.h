#ifndef KV_PERSIST_STORAGE_H__
#define KV_PERSIST_STORAGE_H__

#include "IPersistStorage.h"
#include "kvstorage/adm_kvStorage_c.h"
#include "adm_persist_storage.h"

// #include "adm_com_errorCode.h"

#include <string>

#include "adm_data_stream.h"




class KvStorageSecurity : public IStorgeSecurity
{
public:
    D32S init(const ADM_STORAGE_SECURITY_SUITE_T* pSuite);
    D32S decryptAsStr(const D8U* pInData, D32U uiInDataLen, DataStream& dataStream);
    D32S encryptAsStr(const D8U* pInData, D32U uiInDataLen, DataStream& dataStream);

private:
    ADM_STORAGE_SECURITY_SUITE_T    _suite;
};


class KvPersistStorage : public IPersistStorage
{
public:
    KvPersistStorage();
    ~KvPersistStorage();

    D32S init(const DCHAR* pDbName, ADM_STORAGE_SECURITY_SUITE_T* pSecuritySuite = NULL);

    D32S setString(const DCHAR* pKey, const DCHAR* pValue);
    const DCHAR* getString(const DCHAR* pKey);
    D32S remove(const DCHAR* pKey);
    void clear();

private:
    adm_kvStorage_t*    _pStorage;
    KvStorageSecurity* _pKvSecurity;
    DataStream          _stream;
};


#endif  // KV_PERSIST_STORAGE_H__
#ifndef DATA_STREAM_H__
#define DATA_STREAM_H__

#include "adm_typedefs.h"


class DataStream
{
public:
    DataStream();
    ~DataStream();

    D32S setData(const D8U* pData, D32U uiDataLen);
    inline const D8U* getData() const;
    inline D32U getDataLength() const;

    D32S setAsString(const DCHAR* pStr);
    inline const DCHAR* getAsString() const;

private:
    D8U*    _pData;
    D32U    _uiDataLen;
    D32U    _uiDataBufSize;
};

const D8U* DataStream::getData() const
{
    return _pData;
}

D32U DataStream::getDataLength() const
{
    return _uiDataLen;
}

const DCHAR* DataStream::getAsString() const
{
    return (DCHAR*)_pData;
}

#endif // DATA_STREAM_H__

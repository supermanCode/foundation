#ifndef ADM_CONCURRENT_H
#define ADM_CONCURRENT_H

#include <future>
#include <tuple>
#include <type_traits>
#include <utility>

#include "threadPool.h"

namespace abup
{
namespace ota
{
namespace concurrent
{

template <typename Function, typename... Args>
class InvokeResultT
{
public:
    using type = typename std::result_of<Function(Args...)>::type;
};

template <typename Function, typename... Args>
using InvokeResult = typename InvokeResultT<Function, Args...>::type;

class ConcurrentTaskManager final
{
public:
    ConcurrentTaskManager(uint32_t maxTaskCapacities = mKDefaultMaxTaskCapacities)
        : mThreadPool(maxTaskCapacities)
    {}
    ~ConcurrentTaskManager() = default;

    template <typename Callable, typename... Args>
    auto post(Callable&& func, Args&&... args) -> std::future<InvokeResult<Callable, Args...>>
    {
        return mThreadPool.enqueue(std::forward<Callable>(func), std::forward<Args>(args)...);
    }

private:
    static const int mKDefaultMaxTaskCapacities = 20;
    ThreadPool mThreadPool;
};

template <typename Callable, typename... Args>
auto async(Callable&& func, Args&&... args) -> std::future<InvokeResult<Callable, Args...>>
{
    static ConcurrentTaskManager tm;
    return tm.post(std::forward<Callable>(func), std::forward<Args>(args)...);
}

}  // namespace concurrent
}  // namespace ota
}  // namespace abup

#endif
#pragma once

#include "adm_typedefs.h"
#include "adm_memory.h"

#include "adm_sm_cfg.h"

#ifdef __cplusplus
extern "C" {
#endif

/** 状态枚举 */
typedef enum ADM_SM_STATE_ENUM
{
    ADM_SM_STATE_IDLE,
    ADM_SM_STATE_RUN,
    ADM_SM_STATE_WAITSTOP,
    ADM_SM_STATE_STOP,
    ADM_SM_STATE_EXIT,
    ADM_SM_STATE_INVALID = 0xFF,
} ADM_SM_STATE_EN;

enum ADM_SM_ERRORCODE_ENUM
{
    ADM_SM_OK,
    ADM_SM_ERR_INVALIDHANDLE,
    ADM_SM_ERR_INVALIDSTEP,
    ADM_SM_ERR_BUSY,
    ADM_SM_THREADCREATEERROR
};

enum SM_RESULT_ENUM
{
    SM_RESULT_OK,
    SM_RESULT_ERROR,
};

/* 状态机处理函数定义 */
typedef D32S(*ADM_SM_HANDLE_FN3)(void* pParam1, void* pParam2, void* pParam3);

/**
 * @struct    adm_sm_result_struct
 * @brief     状态机处理函数结果
 * @details   detail description
 */
typedef struct adm_sm_result_struct
{
    D32U resultCode;    ///< 状态机处理函数结果
    D32U stateID;       ///< 状态机下一状态ID
    D32U stateSubID;    ///< 状态机下一状态子ID
} adm_sm_result_t, *adm_sm_result_pt;

/**
 * @struct    adm_sm_step_struct
 * @brief     状态机处理函数
 * @details   detail description
 */
typedef struct adm_sm_step_struct
{
    D32U stateStepID;
    D32U stateSubStepID;
    ADM_SM_HANDLE_FN3 fnHandle;    ///< 状态机处理函数
    adm_sm_result_t resultJump[ADM_SM_MAXRESULTJUMP];  ///< 状态数组,根据状态机处理函数结果切换
} adm_sm_step_t, *adm_sm_step_pt;


extern void adm_sm_init();
extern void adm_sm_uninit();

extern ADM_SM_STATE_EN adm_sm_getState(DHANDLE smHandle);
extern DHANDLE adm_sm_createHandle(const char *smName, const adm_sm_step_t *pSMItems,
                                   D32U itemNumber);
extern D32S adm_sm_destoryHandle(DHANDLE smHandle);
extern D32S adm_sm_reset(DHANDLE smHandle);

//adm_pt修改void*
extern D32S adm_sm_startAsync(DHANDLE smHandle, DHANDLE *hThread, D32U startIndex, void* pParam1,
                              void* pParam2, void* pParam3);
extern D32S adm_sm_start(DHANDLE smHandle, D32U startIndex, void* pParam1, void* pParam2,
                         void* pParam3);
extern D32S adm_sm_stop(DHANDLE smHandle);

#ifdef __cplusplus  // close out "C" linkage in case of c++ compiling
}
#endif



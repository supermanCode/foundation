#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#define ADM_SM_MAXRESULTJUMP        4
#define ADM_SM_PARAMNUMBER          4

#define ADM_SM_NAMELENGTH           32

#define ADM_SM_INVALIDCODE          ((D32U)(-1))

#ifdef __cplusplus  // close out "C" linkage in case of c++ compiling
}
#endif


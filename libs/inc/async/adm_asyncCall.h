#ifndef ADM_ASYNCCALL_H
#define ADM_ASYNCCALL_H

#include <string.h>
#include <functional>
#include <stdint.h>
#include <memory>

namespace abup
{
namespace ota
{
namespace async
{

typedef uint16_t service_t;
typedef uint16_t instance_t;
typedef uint16_t event_t;
typedef uint16_t eventgroup_t;

typedef std::function<void(const std::string& eventMessage)> eventMessageCB;

class AsyncCallImpl;

class AsyncCall
{
public:
    AsyncCall(const size_t threads);
    ~AsyncCall();
    /*Register the callback function to bind to the serviceId instanceId eventId*/
    void Attach(const service_t serviceId, const instance_t instanceId, const event_t eventId, eventMessageCB cb);

    /*Unregister serviceId instanceId eventId*/
    void Detach(const service_t serviceId, const instance_t instanceId, const event_t eventId);
    /*Unregister all serviceId instanceId*/
    void Detach(const service_t serviceId, const instance_t instanceId);
    /*Unregister all serviceId*/
    void Detach(const service_t serviceId);

    /*async handle eventMessage*/
    void AddEventMessageToCb(const service_t serviceId,
        const instance_t instanceId,
        const event_t eventId,
        const std::string& eventMessage);

private:
    /* data */
    std::shared_ptr<AsyncCallImpl> impl_;
};

}  // namespace async
}  // namespace ota
}  // namespace abup

#endif
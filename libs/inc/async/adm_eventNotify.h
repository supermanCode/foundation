#ifndef ADM_EVENTNOTIFY_H
#define ADM_EVENTNOTIFY_H

#include <mutex>
#include <condition_variable>

namespace abup
{
namespace ota
{
namespace async
{

class EventNotify
{
public:
    EventNotify();
    ~EventNotify();

    void notify();
    void NotifyReset();
    /*waitFor second*/
    /*return: true:success false:timeout*/
    bool waitFor(const uint16_t timeout);

private:
    std::mutex mutex_;
    std::condition_variable condition_;
    bool waitFlag_;
};

}  // namespace async
}  // namespace ota
}  // namespace abup

#endif
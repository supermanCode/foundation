#ifndef ADM_FUTUREWAITING_H
#define ADM_FUTUREWAITING_H

#include <future>
#include <mutex>
#include <map>
#include <string>
#include <memory>

namespace abup
{
namespace ota
{
namespace async
{
typedef uint16_t service_t;
typedef uint16_t instance_t;
typedef uint16_t event_t;
typedef uint16_t reqId_t;

class FutureWaitingCenter
{
public:
    FutureWaitingCenter();

    ~FutureWaitingCenter();

    /*生成future,如果是相同ID的future，则会丢弃前一次的future设置为空值,只保留最后一次的future*/
    std::future<std::string> makeFuture(const service_t serviceId, const instance_t instanceId, const event_t eventId, const reqId_t reqId);

    /*给future设置值,当返回false时 errorStr有错误信息*/
    bool setValue(const service_t serviceId,
        const instance_t instanceId,
        const event_t eventId,
        const reqId_t reqId,
        const std::string& data,
        std::string* errorStr);

    /*给future设置空值,当返回false时 errorStr有错误信息*/
    bool setEmptyValue(const service_t serviceId,
        const instance_t instanceId,
        const event_t eventId,
        const reqId_t reqId,
        std::string* errorStr);

private:
    std::mutex mMutex;
    std::map<service_t, std::map<instance_t, std::map<event_t, std::map<reqId_t, std::unique_ptr<std::promise<std::string>>>>>>
        mPromiseLists;
};

}  // namespace async
}  // namespace ota
}  // namespace abup

#endif
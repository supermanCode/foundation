#ifndef ADM_KV_STORAGE_H
#define ADM_KV_STORAGE_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义存储到kv中的数据类型，供调用者明确存储和获取到的数据的类型
 *
 */
typedef enum
{
    ADM_KV_STORAGE_VALUE_TYPE_UINT8 = 1,  //!< uint8
    ADM_KV_STORAGE_VALUE_TYPE_INT8 = 2,  //!< int8
    ADM_KV_STORAGE_VALUE_TYPE_UINT16 = 3,  //!< uint16
    ADM_KV_STORAGE_VALUE_TYPE_INT16 = 4,  //!< int16
    ADM_KV_STORAGE_VALUE_TYPE_UINT32 = 5,  //!< uint32
    ADM_KV_STORAGE_VALUE_TYPE_INT32 = 6,  //!< int32
    ADM_KV_STORAGE_VALUE_TYPE_UINT64 = 7,  //!< uint64
    ADM_KV_STORAGE_VALUE_TYPE_INT64 = 8,  //!< int64
    ADM_KV_STORAGE_VALUE_TYPE_FLOAT = 9,  //!< float
    ADM_KV_STORAGE_VALUE_TYPE_DOUBLE = 10,  //!< double
    ADM_KV_STORAGE_VALUE_TYPE_STRING = 11,  //!< string
    ADM_KV_STORAGE_VALUE_TYPE_JSON = 12,  //!< json
    ADM_KV_STORAGE_VALUE_TYPE_INVALID,  //!< invalid
} ADM_KV_STORAGE_VALUE_TYPE_E;

typedef struct adm_kvStorage_ adm_kvStorage_t;

extern adm_kvStorage_t* adm_kvStorage_open(const char* databaseName, char** errorStr);
extern bool adm_kvstorage_repare(const char* pDbName, char** errorStr);
extern bool adm_kvStorage_destroy(adm_kvStorage_t* kvs, char** errorStr);
extern void adm_kvStorage_close(adm_kvStorage_t* kvs);

extern bool adm_kvStorage_has_key(adm_kvStorage_t* kvs, const char* key, char** errorStr);
extern bool adm_kvStorage_remove_key(adm_kvStorage_t* kvs, const char* key, char** errorStr);
extern bool adm_kvStorage_remove_allKeys(adm_kvStorage_t* kvs, char** errorStr);
extern bool adm_kvStorage_set_string(adm_kvStorage_t* kvs,
    const char* key,
    const char* val,
    const ADM_KV_STORAGE_VALUE_TYPE_E eValType,
    char** errorStr);

extern bool adm_kvStorage_get_string(adm_kvStorage_t* kvs,
    const char* key,
    char** val,
    ADM_KV_STORAGE_VALUE_TYPE_E* eValType,
    char** errorStr);

typedef struct adm_kvStorage_all_key_
{
    int num;
    char* listKeys[0];
} adm_kvStorage_all_key_t;

/**
 * @brief 获取所有的key列表
 * @note
 *  1. 返回值为true时，key_list 使用结束后需要释放内存空间
 *
 * @param kvs       [IN]    kv句柄
 * @param keyList   [OUT]   key列表
 * @param errorStr  [OUT]   如果出错，输出错误信息，调用者回收内存
 * @return true     获取成功
 * @return false    获取失败
 */
extern bool adm_kvStorage_get_allKeys(adm_kvStorage_t* kvs, adm_kvStorage_all_key_t** keyList, char** errorStr);

#ifdef __cplusplus
}
#endif

#endif  // KEY_VALUE_STORAGE_H

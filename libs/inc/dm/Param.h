#ifndef PARAM_H_
#define PARAM_H_


#include "adm_typedefs.h"
#include <map>
#include "StringDict.h"


namespace vus
{
    namespace dm
    {
        class ParamList
        {
        public:
            // D32S init(D32U uiCount);
            D32S setValue(const DCHAR* pName, const DCHAR* pValue);
            // D32S setValue(const DCHAR* pName, D32U uiValue);
            // D32S getValue(const DCHAR* pName, const D64S& iValue);
            // D32S getValue(const DCHAR* pName, const D64U& uiValue);
            D32S getValue(const DCHAR* pName, const DCHAR*& pValue);
            // DCHAR* getValue(D32U uiIndex);

        private:
            typedef std::map<const DCHAR*, const DCHAR*>    _params;

            StringDict  _dict;
        };
    }
}

#endif  // PARAM_H_
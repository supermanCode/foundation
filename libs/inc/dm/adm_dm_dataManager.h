﻿#ifndef ADM_DM_DATA_MANAGER_H_
#define ADM_DM_DATA_MANAGER_H_


#include "adm_typedefs.h"
#include "adm_dm_dataType.h"
#include "adm_dm_dataRule.h"
#include "adm_dm_staticDataRule.h"
#include "adm_dm_jsonDataRule.h"
#include "storage/adm_kv_persistStorage.h"


#ifdef __cplusplus
extern "C"
{
#endif  // __cplusplus


/**
 * @brief DM数据参数列表
 * 
 */
typedef struct ADM_DM_PARAM_LIST_T ADM_DM_PARAM_LIST_T;

/**
 * @brief 创建DM数据参数列表
 * 
 * @return ADM_DM_PARAM_LIST_T* DM数据参数列表对象，NULL失败
 */
ADM_DM_PARAM_LIST_T* adm_dm_paramListCreate();
/**
 * @brief 销毁DM数据参数列表对象
 * 
 * @param pParamList    DM数据参数列表对象
 */
void adm_dm_paramListDestory(ADM_DM_PARAM_LIST_T* pParamList);
/**
 * @brief DM数据参数列表添加参数
 * 
 * @param pParamList    DM数据参数列表对象
 * @param pName         参数名称
 * @param pValue        参数值
 * @return D32S         返回值，[0]成功 [!0]失败
 */
D32S adm_dm_paramListAdd(ADM_DM_PARAM_LIST_T* pParamList, const DCHAR* pName, const DCHAR* pValue);


/**
 * @brief Data Manager
 * 
 */
typedef struct ADM_DM_DATA_MANAGER_T    ADM_DM_DATA_MANAGER_T;

/**
 * @brief 创建数据管理
 * 
 * @return ADM_DM_DATA_MANAGER_T*   创建数据管理对象，NULL失败
 */
ADM_DM_DATA_MANAGER_T* adm_dm_create();

/**
 * @brief 销毁数据管理对象
 * 
 * @param pManager  数据管理对象
 */
void adm_dm_destroy(ADM_DM_DATA_MANAGER_T* pManager);

/**
 * @brief 初始化数据管理
 * 
 * @param pManager  数据管理对象
 * @param pStorage  持久化存储对象，NULL表示不需要持久化存储
 * @return D32S     返回值，[0]成功 [!0]失败
 */
D32S adm_dm_init(ADM_DM_DATA_MANAGER_T* pManager, ADM_KV_PERSIST_STORAGE_T* pStorage);

/**
 * @brief 加载数据管理持久存储数据
 * 
 * @param pManager  数据管理对象
 * @return D32S     返回值，[0]成功 [!0]失败
 */
D32S adm_dm_load(ADM_DM_DATA_MANAGER_T* pManager);

// /**
//  * @brief 添加DM数据规则
//  * 
//  * @param pManager      数据管理对象
//  * @param pRootName     数据规则根名称
//  * @param ppRuleItem    数据规则项
//  * @return D32S         返回值，[0]成功 [!0]失败
//  */
// D32S adm_dm_addDataRule(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pRootName, ADM_DM_DATA_RULE_ITEM_T** ppRuleItem);
// /**
//  * @brief 添加DM数据规则
//  * 
//  * @param pManager      数据管理对象
//  * @param pRootName     数据规则根名称
//  * @param pTypeDefine   数据类型定义
//  * @return D32S         返回值，[0]成功 [!0]失败
//  */
// D32S adm_dm_addDataRuleByDef(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pRootName, const ADM_DM_DATA_TYPE_DEF_T* pTypeDefinition);
/**
 * @brief 添加DM数据规则
 * 
 * @param pManager          数据管理对象
 * @param pDescriptionStr   数据规则描述字符串
 * @param pRootName         数据规则根名称
 * @return D32S             返回值，[0]成功 [!0]失败
 */
D32S adm_dm_addDataRuleByDesc(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pDescriptionStr);
/**
 * @brief 设置DM数据
 * 
 * @param pManager      数据管理对象
 * @param pDataPath     数据路径
 * @param pInData       输入数据
 * @param uiInDataSize  数据长度
 * @return D32S         返回值，[0]成功 [!0]失败
 */
D32S adm_dm_setData(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pDataPath, void* pInData, D32U uiInDataSize);
/**
 * @brief 设置DM数据
 * 
 * @param pManager      数据管理对象
 * @param pDataPath     数据路径
 * @param pParamList    数据路径参数
 * @param pInData       输入数据
 * @param uiInDataSize  输入数据长度
 * @return D32S         返回值，[0]成功 [!0]失败
 */
D32S adm_dm_setDataWithParam(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pDataPath, const ADM_DM_PARAM_LIST_T* pParamList, void* pInData, D32U uiInDataSize);
/**
 * @brief 查询DM数据
 * 
 * @param pManager      数据管理对象
 * @param pDataPath     数据路径
 * @param pOutBuf       输出缓存区
 * @param uiOutBufSize  输出缓存区大小
 * @return D32S         返回值，[0]成功 [!0]失败
 */
D32S adm_dm_queryData(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pDataPath, void* pOutBuf, D32U uiOutBufSize);
/**
 * @brief 查询DM数据
 * 
 * @param pManager      数据管理对象
 * @param pDataPath     数据路径
 * @param pParamList    数据路径参数
 * @param pOutBuf       输出缓存区
 * @param uiOutBufSize  输出缓存区大小
 * @return D32S         返回值，[0]成功 [!0]失败
 */
D32S adm_dm_queryDataWithParam(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pDataPath, const ADM_DM_PARAM_LIST_T* pParamList, void* pOutBuf, D32U uiOutBufSize);
/**
 * @brief 销毁DM数据对象
 * 
 * @param pManager      数据管理对象
 * @param pDataPath     数据路径
 * @param pData         输入数据
 * @param uiDataSize    输入数据长度
 * @return D32S 
 */
D32S adm_dm_destroyData(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pDataPath, void* pData, D32U uiDataSize);
/**
 * @brief 删除DM中数据
 * 
 * @param pManager      数据管理对象
 * @param pDataPath     DM数据路径
 * @return D32S         返回值，[0]成功 [!0]失败
 */
D32S adm_dm_remove(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pDataPath);
/**
 * @brief 删除DM中数据
 * 
 * @param pManager      数据管理对象
 * @param pDataPath     DM数据路径
 * @param pParamList    参数列表
 * @return D32S         返回值，[0]成功 [!0]失败
 */
D32S adm_dm_removeWithParam(ADM_DM_DATA_MANAGER_T* pManager, const DCHAR* pDataPath, const ADM_DM_PARAM_LIST_T* pParamList);
/**
 * @brief 获取KV持久化存储对象
 * 
 * @param pManager      数据管理对象
 * @return ADM_KV_PERSIST_STORAGE_T*    KV持久化存储对象
 */
ADM_KV_PERSIST_STORAGE_T* adm_dm_kvStorageGet(ADM_DM_DATA_MANAGER_T* pManager);
/**
 * @brief 通过静态数据规则设置数据
 * 
 * @param pManager      数据管理对象
 * @param pRule         静态数据规则
 * @param pInData       输入数据
 * @param uiInDataSize  输入数据长度
 * @return D32S         返回值，[0]成功 [!0]失败
 */
D32S adm_dm_setDataBySdr(ADM_DM_DATA_MANAGER_T* pManager, const ADM_DM_STATIC_DATA_RULE_T* pRule, void* pInData, D32U uiInDataSize);
/**
 * @brief 通过静态数据规则设置数据
 * 
 * @param pManager      数据管理对象
 * @param pRule         静态数据规则
 * @param pParamList    参数列表
 * @param pInData       输入数据
 * @param uiInDataSize  输入数据长度
 * @return D32S         返回值，[0]成功 [!0]失败
 */
D32S adm_dm_setDataWithParamBySdr(ADM_DM_DATA_MANAGER_T* pManager, const ADM_DM_STATIC_DATA_RULE_T* pRule, const ADM_DM_PARAM_LIST_T* pParamList, void* pInData, D32U uiInDataSize);
/**
 * @brief 通过静态数据规则查询数据
 * 
 * @param pManager      数据管理对象
 * @param pRule         静态数据规则
 * @param pOutBuf       输出数据缓存区
 * @param uiOutBufSize  输出数据缓存区大小
 * @return D32S         返回值，[0]成功 [!0]失败
 */
D32S adm_dm_queryDataBySdr(ADM_DM_DATA_MANAGER_T* pManager, const ADM_DM_STATIC_DATA_RULE_T* pRule, void* pOutBuf, D32U uiOutBufSize);
/**
 * @brief 通过静态数据规则查询数据
 * 
 * @param pManager      数据管理对象
 * @param pRule         静态数据规则
 * @param pParamList    参数列表
 * @param pOutBuf       输出数据缓存区
 * @param uiOutBufSize  输出数据缓存区大小
 * @return D32S         返回值，[0]成功 [!0]失败
 */
D32S adm_dm_queryDataWithParamBySdr(ADM_DM_DATA_MANAGER_T* pManager, const ADM_DM_STATIC_DATA_RULE_T* pRule, const ADM_DM_PARAM_LIST_T* pParamList, void* pOutBuf, D32U uiOutBufSize);
/**
 * @brief 销毁静态数据规则查询的数据
 * 
 * @param pManager      数据管理对象
 * @param pRule         静态数据规则
 * @param pData         数据
 * @return D32S         返回值，[0]成功 [!0]失败
 */
D32S adm_dm_destroyDataBySdr(ADM_DM_DATA_MANAGER_T* pManager, const ADM_DM_STATIC_DATA_RULE_T* pRule, void* pData, D32U uiDataSize);
/**
 * @brief 通过JSON数据规则设置数据
 * 
 * @param pManager      数据管理对象
 * @param pRule         JSON数据规则
 * @param pJsonstr      JSON字符串
 * @return D32S         返回值，[0]成功 [!0]失败
 */
D32S adm_dm_setDataByJdr(ADM_DM_DATA_MANAGER_T* pManager, const ADM_DM_JSON_DATA_RULE_T* pRule, const DCHAR* pJsonstr);


D32S adm_dm_setKvStorage(ADM_DM_DATA_MANAGER_T* pManager);

#ifdef __cplusplus
}
#endif  // __cplusplus

#endif  // ADM_DM_DATA_MANAGER_H_
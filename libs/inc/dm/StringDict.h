#ifndef STRING_DICT_H
#define STRING_DICT_H

#include "adm_typedefs.h"
#include <map>
#include "adm_string.h"

namespace vus
{
    namespace dm
    {
        class StringDict
        {
        public:
            ~StringDict();

            D32S add(const DCHAR* pKey, const DCHAR* pValue);
            const DCHAR* find(const DCHAR* pKey);
            D32S remove(const DCHAR* pKey);


        private:
            struct StringComparor
            {
                bool operator()(const DCHAR* a, const DCHAR* b) const
                {
                    return adm_strcmp(a, b) < 0;
                }
            };

            typedef std::map<const DCHAR*, const DCHAR*, StringComparor>   Map;
            typedef Map::iterator                          MapIter;
            typedef std::pair<const DCHAR*, const DCHAR*>  MapPair;

        public:
            class Iterator
            {
            public:
                Iterator(StringDict* pDict);

                DBOOL first();
                DBOOL next();

            public:
                const DCHAR* pKey;
                const DCHAR* pValue;

            private:
                Map* _pMap;
                MapIter _iter;
            };

        private:
            Map     _map;
        };
    }
}

#endif /* STRING_DICT_H */

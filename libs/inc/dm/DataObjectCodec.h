#ifndef DATA_OBJECT_CODEC_H_
#define DATA_OBJECT_CODEC_H_



#include "Object.h"
#include "datatype/DataStruct.h"
#include "datatype/DataStructCodec.h"
#include "datatype/DataStructCodecHelper.h"

namespace vus
{
    namespace dm
    {
        class DataObjectDecodeHelper : public DataStructDecodeHelper
        {
        public:
            DataObjectDecodeHelper(ObjectSetter* pSetter): _pSetter(pSetter){}

            D32S init(const DataTypeDef* pTypeDef, void* pData, void*& pOutData);
            D32S setAsInt(const DataTypeDef* pTypeDef, void* pData, D64S iValue);
            D32S setAsUInt(const DataTypeDef* pTypeDef, void* pData, D64U uiValue);
            D32S setAsReal(const DataTypeDef* pTypeDef, void* pData, DOUBLE dValue);
            D32S setAsString(const DataTypeDef* pTypeDef, void* pData, const DCHAR* pValue);
            D32S setNull(const DataTypeDef* pTypeDef, void* pData);
            D32S setArrayCount(const DataTypeDef* pTypeDef, void* pData, D32U uiCount);
            D32S getArrayMember(const DataTypeDef* pTypeDef, D32U uiIndex, void* pData, void*& pOutData);
            D32S getObjectMember(const DataTypeDef* pTypeDef, void* pData, void*& pOutData);

        private:
            Value* newValueNode(D32S iType);

        private:
            ObjectSetter* _pSetter;
        };


        class DataObjectEncodeHelper : public DataStructEncodeHelper
        {
        public:
            D32S init(const DataTypeDef* pTypeDef, void* pData, void*& pOutData);
            D32S asInt(const DataTypeDef* pTypeDef, void* pData, D64S& iValue);
            D32S asUInt(const DataTypeDef* pTypeDef, void* pData, D64U& uiValue);
            D32S asReal(const DataTypeDef* pTypeDef, void* pData, DOUBLE& dValue);
            D32S asString(const DataTypeDef* pTypeDef, void* pData, const DCHAR*& pValue);
            D32S getArrayCount(const DataTypeDef* pTypeDef, void* pData, D32U& uiCount);
            D32S getArray(const DataTypeDef* pTypeDef, void* pData, void*& pOutData);
            D32S getArrayMember(const DataTypeDef* pTypeDef, D32U uiIndex, void* pData, void*& pOutData);
            D32S getObjectMember(const DataTypeDef* pTypeDef, void* pData, void*& pOutData);

        private:
            DBOOL checkValueNode(const Value* pValue, const DataTypeDef* pTypeDef);
        };


        class DataObjectDecoder : public DataStructDecoder
        {
        public:
            DataObjectDecoder(ObjectSetter* pSetter) : DataStructDecoder(&_helper), _helper(pSetter){}

        private:
            DataObjectDecodeHelper  _helper;
        };


        class DataObjectEncoder : public DataStructEncoder
        {
        public:
            DataObjectEncoder() : DataStructEncoder(&_helper) {}

        private:
            DataObjectEncodeHelper  _helper;
        };
    }
}


#endif  // DATA_OBJECT_CODEC_H_
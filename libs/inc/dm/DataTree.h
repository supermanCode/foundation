#ifndef DATA_TREE_H_
#define DATA_TREE_H_


#include "Object.h"
#include "Param.h"
#include "datatype/DataType.h"
#include "datatype/DataStruct.h"
#include "datatype/StringUtil.h"
#include <map>
#include "DataPath.h"
#include "DataSearcher.h"
#include <string>
#include "storage/IPersistStorage.h"


using namespace vus;
namespace vus
{
    namespace dm
    {
        ValueType dataType2ObjectType(D32S iType);


        class DataDescriptionTree
        {
            friend class DataDescriptionTreeIterator;
        public:
            ~DataDescriptionTree();

            D32S add(const DCHAR* pDescriptionStr);

            const DataTypeDef* find(const DCHAR* pDataPath);
            const DataTypeDef* find(const DCHAR* pDataPath, const DataTypeDef* pType);
            const DataTypeDef* find(DataPathNode* pDataPathNode);
            const DataTypeDef* find(DataPathNode* pDataPathNode, const DataTypeDef* pType);


        private:
            const DataTypeDef* checkout(DataPathNode* pDataPathNode, const DataTypeDef* pType);
            const DataTypeDef* find(DataPath* pDataPath, const DataTypeDef* pType);


        private:
            typedef std::map<std::string, DataTypeDef*>    TypeMap;
            typedef TypeMap::iterator                       TypeMapIter;
            typedef std::pair<std::string, DataTypeDef*>   TypePair;

            TypeMap         _typeMap;
            std::string     _errorDescr;
        };

        class DataDescriptionTreeParser
        {
        public:
            static D32S parse(DataTypeDef* pType, const DCHAR* pDescriptionStr);
            static void destroyType(DataTypeDef* pType);
        };


        class DataTree;
        class DataTreeStorage
        {
        public:
            DataTreeStorage(DataTree* pDataTree, IPersistStorage* pKvStorage);

            D32S save(const DCHAR* pDataPath);
            // D32S load(const DCHAR* pKey);
            D32S load(const DCHAR* pRootName);
            D32S load();
            void cleanup();

            inline IPersistStorage* getPersitStorage() const;
        private:
            DataTree*           _pDataTree;
            IPersistStorage*    _pKvPersistStorage;
        };

        IPersistStorage* DataTreeStorage::getPersitStorage() const
        {
            return _pKvPersistStorage;
        }

        class JsonDataRule;
        class DataTree
        {
            friend class ObjectDataRemover;
            friend class ObjectDataSearcher;
            friend class ObjectDataGetter;
            friend class ObjectDataSetter;
            friend class DataTreeStorage;
        public:
            DataTree();
            ~DataTree();

            D32S init(IPersistStorage* pKvStorage = NULL);

            D32S setData(const DCHAR* pDataPath, const void* pData, D32U uiDataSize);
            D32S setData(const DCHAR* pDataPath, ParamList& paramList, const void* pData, D32U uiDataSize);
            D32S queryData(const DCHAR* pDataPath, void* pOut, D32U uiOutMaxSize);
            D32S queryData(const DCHAR* pDataPath, ParamList& paramList, void* pOut, D32U uiOutMaxSize);

            D32S removeData(const DCHAR* pDataPath);
            D32S removeData(const DCHAR* pDataPath, ParamList& paramList);
            Value* queryData(const DCHAR* pDataPath);
            Value* queryData(const DCHAR* pDataPath, ParamList& paramList);

            D32S destroyData(const DCHAR* pDataPath, void* pData, D32U uiDataSize);

            D32S queryData(const DCHAR* pDataPath, DataStruct* pDataStruct);
            D32S queryData(const DCHAR* pDataPath, ParamList* pParamList, DataStruct* pDataStruct);
            D32S destroyData(DataStruct* pDataStruct);

            D32S setData(const DCHAR* pJsonStr, JsonDataRule* pRule);

            inline DataDescriptionTree* getDescriptionTree();
            inline IPersistStorage* getKvStorage() const;
            D32S load();
            D32S clear();

        private:
            Value* queryData(const DCHAR* pDataPath, ParamList* pParamList);


        private:
            D32S queryData(const DCHAR* pDataPath, ParamList* pParamList, void* pOut, D32U uiOutMaxSize);

        private:
            Object                  _root;
            DataDescriptionTree     _descriptionTree;
            DataTreeStorage*        _pStorage;

        };


        DataDescriptionTree* DataTree::getDescriptionTree()
        {
            return &_descriptionTree;
        }

        IPersistStorage* DataTree::getKvStorage() const
        {
            return NULL != _pStorage ? _pStorage->getPersitStorage() : NULL;
        }


        class ObjectDataGetter
        {
        public:
            ObjectDataGetter(DataTree* pDataTree);

            inline void setVarSet(VarSet* pVarSet);
            Value* get(const DCHAR* pDataPath, const DataTypeDef*& pType);
            Value* get(const DCHAR* pDataPath, ParamList& paramList, const DataTypeDef*& pType);

            Value* get(const DCHAR* pDataPath, const DataTypeDef* pRootType, Value* pRoot, const DataTypeDef*& pType);
            Value* get(const DCHAR* pDataPath, const DataTypeDef* pRootType, Value* pRoot, ParamList& paramList, const DataTypeDef*& pType);
            Value* get(DataPathNode* pPathNode, const DataTypeDef* pRootType, Value* pRoot, const DataTypeDef*& pType);

            Value* get(const DCHAR* pDataPath, D32U uiIndex, const DataTypeDef*& pType);
            Value* get(const DCHAR* pDataPath, D32U uiIndex, ParamList& paramList, const DataTypeDef*& pType);

            Value* get(Array* pArray, D32U uiIndex, ValueType type);
            Value* get(Object* pObject, const DCHAR* pName, ValueType type);

        private:
            Value* getByPathNode(const DataTypeDef* pRootType, Value* pRoot, DataPathNode* pPathNode, const DataTypeDef*& pType);
            Value* getByDataPath(const DataTypeDef* pRootType, Value* pRoot, DataPath* pDataPath, const DataTypeDef*& pType);
            Value* getByDataPath(const DataTypeDef* pRootType, Value* pRoot, const DCHAR* pPath, const DataTypeDef*& pType);
            Value* getByVar(const DataTypeDef* pRootType, Value* pRoot, DataPathNode* pPathNode, const DataTypeDef*& pType);

            Value* addObjectMember(DataPathNode* pPathNode, const DataTypeDef* pRootType, Object* pRoot, const DataTypeDef*& pType);
            Value* addArrayMember(DataPathNode* pPathNode, const DataTypeDef* pRootType, Array* pRoot, const DataTypeDef*& pType);
            Value* addDataByPathNode(DataPathNode* pPathNode, const DataTypeDef* pRootType, Value* pRoot, const DataTypeDef*& pType);
            Value* addDataByDataPath(DataPath* pDataPath, const DataTypeDef* pRootType, Value* pRoot, const DataTypeDef*& pType);
            Value* addDataByDataPath(const DCHAR* pDataPath, const DataTypeDef* pRootType, Value* pRoot);
            // Value* addDataByPathNode(Value* pRoot, DataPathNode* pPathNode, const DataTypeDef*& pType);
            Value* fixDataByPathNode(Value* pRoot, DataPathNode* pPathNode, const DataTypeDef*& pType);
            Value* createRootData(DataPathNode* pPathNode, const DataTypeDef*& pType);

            // Value* newValueNode(D32S iType);
            

        protected:
            typedef std::map<const DCHAR*, Value*>       DataMap;
            typedef DataMap::iterator                    DataMapIter;
            typedef std::pair<const DCHAR*, Value*>      DataPair;

            DataMap             _dataMap;
            VarSet*             _pVarSet;
            DataTree*           _pDataTree;
            ParamList*          _pParamList;
            ObjectSetter        _objectSetter;
        };

        void ObjectDataGetter::setVarSet(VarSet* pVarSet)
        {
            _pVarSet = pVarSet;
        }


        class ObjectDataSetter: public ObjectDataGetter
        {
        public:
            ObjectDataSetter(DataTree* pDataTree);

            D32S set(const DCHAR* pDataPath, const void* pData, D32U uiDataSize);
            D32S set(const DCHAR* pDataPath, ParamList& paramList, const void* pData, D32U uiDataSize);

            D32S set(Value* pValue, D64S iValue);
            D32S set(Value* pValue, D64U uiValue);
            D32S set(Value* pValue, DOUBLE dValue);
            D32S set(Value* pValue, const DCHAR* pStr);

            D32S remove(const DCHAR* pDataPath, ParamList* pParamList);

            void undo();
        private:
            D32S setData(Value* pValue, const DataTypeDef* pType, const void* pData, D32U uiDataSize);
        };


        //class ObjectDataRemover
        //{
        //public:
        //    ObjectDataRemover(DataTree* pDataTree);

        //    D32S remove(const DCHAR* pDataPath);
        //    D32S remove(const DCHAR* pDataPath, ParamList& paramList);

        //private:
        //    D32S remove(const DCHAR* pDataPath, ParamList* pParamList);


        //private:
        //    DataTree* _pDataTree;
        //    ParamList* _pParamList;
        //};

    }
}

#endif  // DATA_TREE_H_
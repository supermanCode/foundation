#ifndef DATA_OBJECT_SEARCHER_H
#define DATA_OBJECT_SEARCHER_H



#include "Object.h"
#include "DataPath.h"
#include "datatype/DataType.h"

using namespace vus;

namespace vus
{
    namespace dm
    {
        typedef struct 
        {
            Value*  pParent;
            Value*  pValue;
        } ObjectValueDetail;

        class DataTree;
        class ObjectDataSearcher
        {
        public:
            ObjectDataSearcher(DataTree* pDataTree);

            Value* find(const DCHAR* pPath, ParamList* pParamList = NULL);
            Value* find(Value* pRoot, const DCHAR* pPath, ParamList* pParamList = NULL);
            Value* find(DataPathNode* pPathNode, ParamList* pParamList = NULL);
            Value* find(Value* pRoot, DataPathNode* pPathNode, ParamList* pParamList = NULL);

            D32S find(const DCHAR* pPath, ParamList* pParamList, ObjectValueDetail* pDetail);

        private:
            DBOOL meet(Object* pObject, DataPathNode* pNode, ParamList* pParamList);
            Object* search(Array* pArray, DataPathNode* pNode, ParamList* pParamList);
            Value* search(Value* pRoot, DataPathNode* pNode, ParamList* pParamList, ObjectValueDetail* pValueDetail);

        private:
            DataTree*   _pDataTree;
        };
    }
}

#endif /* DATA_OBJECT_SEARCHER_H */

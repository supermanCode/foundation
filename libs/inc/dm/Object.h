#ifndef OBJECT_H_
#define OBJECT_H_


#include "adm_typedefs.h"
#include <map>
#include <vector>
#include <list>
#include "adm_string.h"
#include "datatype/StringUtil.h"


namespace vus
{
    namespace dm
    {

        enum ValueType
        {
            VALUE_INVALID = 0,
            VALUE_INT = 1,
            VALUE_UINT,
            VALUE_REAL,
            VALUE_STRING,
            VALUE_ARRAY,
            VALUE_OBJECT
        };

        class Value
        {
        public:
            Value() { _iType = VALUE_INVALID; }
            virtual ~Value() {};

            inline D32S getType() const;
        protected:
            D32S    _iType;
        };

        D32S Value::getType() const
        {
            return _iType;
        }

        class Number : public Value
        {
        public:
            Number();
            Number(D32S iType);
            ~Number();
            D32S asInt32() const;
            D32U asUInt32() const;
            D64S asInt64() const;
            D64U asUInt64() const;
            DOUBLE asDouble() const;
            void setAsInt32(D32S iValue);
            void setAsUInt32(D32U uiValue);
            void setAsInt64(D64S iValue);
            void setAsUInt64(D64U uiValue);
            void setAsDouble(DOUBLE dValue);

        private:
            union
            {
                D32S    i32;
                D32U    ui32;
                D64S    i64;
                D64U    ui64;
                DOUBLE  d;
            }_value;
        };

        class String : public Value
        {
        public:
            String();
            ~String();

            const DCHAR* asString() const;
            D32S setString(const DCHAR* pValue);
            
        private:
            DCHAR*  _pValue;
            D32U    _uiSize;
        };

        class Array : public Value
        {
        public:
            Array();
            ~Array();

            Value* get(D32U uiIndex);
            D32S add(D32U uiIndex, Value* pValue);
            D32S append(Value* pValue);
            D32S insert(D32U uiIndex, Value* pValue);
            D32S remove(D32U uiIndex);
            D32S remove(Value* pValue);
            D32S remove(Value* pValue, D32U& uiIndex);
            Value* removeEx(D32U uiIndex);
            D32U count();

        private:
//             typedef std::map<D32U, Value*>  Map;
//             typedef Map::iterator           MapIter;
//             typedef std::pair<D32U, Value*> MapPair;
// 
//             Map         _map;
//             D32U        _uiCount;

            typedef std::vector<Value*>::iterator   VectorIter;
            std::vector<Value*> _vector;
        };



        class Object : public Value
        {
        public:
            Object();
            ~Object();

            Value* get(const DCHAR* pName);

            D32S asInt(const DCHAR* pName, D64S& iValue);
            D32S asUInt(const DCHAR* pName, D64U& uiValue);
            D32S asReal(const DCHAR* pName, DOUBLE& dValue);
            Number* asNumber(const DCHAR* pName);
            D32S asString(const DCHAR* pName, const DCHAR*& pStr);
            String* asString(const DCHAR* pName);
            Array* asArray(const DCHAR* pName);
            Object* asObject(const DCHAR* pName);

            D32S add(const DCHAR* pName, Value* pValue);
            D32S remove(const DCHAR* pName);
            D32S remove(Value* pValue);
            const DCHAR* getName(Value* pValue);
            Value* removeEx(const DCHAR* pName);
            void clear();
            D32S add(const DCHAR* pName, D64S iValue);
            D32S add(const DCHAR* pName, D64U uiValue);
            D32S add(const DCHAR* pName, DOUBLE dValue);
            D32S add(const DCHAR* pName, const DCHAR* pValue);

            Array* addArray(const DCHAR* pName);
            Object* addObject(const DCHAR* pName);

            static Number* newNumber();
            static String* newString();
            static Array* newArray();
            static Object* newObject();



        private:
            struct StringComparor
            {
                bool operator()(const DCHAR* a, const DCHAR* b) const
                {
                    return adm_strcmp(a, b) < 0;
                }
            };

            typedef std::map<const DCHAR*, Value*, StringComparor>  Map;
            typedef Map::iterator                   MapIter;
            typedef std::pair<const DCHAR*, Value*> MapPair;

        public:
            class Iterator 
            {
            public:
                Iterator(Object& object) : _object(object)
                {
                    _iter = object._map.begin();
                }

                DBOOL next()
                {
                    if (_iter != _object._map.cend())
                    {
                        pName = _iter->first;
                        pValue = _iter->second;
                        _iter++;
                        return TRUE;
                    }
                    else
                    {
                        pName = NULL;
                        pValue = NULL;
                        return FALSE;
                    }
                }
            
                const DCHAR*    pName;
                Value*          pValue;

            private:
                MapIter _iter;
                Object& _object;
            };


        private:
            Map _map;
        };

        class ObjectJsonSerializer
        {
        public:
            const DCHAR* serialize(Value* pValue);

        private:
            D32S int2JsonStr(Number* pNumber);
            D32S uint2JsonStr(Number* pNumber);
            D32S real2JsonStr(Number* pNumber);
            D32S string2JsonStr(String* pString);
            D32S array2JsonStr(Array* pArray);
            D32S objectMember2JsonStr(const DCHAR* pName, Value* pMember);
            D32S object2JsonStr(Object* pObject);
            D32S value2JsonStr(Value* pValue);
        private:
            std::string     _jsonStr;
        };

        class ObjectJsonParser
        {
        public:
            ObjectJsonParser();
            Value* parse(const DCHAR* pJsonStr);

        private:
            D32S parseNumber(Value*& pValue);
            D32S parseString(Value*& pValue);
            D32S parseArray(Array* pArray);
            D32S parseArray(Value*& pValue);
            D32S parseObjectMember(Object* pObject);
            D32S parseObject(Object* pObject);
            D32S parseObject(Value*& pValue);
            D32S parseValue(Value*& pValue);

        private:
            StringUtil  _util;
        };


        class ObjectSetter
        {
        public:
            ~ObjectSetter();

            Value* create(ValueType type);
            D32S numberSet(Number* pNumber, D32S iValue);
            D32S numberSet(Number* pNumber, D32U uiValue);
            D32S numberSet(Number* pNumber, D64S iValue);
            D32S numberSet(Number* pNumber, D64U uiValue);
            D32S numberSet(Number* pNumber, DOUBLE dValue);
            D32S stringSet(String* pString, const DCHAR* pValue);
            Value* objectAdd(Object* pObject, const DCHAR* pName, ValueType type);
            D32S objectAdd(Object* pObject, const DCHAR* pName, Value* pValue);
            D32S objectRemove(Object* pObject, const DCHAR* pName);
            D32S objectRemove(Object* pObject, Value* pValue);
            Value* arrayAdd(Array* pArray, D32U uiIndex, ValueType type);
            D32S arrayAdd(Array* pArray, D32U uiIndex, Value* pValue);
            Value* arrayAppend(Array* pArray, ValueType type);
            D32S arrayAppend(Array* pArray, Value* pValue);
            D32S arrayRemove(Array* pArray, D32U uiIndex);
            D32S arrayRemove(Array* pArray, Value* pValue);
            void undo();

  

        private:
            typedef enum
            {
                OBJECT_DO_CREATE,
                OBJECT_DO_ADD,
                OBJECT_DO_MODIFY,
                OBJECT_DO_DELETE,
            } ObjectDoType;

            typedef enum
            {
                OBJECT_DO_DATA_INT = 1,
                OBJECT_DO_DATA_UINT,
                OBJECT_DO_DATA_REAL,
                OBJECT_DO_DATA_STRING,
                OBJECT_DO_DATA_ARRAY,
                OBJECT_DO_DATA_OBJECT,
            } ObjectDoDataType;

            struct ObjectDoRecord
            {
                D32S    doType;
                D32S    dataType;
                union
                {
                    Value*  pValue;
                    Number* pNumber;
                    String* pString;
                    Array*  pArray;
                    Object* pObject;
                } v1;

                union
                {
                    D32S    i32;
                    D32U    ui32;
                    D64S    i64;
                    D64U    ui64;
                    DOUBLE  d;
                    DCHAR*  pString;
                    D32U    uiIndex;
                    Value*  pValue;
                } v2;

                union
                {
                    DCHAR*  pName;
                    D32U    uiIndex;
                } v3;
            };

            inline D32S addRecord(ObjectDoRecord& record);

            D32S undoCreate(ObjectDoRecord& record);
            D32S undoAdd(ObjectDoRecord& record);
            D32S undoModify(ObjectDoRecord& record);
            D32S undoDelete(ObjectDoRecord& record);
            
            void cleanupModify(ObjectDoRecord& record);
            void cleanupDelete(ObjectDoRecord& record);
            void cleanup();
        private:
            std::list<ObjectDoRecord>   _doRecords;
        };

        D32S ObjectSetter::addRecord(ObjectDoRecord& record)
        {
            _doRecords.push_back(record);
            return 0;
        }
    }
}

#endif  // OBJECT_H_
#ifndef DATA_PATH_H_
#define DATA_PATH_H_


#include "adm_typedefs.h"

#include "Param.h"
#include "datatype/StringUtil.h"
#include <map>

namespace vus
{
    namespace dm
    {
        class DataPathNode
        {
            friend class DataPath;
        public:
            DataPathNode();

            inline const DCHAR* getName() const;
            inline DBOOL isRoot();
            inline const DCHAR* getRootName() const;
            inline DBOOL isVar();
            inline const DCHAR* getVarName() const;
            inline DBOOL hasSearchKey();

        private:
            typedef std::map<const DCHAR*, const DCHAR*>    Map;
            typedef Map::iterator                           MapIter;
            typedef std::pair<const DCHAR*, const DCHAR*>   MapPair;


        public:
            class SearchKeyIter
            {
            public:
                SearchKeyIter(DataPathNode* pNode);

                DBOOL first();
                DBOOL next();

                inline const DCHAR* getKey() const;
                inline const DCHAR* getValue() const;
                inline DBOOL isParam();
                inline const DCHAR* getParamName() const;

            private:

                const DCHAR*            _pKey;
                const DCHAR*            _pValue;
                DataPathNode*           _pNode;
                DataPathNode::MapIter   _iter;
            };


        private:
            const DCHAR*    _pName;
            Map             _searchKeys;
        };

        const DCHAR* DataPathNode::getName() const
        {
            return _pName;
        }

        DBOOL DataPathNode::isRoot()
        {
            return '/' == _pName[0] ? TRUE : FALSE;
        }

        const DCHAR* DataPathNode::getRootName() const
        {
            return &_pName[1];
        }

        DBOOL DataPathNode::isVar()
        {
            return '$' == _pName[0] ? TRUE : FALSE;
        }

        const DCHAR* DataPathNode::getVarName() const
        {
            return &_pName[1];
        }

        DBOOL DataPathNode::hasSearchKey()
        {
            return _searchKeys.size() > 0U ? TRUE : FALSE;
        }

        const DCHAR* DataPathNode::SearchKeyIter::getKey() const
        {
            return _pKey;
        }

        const DCHAR* DataPathNode::SearchKeyIter::getValue() const
        {
            return _pValue;
        }

        DBOOL DataPathNode::SearchKeyIter::isParam()
        {
            return ':' == _pValue[0] ? TRUE : FALSE;
        }

        const DCHAR* DataPathNode::SearchKeyIter::getParamName() const
        {
            return &_pValue[1];
        }



        class DataPath
        {
        public:
            DataPath(const DCHAR* pPath);
            DataPath(DataPath& rhs);
            ~DataPath();

            DataPathNode* first();
            DataPathNode* next();
            inline DataPathNode* current();

            //static D32S getRootPath(const DCHAR* pPath, DataPath* pDataPath);

//         public:
//             class NodeIter
//             {
//             public:
//                 NodeIter(DataPath* pPath)
//                 {
//                     _pPath = pPath;
//                     pNode = NULL;
//                 }
// 
//                 DataPathNode* first()
//                 {
//                     pNode = _pPath->first();
//                     return pNode;
//                 }
// 
//                 DataPathNode* next()
//                 {
//                     pNode = _pPath->next();
//                     return pNode;
//                 }
// 
//                 DataPathNode* pNode;
// 
//             private:
//                 DataPath*   _pPath;
//             };


        private:
            D32S parse();
            D32S parseSearchKey();
            D32S parseSearchKeys();

        private:
            const DCHAR*    _pPath;
            DCHAR*          _pBuf;
            D32U            _uiOffset;
            StringUtil      _util;
            DataPathNode    _node;
        };

        DataPathNode* DataPath::current()
        {
            return &_node;
        }

// class PathIter
// {
// public:
//     PathIter();
//     ~PathIter();
//     const DCHAR* first(const DCHAR* pPath);
//     const DCHAR* first(const DCHAR* pPath, const ParamList& paramList);
//     const DCHAR* next();
//     inline const DCHAR* getFullPath() const;
// 
// private:
//     const DCHAR* get();
// 
// private:
//     DCHAR* _pPath;
//     DCHAR* _pStart;
//     DCHAR* _pEnd;
// };
// 
// const DCHAR* PathIter::getFullPath() const
// {
//     return _pPath;
// }
// 
// 
// 
// class PathWithParamIter
// {
// public:
//     PathWithParamIter(const DCHAR* pPath, const ParamList& paramList);
//     ~PathWithParamIter();
// 
//     const DCHAR* first();
//     const DCHAR* next();
//     inline const DCHAR* getFullPath() const;
// 
// private:
//     const DCHAR* get();
// 
// private:
//     const DCHAR*        _pSrcPath;
//     const ParamList&    _paramList;
//     DCHAR* _pPath;
//     DCHAR* _pStart;
//     DCHAR* _pEnd;
// };
// 
// const DCHAR* PathIter::getFullPath() const
// {
//     return _pPath;
// }


    }
}

#endif  // DATA_PATH_H_
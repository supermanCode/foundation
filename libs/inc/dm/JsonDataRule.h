#pragma once

#ifndef JSON_DATA_RULE_H_
#define JSON_DATA_RULE_H_

#include "adm_typedefs.h"
#include <map>
#include "datatype/StringUtil.h"
#include "DataTree.h"
#include "VarSet.h"
#include "datatype/DataStructCodec.h"

struct json_object;

namespace vus
{
    namespace dm
    {
        class JsonDataRule
        {
        public:
            ~JsonDataRule();

            inline StringDict* getRuleItems();
            inline VarSet* getVarSet();
            
            // D32S addItem(const DCHAR* pDataPath, const DCHAR* pJsonPath);
            D32S setItems(const DCHAR* pDescriptionStr);



        private:
            StringDict  _items;
            VarSet      _vars;
        };

        StringDict* JsonDataRule::getRuleItems()
        {
            return &_items;
        }

        VarSet* JsonDataRule::getVarSet()
        {
            return &_vars;
        }


        class JsonDataRuleParser
        {
        public:
            JsonDataRuleParser(const DCHAR* pDescriptionStr) : _util(pDescriptionStr)
            {

            }

            D32S parse(StringDict* pDict);

        private:
            D32S parseOne(StringDict* pDict);
            D32S parseAll(StringDict* pDict);

        private:
            StringUtilEx _util;
        };



        class JsonDataMapObjectGetterHelper : public DataPathWokerHelper
        {
            friend class JsonDataMapObjectGetter;
        private:
            JsonDataMapObjectGetterHelper(ObjectDataSetter* pDataSetter);

            D32S handle(DataPathNode* pNode, void* pInData, void*& pOutData);

        private:
            ObjectDataSetter* _pDataSetter;
            Value* _pValue;
            const DataTypeDef* _pType;
        };

        class JsonDataMapObjectGetter
        {
        public:
            JsonDataMapObjectGetter(ObjectDataSetter* pDataSetter);
            inline void setVarSet(VarSet* pVarSet);
            Value* get(DataPath* pDataPath, Value* pRoot, const DataTypeDef* pRootType, const DataTypeDef*& pType);

        private:
            JsonDataMapObjectGetterHelper  _helper;
            DataPathWorker              _worker;

        };

        inline void JsonDataMapObjectGetter::setVarSet(VarSet* pVarSet)
        {
            _worker.setVarSet(pVarSet);
        }


        class JsonDataMapObjectConvertHelper: public DataPathWokerHelper
        {
            friend class JsonDataMapObjectConverter;
        private:
            D32S handle(DataPathNode* pNode, void* pInData, void*& pOutData);
        private:
            struct json_object*     _pJsonObject;
        };

        class JsonDataMapObjectConverter
        {
        public:
            JsonDataMapObjectConverter(ObjectDataSetter* pDataSetter);

            inline void setVarSet(VarSet* pVarSet);
            D32S convert(const DCHAR* pJsonDataPath, const DCHAR* pDataPath, struct json_object* pRoot);
            D32S convert(DataPath* pJsonDataPath, DataPath* pDataPath, struct json_object* pRoot);

        private:
            D32S convert(struct json_object* pRoot, Value* pValue, const DataTypeDef* pRootType);

            D32S covertJsonBoolean(struct json_object* pRoot, Value* pValue);
            D32S covertJsonInt(struct json_object* pRoot, Value* pValue);
            D32S covertJsonDouble(struct json_object* pRoot, Value* pValue);
            D32S covertJsonString(struct json_object* pRoot, Value* pValue);
            D32S covertJsonMember(struct json_object* pRoot, Value* pValue, const DataTypeDef* pRootType);
            D32S covertJsonArray(struct json_object* pRoot, Value* pValue, const DataTypeDef* pRootType);
 
        private:
            DataPath*           _pDataPath;
            DataPath*           _pJsonDataPath;
            struct json_object* _pJsonObject;
            ObjectDataSetter*   _pDataSetter;
            VarSet*             _pVarSet;

            DataPathWorker          _worker;
            JsonDataMapObjectConvertHelper   _helper;
        };

        void JsonDataMapObjectConverter::setVarSet(VarSet* pVarSet)
        {
            _pVarSet = pVarSet;
            _worker.setVarSet(pVarSet);
        }


        class JsonDataConverter
        {
        public:
            JsonDataConverter(DataTree* pDataTree);

            D32S convert(const DCHAR* pJsonStr, JsonDataRule* pRule);

        private:
            D32S convert(struct json_object* pRoot, JsonDataRule* pRule);

        private:
            DataTree*   _pDataTree;
        };
    }
}


#endif  // JSON_DATA_RULE_H_
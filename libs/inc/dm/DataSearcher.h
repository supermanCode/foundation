#ifndef DATA_SEARCHER_H_
#define DATA_SEARCHER_H_


#include "DataPath.h"
#include "VarSet.h"


namespace vus
{
    namespace dm
    {
        class DataSearchHelper
        {
        public:
            // virtual void* init(void* pData, void*& pOut);
            virtual void* getRoot(DataPathNode* pPathNode, void* pData, void*& pRoot) = 0;
            virtual void* getNode(DataPathNode* pPathNode, void* pRoot, void*& pData) = 0;
        };


        class DataSearcher
        {
        public:
            DataSearcher(void* pData, VarSet* pVarSet, DataSearchHelper* pHelper);

            void* search(const DCHAR* pPath);
            void* search(const DCHAR* pPath, void* pRoot);

        private:
            void* searchByVar(const DCHAR* pVarName, void* pRoot);
            void* searchByPathNode(DataPathNode* pPathNode, void* pRoot);
            void* searchByDataPath(const DCHAR* pPath, void* pRoot);

        private:
            typedef std::map<const DCHAR*, void*>       DataMap;
            typedef DataMap::iterator                   DataMapIter;
            typedef std::pair<const DCHAR*, void*>      DataPair;

            DataMap     _dataMap;
            VarSet*     _pVarSet;
            void*       _pData;
            DataSearchHelper* _pHepler;
        };



        class DataPathWokerHelper
        {
        public:
            virtual D32S handle(DataPathNode* pNode, void* pInData, void*& pOutData) = 0;
            // virtual D32S handleTail(DataPathNode* pNode, void* pInData) = 0;
            inline void over();
            inline DBOOL isOver() const;
        private:
            DBOOL       _isOver;
        };

        void DataPathWokerHelper::over()
        {
            _isOver = TRUE;
        }

        DBOOL DataPathWokerHelper::isOver() const
        {
            return _isOver;
        }


        class DataPathWorker
        {
        public:
            DataPathWorker(DataPathWokerHelper* pHelper);
            inline void setVarSet(VarSet* pVarSet);


            D32S foreach(const DCHAR* pDataPath, void* pInData);
            D32S foreach(DataPath* pDataPath, void* pInData);

        private:
            D32S foreach(const DCHAR* pDataPath, void* pInData, void*& pOutData);
            D32S foreach(DataPath* pDataPath, void* pInData, void*& pOutData);

            D32S handleByNode(DataPathNode* pPathNode, void* pInData, void*& pOutData);
            D32S handleByVar(DataPathNode* pPathNode, void* pInData, void*& pOutData);

        private:
            typedef std::map<const DCHAR*, void*>       DataMap;
            typedef DataMap::iterator                   DataMapIter;
            typedef std::pair<const DCHAR*, void*>      DataPair;

            DataPath*               _pDataPath;
            DataMap                 _dataMap;
            VarSet*                 _pVarSet;
            DataPathWokerHelper*    _pHelper;
        };

        void DataPathWorker::setVarSet(VarSet* pVarSet)
        {
            _pVarSet = pVarSet;
        }
    }
}

#endif  // DATA_SEARCHER_H_
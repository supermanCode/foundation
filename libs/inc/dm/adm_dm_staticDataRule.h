﻿#ifndef ADM_DM_STATIC_DATA_RULE_H
#define ADM_DM_STATIC_DATA_RULE_H

#include "adm_typedefs.h"

#ifdef __cplusplus
extern "C"
{
#endif  // __cplusplus


/**
 * @brief 静态数据描述
 * 
 */
typedef struct
{
    D32S            uiType;     // 数据类型
    D32U            uiSize;     // 数据大小
    const DCHAR*    pDataPath;  // DM数据路径
} ADM_DM_STATIC_DATA_DESC_T;


/**
 * @brief 静态数据规则项(Static Data Rule Item)
 * 
 */
typedef struct ADM_DM_STATIC_DATA_RULE_ITEM_T   ADM_DM_STATIC_DATA_RULE_ITEM_T;

/**
 * @brief 静态数据规则项添加子项
 * 
 * @param pRuleItem         静态数据规则项
 * @param pDescription      子项静态数据描述
 * @return D32S             返回值，[0]成功 [!0]失败
 */
D32S adm_dm_sdrItemAddSub(ADM_DM_STATIC_DATA_RULE_ITEM_T* pRuleItem, const ADM_DM_STATIC_DATA_DESC_T* pDescription);
/**
 * @brief 静态数据规则项添加子项
 * 
 * @param pRuleItem         静态数据规则项
 * @param pDescription      子项静态数据描述
 * @param ppSubRuleItem     静态数据规则子项
 * @return D32S             返回值，[0]成功 [!0]失败
 */
D32S adm_dm_sdrItemAddSubEx(ADM_DM_STATIC_DATA_RULE_ITEM_T* pRuleItem, const ADM_DM_STATIC_DATA_DESC_T* pDescription, ADM_DM_STATIC_DATA_RULE_ITEM_T** ppSubRuleItem);


/**
 * @brief 静态数据规则(Static Data Rule)
 * 
 */
typedef struct ADM_DM_STATIC_DATA_RULE_T   ADM_DM_STATIC_DATA_RULE_T;

/**
 * @brief 创建静态数据规则
 * 
 * @return ADM_DM_STATIC_DATA_RULE_T* 静态数据规则对象，NULL失败
 */
ADM_DM_STATIC_DATA_RULE_T* adm_dm_sdrCreate();
/**
 * @brief 销毁静态数据规则对象
 * 
 * @param pRule     静态数据规则对象
 */
void adm_dm_sdrDestroy(ADM_DM_STATIC_DATA_RULE_T* pRule);
/**
 * @brief 添加静态数据规则变量
 * 
 * @param pRule     静态数据规则对象
 * @param pName     变量名
 * @param pValue    变量值
 * @return D32S     返回值，[0]成功 [!0]失败
 */
D32S adm_dm_sdrAddVar(ADM_DM_STATIC_DATA_RULE_T* pRule, const DCHAR* pName, const DCHAR* pValue);
/**
 * @brief 设置静态数据规则
 * 
 * @param pRule         静态数据规则对象
 * @param pDescription  静态数据规则描述
 * @param ppRuleItem    静态数据规则项
 * @return D32S         返回值，[0]成功 [!0]失败
 */
D32S adm_dm_sdrSetRule(ADM_DM_STATIC_DATA_RULE_T* pRule, const ADM_DM_STATIC_DATA_DESC_T* pDescription, ADM_DM_STATIC_DATA_RULE_ITEM_T** ppRuleItem);
/**
 * @brief 通过描述字符串设置静态数据规则
 * 
 * @param pRule             静态数据规则对象
 * @param pDescriptionStr   静态数据规则描述字符串
 * @return D32S             返回值，[0]成功 [!0]失败
 */
D32S adm_dm_sdrSetRuleByDesc(ADM_DM_STATIC_DATA_RULE_T* pRule, const DCHAR* pDescriptionStr);
// /**
//  * @brief 获取静态数据规则项
//  * 
//  * @param pRule     静态数据规则对象
//  * @return const ADM_DM_STATIC_DATA_RULE_ITEM_T*    静态数据规则项
//  */
// const ADM_DM_STATIC_DATA_RULE_ITEM_T* adm_dm_sdrGetRule(ADM_DM_STATIC_DATA_RULE_T* pRule);

#ifdef __cplusplus
}
#endif  // __cplusplus

#endif /* ADM_DM_STATIC_DATA_RULE_H */

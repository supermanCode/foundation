﻿#ifndef ADM_DM_JSON_DATA_RULE_H
#define ADM_DM_JSON_DATA_RULE_H


#include "adm_typedefs.h"

#ifdef __cplusplus
extern "C"
{
#endif  // __cplusplus


/**
 * @brief JSON数据规则(JSON Data Rule)
 * 
 */
typedef struct ADM_DM_JSON_DATA_RULE_T   ADM_DM_JSON_DATA_RULE_T;
/**
 * @brief 创建JSON数据规则
 * 
 * @return ADM_DM_JSON_DATA_RULE_T* JSON数据规则对象，NULL失败
 */
ADM_DM_JSON_DATA_RULE_T* adm_dm_jdrCreate();
/**
 * @brief 销毁JSON数据规则对象
 * 
 * @param pRule     JSON数据规则对象
 */
void adm_dm_jdrDestroy(ADM_DM_JSON_DATA_RULE_T* pRule);
/**
 * @brief 添加JSON数据规则变量
 * 
 * @param pRule     JSON数据规则
 * @param pName     变量名
 * @param pValue    变量值
 * @return D32S     返回值，[0]成功 [!0]失败
 */
D32S adm_dm_jdrAddVar(ADM_DM_JSON_DATA_RULE_T* pRule, const DCHAR* pName, const DCHAR* pValue);
// /**
//  * @brief 添加JSON数据规则项
//  * 
//  * @param pRule     JSON数据规则
//  * @param pDataPath DM数据路径
//  * @param pJsonPath JSON数据路径
//  * @return D32S     返回值，[0]成功 [!0]失败
//  */
// D32S adm_dm_jdrAddItem(ADM_DM_JSON_DATA_RULE_T* pRule, const DCHAR* pDataPath, const DCHAR* pJsonPath);
/**
 * @brief 通过JSON数据规则字符串添加多条JSON数据规则项
 * 
 * @param pRule             JSON数据规则
 * @param pDescriptionStr   JSON数据规则项描述
 * @return D32S             返回值，[0]成功 [!0]失败
 */
D32S adm_dm_jdrAddItemsByDesc(ADM_DM_JSON_DATA_RULE_T* pRule, const DCHAR* pDescriptionStr);


#ifdef __cplusplus
}
#endif  // __cplusplus

#endif /* ADM_DM_JSON_DATA_RULE_H */

﻿#ifndef ADM_DM_DATA_RULE_H
#define ADM_DM_DATA_RULE_H


#include "adm_typedefs.h"

#ifdef __cplusplus
extern "C"
{
#endif  // __cplusplus


/**
 * @brief DM数据规则项(Data Rule Item)
 * 
 */
typedef struct ADM_DM_DATA_RULE_ITEM_T  ADM_DM_DATA_RULE_ITEM_T;
 /**
  * @brief DM数据规则项添加子项
  * 
  * @param pRuleItem     DM数据规则项
  * @param pDescription  DM数据描述
  * @return D32S         返回值，[0]成功 [!0]失败
  */
 D32S adm_dm_drItemAddSub(ADM_DM_DATA_RULE_ITEM_T* pRuleItem, const ADM_DM_DATA_TYPE_DESC_T* pDescription);
 /**
  * @brief DM数据规则项添加子项
  * 
  * @param pRuleItem     DM数据规则项
  * @param pDescription  DM数据描述
  * @param ppSubRuleItem DM数据规则项子项
  * @return D32S         返回值，[0]成功 [!0]失败
  */
 D32S adm_dm_drItemAddSubEx(ADM_DM_DATA_RULE_ITEM_T* pRuleItem, const ADM_DM_DATA_TYPE_DESC_T* pDescription, ADM_DM_DATA_RULE_ITEM_T** ppSubRuleItem);



#ifdef __cplusplus
}
#endif  // __cplusplus

#endif /* ADM_DM_DATA_RULE_H */

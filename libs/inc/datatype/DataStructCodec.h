﻿#ifndef DATA_STRUCT_CODEC_H_
#define DATA_STRUCT_CODEC_H_

#include "DataStructCodecHelper.h"
#include "DataStruct.h"


namespace vus
{
    class DataStructEncoder
    {
    public:
        DataStructEncoder(DataStructEncodeHelper* pHelper);
        D32S encode(const DataTypeDef* pTypeDef, void* pData, void* pOutData, D32U uiOutMaxSize);

    private:
        D32S encodeInt(const DataTypeDef* pTypeDef, void* pData);
        D32S encodeUInt(const DataTypeDef* pTypeDef, void* pData);
        D32S encodeReal(const DataTypeDef* pTypeDef, void* pData);
        D32S encodeString(const DataTypeDef* pTypeDef, void* pData);
        D32S encodeArrayMember(const DataTypeDef* pTypeDef, void* pData, D32U uiIndex);
        D32S encodeArray(const DataTypeDef* pTypeDef, void* pData, D32U uiCount);
        D32S encodeArray(const DataTypeDef* pTypeDef, void* pData);
        D32S encodeObject(const DataTypeDef* pTypeDef, void* pData);
        D32S encodeMember(const DataTypeDef* pTypeDef, void* pData);
        D32S encodeOption(const DataTypeDef* pTypeDef, void* pData);
        D32S encode(const DataTypeDef* pTypeDef, void* pData);

    private:
        DataStructEncodeHelper* _pHelper;
        DataStructWriter            _writer;
    };


    class DataStructDecoder
    {
    public:
        DataStructDecoder(DataStructDecodeHelper* pHelper);

        D32S decode(const DataTypeDef* pTypeDef, void* pData, void* pDataBuf, D32U uiDataBufSize);

    private:
        D32S decodeInt(const DataTypeDef* pTypeDef, void* pData);
        D32S decodeUInt(const DataTypeDef* pTypeDef, void* pData);
        D32S decodeReal(const DataTypeDef* pTypeDef, void* pData);
        D32S decodeString(const DataTypeDef* pTypeDef, void* pData);
        D32S decodeArrayMember(const DataTypeDef* pTypeDef, void* pData, D32U uiIndex);
        D32S decodeArray(const DataTypeDef* pSubType, void* pData, D32U uiCount);
        D32S decodeArray(const DataTypeDef* pTypeDef, void* pData);
        D32S decodeObject(const DataTypeDef* pTypeDef, void* pData);
        D32S decodeMember(const DataTypeDef* pTypeDef, void* pData);
        D32S decodeOption(const DataTypeDef* pTypeDef, void* pData);
        D32S decode(const DataTypeDef* pTypeDef, void* pData);

    private:
        DataStructDecodeHelper* _pHelper;
        DataStructReader        _reader;
    };

}

#endif  // DATA_STRUCT_CODEC_H_
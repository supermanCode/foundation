﻿#ifndef DATA_STRUCT_H
#define DATA_STRUCT_H


#include "adm_typedefs.h"
#include "DataType.h"

namespace vus
{
//     struct DataStructType: public DataTypeDef
//     {
// //     public:
// //         D32S initBy();
// // 
// //         inline const DataTypeDef* getDataType();
// // 
// //     private:
//         // DataTypeDef _type;
//         D32U        uiMemorySize;
//         D32U        uiOffset;
//     
//     const DataTypeDef* DataStructType::getDataType()
//     {
//         return &_type;
//     }


// 

//     class DataStructTypeBuilder

//     {

//     public:

//         static D32S build(DataStructType* pStructType, const DataTypeDef* pType);

// 

//     private:

//         static D32S initStructType(DataStructType* pStructType, const DataTypeDef* pType);

//         static D32S initStructNumber(DataStructType* pStructType, const DataTypeDef* pType);

//         static D32S initStructString(DataStructType* pStructType, const DataTypeDef* pType);

//         static D32S initStructArray(DataStructType* pStructType, const DataTypeDef* pType);

//         static D32S initStructObject(DataStructType* pStructType, const DataTypeDef* pType);

//         static D32S initStructMember(DataStructType* pStructType, const DataTypeDef* pType);

//     };


    struct TypeSize
    {
        D32U    uiSize;
        D32U    uiAssign;
    };

    class DataStructTypeInitializer
    {
    public:
        DataStructTypeInitializer();
        D32S init(DataTypeDef* pType);

    private:
        void initNumber(DataTypeDef* pType, D32U& uiAlign);
        void initString(DataTypeDef* pType, D32U& uiAlign);
        void initPointer(DataTypeDef* pType, D32U& uiAlign);
        D32S initArray(DataTypeDef* pType, D32U& uiAlign);
        D32S initObject(DataTypeDef* pType, D32U& uiAlign);
        // D32S initMember(DataTypeDef* pType, D32U& uiAlign);
        D32S initType(DataTypeDef* pType, D32U& uiAlign);
        

    private:
        D32U    _uiSize;
    };




    class DataStructBuilder
    {
    public:
        static void* createStruct(D32U uiSize);
        static void destroyStruct(void* pData);
        static void* createString(D32U uiSize);
        static void destroyString(void* pPtr);
        static void* createArray(D32U uiSize);
        static D32U getArrayTotalSize(void* pPtr);
        static void destroyArray(void* pPtr);

        static D32S clone(const DataTypeDef* pType, const void* pData, D32U uiDataSize, void* pOut, D32U uiSize);
    };


    class DataStructReader;
    class DataStructWriter
    {
    public:
        DataStructWriter();
        DataStructWriter(const void* pBuf, D32U uiBufSize);

        void setBuffer(const void* pBuf, D32U uiBufSize);

        D32S writeInt8(D8S iValue);
        D32S writeInt16(D16S iValue);
        D32S writeInt32(D32S iValue);
        D32S writeInt64(D64S iValue);
        D32S writeUInt8(D8U uiValue);
        D32S writeUInt16(D16U uiValue);
        D32S writeUInt32(D32U uiValue);
        D32S writeUInt64(D64U uiValue);
        D32S writeFloat(DFLOAT fValue);
        D32S writeDouble(DOUBLE dValue);
        D32S writePointer(void* pPtr);
        D32S writeBytes(const D8U* pBytes, D32U uiLength);
        D32S writeString(const DCHAR* pStr);
        D32S writeFixedString(const DCHAR* pStr, D32U uiFixedSize);
        D32S writeArray(DataStructWriter* pWriter, D32U uiCount, D32U uiMemberSize);
        D32S writeArrayMember(D32U uiIndex, D32U uiMemberSize);
        D32S writeFixedArray(DataStructWriter* pWriter, D32U uiCount, D32U uiMemberSize);
        D32S writeObject(DataStructWriter* pWriter);

        D32S writeInt8(DataStructReader* pReader);
        D32S writeInt16(DataStructReader* pReader);
        D32S writeInt32(DataStructReader* pReader);
        D32S writeInt64(DataStructReader* pReader);
        D32S writeUInt8(DataStructReader* pReader);
        D32S writeUInt16(DataStructReader* pReader);
        D32S writeUInt32(DataStructReader* pReader);
        D32S writeUInt64(DataStructReader* pReader);
        D32S writeFloat(DataStructReader* pReader);
        D32S writeDouble(DataStructReader* pReader);
        D32S writeString(DataStructReader* pReader);
        D32S writeFixedString(DataStructReader* pReader, D32U uiFixedSize);

        inline void anchor(D32U uiOffset);

    private:
        D8U* _pBuf;
        D32U    _uiSize;
        D32U    _uiOffset;
    };

    inline void DataStructWriter::anchor(D32U uiOffset)
    {
        _uiOffset = uiOffset;
    }

    class DataStructReader
    {
    public:
        DataStructReader();
        DataStructReader(const void* pData, D32U uiDataSize);

        void setBuffer(const void* pData, D32U uiDataSize);
        D32S readInt8(D8S& iValue);
        D32S readInt16(D16S& iValue);
        D32S readInt32(D32S& iValue);
        D32S readInt64(D64S& iValue);
        D32S readUInt8(D8U& uiValue);
        D32S readUInt16(D16U& uiValue);
        D32S readUInt32(D32U& uiValue);
        D32S readUInt64(D64U& uiValue);
        D32S readFloat(DFLOAT& fValue);
        D32S readDouble(DOUBLE& dValue);
        D32S readBytes(D8U bytes[], D32U uiSize);
        D32S readPointer(void*& pPtr);
        const DCHAR* readFixedString(D32U uiFixedSize, D32U& uiSize);
        const DCHAR* readString(D32U& uiSize);
//         D32S readArray(DataStructReader* pReader, D32U& uiCount, D32U uiMemberSize);
//         D32S readArray(DataStructReader* pReader, D32U uiMemberSize);
        D32S readArray(void*& pArray, D32U& uiCount);
        D32S readArrayCount(D32U& uiCount);
        D32S readArrayMember(D32U uiIndex, D32U uiSize);
        D32S readFixedArray(DataStructReader* pReader, D32U uiCount, D32U uiMemberSize);
        D32S readObject(DataStructReader* pReader);
        D32S readObject(void*& pObject);
        // D32S readObject(DataStructReader* pReader);
        D32S skipByType(D32U uiSize);
        D32S skipBySize(D32U uiSize);

        inline void anchor(D32U uiOffset);

    private:
        const D8U* _pData;
        D32U        _uiSize;
        D32U        _uiOffset;
    };

    inline void DataStructReader::anchor(D32U uiOffset)
    {
        _uiOffset = uiOffset;
    }



    class DataStructDestructor
    {
    public:
        DataStructDestructor(const DataTypeDef* pType);

        D32S destroy(const void* pData, D32U uiDataSize);

    private:
        D32S destroyNumber(const DataTypeDef* pType);
        D32S destroyString(const DataTypeDef* pType);
        D32S destroyArray(const DataTypeDef* pType);
        D32S destroyObject(const DataTypeDef* pType);
        D32S destroyMember(const DataTypeDef* pType);
        D32S destroyOptional(const DataTypeDef* pType);

    private:
        const DataTypeDef* _pType;
        DataStructReader    _reader;
    };


    class DataStructCloner
    {
    public:
        DataStructCloner(const DataTypeDef* pType);

        D32S clone(const void* pData, D32U uiDataSize, void* pOut, D32U uiOutMaxSize);

    private:
        DataStructCloner();

    private:
        D32S cloneInt(const DataTypeDef* pType);
        D32S cloneUInt(const DataTypeDef* pType);
        D32S cloneReal(const DataTypeDef* pType);
        D32S cloneString(const DataTypeDef* pType);
        D32S cloneArray(const DataTypeDef* pType, D32U uiCount);
        D32S cloneArray(const DataTypeDef* pType);
        D32S cloneObject(const DataTypeDef* pType);
        D32S cloneMember(const DataTypeDef* pType);
        D32S cloneOptional(const DataTypeDef* pType);

    private:
        const DataTypeDef* _pType;
        DataStructReader    _reader;
        DataStructWriter    _writer;
    };


    class DataStruct
    {
        friend class DataStructInitializer;
    public:
        // DataStruct(void* pData, D32U uiDataSize);
        DataStruct();
        DataStruct(const DataTypeDef* pType, void* pData, D32U uiDataSize);

        D32S clone(void* pOut, D32U uiOutMaxSize);
        D32S clone(DataStruct* pDataStruct);
        D32S copy(const void* pData, D32U uiDataSize);
        D32S destroy();

        inline void* getDataBuf();
        inline D32U getDataBufSize();
        
        D32S setInt(D32S iValue);
        D32S setInt64(D64S iValue);
        D32S setUInt(D32U uiValue);
        D32S setUInt64(D64U uiValue);
        D32S setString(const DCHAR* pValue);
        D32S setFixedString(D32U uiFixedSize, const DCHAR* pValue);

//         D32S setInt(const DCHAR* pName, D32S iValue);
//         D32S setInt64(const DCHAR* pName, D64S iValue);
//         D32S setUInt(const DCHAR* pName, D32U uiValue);
//         D32S setUInt64(const DCHAR* pName, D64U uiValue);
//         D32S setString(const DCHAR* pName, const DCHAR* pValue);
//         D32S getArrayCount();
//         D32S getArrayMember(D32U uiIndex, DataStruct* pDataStruct);
//         D32S getObject(const DCHAR* pName, DataStruct* pDataStruct);
//         D32S getObject(D32U uiIndex, DataStruct* pDataStruct);

    private:
        const DataTypeDef* _pType;
        void*   _pData;
        D32U    _uiSize;
    };

    inline void* DataStruct::getDataBuf()
    {
        return _pData;
    }

    inline D32U DataStruct::getDataBufSize()
    {
        return _uiSize;
    }


    class DataStructInitializer
    {
    public:
        DataStructInitializer(DataStruct* pDataStruct)
        {
            _pDataStruct = pDataStruct;
        }

        inline void setType(const DataTypeDef* pType);
        inline void setDataBuf(void* pData, D32U uiDataSize);
    private:
        DataStruct*     _pDataStruct;
    };

    void DataStructInitializer::setType(const DataTypeDef* pType)
    {
        _pDataStruct->_pType = pType;
    }

    void DataStructInitializer::setDataBuf(void* pData, D32U uiDataSize)
    {
        _pDataStruct->_pData = pData;
        _pDataStruct->_uiSize = uiDataSize;
    }
}

#endif /* DATA_STRUCT_H */

﻿#ifndef ADM_DATATYPE_H_
#define ADM_DATATYPE_H_

#include "adm_typedefs.h"

#ifdef __cplusplus
extern "C"
{
#endif  // __cplusplus

/**
 * @brief 数据类型
 *
 */
typedef enum
{
    ADM_DM_DATA_INT = 1,    // 整数
    ADM_DM_DATA_UINT,       // 正整数
    ADM_DM_DATA_REAL,       // 实数
    ADM_DM_DATA_STRING,     // 字符串
    ADM_DM_DATA_ARRAY,      // 数组
    ADM_DM_DATA_OBJECT      // 对象
} ADM_DM_DATA_TYPE_E;


/**
 * @brief 数据类型描述
 *
 */
typedef struct
{
    const DCHAR*    pName;      // 数据名称
    D32U            uiType;     // 数据类型
    D32U            uiSize;     // 数据大小
//     DCHAR*  pDefault;        // 默认值
//     DCHAR*  pConstraint;     // 约束
} ADM_DM_DATA_TYPE_DESC_T;


typedef struct ADM_DM_DATA_TYPE_DEF
{
    const DCHAR*    pName;      // 数据名称
    D32U            uiType;     // 数据类型
    D32U            uiSize;     // 数据大小或对象成员个数
    void*           pDefault;   // 默认值
    D32U            uiMemorySize;
    D32U            uiOffset;
    const struct ADM_DM_DATA_TYPE_DEF* pSubType;  // 子成员类型
} ADM_DM_DATA_TYPE_DEF_T;



#define ADM_DM_DATA_TYPE_INT()                  (D32U)ADM_DM_DATA_INT, (D32U)0U, NULL
#define ADM_DM_DATA_TYPE_INT_S(size)            (D32U)ADM_DM_DATA_INT, (D32U)size, NULL
#define ADM_DM_DATA_TYPE_UINT()                 (D32U)ADM_DM_DATA_UINT, (D32U)0U, NULL
#define ADM_DM_DATA_TYPE_UINT_S(size)           (D32U)ADM_DM_DATA_UINT, (D32U)size, NULL
#define ADM_DM_DATA_TYPE_STRING()               (D32U)ADM_DM_DATA_STRING, (D32U)0U, NULL
#define ADM_DM_DATA_TYPE_STRING_S(size)         (D32U)ADM_DM_DATA_STRING, (D32U)size, NULL
#define ADM_DM_DATA_TYPE_ARRAY(member)          (D32U)ADM_DM_DATA_ARRAY, (D32U)0U, member
#define ADM_DM_DATA_TYPE_ARRAY_S(member, size)  (D32U)ADM_DM_DATA_ARRAY, (D32U)size, member
#define ADM_DM_DATA_TYPE_OBJECT(members)        (D32U)ADM_DM_DATA_OBJECT, (D32U)sizeof(members)/sizeof((members)[0]), members
#define ADM_DM_DATA_ARRAY_DEF(member)           {NULL, ADM_DM_DATA_TYPE_ARRAY(member)}
#define ADM_DM_DATA_OBJECT_MEMBER(name, type)   {name, type}
#define ADM_DM_DATA_OBJECT_DEF(members)         {NULL, ADM_DM_DATA_TYPE_OBJECT(members)}


D32S adm_datatype_parse(ADM_DM_DATA_TYPE_DEF_T* pType, const DCHAR* pDescription);
D32S adm_datatype_destroy(const ADM_DM_DATA_TYPE_DEF_T* pType);

#ifdef __cplusplus
}
#endif  // __cplusplus

#endif  // ADM_DATATYPE_H_

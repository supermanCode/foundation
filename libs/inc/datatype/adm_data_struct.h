﻿#ifndef ADM_DATA_STRUCT_H_
#define ADM_DATA_STRUCT_H_


#include "adm_datatype.h"

#ifdef __cplusplus
extern "C"
{
#endif  // __cplusplus

typedef struct
{
    void*   _t;
    void*   _d;
    D32U    _s;
} ADM_DATA_STRUCT_T;

void adm_dataStruct_init(ADM_DATA_STRUCT_T* pStruct, const ADM_DM_DATA_TYPE_DEF_T* pTypeDef, void* pData, D32S uiDataSize);
D32S adm_dataStruct_clone(ADM_DATA_STRUCT_T* pStruct, ADM_DATA_STRUCT_T* pDestStruct);
D32S adm_dataStruct_copy(ADM_DATA_STRUCT_T* pStruct, const void* pData, D32S uiDataSize);
D32S adm_dataStruct_destroy(ADM_DATA_STRUCT_T* pStruct);

#ifdef __cplusplus
}
#endif  // __cplusplus


#endif  // ADM_DATA_STRUCT_H_
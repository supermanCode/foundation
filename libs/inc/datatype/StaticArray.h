﻿#pragma once
#include "adm_typedefs.h"
#include <new>
#include "adm_memory.h"


template <class T>
class StaticArray
{
    template <class T>
    friend class StaticArrayIter;
public:
    ~StaticArray();

    static StaticArray<T>* create(D32U uiSize);
    T* add();

private:
    StaticArray();

private:
    D32U    _uiSize;
    D32U    _uiCount;
    T       _elements[1];
};

template <class T>
StaticArray<T>::~StaticArray()
{
    if (_uiCount > 0)
    {
        for (D32U i = 0; i < _uiCount; i++)
        {
            _elements[i].T::~T();
        }
    }
}

template <class T>
StaticArray<T>* StaticArray<T>::create(D32U uiSize)
{
    StaticArray<T>* pArray = (StaticArray<T>*)adm_malloc(sizeof(StaticArray<T>) + sizeof(T) * (uiSize - 1U));
    if (NULL != pArray)
    {
        for (D32U i = 0; i < uiSize; i++)
        {
            new (&pArray->_elements[i]) T();
        }

        pArray->_uiSize = uiSize;
        pArray->_uiCount = 0;
    }

    return pArray;
}

template <class T>
T* StaticArray<T>::add()
{
    return _uiCount < _uiSize ? &_elements[_uiCount++] : NULL;
}



template <class T>
class StaticArrayIter
{
public:
    StaticArrayIter(StaticArray<T>* pArray);

    T* next();

private:
    StaticArray<T>* _pArray;
    D32U            _uiPos;
};

template <class T>
StaticArrayIter<T>::StaticArrayIter(StaticArray<T>* pArray)
{
    _pArray = pArray;
    _uiPos = 0;
}

template <class T>
T* StaticArrayIter<T>::next()
{
    return _uiPos < _pArray->_uiCount ? &_pArray->_elements[_uiPos++] : NULL;
}

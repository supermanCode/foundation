﻿#ifndef DATA_TYPE_H_
#define DATA_TYPE_H_


#include "adm_typedefs.h"

namespace vus
{
    /**
     * @brief 数据类型
     *
     */
    typedef enum
    {
        DM_DATA_INVALID = 0,
        DM_DATA_INT = 1,    // 整数
        DM_DATA_UINT,       // 正整数
        DM_DATA_REAL,       // 实数
        DM_DATA_STRING,     // 字符串
        DM_DATA_ARRAY,      // 数组
        DM_DATA_OBJECT      // 对象
    } DataType;


    /**
     * @brief 数据类型描述
     *
     */
    struct DataTypeDescription
    {
        const DCHAR*    pName;      // 数据名称
        D32U            uiType;     // 数据类型
        D32U            uiSize;     // 数据大小
        void*           pDefault;   // 默认值
        //const DCHAR*    pConstraint;     // 约束
        D32U            uiOptional; // 是否可选，0不可选，1可选
    };


    struct DataTypeDef
    {
        const DCHAR*        pName;          // 数据名称
        D32U                uiOptional : 1; // 是否可选，0不可选，1可选
        D32U                uiHasDefault : 1;   // 是否有默认值
        D32U                uiType : 30;     // 数据类型
        D32U                uiSize;         // 数据大小或对象成员个数
        void*               pDefault;       // 默认值
        // const DCHAR*        pConstraint;     // 约束
        D32U                uiMemorySize;
        D32U                uiOffset;
        /*const*/ DataTypeDef*  pSubType;   // 子成员类型
    };



    #define DM_DATA_TYPE_INT()                  (D32U)DM_DATA_INT, (D32U)0U, NULL, 0
    #define DM_DATA_TYPE_INT_S(size)            (D32U)DM_DATA_INT, (D32U)size, NULL, 0
    #define DM_DATA_TYPE_UINT()                 (D32U)DM_DATA_UINT, (D32U)0U, NULL, 0
    #define DM_DATA_TYPE_UINT_S(size)           (D32U)DM_DATA_UINT, (D32U)size, NULL, 0
    #define DM_DATA_TYPE_STRING()               (D32U)DM_DATA_STRING, (D32U)0U, NULL, 0
    #define DM_DATA_TYPE_STRING_S(size)         (D32U)DM_DATA_STRING, (D32U)size, NULL, 0
    #define DM_DATA_TYPE_ARRAY(member)          (D32U)DM_DATA_ARRAY, (D32U)0U, member, 0
    #define DM_DATA_TYPE_ARRAY_S(member, size)  (D32U)DM_DATA_ARRAY, (D32U)size, member, 0
    #define DM_DATA_TYPE_OBJECT(members)        (D32U)DM_DATA_OBJECT, (D32U)sizeof(members)/sizeof((members)[0]), members, 0
    #define DM_DATA_ARRAY_DEF(member)           {NULL, ADM_DM_DATA_TYPE_ARRAY(member)}
    #define DM_DATA_OBJECT_MEMBER(name, type)   {name, type}
    #define DM_DATA_OBJECT_DEF(members)         {NULL, ADM_DM_DATA_TYPE_OBJECT(members)}

    //
    //DataTypeDef s_ecuDidValueDefine[] = {
    //    ADM_DM_DATA_OBJECT_MEMBER("type", ADM_DM_DATA_TYPE_INT()),
    //    ADM_DM_DATA_OBJECT_MEMBER("length", ADM_DM_DATA_TYPE_INT()),
    //    ADM_DM_DATA_OBJECT_MEMBER("value", ADM_DM_DATA_TYPE_STRING()),
    //};
    //
    //DataTypeDef s_ecuDidDefine[] = {
    //    ADM_DM_DATA_OBJECT_MEMBER("did", ADM_DM_DATA_TYPE_STRING()),
    //    ADM_DM_DATA_OBJECT_MEMBER("value", ADM_DM_DATA_TYPE_OBJECT(s_ecuDidValueDefine)),
    //};
    //
    //DataTypeDef s_ecuDidMDefine = ADM_DM_DATA_OBJECT_DEF(s_ecuDidDefine);
    //
    //DataTypeDef s_ecuInfoMDefine[] = {
    //    ADM_DM_DATA_OBJECT_MEMBER("ecuReqId", ADM_DM_DATA_TYPE_STRING()),
    //    ADM_DM_DATA_OBJECT_MEMBER("ecuSwid", ADM_DM_DATA_TYPE_STRING()),
    //    ADM_DM_DATA_OBJECT_MEMBER("dids", ADM_DM_DATA_TYPE_ARRAY(&s_ecuDidMDefine)),
    //};
    //
    //DataTypeDef s_ecuInfosDefine = ADM_DM_DATA_ARRAY_DEF(s_ecuInfoMDefine);
    //DataTypeDef s_ecuInfoManagerDefine[] = {
    //    ADM_DM_DATA_OBJECT_MEMBER("ecuInfos", ADM_DM_DATA_TYPE_ARRAY(&s_ecuInfosDefine))
    //};
}


#endif  // DATA_TYPE_H_
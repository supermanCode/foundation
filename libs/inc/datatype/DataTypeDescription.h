﻿#ifndef DATA_TYPE_DESCRIPTION_H_
#define DATA_TYPE_DESCRIPTION_H_


#include "DataType.h"
#include "StringUtil.h"
#include <map>
#include <string>

namespace vus
{

    class DataTypeDescriptionParser
    {
    public:
        D32S init();

        DataTypeDescriptionParser(const DCHAR* pDescriptionStr);
        D32S parse(DataTypeDef* pType);
        static void destroyType(const DataTypeDef* pType);

    private:
        typedef D32S(DataTypeDescriptionParser::* ParseTypeHandler)(DataTypeDef* pType);

        D32S parseSize(D32U& uiSize);
        D32S parseInt(DataTypeDef* pType);
        D32S parseUInt(DataTypeDef* pType);
        D32S parseReal(DataTypeDef* pType);
        D32S parseString(DataTypeDef* pType);
        D32S parseArray(DataTypeDef* pType);
        D32S parseObject(DataTypeDef* pType);
        D32S parseType(DataTypeDef* pType);
        D32S parseMember(DataTypeDef* pType);
        D32S parseOptional(DataTypeDef* pType);
        D32S parseDefault(DataTypeDef* pType);

        static void destroyArrayType(const DataTypeDef* pType);
        static void destroyObjectType(const DataTypeDef* pType);

    private:
        typedef std::map<std::string, ParseTypeHandler>    TypeMap;
        typedef TypeMap::iterator                           TypeMapIter;
        typedef std::pair<std::string, ParseTypeHandler>   TypeMapPair;

        TypeMap     _typeMap;

        StringUtil  _util;
    };

}

#endif  // DATA_TYPE_DESCRIPTION_H_
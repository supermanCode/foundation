﻿#ifndef DATA_STRUCT_CODEC_HELPER_H_
#define DATA_STRUCT_CODEC_HELPER_H_

#include "DataType.h"

namespace vus
{
    class DataStructDecodeHelper
    {
    public:
        virtual D32S init(const DataTypeDef* pTypeDef, void* pData, void*& pOutData) = 0;
        virtual D32S setAsInt(const DataTypeDef* pTypeDef, void* pData, D64S iValue) = 0;
        virtual D32S setAsUInt(const DataTypeDef* pTypeDef, void* pData, D64U uiValue) = 0;
        virtual D32S setAsReal(const DataTypeDef* pTypeDef, void* pData, DOUBLE dValue) = 0;
        virtual D32S setAsString(const DataTypeDef* pTypeDef, void* pData, const DCHAR* pValue) = 0;
        virtual D32S setNull(const DataTypeDef* pTypeDef, void* pData) = 0;
        virtual D32S getArray(const DataTypeDef* pTypeDef, void* pData, void*& pOutData) { pOutData = pData; return 0; };
        virtual D32S setArrayCount(const DataTypeDef* pTypeDef, void* pData, D32U uiCount) = 0;
        virtual D32S getArrayMember(const DataTypeDef* pTypeDef, D32U uiIndex, void* pData, void*& pOutData) = 0;
        virtual D32S getObjectMember(const DataTypeDef* pTypeDef, void* pData, void*& pOutData) = 0;
    };

    class DataStructEncodeHelper
    {
    public:
        virtual D32S init(const DataTypeDef* pTypeDef, void* pData, void*& pOutData) = 0;
        virtual D32S asInt(const DataTypeDef* pTypeDef, void* pData, D64S& iValue) = 0;
        virtual D32S asUInt(const DataTypeDef* pTypeDef, void* pData, D64U& uiValue) = 0;
        virtual D32S asReal(const DataTypeDef* pTypeDef, void* pData, DOUBLE& dValue) = 0;
        virtual D32S asString(const DataTypeDef* pTypeDef, void* pData, const DCHAR*& pValue) = 0;
        virtual D32S getArrayCount(const DataTypeDef* pTypeDef, void* pData, D32U& uiCount) = 0;
        virtual D32S getArray(const DataTypeDef* pTypeDef, void* pData, void*& pOutData) = 0;
        virtual D32S getArrayMember(const DataTypeDef* pTypeDef, D32U uiIndex, void* pData, void*& pOutData) = 0;
        // virtual D32S getObject(const DataTypeDef* pTypeDef, void* pData, void*& pOutData) = 0;
        virtual D32S getObjectMember(const DataTypeDef* pTypeDef, void* pData, void*& pOutData) = 0;
    };
}

#endif  // DATA_STRUCT_CODEC_HELPER_H_
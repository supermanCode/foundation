﻿#ifndef STRING_UTIL_H_
#define STRING_UTIL_H_

#include "adm_typedefs.h"
#include <string>


class StringUtil
{
public:
    StringUtil(const DCHAR* pStr)
    {
        _pPos = pStr;
    }

    StringUtil& operator=(StringUtil& util);


    DBOOL isSpace(DCHAR chr);
    void skipSpace();
    DBOOL isWhiteSpace(DCHAR chr);
    void skipWhiteSpace();
    
    D32U popAlphanumericStr(DCHAR* pStr);
    // D32U popString(std::string& str);
    D32U popEscapeString(std::string& str);
    D32U popNumberString(std::string& str, DBOOL& bIsDecimal, DBOOL& bIsNegative);
    D32U popInteger(D32S& iValue);
    DBOOL compareString(const DCHAR* pStr);

    D32S addEscape(std::string& str);

    inline DCHAR popChr();
    inline DCHAR getChr();
    inline void skipChr();
    inline const DCHAR* getRemainStr() const;
protected:
    const DCHAR* _pPos;

};

DCHAR StringUtil::getChr()
{
    return *_pPos;
}

void StringUtil::skipChr()
{
    _pPos++;
}

DCHAR StringUtil::popChr()
{
    DCHAR chr = *_pPos;
    _pPos++;
    return chr;
}

const DCHAR* StringUtil::getRemainStr() const
{
    return _pPos;
}

class StringUtilEx : public StringUtil
{
public:
    StringUtilEx(const DCHAR* pStr) : StringUtil(pStr)
    {

    }

    D32U popUriString(DCHAR* pStr);
};

#endif  // STRING_UTIL_H_

